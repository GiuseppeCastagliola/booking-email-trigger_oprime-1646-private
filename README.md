Booking-email-trigger 
==============
[![Build Status](http://bcn-jenkins-01.odigeo.org/jenkins/job/booking-email-tracking/badge/icon)](http://bcn-jenkins-01.odigeo.org/jenkins/job/booking-email-tracking/)
[![Lines of Code](http://bcn-qa-01:8090/api/project_badges/measure?project=com.odigeo.email.tracking%3Abooking-email-tracking&metric=ncloc)](http://bcn-qa-01:8090/dashboard?id=com.odigeo.email.tracking%3Abooking-email-tracking)
[![Maintainability Rating](http://bcn-qa-01:8090/api/project_badges/measure?project=com.odigeo.email.tracking%3Abooking-email-tracking&metric=sqale_rating)](http://bcn-qa-01:8090/dashboard?id=com.odigeo.email.tracking%3Abooking-email-tracking)
[![Code coverage](http://bcn-qa-01:8090/api/project_badges/measure?project=com.odigeo.email.tracking%3Abooking-email-tracking&metric=coverage)](http://bcn-qa-01:8090/dashboard?id=com.odigeo.email.tracking%3Abooking-email-tracking)

HOWTO execute the functional tests
------------------------------

1. First, you need to compile the whole project with maven. If using the command line, just type

        mvn clean install

2. Then you can execute the functional tests with the following command. 

        mvn clean test -pl functional-tests -Prun-code-checks

    Mind that this command will execute all the Gherkin tests it finds under `functional-tests/src/test/resources/` and its sub-directories. You can edit the tests that exist already or add some of your own.

    Mind that the test uses a random generator to fill in missing input data, but you can configure the "seed" of the generator. By setting the seed to a fixed value, you can eliminate all randomness and make the test repeatable. Also, each test takes care of printing the seed it used, so you can reproduce it afterwards. For instance, with
    
        mvn clean test -pl functional-tests -Prun-code-checks -Dodigeo.test.seed=123
        
3. It is possible to execute the tests for a specific version in Maven by using the cucumber options parameter and applying a specific tag

        mvn clean test -pl functional-tests -Prun-code-checks -Dcucumber.options="--tags @SinceMetasearchApiVersion1"

Useful URLs
------------------------------

Metrics - http://booking-email-trigger.app-prod.services.odigeo.com:8080/booking-email-trigger/engineering/metrics

Build - http://booking-email-trigger.app-prod.services.odigeo.com:8080/booking-email-trigger/engineering/build/

Healthcheck - http://booking-email-trigger.app-prod.services.odigeo.com:8080/booking-email-trigger/engineering/healthcheck

## Changelog ##

### Version 1.18.2 ###
* OPRIME-1642 - Kafka Configuration for the welcome prime emai

### Version 1.16.0 ###
* ICE-1743 - Remove Change Dates - Under agent review email

### Version 1.14.0 ###
* ICE-1627 - Resend requests with departure date in the past are now delivered

### Version 1.13.0 ###
* ASQ-1506 - Start reading and processing the voucher created email

### Version 1.12.0 ###
* ICE-1571 - Remove integration with flightstats

### Version 1.11.0 ###
* ICE-1542 - Create new endpoint resendEmail(bookingId) in bookingEmailTrigger

### Version 1.10.1 ###
* ICE-1508 - Allow GDS emails on cancellations received in flight stats

### Version 1.10.0 ###
* OCP-1230 - Add missing functional tests for forward Email and persist Email

### Version 1.7.0 ###
* OCP-1230 - Add new endpoints service/forwardEmail and service/persistEmail for OdigeoConnect Airline Voucher e-mails

### Version 1.6.1 ###
* ICE-1388 - Trigger cancellation emails for LC flights

### Version 1.4.4 ###
* ICE-1217 - Send email type to mailer

### Version 1.4.3 ###
* ICE-1182 - Add new metric for email type

### Version 1.4.2 ###
* ICE-1132 - Resend email service

### Version 1.4.1 ###
* ICE-463 - Rename class `BookingDetailCache` to `EmailSentCache`

### Version 1.4.0 ###
* ICE-463 - Filter renewal bookings from sending a confirmation email 

### Version 1.3.3 ###
* ICE-256 - Update to messaging-utils 1.8.0
