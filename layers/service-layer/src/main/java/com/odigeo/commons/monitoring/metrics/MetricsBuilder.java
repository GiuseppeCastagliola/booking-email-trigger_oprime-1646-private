package com.odigeo.commons.monitoring.metrics;

import com.odigeo.email.render.v1.contract.EmailType;
import com.odigeo.email.trigger.trigger.EmailRequester;
import com.odigeo.email.trigger.trigger.ExecutionResult;

public final class MetricsBuilder {

    private MetricsBuilder() { }

    public static Metric buildTriggerMetric(String component, ExecutionResult executionResult) {
        return new Metric.Builder(component)
                .tag(MetricsConstants.EXECUTION_RESULT, String.valueOf(executionResult))
                .policy(MetricsPolicy.Precision.MINUTELY, MetricsPolicy.Retention.TRIMESTER)
                .build();
    }

    public static Metric buildScheduleChangesMetric(String component, String change) {
        return new Metric.Builder(component)
                .tag(MetricsConstants.EXECUTION_RESULT, change)
                .policy(MetricsPolicy.Precision.MINUTELY, MetricsPolicy.Retention.TRIMESTER)
                .build();
    }

    public static Metric buildTriggerMetric(String component, EmailType emailType, EmailRequester emailRequester) {
        return new Metric.Builder(component)
                .tag(MetricsConstants.EMAIL_TYPE, String.valueOf(emailType))
                .tag(MetricsConstants.EMAIL_REQUESTER, String.valueOf(emailRequester))
                .policy(MetricsPolicy.Precision.MINUTELY, MetricsPolicy.Retention.TRIMESTER)
                .build();
    }
}

