package com.odigeo.commons.monitoring.metrics;

public class MetricsConstants {

    public static final String REGISTRY_BOOKING_EMAIL_TRIGGER = "booking-email-trigger";

    public static final String BOOKING_UPDATES = "bookingUpdates";
    public static final String RESEND_EMAIL = "resendEmails";
    public static final String EMAIL_TRIGGER = "emailTrigger";
    public static final String EMAIL_REQUESTER = "emailRequester";
    public static final String BOOKING_RECOVERY = "bookingRecovery";
    public static final String SCHEDULE_CHANGES = "scheduleChanges";
    public static final String FLIGHT_CANCELLED = "flightcancelled";
    public static final String PURE_LOWCOST = "purelowcost";
    public static final String NOT_PURE_LOWCOST = "notpurelowcost";
    public static final String EXECUTION_RESULT = "executionResult";
    public static final String EMAIL_TYPE = "emailType";
    public static final String MEMBERSHIP_EMAILS_CONSUMER = "membershipEmailsConsumer";
}


