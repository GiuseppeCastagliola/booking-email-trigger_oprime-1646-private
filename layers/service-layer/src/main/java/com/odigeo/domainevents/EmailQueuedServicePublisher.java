package com.odigeo.domainevents;


import com.odigeo.commons.messaging.MessagePublisher;
import com.odigeo.commons.messaging.PublishMessageException;
import com.odigeo.commons.messaging.Publisher;
import com.odigeo.commons.messaging.domain.events.DomainEvent;
import com.odigeo.retail.bookingemailtrigger.email.EmailQueued;
import org.apache.log4j.Logger;

public class EmailQueuedServicePublisher {
    private static final Logger LOGGER = Logger.getLogger(EmailQueuedServicePublisher.class);
    public static final String EMAILQUEUED_TOPIC = "BOOKING_EMAIL_TRIGGER.EMAIL";

    @MessagePublisher(topic = EMAILQUEUED_TOPIC)
    private Publisher<DomainEvent> publisher;

    @SuppressWarnings("PMD.AvoidCatchingGenericException")
    public void publishMessage(EmailQueued emailQueued) {
        DomainEvent<EmailQueued> emailQueuedDomainEvent = new DomainEvent<>(emailQueued);
        try {
            publisher.publish(emailQueuedDomainEvent);
        } catch (PublishMessageException | RuntimeException e) {
            LOGGER.error("Error publishing email queued", e);
        }
    }
}
