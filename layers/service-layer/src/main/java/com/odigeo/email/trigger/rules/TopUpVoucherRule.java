package com.odigeo.email.trigger.rules;

import com.odigeo.email.render.v1.contract.EmailType;
import com.odigeo.email.trigger.trigger.TriggerInfo;
import com.odigeo.email.trigger.trigger.Validation;

public class TopUpVoucherRule implements TriggerRule {

    private static final String VOUCHER_ID_KEY = "voucherId";

    @Override
    public void validate(TriggerInfo triggerInfo) {
        if (EmailType.TOP_UP_VOUCHER_CREATED.equals(triggerInfo.getEmailType()) && !triggerInfo.getAdditionalParameters().containsKey(VOUCHER_ID_KEY)) {
            throw new TriggerRuleException(Validation.MISSING_ADDITIONAL_PARAMETER);
        }
    }
}
