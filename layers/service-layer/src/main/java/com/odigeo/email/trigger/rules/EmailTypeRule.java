package com.odigeo.email.trigger.rules;

import com.odigeo.email.trigger.trigger.TriggerInfo;
import com.odigeo.email.trigger.trigger.Validation;

public class EmailTypeRule implements TriggerRule {

    @Override
    public void validate(TriggerInfo triggerInfo) {
        if (triggerInfo.getEmailType() == null) {
            throw new TriggerRuleException(Validation.MISSING_EMAIL_TYPE);
        }
    }
}
