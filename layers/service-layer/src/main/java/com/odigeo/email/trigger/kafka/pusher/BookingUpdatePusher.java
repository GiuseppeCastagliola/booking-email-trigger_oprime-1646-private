package com.odigeo.email.trigger.kafka.pusher;

import com.google.inject.Inject;
import com.google.inject.Singleton;
import com.odigeo.email.trigger.configuration.Configuration;

@Singleton
public class BookingUpdatePusher extends Pusher {

    @Inject
    public BookingUpdatePusher(Configuration configuration) {
        super(configuration.getKafkaConfiguration().getBookingUpdatesTopic(), configuration.getKafkaConfiguration().getBootstrapServerUrl());
    }

}
