package com.odigeo.email.trigger.factories;

import com.google.inject.Singleton;
import com.odigeo.bookingapi.v12.responses.BookingDetail;
import com.odigeo.email.trigger.booking.builder.BookingProductsBuilder;
import com.odigeo.email.trigger.booking.product.BookingProducts;

@Singleton
public class BookingProductsFactory {

    public BookingProducts create(BookingDetail bookingDetail) {
        return new BookingProductsBuilder(bookingDetail).build();
    }

}
