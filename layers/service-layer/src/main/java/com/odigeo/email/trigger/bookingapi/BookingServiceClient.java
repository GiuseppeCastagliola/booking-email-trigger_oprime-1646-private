package com.odigeo.email.trigger.bookingapi;

import com.google.common.base.Strings;
import com.google.inject.Inject;
import com.google.inject.Singleton;
import com.odigeo.bookingapi.client.v12.BookingApiServiceFactory;
import com.odigeo.bookingapi.v12.BookingApiService;
import com.odigeo.bookingapi.v12.responses.BookingDetail;
import com.odigeo.email.trigger.configuration.BookingServiceConfiguration;
import com.odigeo.email.trigger.configuration.Configuration;


import java.io.IOException;
import java.util.Locale;

@Singleton
public class BookingServiceClient {

    private final BookingServiceConfiguration bookingServiceConfiguration;

    @Inject
    public BookingServiceClient(Configuration configuration) {
        this.bookingServiceConfiguration = configuration.getBookingServiceConfiguration();
    }

    public BookingDetail getBookingDetail(Locale locale, long bookingId) throws IOException {
        BookingDetail bookingDetail = getBookingApiService().getBooking(bookingServiceConfiguration.getUser(), bookingServiceConfiguration.getPass(), locale, bookingId);

        if (!Strings.isNullOrEmpty(bookingDetail.getErrorMessage())) {
            throw new IOException(bookingDetail.getErrorMessage());
        }

        return bookingDetail;
    }

    private BookingApiService getBookingApiService() {
        return BookingApiServiceFactory.getInstance().getBookingService(BookingApiService.class, bookingServiceConfiguration.getUrl());
    }

}
