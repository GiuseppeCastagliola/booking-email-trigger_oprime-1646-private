package com.odigeo.email.trigger.rules;

import com.google.inject.Inject;
import com.odigeo.email.trigger.email.TriggerHelper;
import com.odigeo.email.trigger.trigger.TriggerInfo;
import com.odigeo.email.trigger.trigger.Validation;
import lombok.EqualsAndHashCode;

@EqualsAndHashCode
public class DepartureDateRule implements TriggerRule {

    private final TriggerHelper triggerHelper;

    @Inject
    public DepartureDateRule(TriggerHelper triggerHelper) {
        this.triggerHelper = triggerHelper;
    }

    @Override
    public void validate(TriggerInfo triggerInfo) {
        if (triggerHelper.isDepartureDatePast(triggerInfo.getBookingDetail())) {
            throw new TriggerRuleException(Validation.DEPARTURE_PAST);
        }
    }
}

