package com.odigeo.email.trigger.kafka.processor;

import com.edreams.configuration.ConfigurationEngine;
import com.odigeo.email.trigger.rules.RulesInterface;
import com.odigeo.email.trigger.trigger.EmailRequester;
import com.odigeo.email.trigger.trigger.ExecutionResult;
import com.odigeo.email.trigger.trigger.TriggerService;
import com.odigeo.email.trigger.util.Constants;
import com.odigeo.messaging.utils.BasicMessage;
import com.odigeo.messaging.utils.ConsumerIterator;
import com.odigeo.messaging.utils.MessageDataAccessException;
import com.odigeo.messaging.utils.MessageParserException;
import org.apache.log4j.Logger;

import java.io.IOException;

public abstract class KafkaBasicMessageProcessor implements Runnable {

    private static final Logger LOGGER = Logger.getLogger(KafkaBasicMessageProcessor.class);

    private final TriggerService triggerService = ConfigurationEngine.getInstance(TriggerService.class);
    protected final ConsumerIterator<BasicMessage> consumerIterator;

    public KafkaBasicMessageProcessor(ConsumerIterator<BasicMessage> consumerIterator) {
        this.consumerIterator = consumerIterator;
    }

    protected abstract EmailRequester getEmailRequester();

    @Override
    public void run() {
        try {
            while (consumerIterator.hasNext()) {

                try {
                    BasicMessage message = consumerIterator.next();
                    processMessage(message);
                } catch (MessageDataAccessException | MessageParserException | IOException e) {
                    LOGGER.error("Error parsing message...", e);
                }
            }
        } catch (MessageDataAccessException e) {
            LOGGER.error("Error reading message/s " + getClass().getName() + " stopped.", e);
        }
    }

    protected abstract void processMessage(BasicMessage basicMessage) throws IOException;

    @SuppressWarnings("PMD.AvoidCatchingGenericException")
    protected ExecutionResult executeTrigger(BasicMessage message, RulesInterface rules) {
        ExecutionResult executionResult;
        try {
            executionResult = triggerService.execute(message.getKey(), rules, getEmailRequester());
        } catch (Exception e) {
            LOGGER.error("Error processing message" + Constants.LOG_SEPARATOR + message.getKey(), e);
            executionResult = ExecutionResult.KO_UNRECOVERABLE;
        }

        return executionResult;
    }
}
