package com.odigeo.email.trigger.kafka.poller;

import com.odigeo.email.trigger.kafka.processor.KafkaBasicMessageProcessor;
import com.odigeo.messaging.utils.BasicMessage;
import com.odigeo.messaging.utils.Consumer;
import com.odigeo.messaging.utils.ConsumerIterator;
import com.odigeo.messaging.utils.kafka.KafkaJsonConsumer;
import org.apache.log4j.Logger;

import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class KafkaPoller<T extends KafkaBasicMessageProcessor> {
    private static final Logger LOGGER = Logger.getLogger(KafkaPoller.class);
    private final int numThreads;
    private final ExecutorService executorService;
    private final Consumer<BasicMessage> consumer;
    private final Constructor<T> kafkaBasicMessageProcessorConstructor;


    public KafkaPoller(int numThreads, String groupId, String zookeeperUrl, String topic, Class<T> kafkaBasicMessageProcessorClass) throws NoSuchMethodException {
        this.numThreads = numThreads;
        this.executorService = Executors.newFixedThreadPool(numThreads);
        this.consumer = new KafkaJsonConsumer<>(BasicMessage.class, zookeeperUrl, groupId, topic);
        this.kafkaBasicMessageProcessorConstructor = kafkaBasicMessageProcessorClass.getConstructor(ConsumerIterator.class);
    }

    public void init() {
        consumer.connectAndIterate(numThreads).forEach(consumerIterator -> {
            try {
                executorService.submit(kafkaBasicMessageProcessorConstructor.newInstance(consumerIterator));
            } catch (InstantiationException | IllegalAccessException | InvocationTargetException e) {
                LOGGER.error(e);
            }
        });
    }

    public void destroy() {
        if (consumer != null) {
            consumer.close();
        }
    }
}
