package com.odigeo.email.trigger.kafka;

import com.codahale.metrics.health.HealthCheck;

import javax.net.SocketFactory;
import java.io.IOException;
import java.net.Socket;
import java.util.stream.Stream;

public class KafkaConnectivityHealthCheck extends HealthCheck {

    private final String component;
    private final String urlWithPort;

    public KafkaConnectivityHealthCheck(String component, String urlWithPort) {
        this.component = component;
        this.urlWithPort = urlWithPort;
    }

    @Override
    protected HealthCheck.Result check() {
        if (Stream.of(urlWithPort.split(",")).anyMatch(this::isHealthy)) {
            return Result.healthy(component + " is up on  " + urlWithPort);
        } else {
            return Result.unhealthy(component + " is down on " + urlWithPort);
        }
    }

    private boolean isHealthy(String url) {
        String[] zooKeeperUrl = url.split(":");
        String zooKeeperHost = zooKeeperUrl[0];
        Integer zooKeeperPort = Integer.valueOf(zooKeeperUrl[1]);
        try {
            Socket pingSocket = SocketFactory.getDefault().createSocket(zooKeeperHost, zooKeeperPort);
            pingSocket.close();
        } catch (IOException e) {
            return false;
        }
        return true;
    }
}
