package com.odigeo.email.trigger.rules;

import com.odigeo.bookingapi.v12.responses.AccommodationBooking;
import com.odigeo.bookingapi.v12.responses.BookingDetail;
import com.odigeo.bookingapi.v12.responses.ItineraryBooking;
import com.odigeo.bookingapi.v12.responses.ProductCategoryBooking;
import com.odigeo.email.trigger.trigger.TriggerInfo;
import com.odigeo.email.trigger.trigger.Validation;
import org.apache.commons.collections.CollectionUtils;

import java.util.List;
import java.util.function.Predicate;
import java.util.stream.Collectors;

public class ItineraryOrAccommodationRule implements TriggerRule {

    static final Class<ItineraryBooking> ITINERARY_BOOKING_CLASS = ItineraryBooking.class;
    static final Class<AccommodationBooking> ACCOMMODATION_BOOKING_CLASS = AccommodationBooking.class;

    @Override
    public void validate(TriggerInfo triggerInfo) {

        final List<ProductCategoryBooking> itineraryBookingList = getProductCategoryBookingListByClass(triggerInfo.getBookingDetail(), ITINERARY_BOOKING_CLASS);
        if (CollectionUtils.isEmpty(itineraryBookingList)) {

            final List<ProductCategoryBooking> accommodationBookingList = getProductCategoryBookingListByClass(triggerInfo.getBookingDetail(), ACCOMMODATION_BOOKING_CLASS);
            if (CollectionUtils.isEmpty(accommodationBookingList)) {

                throw new TriggerRuleException(Validation.MUST_BE_IGNORED);
            }
        }
    }

    private List<ProductCategoryBooking> getProductCategoryBookingListByClass(final BookingDetail bookingDetail, final Class clazz) {
        return bookingDetail.getBookingProducts().stream()
                .filter(filterProductCategoryBookingByClass(clazz))
                .collect(Collectors.toList());
    }

    private Predicate<ProductCategoryBooking> filterProductCategoryBookingByClass(final Class clazz) {
        return productCategoryBooking -> productCategoryBooking.getClass().equals(clazz);
    }
}

