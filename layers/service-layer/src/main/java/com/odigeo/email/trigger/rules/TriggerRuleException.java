package com.odigeo.email.trigger.rules;

import com.odigeo.email.trigger.trigger.Validation;

public class TriggerRuleException extends RuntimeException {

    private final Validation validation;

    public TriggerRuleException(Validation validation) {
        this.validation = validation;
    }

    public Validation getValidation() {
        return validation;
    }
}

