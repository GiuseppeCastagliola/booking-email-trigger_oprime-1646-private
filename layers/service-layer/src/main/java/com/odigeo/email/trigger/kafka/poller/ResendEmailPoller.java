package com.odigeo.email.trigger.kafka.poller;

import com.google.inject.Inject;
import com.google.inject.Singleton;
import com.odigeo.email.trigger.configuration.Configuration;
import com.odigeo.email.trigger.kafka.processor.ResendEmailProcessor;

@Singleton
public class ResendEmailPoller extends KafkaPoller<ResendEmailProcessor> {

    @Inject
    public ResendEmailPoller(Configuration configuration) throws NoSuchMethodException {
        super(configuration.getKafkaConfiguration().getNumThreadsRecovery(),
                configuration.getKafkaConfiguration().getGroupId(),
                configuration.getKafkaConfiguration().getZookeeperUrl(),
                configuration.getKafkaConfiguration().getResendEmailsTopic(),
                ResendEmailProcessor.class);
    }

}

