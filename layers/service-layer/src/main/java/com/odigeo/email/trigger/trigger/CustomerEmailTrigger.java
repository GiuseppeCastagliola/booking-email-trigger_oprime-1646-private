package com.odigeo.email.trigger.trigger;

import com.google.inject.Inject;
import com.google.inject.Singleton;
import com.odigeo.bookingapi.v12.responses.BookingDetail;
import com.odigeo.commons.monitoring.metrics.MetricsBuilder;
import com.odigeo.commons.monitoring.metrics.MetricsConstants;
import com.odigeo.commons.monitoring.metrics.MetricsUtils;
import com.odigeo.domainevents.EmailQueuedServicePublisher;
import com.odigeo.email.trigger.bookingapi.EmailSentCache;
import com.odigeo.email.trigger.email.CustomerEmailTagManager;
import com.odigeo.email.trigger.email.FromEmailSenderNameHelper;
import com.odigeo.email.trigger.factories.EmailQueuedFactory;
import com.odigeo.email.trigger.mailer.MailerService;
import com.odigeo.email.trigger.render.RenderInfo;
import com.odigeo.email.trigger.tracking.CustomerEmailManager;
import com.odigeo.email.trigger.util.Constants;
import com.odigeo.mailer.v1.EmailSendResponse;
import com.odigeo.mailer.v1.InvalidParametersException;
import com.odigeo.mailer.v1.RemoteException;
import com.odigeo.retail.bookingemailtrigger.email.EmailQueued;
import org.apache.log4j.Logger;

import java.util.Collections;

@Singleton
public class CustomerEmailTrigger {
    private static final Logger LOGGER = Logger.getLogger(CustomerEmailTrigger.class);

    private final MailerService mailerService;
    private final CustomerEmailManager customerEmailManager;
    private final EmailSentCache emailSentCache;
    private final CustomerEmailTagManager customerEmailTagManager;
    private final FromEmailSenderNameHelper fromEmailSenderNameHelper;
    private final EmailQueuedServicePublisher emailQueuedServicePublisher;
    private final EmailQueuedFactory emailQueuedFactory;

    @Inject
    public CustomerEmailTrigger(MailerService mailerService, CustomerEmailManager customerEmailManager, EmailSentCache emailSentCache, CustomerEmailTagManager customerEmailTagManager, FromEmailSenderNameHelper fromEmailSenderNameHelper, EmailQueuedServicePublisher emailQueuedServicePublisher, EmailQueuedFactory emailQueuedFactory) {
        this.mailerService = mailerService;
        this.customerEmailManager = customerEmailManager;
        this.emailSentCache = emailSentCache;
        this.customerEmailTagManager = customerEmailTagManager;
        this.fromEmailSenderNameHelper = fromEmailSenderNameHelper;
        this.emailQueuedServicePublisher = emailQueuedServicePublisher;
        this.emailQueuedFactory = emailQueuedFactory;
    }

    @SuppressWarnings("PMD.AvoidCatchingGenericException")
    public ExecutionResult execute(TriggerInfo triggerInfo, RenderInfo renderInfo) {
        try {
            sendEmail(
                    triggerInfo,
                    renderInfo,
                    Constants.TEMPLATE_ID,
                    triggerInfo.getBookingDetail().getBookingBasicInfo().getWebsite().getCode(),
                    triggerInfo.getBookingDetail().getBookingBasicInfo().getWebsite().getBrand()
            );
            return ExecutionResult.OK;

        } catch (RuntimeException | RemoteException | InvalidParametersException e) {
            LOGGER.error(triggerInfo.getBookingDetail().getBookingBasicInfo().getId() + Constants.LOG_SEPARATOR + triggerInfo.getEmailType() + Constants.LOG_SEPARATOR + Status.CUSTOMER_RECOVERY, e);
            return ExecutionResult.KO_RECOVERABLE;
        }
    }

    public void sendEmail(TriggerInfo triggerInfo, RenderInfo renderInfo, String templateId, String templateCode, String templateBrand) throws RemoteException, InvalidParametersException {
        final EmailSendResponse response = triggerCustomerEmail(triggerInfo.getBookingDetail(), renderInfo, customerEmailTagManager.getEmailTagFromTriggerInfo(triggerInfo), templateId, templateCode, templateBrand);
        persistEmail(triggerInfo, renderInfo);
        pushEmailQueuedEvent(triggerInfo, response);
    }

    public void persistEmail(TriggerInfo triggerInfo, RenderInfo renderInfo) {
        customerEmailManager.index(triggerInfo, renderInfo);
        LOGGER.info(triggerInfo.getBookingDetail().getBookingBasicInfo().getId() + Constants.LOG_SEPARATOR + triggerInfo.getEmailType() + Constants.LOG_SEPARATOR + Status.CUSTOMER_TRIGGERED);
        emailSentCache.addInCache(triggerInfo);
        MetricsUtils.incrementMeter(MetricsBuilder.buildTriggerMetric(MetricsConstants.EMAIL_TRIGGER, triggerInfo.getEmailType(), triggerInfo.getEmailRequester()), MetricsConstants.REGISTRY_BOOKING_EMAIL_TRIGGER);
    }

    private void pushEmailQueuedEvent(final TriggerInfo triggerInfo, final EmailSendResponse response) {
        if (response != null && com.odigeo.mailer.v1.Status.QUEUED.equals(response.getStatus())) {
            final EmailQueued emailQueued = emailQueuedFactory.createEmailQueued(
                    triggerInfo.getBookingDetail().getBookingBasicInfo().getId(),
                    triggerInfo.getEmailType(),
                    response.getDeliveryId()
            );
            emailQueuedServicePublisher.publishMessage(emailQueued);
        }

    }

    private EmailSendResponse triggerCustomerEmail(BookingDetail bookingDetail, RenderInfo renderInfo, String emailTag, String templateId, String templateCode, String templateBrand) throws RemoteException, InvalidParametersException {
        return mailerService.sendEmail(
                fromEmailSenderNameHelper.determineFromAddressAndName(bookingDetail),
                Collections.singletonList(bookingDetail.getBuyer().getMail()),
                bookingDetail.getUserSessionLocale(),
                templateCode,
                templateBrand,
                templateId,
                emailTag,
                renderInfo.getParameters(),
                renderInfo.getAttachments()
        );
    }

}
