package com.odigeo.email.trigger.rules;

import com.google.inject.Inject;
import com.odigeo.email.trigger.tracking.CustomerEmailManager;
import com.odigeo.email.trigger.trigger.TriggerInfo;
import com.odigeo.email.trigger.trigger.Validation;
import org.elasticsearch.action.search.SearchResponse;

import java.util.Optional;

public class WelcomeToPrimeNeverTriggeredRule implements TriggerRule {

    private final CustomerEmailManager customerEmailManager;

    @Inject
    public WelcomeToPrimeNeverTriggeredRule(CustomerEmailManager customerEmailManager) {
        this.customerEmailManager = customerEmailManager;
    }

    @Override
    public void validate(TriggerInfo triggerInfo) {
        Optional<SearchResponse> searchResponse = customerEmailManager.isWelcomeToPrimeNeverTriggered(triggerInfo.getBookingDetail().getBookingBasicInfo().getId());
        if (searchResponse.isPresent()) {
            searchResponse.filter(searchResp -> searchResp.getHits().getTotalHits() == 0)
                    .orElseThrow(() -> new TriggerRuleException(Validation.ALREADY_TRIGGERED));
        }
    }
}

