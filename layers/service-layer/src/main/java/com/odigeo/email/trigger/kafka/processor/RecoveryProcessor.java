package com.odigeo.email.trigger.kafka.processor;

import com.edreams.util.date.LocalizedDate;
import com.odigeo.commons.monitoring.metrics.MetricsBuilder;
import com.odigeo.commons.monitoring.metrics.MetricsConstants;
import com.odigeo.commons.monitoring.metrics.MetricsUtils;
import com.odigeo.email.trigger.rules.Rules;
import com.odigeo.email.trigger.rules.RulesInterface;
import com.odigeo.email.trigger.trigger.EmailRequester;
import com.odigeo.email.trigger.trigger.ExecutionResult;
import com.odigeo.email.trigger.util.Constants;
import com.odigeo.messaging.utils.BasicMessage;
import com.odigeo.messaging.utils.ConsumerIterator;
import org.apache.log4j.Logger;

import java.text.ParseException;

public final class RecoveryProcessor extends KafkaBasicMessageProcessor {
    private static final Logger LOGGER = Logger.getLogger(RecoveryProcessor.class);
    private static final RulesInterface RECOVERY_RULES = new Rules();

    private static final int MAX_ATTEMPTS = 18;
    private static final int RECOVERY_TIME_MILLISECONDS = 600000;

    public RecoveryProcessor(ConsumerIterator<BasicMessage> consumerIterator) {
        super(consumerIterator);
    }

    @Override
    protected EmailRequester getEmailRequester() {
        return EmailRequester.RECOVERY;
    }

    @Override
    protected void processMessage(BasicMessage message) {
        int attempt = Integer.parseInt(message.getExtraParameters().get(Constants.ATTEMPT));
        LOGGER.info("Processing" + Constants.LOG_SEPARATOR + message.getTopicName() + Constants.LOG_SEPARATOR + message.getKey() + Constants.LOG_SEPARATOR + attempt);
        ExecutionResult executionResult = isValidAttempt(message);
        waitRecoveryTime(message, executionResult);
        executionResult = ExecutionResult.OK.equals(executionResult) ? executeTrigger(message, RECOVERY_RULES) : executionResult;
        executionResult.closeExecution(message);
        MetricsUtils.incrementMeter(MetricsBuilder.buildTriggerMetric(MetricsConstants.BOOKING_RECOVERY, executionResult), MetricsConstants.REGISTRY_BOOKING_EMAIL_TRIGGER);
        LOGGER.info("Execution closed" + Constants.LOG_SEPARATOR + message.getTopicName() + Constants.LOG_SEPARATOR + message.getKey() + Constants.LOG_SEPARATOR + executionResult + Constants.LOG_SEPARATOR + attempt);
    }

    public static RulesInterface getRecoveryRules() {
        return RECOVERY_RULES;
    }

    private ExecutionResult isValidAttempt(BasicMessage message) {
        int attempt = Integer.parseInt(message.getExtraParameters().get(Constants.ATTEMPT));
        if (attempt > MAX_ATTEMPTS) {
            LOGGER.info("Max attempts" + Constants.LOG_SEPARATOR + message.getTopicName() + Constants.LOG_SEPARATOR + message.getKey());
            return ExecutionResult.OVER_MAX_ATTEMPTS_RECOVERY;
        }
        return ExecutionResult.OK;
    }

    private void waitRecoveryTime(BasicMessage message, ExecutionResult executionResult) {
        try {
            if (ExecutionResult.OK.equals(executionResult)) {
                long timestamp = LocalizedDate.getInstance(message.getExtraParameters().get(Constants.TIMESTAMP), Constants.DATE_FORMAT).getDate().getTime();
                long now = LocalizedDate.getInstance().getDate().getTime();
                long waitTime = RECOVERY_TIME_MILLISECONDS - (now - timestamp);
                if (waitTime > 0) {
                    Thread.sleep(waitTime);
                }
            }
        } catch (ParseException e) {
            LOGGER.error(e);
        } catch (InterruptedException e) {
            LOGGER.error(e);
            Thread.currentThread().interrupt();
        }
    }

}
