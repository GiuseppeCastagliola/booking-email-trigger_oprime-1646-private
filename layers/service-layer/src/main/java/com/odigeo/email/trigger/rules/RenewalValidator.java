package com.odigeo.email.trigger.rules;

import com.odigeo.bookingapi.v12.responses.ProductCategoryBooking;
import com.odigeo.email.trigger.trigger.TriggerInfo;
import com.odigeo.email.trigger.trigger.Validation;
import com.odigeo.email.trigger.util.Constants;
import lombok.EqualsAndHashCode;
import org.apache.commons.collections.CollectionUtils;

import java.util.List;

@EqualsAndHashCode
public class RenewalValidator implements TriggerRule {

    public void validate(TriggerInfo triggerInfo) {
        List<ProductCategoryBooking> productCategoryBookings = triggerInfo.getBookingDetail().getBookingProducts();
        if (CollectionUtils.isNotEmpty(productCategoryBookings)
                && productCategoryBookings.size() == 1
                && Constants.MEMBERSHIP_RENEWAL.equals(productCategoryBookings.get(0).getProductType())) {
            throw new TriggerRuleException(Validation.MEMBERSHIP_RENEWAL);
        }
    }
}

