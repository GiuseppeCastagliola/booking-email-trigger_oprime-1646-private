package com.odigeo.email.trigger.rules;

import com.edreams.configuration.ConfigurationEngine;
import lombok.EqualsAndHashCode;

import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@EqualsAndHashCode
public class ResendEmailRules implements RulesInterface {

    @Override
    public List<TriggerRule> get() {
        return Stream.of(ConfigurationEngine.getInstance(CacheRule.class),
                new WhiteLabelRule(),
                new BookingStatusRule(),
                new BuyerRule(),
                new RenewalValidator()).
                collect(Collectors.toList());
    }
}
