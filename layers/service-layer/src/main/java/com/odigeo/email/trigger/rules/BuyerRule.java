package com.odigeo.email.trigger.rules;

import com.odigeo.email.trigger.trigger.TriggerInfo;
import com.odigeo.email.trigger.trigger.Validation;
import lombok.EqualsAndHashCode;

@EqualsAndHashCode
public class BuyerRule implements TriggerRule {

    @Override
    public void validate(TriggerInfo triggerInfo) {
        if (triggerInfo.getBookingDetail().getBuyer() == null) {
            throw new TriggerRuleException(Validation.MISSING_BUYER);
        }
    }
}

