package com.odigeo.email.trigger.trigger;

import com.google.inject.Inject;
import com.google.inject.Singleton;
import com.odigeo.email.trigger.render.RenderInfo;
import com.odigeo.email.trigger.render.RenderServiceClient;

import java.io.IOException;

@Singleton
public class TriggerExecutor {

    private final RenderServiceClient renderServiceClient;
    private final CustomerEmailTrigger customerEmailTrigger;

    @Inject
    public TriggerExecutor(RenderServiceClient renderServiceClient, CustomerEmailTrigger customerEmailTrigger) {
        this.renderServiceClient = renderServiceClient;
        this.customerEmailTrigger = customerEmailTrigger;
    }

    public ExecutionResult execute(TriggerInfo triggerInfo) throws IOException {
        RenderInfo renderInfo = renderServiceClient.render(triggerInfo);
        return customerEmailTrigger.execute(triggerInfo, renderInfo);
    }

}
