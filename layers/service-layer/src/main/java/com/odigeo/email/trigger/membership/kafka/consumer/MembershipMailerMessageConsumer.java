package com.odigeo.email.trigger.membership.kafka.consumer;

import com.google.inject.Inject;
import com.google.inject.Singleton;
import com.odigeo.commons.messaging.KafkaConsumer;
import com.odigeo.commons.messaging.MessageProcessor;
import com.odigeo.email.trigger.kafka.consumer.ConsumerFactory;
import com.odigeo.email.trigger.membership.kafka.MembershipMailerMessageProcessor;
import com.odigeo.membership.message.MembershipMailerMessage;
import org.apache.log4j.Logger;

@Singleton
public class MembershipMailerMessageConsumer {

    private static final Logger LOGGER = Logger.getLogger(MembershipMailerMessageConsumer.class);
    public static final String MEMBERSHIP_ACTIVATIONS_KAFKA_TOPIC = "MEMBERSHIP_TRANSACTIONAL_ACTIVATIONS_v1";
    static final String MESSAGE_GROUP = "booking-email-trigger-membership";
    static final int THREADS_COUNT = 1;

    private final MessageProcessor<MembershipMailerMessage> messageProcessor;
    private final KafkaConsumer<MembershipMailerMessage> consumer;

    @Inject
    public MembershipMailerMessageConsumer(MembershipMailerMessageProcessor messageProcessor, ConsumerFactory consumerFactory) {
        this.messageProcessor = messageProcessor;
        consumer = consumerFactory.newConsumer(MEMBERSHIP_ACTIVATIONS_KAFKA_TOPIC, MESSAGE_GROUP, MembershipMailerMessage.class);
    }

    public void launch() {
        consumer.start(messageProcessor, THREADS_COUNT);
    }

    public void shutdown() {
        if (consumer != null) {
            try {
                consumer.shutdown();
            } catch (InterruptedException e) {
                LOGGER.error("MembershipMailerMessageConsumer shutdown interrupted", e);
            }
        }
    }

}
