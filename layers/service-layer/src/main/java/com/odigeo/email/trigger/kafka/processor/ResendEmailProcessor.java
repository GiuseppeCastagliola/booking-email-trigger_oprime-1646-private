package com.odigeo.email.trigger.kafka.processor;

import com.edreams.configuration.ConfigurationEngine;
import com.google.inject.Inject;
import com.odigeo.commons.monitoring.metrics.MetricsBuilder;
import com.odigeo.commons.monitoring.metrics.MetricsConstants;
import com.odigeo.commons.monitoring.metrics.MetricsUtils;
import com.odigeo.email.trigger.factories.RulesFactory;
import com.odigeo.email.trigger.trigger.EmailRequester;
import com.odigeo.email.trigger.trigger.ExecutionResult;
import com.odigeo.email.trigger.util.Constants;
import com.odigeo.messaging.utils.BasicMessage;
import com.odigeo.messaging.utils.ConsumerIterator;
import org.apache.log4j.Logger;

public class ResendEmailProcessor extends KafkaBasicMessageProcessor {

    private static final Logger LOGGER = Logger.getLogger(ResendEmailProcessor.class);
    private final RulesFactory rulesFactory;


    @Inject
    public ResendEmailProcessor(ConsumerIterator<BasicMessage> consumerIterator) {
        super(consumerIterator);
        this.rulesFactory = ConfigurationEngine.getInstance(RulesFactory.class);
    }

    @Override
    protected EmailRequester getEmailRequester() {
        return EmailRequester.RESEND;
    }

    @Override
    protected void processMessage(BasicMessage basicMessage) {
        ExecutionResult executionResult = executeTrigger(basicMessage, rulesFactory.getResendEmailRules());
        executionResult.closeExecution(basicMessage);
        MetricsUtils.incrementMeter(MetricsBuilder.buildTriggerMetric(MetricsConstants.RESEND_EMAIL, executionResult), MetricsConstants.REGISTRY_BOOKING_EMAIL_TRIGGER);
        LOGGER.info("Execution closed" + Constants.LOG_SEPARATOR + basicMessage.getTopicName() + Constants.LOG_SEPARATOR + basicMessage.getKey() + Constants.LOG_SEPARATOR + executionResult);
    }


}


