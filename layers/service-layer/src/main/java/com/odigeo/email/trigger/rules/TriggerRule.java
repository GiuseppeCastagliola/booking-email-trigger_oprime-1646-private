package com.odigeo.email.trigger.rules;

import com.odigeo.email.trigger.trigger.TriggerInfo;

public interface TriggerRule {

    void validate(TriggerInfo triggerInfo);
}

