package com.odigeo.email.trigger.changedates.kafka;

import com.edreams.configuration.ConfiguredInPropertiesFile;
import com.google.inject.Singleton;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Singleton
@Getter
@Setter
@NoArgsConstructor
@ConfiguredInPropertiesFile
@SuppressWarnings("PMD.UnusedPrivateField")
public class KafkaChangeDatesConfiguration {

    private String zookeeperUrl;
    private String bootstrapServerUrl;
    private String groupId;
    private int numThreads;
    private String topic;

}
