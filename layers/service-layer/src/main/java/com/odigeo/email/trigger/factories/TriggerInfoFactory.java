package com.odigeo.email.trigger.factories;

import com.odigeo.bookingapi.v12.responses.BookingDetail;
import com.odigeo.email.render.v1.contract.EmailType;
import com.odigeo.email.trigger.booking.product.BookingProducts;
import com.odigeo.email.trigger.trigger.EmailRequester;
import com.odigeo.email.trigger.trigger.TriggerInfo;

import java.util.Collections;
import java.util.Map;

public class TriggerInfoFactory {

    public TriggerInfo constructTriggerInfo(BookingDetail bookingDetail, EmailType emailType, BookingProducts bookingProducts, EmailRequester emailRequester) {
        return constructTriggerInfo(bookingDetail, emailType, bookingProducts, emailRequester, Collections.emptyMap());
    }

    public TriggerInfo constructTriggerInfo(BookingDetail bookingDetail, EmailType emailType, BookingProducts bookingProducts, EmailRequester emailRequester, Map<String, Object> additionalParameters) {
        return new TriggerInfo(bookingDetail, emailType, bookingProducts, emailRequester, additionalParameters);
    }
}

