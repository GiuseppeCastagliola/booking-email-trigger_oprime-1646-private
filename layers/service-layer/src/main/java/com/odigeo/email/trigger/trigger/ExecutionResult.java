package com.odigeo.email.trigger.trigger;

import com.edreams.configuration.ConfigurationEngine;
import com.odigeo.email.trigger.kafka.pusher.RecoveryPusher;
import com.odigeo.messaging.utils.BasicMessage;

import java.util.function.Consumer;

public enum ExecutionResult {

    OK(basicMessage -> { }),
    OK_PRODUCT_CHANGES(basicMessage -> { }),
    BOOKING_IN_CACHE(basicMessage -> { }),
    DEPARTURE_PAST(basicMessage -> { }),
    INVALID_STATUS(basicMessage -> { }),
    MISSING_BUYER(basicMessage -> { }),
    ALREADY_TRIGGERED(basicMessage -> { }),
    OVER_MAX_ATTEMPTS_RECOVERY(basicMessage -> { }),
    KO_UNRECOVERABLE(basicMessage -> { }),
    MEMBERSHIP_RENEWAL(basicMessage -> { }),
    WHITE_LABEL(basicMessage -> { }),
    NOT_PURE_LOWCOST(basicMessage -> { }),
    MISSING_EMAIL_TYPE(basicMessage -> { }),
    MUST_BE_IGNORED(basicMessage -> { }),
    KO_RECOVERABLE(basicMessage -> ConfigurationEngine.getInstance(RecoveryPusher.class).publishMessage(basicMessage, 1)),
    MISSING_ADDITIONAL_PARAMETER(basicMessage -> { });

    private final Consumer<BasicMessage> consumer;

    ExecutionResult(Consumer<BasicMessage> consumer) {
        this.consumer = consumer;
    }

    public void closeExecution(BasicMessage basicMessage) {
        consumer.accept(basicMessage);
    }

}

