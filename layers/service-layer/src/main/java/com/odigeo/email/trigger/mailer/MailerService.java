package com.odigeo.email.trigger.mailer;

import com.google.inject.Inject;
import com.google.inject.Singleton;
import com.odigeo.commons.rest.ServiceBuilder;
import com.odigeo.commons.rest.configuration.ConnectionConfiguration;
import com.odigeo.commons.rest.configuration.URLConfiguration;
import com.odigeo.commons.rest.error.SimpleRestErrorsHandler;
import com.odigeo.email.trigger.configuration.Configuration;
import com.odigeo.email.trigger.configuration.MailerConfiguration;
import com.odigeo.mailer.v1.Attachment;
import com.odigeo.mailer.v1.Credentials;
import com.odigeo.mailer.v1.EmailSendRequest;
import com.odigeo.mailer.v1.EmailSendResponse;
import com.odigeo.mailer.v1.InvalidParametersException;
import com.odigeo.mailer.v1.RemoteException;
import com.odigeo.mailer.v1.TemplateEmailDeliveryService;
import com.odigeo.mailer.v1.Website;

import java.util.List;
import java.util.Map;

@Singleton
public class MailerService {

    private final Credentials credentials;
    private final TemplateEmailDeliveryService templateService;

    @Inject
    public MailerService(Configuration configuration) {
        MailerConfiguration mailerConfiguration = configuration.getMailerConfiguration();
        credentials = new Credentials(mailerConfiguration.getUser(), mailerConfiguration.getPass());
        ConnectionConfiguration connectionConfiguration = new ConnectionConfiguration();
        connectionConfiguration.setConnectionTimeoutInMillis(mailerConfiguration.getTimeout());
        connectionConfiguration.setConnectionTimeoutInMillis(mailerConfiguration.getTimeout());
        URLConfiguration urlConfiguration = new URLConfiguration(mailerConfiguration.getUrl() + mailerConfiguration.getService());
        SimpleRestErrorsHandler restErrorsHandler = new SimpleRestErrorsHandler(TemplateEmailDeliveryService.class);
        templateService = new ServiceBuilder<>(TemplateEmailDeliveryService.class, urlConfiguration, restErrorsHandler).withConnectionConfiguration(connectionConfiguration).build();
    }

    public EmailSendResponse sendEmail(
            String from,
            List<String> recipients,
            String locale,
            String websiteCode,
            String brandCode,
            String templateId,
            String emailTag,
            Map<String, String> parameters,
            List<Attachment> attachments
    ) throws InvalidParametersException, RemoteException {
        return templateService.sendEmail(createRequest(from, recipients, locale, websiteCode, brandCode, templateId, emailTag, parameters, attachments));
    }

    private EmailSendRequest createRequest(
            String from,
            List<String> recipients,
            String locale,
            String websiteCode,
            String brandCode,
            String templateId,
            String emailTag,
            Map<String, String> parameters,
            List<Attachment> attachments
    ) {
        EmailSendRequest request = new EmailSendRequest();
        request.setCredentials(credentials);
        request.setFrom(from);
        request.setTo(recipients);
        request.setLocale(locale);
        request.setWebsite(new Website(websiteCode, brandCode));
        request.setTemplateId(templateId);
        request.setTag(emailTag);
        request.setParameters(parameters);
        request.setAttachments(attachments);
        return request;
    }
}
