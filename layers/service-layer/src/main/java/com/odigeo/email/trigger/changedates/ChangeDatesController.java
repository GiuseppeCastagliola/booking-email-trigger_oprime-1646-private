package com.odigeo.email.trigger.changedates;

import com.google.common.collect.ImmutableMap;
import com.google.inject.Inject;
import com.google.inject.Singleton;
import com.odigeo.bookingchange.message.UserBookingChangeRequestStatus;
import com.odigeo.email.render.v1.contract.EmailType;
import com.odigeo.email.trigger.bookingapi.BookingServiceClient;
import com.odigeo.email.trigger.events.EventController;
import com.odigeo.email.trigger.factories.TriggerInfoFactory;
import com.odigeo.email.trigger.trigger.TriggerExecutor;
import com.odigeo.email.trigger.trigger.TriggerServiceRulesValidator;

import java.util.Map;
import java.util.Optional;

@Singleton
public class ChangeDatesController extends EventController {

    private final Map<UserBookingChangeRequestStatus, EmailType> emailTypeMapper = new ImmutableMap.Builder<UserBookingChangeRequestStatus, EmailType>()
            .put(UserBookingChangeRequestStatus.REQUESTED, EmailType.CHANGE_REQUESTED)
            .put(UserBookingChangeRequestStatus.PROCESSING, EmailType.CHANGE_PROCESSING)
            .put(UserBookingChangeRequestStatus.AGENT_REVIEW, EmailType.CHANGE_UNDER_REVIEW)
            .build();

    @Inject
    ChangeDatesController(BookingServiceClient bookingServiceClient, TriggerExecutor triggerExecutor, TriggerInfoFactory triggerInfoFactory, TriggerServiceRulesValidator triggerServiceRulesValidator) {
        super(bookingServiceClient, triggerExecutor, triggerInfoFactory, triggerServiceRulesValidator);
    }

    @Override
    protected EmailType getEmailTypeFromUserBookingChangeRequestStatus(final UserBookingChangeRequestStatus userBookingChangeRequestStatus) {
        return Optional.ofNullable(emailTypeMapper.get(userBookingChangeRequestStatus))
                .orElseThrow(() -> new IllegalArgumentException("There is no EmailTypeMapping for the following UserBookingChangeRequestStatus" + userBookingChangeRequestStatus)
                );
    }
}
