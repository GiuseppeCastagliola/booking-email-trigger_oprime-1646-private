package com.odigeo.email.trigger.kafka.processor;

import com.odigeo.commons.monitoring.metrics.MetricsBuilder;
import com.odigeo.commons.monitoring.metrics.MetricsConstants;
import com.odigeo.commons.monitoring.metrics.MetricsUtils;
import com.odigeo.email.trigger.rules.Rules;
import com.odigeo.email.trigger.rules.RulesInterface;
import com.odigeo.email.trigger.trigger.EmailRequester;
import com.odigeo.email.trigger.trigger.ExecutionResult;
import com.odigeo.email.trigger.util.Constants;
import com.odigeo.messaging.utils.BasicMessage;
import com.odigeo.messaging.utils.ConsumerIterator;
import org.apache.log4j.Logger;

public class BookingUpdatesProcessor extends KafkaBasicMessageProcessor {
    private static final Logger LOGGER = Logger.getLogger(BookingUpdatesProcessor.class);
    private static final RulesInterface BOOKING_UPDATES_RULES = new Rules();

    public BookingUpdatesProcessor(ConsumerIterator<BasicMessage> consumerIterator) {
        super(consumerIterator);
    }

    @Override
    protected EmailRequester getEmailRequester() {
        return EmailRequester.UPDATE;
    }

    @Override
    protected void processMessage(BasicMessage basicMessage) {
        LOGGER.info("Processing " + Constants.LOG_SEPARATOR + basicMessage.getTopicName() + Constants.LOG_SEPARATOR + basicMessage.getKey());
        ExecutionResult executionResult = executeTrigger(basicMessage, BOOKING_UPDATES_RULES);
        executionResult.closeExecution(basicMessage);
        MetricsUtils.incrementMeter(MetricsBuilder.buildTriggerMetric(MetricsConstants.BOOKING_UPDATES, executionResult), MetricsConstants.REGISTRY_BOOKING_EMAIL_TRIGGER);
        LOGGER.info("Execution closed" + Constants.LOG_SEPARATOR + basicMessage.getTopicName() + Constants.LOG_SEPARATOR + basicMessage.getKey() + Constants.LOG_SEPARATOR + executionResult);
    }

    public static RulesInterface getBookingUpdatesRules() {
        return BOOKING_UPDATES_RULES;
    }
}
