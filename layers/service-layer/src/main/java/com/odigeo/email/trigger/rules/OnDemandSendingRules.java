package com.odigeo.email.trigger.rules;

import lombok.EqualsAndHashCode;

import java.util.Collections;
import java.util.List;

@EqualsAndHashCode
public class OnDemandSendingRules implements RulesInterface {

    @Override
    public List<TriggerRule> get() {
        return Collections.singletonList(new TopUpVoucherRule());
    }
}
