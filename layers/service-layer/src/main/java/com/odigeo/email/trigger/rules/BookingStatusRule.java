package com.odigeo.email.trigger.rules;

import com.odigeo.bookingapi.v12.requests.Status;
import com.odigeo.email.render.v1.contract.EmailType;
import com.odigeo.email.trigger.trigger.TriggerInfo;
import com.odigeo.email.trigger.trigger.Validation;
import lombok.EqualsAndHashCode;

@EqualsAndHashCode
public class BookingStatusRule implements TriggerRule {

    @Override
    public void validate(TriggerInfo triggerInfo) {
        if (!hasAllowedStatus(triggerInfo.getBookingDetail().getBookingStatus()) || !hasAllowedStatusForEmailType(triggerInfo)) {
            throw new TriggerRuleException(Validation.INVALID_STATUS);
        }
    }

    private boolean hasAllowedStatus(String bookingStatus) {
        return Status.REQUEST.name().equals(bookingStatus)
                || Status.CONTRACT.name().equals(bookingStatus)
                || Status.DIDNOTBUY.name().equals(bookingStatus);
    }

    private boolean hasAllowedStatusForEmailType(TriggerInfo triggerInfo) {
        return !EmailType.KO.equals(triggerInfo.getEmailType()) || Status.DIDNOTBUY.name().equals(triggerInfo.getBookingDetail().getBookingStatus());
    }
}

