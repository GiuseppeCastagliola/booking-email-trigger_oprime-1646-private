package com.odigeo.email.trigger.kafka.pusher;

import com.google.inject.Inject;
import com.google.inject.Singleton;
import com.odigeo.email.trigger.configuration.Configuration;

@Singleton
public class RecoveryPusher extends Pusher {

    @Inject
    public RecoveryPusher(Configuration configuration) {
        super(configuration.getKafkaConfiguration().getRecoveryTopic(), configuration.getKafkaConfiguration().getBootstrapServerUrl());
    }

}
