package com.odigeo.email.trigger.rules;

import com.edreams.configuration.ConfigurationEngine;
import com.odigeo.email.trigger.email.TriggerHelper;

import java.util.Arrays;
import java.util.List;

public class Rules implements RulesInterface {


    @Override
    public List<TriggerRule> get() {
        return Arrays.asList(ConfigurationEngine.getInstance(CacheRule.class),
                new ItineraryOrAccommodationRule(),
                new WhiteLabelRule(),
                new DepartureDateRule(ConfigurationEngine.getInstance(TriggerHelper.class)),
                new BookingStatusRule(),
                new BuyerRule(),
                new EmailTypeRule(),
                ConfigurationEngine.getInstance(BookingProductsRule.class),
                new RenewalValidator());
    }
}

