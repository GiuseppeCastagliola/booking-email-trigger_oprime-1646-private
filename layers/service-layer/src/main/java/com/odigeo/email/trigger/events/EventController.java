package com.odigeo.email.trigger.events;

import com.google.inject.Inject;
import com.odigeo.bookingapi.v12.responses.BookingDetail;
import com.odigeo.bookingchange.message.StatusUpdated;
import com.odigeo.bookingchange.message.UserBookingChangeRequestStatus;
import com.odigeo.commons.concurrent.monitoring.metric.MetricBuilder;
import com.odigeo.commons.monitoring.metrics.MetricsConstants;
import com.odigeo.commons.monitoring.metrics.MetricsUtils;
import com.odigeo.email.render.v1.contract.EmailType;
import com.odigeo.email.trigger.booking.builder.BookingProductsBuilder;
import com.odigeo.email.trigger.bookingapi.BookingServiceClient;
import com.odigeo.email.trigger.factories.TriggerInfoFactory;
import com.odigeo.email.trigger.rules.RulesInterface;
import com.odigeo.email.trigger.trigger.EmailRequester;
import com.odigeo.email.trigger.trigger.ExecutionResult;
import com.odigeo.email.trigger.trigger.TriggerExecutor;
import com.odigeo.email.trigger.trigger.TriggerInfo;
import com.odigeo.email.trigger.trigger.TriggerServiceRulesValidator;

import java.io.IOException;
import java.util.Locale;

public abstract class EventController {

    private final BookingServiceClient bookingServiceClient;
    private final TriggerExecutor triggerExecutor;
    private final TriggerInfoFactory triggerInfoFactory;
    private final TriggerServiceRulesValidator triggerServiceRulesValidator;

    @Inject
    public EventController(BookingServiceClient bookingServiceClient, TriggerExecutor triggerExecutor, TriggerInfoFactory triggerInfoFactory, TriggerServiceRulesValidator triggerServiceRulesValidator) {
        this.bookingServiceClient = bookingServiceClient;
        this.triggerExecutor = triggerExecutor;
        this.triggerInfoFactory = triggerInfoFactory;
        this.triggerServiceRulesValidator = triggerServiceRulesValidator;
    }

    public void onMessage(final StatusUpdated statusUpdatedMessage, RulesInterface rules) throws IOException {
        final BookingDetail bookingDetail = bookingServiceClient.getBookingDetail(Locale.getDefault(), statusUpdatedMessage.getBookingId());
        final EmailType emailType = getEmailTypeFromUserBookingChangeRequestStatus(statusUpdatedMessage.getRequestStatus());
        final TriggerInfo triggerInfo = triggerInfoFactory.constructTriggerInfo(
                bookingDetail,
                emailType,
                new BookingProductsBuilder(bookingDetail).build(),
                EmailRequester.UPDATE
        );
        ExecutionResult executionResult = triggerServiceRulesValidator.validate(triggerInfo, rules);
        if (executionResult.equals(ExecutionResult.OK)) {
            triggerExecutor.execute(triggerInfo);
            MetricsUtils.incrementCounter(new MetricBuilder(emailType.toString()).build(), MetricsConstants.REGISTRY_BOOKING_EMAIL_TRIGGER);
        }
    }

    protected abstract EmailType getEmailTypeFromUserBookingChangeRequestStatus(final UserBookingChangeRequestStatus userBookingChangeRequestStatus);
}
