package com.odigeo.email.trigger.factories;

import com.odigeo.email.trigger.rules.CancellationSendingRules;
import com.odigeo.email.trigger.rules.OnDemandSendingRules;
import com.odigeo.email.trigger.rules.ResendEmailRules;
import com.odigeo.email.trigger.rules.RulesInterface;

public class RulesFactory {

    private static final RulesInterface RESEND_EMAIL_RULES = new ResendEmailRules();
    private static final RulesInterface ON_DEMAND_EMAIL_RULES = new OnDemandSendingRules();
    private static final RulesInterface CANCELLATION_EMAIL_RULES = new CancellationSendingRules();

    public RulesInterface getResendEmailRules() {
        return RESEND_EMAIL_RULES;
    }

    public RulesInterface getOnDemandEmailRules() {
        return ON_DEMAND_EMAIL_RULES;
    }

    public RulesInterface getCancellationEmailRules() {
        return CANCELLATION_EMAIL_RULES;
    }
}
