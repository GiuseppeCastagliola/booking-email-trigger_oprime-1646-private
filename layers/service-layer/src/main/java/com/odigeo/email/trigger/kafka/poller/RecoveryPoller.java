package com.odigeo.email.trigger.kafka.poller;

import com.google.inject.Inject;
import com.google.inject.Singleton;
import com.odigeo.email.trigger.configuration.Configuration;
import com.odigeo.email.trigger.kafka.processor.RecoveryProcessor;

@Singleton
public final class RecoveryPoller extends KafkaPoller<RecoveryProcessor> {

    @Inject
    public RecoveryPoller(Configuration configuration) throws NoSuchMethodException {
        super(configuration.getKafkaConfiguration().getNumThreadsRecovery(),
                configuration.getKafkaConfiguration().getGroupId(),
                configuration.getKafkaConfiguration().getZookeeperUrl(),
                configuration.getKafkaConfiguration().getRecoveryTopic(),
                RecoveryProcessor.class);
    }
}
