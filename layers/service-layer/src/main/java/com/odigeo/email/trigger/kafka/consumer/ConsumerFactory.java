package com.odigeo.email.trigger.kafka.consumer;

import com.google.inject.Singleton;
import com.odigeo.commons.messaging.JsonDeserializer;
import com.odigeo.commons.messaging.KafkaConsumer;
import com.odigeo.commons.messaging.Message;


@Singleton
public class ConsumerFactory {

    public <T extends Message> KafkaConsumer<T> newConsumer(String topicName, String groupId, Class<T> messageClass) {
        return new KafkaConsumer<>(new JsonDeserializer<>(messageClass), topicName, groupId);
    }
}
