package com.odigeo.email.trigger.cancellation.kafka;

import com.edreams.configuration.ConfiguredInPropertiesFile;
import com.google.inject.Singleton;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Singleton
@Getter
@Setter
@NoArgsConstructor
@ConfiguredInPropertiesFile
@SuppressWarnings("PMD.UnusedPrivateField")
public class KafkaVoluntaryCancellationConfiguration {

    private String zookeeperUrl;
    private String bootstrapServerUrl;
    private String groupId;
    private int numThreads;
    private String topic;

}



