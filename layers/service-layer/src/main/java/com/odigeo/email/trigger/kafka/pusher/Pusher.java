package com.odigeo.email.trigger.kafka.pusher;

import com.edreams.util.date.LocalizedDate;
import com.odigeo.email.trigger.util.Constants;
import com.odigeo.messaging.utils.BasicMessage;
import com.odigeo.messaging.utils.MessageDataAccessException;
import com.odigeo.messaging.utils.Publisher;
import com.odigeo.messaging.utils.kafka.KafkaJsonPublisher;
import org.apache.log4j.Logger;

import java.util.Collections;

public class Pusher {
    private static final Logger LOGGER = Logger.getLogger(Pusher.class);
    final String topicName;
    final Publisher<BasicMessage> publisher;

    public Pusher(String topicName, String serverUrl) {
        this.topicName = topicName;
        this.publisher = new KafkaJsonPublisher(new KafkaJsonPublisher.Builder(serverUrl));
    }

    public void publishMessage(BasicMessage message, Integer attempt) {
        message.setTopicName(topicName);
        message.addExtraParameter(Constants.TIMESTAMP, LocalizedDate.getInstance().convertToString(Constants.DATE_FORMAT));
        message.addExtraParameter(Constants.ATTEMPT, attempt.toString());

        try {
            publisher.publishMessages(Collections.singletonList(message));
            LOGGER.info("Published" + Constants.LOG_SEPARATOR + message.getTopicName() + Constants.LOG_SEPARATOR + message.getKey());
        } catch (MessageDataAccessException e) {
            LOGGER.error("Exception publishing" + Constants.LOG_SEPARATOR + message.getTopicName() + Constants.LOG_SEPARATOR + message.getKey(), e);
        }
    }
}
