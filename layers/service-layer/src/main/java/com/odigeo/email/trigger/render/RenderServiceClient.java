package com.odigeo.email.trigger.render;

import com.google.inject.Inject;
import com.google.inject.Singleton;
import com.odigeo.email.render.v1.contract.Credentials;
import com.odigeo.email.render.v1.contract.RemoteException;
import com.odigeo.email.render.v1.contract.RenderRequest;
import com.odigeo.email.render.v1.contract.RenderResponse;
import com.odigeo.email.render.v1.contract.RenderService;
import com.odigeo.email.trigger.configuration.BookingServiceConfiguration;
import com.odigeo.email.trigger.configuration.Configuration;
import com.odigeo.email.trigger.trigger.TriggerInfo;
import com.odigeo.email.trigger.util.Constants;
import org.apache.log4j.Logger;

import java.io.IOException;

@Singleton
public class RenderServiceClient {
    private static final Logger LOGGER = Logger.getLogger(RenderServiceClient.class);

    private final RenderService renderService;
    private final BookingServiceConfiguration bookingServiceConfiguration;

    @Inject
    public RenderServiceClient(RenderService renderService, Configuration configuration) {
        this.renderService = renderService;
        bookingServiceConfiguration = configuration.getBookingServiceConfiguration();
    }

    public RenderInfo render(TriggerInfo triggerInfo) throws IOException {
        try {
            RenderResponse renderResponse = renderService.renderTemplate(createRenderRequest(triggerInfo));
            return new RenderInfoBuilder(renderResponse, triggerInfo.getBookingDetail().getBookingBasicInfo().getId(), triggerInfo.getEmailType()).build();
        } catch (RemoteException e) {
            LOGGER.error("Error rendering " + triggerInfo.getBookingDetail().getBookingBasicInfo().getId() + Constants.LOG_SEPARATOR + triggerInfo.getEmailType());
            throw new IOException(e);
        }
    }

    private RenderRequest createRenderRequest(TriggerInfo triggerInfo) {
        return new RenderRequest(
                new Credentials(bookingServiceConfiguration.getUser(), bookingServiceConfiguration.getPass()),
                triggerInfo.getBookingDetail().getBookingBasicInfo().getId(),
                triggerInfo.getEmailType(),
                triggerInfo.getAdditionalParameters());
    }
}

