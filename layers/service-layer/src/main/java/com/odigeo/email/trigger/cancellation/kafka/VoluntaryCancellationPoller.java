package com.odigeo.email.trigger.cancellation.kafka;

import com.google.inject.Inject;
import com.odigeo.bookingchange.message.CancellationStatusUpdatedMessage;
import com.odigeo.email.trigger.cancellation.VoluntaryCancellationMessageProcessor;

import com.odigeo.messaging.utils.Consumer;
import com.odigeo.messaging.utils.kafka.KafkaJsonConsumer;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class VoluntaryCancellationPoller {

    private final ExecutorService executorService;
    private final Consumer<CancellationStatusUpdatedMessage> consumer;
    private final KafkaVoluntaryCancellationConfiguration configuration;

    @Inject
    public VoluntaryCancellationPoller(final KafkaVoluntaryCancellationConfiguration kafkaVoluntaryCancellationConfiguration) {
        this.configuration = kafkaVoluntaryCancellationConfiguration;
        this.executorService = Executors.newFixedThreadPool(configuration.getNumThreads());
        this.consumer = new KafkaJsonConsumer<>(CancellationStatusUpdatedMessage.class, configuration.getZookeeperUrl(), configuration.getGroupId(), configuration.getTopic());
    }

    public void init() {
        consumer.connectAndIterate(configuration.getNumThreads()).forEach(consumerIterator -> {
            executorService.submit(new VoluntaryCancellationMessageProcessor(consumerIterator));
        });
    }

    public void destroy() {
        if (consumer != null) {
            consumer.close();
        }
    }
}

