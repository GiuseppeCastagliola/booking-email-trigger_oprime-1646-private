package com.odigeo.email.trigger.rules;

import com.odigeo.email.render.v1.contract.EmailType;
import com.odigeo.email.trigger.trigger.TriggerInfo;
import com.odigeo.email.trigger.trigger.Validation;
import lombok.EqualsAndHashCode;

import java.util.Arrays;
import java.util.List;

@EqualsAndHashCode
public class IgnoredEmailTypesRule implements TriggerRule {

    private static final List<EmailType> IGNORED_EMAIL_TYPES = Arrays.asList(
            EmailType.CANCELLATION_REQUESTED,
            EmailType.CHANGE_REQUESTED
    );

    @Override
    public void validate(TriggerInfo triggerInfo) {
        if (IGNORED_EMAIL_TYPES.contains(triggerInfo.getEmailType())) {
            throw new TriggerRuleException(Validation.MUST_BE_IGNORED);
        }
    }
}
