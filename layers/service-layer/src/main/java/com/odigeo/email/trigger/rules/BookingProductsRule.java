package com.odigeo.email.trigger.rules;

import com.google.inject.Inject;
import com.odigeo.email.render.v1.contract.EmailType;
import com.odigeo.email.trigger.booking.product.BookingProducts;
import com.odigeo.email.trigger.tracking.CustomerEmailManager;
import com.odigeo.email.trigger.trigger.TriggerInfo;
import com.odigeo.email.trigger.trigger.Validation;

import java.util.Optional;

public class BookingProductsRule implements TriggerRule {

    private final CustomerEmailManager customerEmailManager;

    @Inject
    public BookingProductsRule(CustomerEmailManager customerEmailManager) {
        this.customerEmailManager = customerEmailManager;
    }

    @Override
    public void validate(TriggerInfo triggerInfo) {

        Optional<BookingProducts> bookingProducts = customerEmailManager.searchBookingProducts(triggerInfo.getBookingDetail().getBookingBasicInfo().getId(), triggerInfo.getEmailType());
        if (bookingProducts.isPresent()) {
            if (!triggerInfo.getEmailType().equals(EmailType.CONFIRMATION) || triggerInfo.getBookingProducts().equals(bookingProducts.get())) {
                throw new TriggerRuleException(Validation.ALREADY_TRIGGERED);
            }
        }
    }
}

