package com.odigeo.email.trigger.membership.kafka;

import com.google.inject.Inject;
import com.google.inject.Singleton;
import com.odigeo.commons.concurrent.monitoring.metric.MetricBuilder;
import com.odigeo.commons.monitoring.metrics.MetricsConstants;
import com.odigeo.commons.monitoring.metrics.MetricsUtils;
import com.odigeo.email.render.v1.contract.EmailType;
import com.odigeo.email.trigger.rules.RulesInterface;
import com.odigeo.email.trigger.rules.WelcomeToPrimeEmailRules;
import com.odigeo.email.trigger.trigger.EmailRequester;
import com.odigeo.email.trigger.trigger.ExecutionResult;
import com.odigeo.email.trigger.trigger.TriggerService;
import com.odigeo.membership.message.MembershipMailerMessage;
import com.odigeo.membership.message.enums.MessageType;

import java.io.IOException;
import java.util.Collections;

import static com.odigeo.email.trigger.trigger.ExecutionResult.OK;

@Singleton
public class MembershipMailerMessageController {
    private static final RulesInterface WELCOME_TO_PRIME_RULES = new WelcomeToPrimeEmailRules();
    private final TriggerService triggerService;

    @Inject
    public MembershipMailerMessageController(TriggerService triggerService) {
        this.triggerService = triggerService;
    }

    public void onMessage(MembershipMailerMessage message) throws IOException {
        final EmailType emailType = getEmailTypeFromMessage(message);
        ExecutionResult executionResult = triggerService.execute(String.valueOf(message.getBookingId()), WELCOME_TO_PRIME_RULES, EmailRequester.UPDATE,
                emailType,
                Collections.singletonMap(MembershipMailerMessage.class.getName(), message));
        if (OK.equals(executionResult)) {
            MetricsUtils.incrementCounter(new MetricBuilder(emailType.toString()).build(), MetricsConstants.REGISTRY_BOOKING_EMAIL_TRIGGER);
        }
    }

    private EmailType getEmailTypeFromMessage(MembershipMailerMessage message) {
        if (MessageType.WELCOME_TO_PRIME == message.getMessageType()) {
            return EmailType.WELCOME_TO_PRIME;
        }
        throw new UnsupportedOperationException("No email type is linked to message type " + message.getMessageType());
    }

}
