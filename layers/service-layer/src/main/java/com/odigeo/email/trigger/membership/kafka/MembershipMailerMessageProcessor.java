package com.odigeo.email.trigger.membership.kafka;

import com.google.inject.Inject;
import com.google.inject.Singleton;
import com.odigeo.commons.concurrent.monitoring.metric.MetricBuilder;
import com.odigeo.commons.messaging.MessageProcessor;
import com.odigeo.commons.monitoring.metrics.MetricsConstants;
import com.odigeo.commons.monitoring.metrics.MetricsUtils;
import com.odigeo.membership.message.MembershipMailerMessage;
import lombok.extern.slf4j.Slf4j;

import java.io.IOException;

import static com.odigeo.membership.message.enums.MessageType.WELCOME_TO_PRIME;
@Slf4j
@Singleton
public class MembershipMailerMessageProcessor implements MessageProcessor<MembershipMailerMessage> {

    private final MembershipMailerMessageController controller;

    @Inject
    public MembershipMailerMessageProcessor(MembershipMailerMessageController controller) {
        this.controller = controller;
    }

    @Override
    public void onMessage(MembershipMailerMessage membershipMailerMessage) {
        MetricsUtils.incrementCounter(new MetricBuilder(MetricsConstants.MEMBERSHIP_EMAILS_CONSUMER).build(), MetricsConstants.REGISTRY_BOOKING_EMAIL_TRIGGER);
        if (WELCOME_TO_PRIME == membershipMailerMessage.getMessageType()) {
            handleWelcomeToPrimeMessage(membershipMailerMessage);
        }
    }

    private void handleWelcomeToPrimeMessage(MembershipMailerMessage membershipMailerMessage) {
        try {
            controller.onMessage(membershipMailerMessage);
        } catch (IOException e) {
            log.error("Error processing message for membership " + membershipMailerMessage.getMembershipId(), e);
        }
    }
}
