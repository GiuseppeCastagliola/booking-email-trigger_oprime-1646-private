package com.odigeo.email.trigger.trigger;

import com.google.inject.Inject;
import com.google.inject.Singleton;
import com.odigeo.bookingapi.v12.responses.BookingDetail;
import com.odigeo.email.render.v1.contract.EmailType;
import com.odigeo.email.trigger.booking.builder.BookingProductsBuilder;
import com.odigeo.email.trigger.bookingapi.BookingServiceClient;
import com.odigeo.email.trigger.email.EmailTypeCalculator;
import com.odigeo.email.trigger.factories.TriggerInfoFactory;
import com.odigeo.email.trigger.rules.RulesInterface;

import java.io.IOException;
import java.io.UncheckedIOException;
import java.util.Locale;
import java.util.Map;

@Singleton
public class TriggerService {

    private final TriggerExecutor triggerExecutor;
    private final BookingServiceClient bookingServiceClient;
    private final EmailTypeCalculator emailTypeCalculator;
    private final TriggerInfoFactory triggerInfoFactory;
    private final TriggerServiceRulesValidator triggerServiceRulesValidator;

    @Inject
    public TriggerService(TriggerExecutor triggerExecutor, BookingServiceClient bookingServiceClient, EmailTypeCalculator emailTypeCalculator, TriggerInfoFactory triggerInfoFactory, TriggerServiceRulesValidator triggerServiceRulesValidator) {
        this.triggerExecutor = triggerExecutor;
        this.bookingServiceClient = bookingServiceClient;
        this.emailTypeCalculator = emailTypeCalculator;
        this.triggerInfoFactory = triggerInfoFactory;
        this.triggerServiceRulesValidator = triggerServiceRulesValidator;
    }

    public ExecutionResult execute(String key, RulesInterface rules, EmailRequester emailRequester) throws IOException {
        BookingDetail bookingDetail = getBookingDetail(key);
        EmailType emailType = emailTypeCalculator.calculate(bookingDetail);
        TriggerInfo triggerInfo = triggerInfoFactory.constructTriggerInfo(bookingDetail, emailType, new BookingProductsBuilder(bookingDetail).build(), emailRequester);
        return validateAndExecuteTrigger(rules, triggerInfo);
    }

    public ExecutionResult execute(String key, RulesInterface rules, EmailRequester emailRequester, EmailType emailType, Map<String, Object> additionalParameter) throws IOException {
        BookingDetail bookingDetail = getBookingDetail(key);
        TriggerInfo triggerInfo = triggerInfoFactory.constructTriggerInfo(bookingDetail, emailType, new BookingProductsBuilder(bookingDetail).build(), emailRequester, additionalParameter);
        return validateAndExecuteTrigger(rules, triggerInfo);
    }

    public void triggerEmailSend(Long bookingId, String templateName, Map<String, Object> additionalParameters, RulesInterface rules) throws IllegalArgumentException {
        EmailType emailType = emailTypeCalculator.getEmailTypeFromTemplateName(templateName);
        try {
            BookingDetail bookingDetail = bookingServiceClient.getBookingDetail(Locale.getDefault(), bookingId);
            TriggerInfo triggerInfo = triggerInfoFactory.constructTriggerInfo(bookingDetail, emailType, new BookingProductsBuilder(bookingDetail).build(), EmailRequester.UPDATE, additionalParameters);
            ExecutionResult executionResult = triggerServiceRulesValidator.validate(triggerInfo, rules);
            if (executionResult.equals(ExecutionResult.OK)) {
                triggerExecutor.execute(triggerInfo);
            }
        } catch (IOException e) {
            throw new UncheckedIOException(e);
        }
    }

    private ExecutionResult validateAndExecuteTrigger(RulesInterface rules, TriggerInfo triggerInfo) throws IOException {
        ExecutionResult executionResult = triggerServiceRulesValidator.validate(triggerInfo, rules);
        return (executionResult.equals(ExecutionResult.OK) ? triggerExecutor.execute(triggerInfo) : executionResult);
    }

    private BookingDetail getBookingDetail(String key) throws IOException {
        long bookingId = Long.parseLong(key);
        return bookingServiceClient.getBookingDetail(Locale.getDefault(), bookingId);
    }
}
