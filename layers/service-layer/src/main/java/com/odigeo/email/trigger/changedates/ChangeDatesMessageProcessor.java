package com.odigeo.email.trigger.changedates;

import com.edreams.configuration.ConfigurationEngine;
import com.odigeo.bookingchange.message.ChangeDatesStatusUpdatedMessage;
import com.odigeo.bookingchange.message.UserBookingChangeRequestStatus;
import com.odigeo.email.trigger.events.EventController;
import com.odigeo.email.trigger.factories.RulesFactory;
import com.odigeo.messaging.utils.ConsumerIterator;
import org.apache.log4j.Logger;

import java.io.IOException;
import java.util.EnumSet;

public class ChangeDatesMessageProcessor implements Runnable {

    private static final Logger LOGGER = Logger.getLogger(ChangeDatesMessageProcessor.class);
    private final ConsumerIterator<ChangeDatesStatusUpdatedMessage> consumerIterator;
    private static final EnumSet<UserBookingChangeRequestStatus> EMAILABLE_STATUSES = EnumSet.of(
            UserBookingChangeRequestStatus.REQUESTED,
            UserBookingChangeRequestStatus.PROCESSING
    );
    private final RulesFactory rulesFactory = ConfigurationEngine.getInstance(RulesFactory.class);
    private final EventController eventController = ConfigurationEngine.getInstance(ChangeDatesController.class);

    public ChangeDatesMessageProcessor(ConsumerIterator<ChangeDatesStatusUpdatedMessage> consumerIterator) {
        this.consumerIterator = consumerIterator;
    }

    @Override
    @SuppressWarnings("PMD.AvoidCatchingGenericException")
    public void run() {
        try {
            while (consumerIterator.hasNext()) {
                try {
                    final ChangeDatesStatusUpdatedMessage statusUpdatedMessage = consumerIterator.next();
                    processMessage(statusUpdatedMessage);
                } catch (Exception e) {
                    LOGGER.error("Error parsing message...", e);
                }
            }
        } catch (Exception e) {
            LOGGER.error("Error reading message/s " + getClass().getName() + " stopped.", e);
        }
    }

    private void processMessage(ChangeDatesStatusUpdatedMessage statusUpdatedMessage) throws IOException {
        if (isChangeDatesRequest(statusUpdatedMessage)) {
            eventController.onMessage(statusUpdatedMessage, rulesFactory.getCancellationEmailRules());
            LOGGER.info("Processed change dates email with status " + statusUpdatedMessage.getRequestStatus() + " for booking: " + statusUpdatedMessage.getBookingId());
        } else {
            LOGGER.debug("Message discarded for booking " + statusUpdatedMessage.getBookingId() + " with status " + statusUpdatedMessage.getRequestStatus());
        }
    }

    private boolean isChangeDatesRequest(final ChangeDatesStatusUpdatedMessage statusUpdatedMessage) {
        return EMAILABLE_STATUSES.contains(statusUpdatedMessage.getRequestStatus());
    }
}
