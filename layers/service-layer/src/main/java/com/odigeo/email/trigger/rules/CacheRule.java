package com.odigeo.email.trigger.rules;

import com.google.inject.Inject;
import com.odigeo.email.trigger.bookingapi.EmailSentCache;
import com.odigeo.email.trigger.trigger.TriggerInfo;
import com.odigeo.email.trigger.trigger.Validation;

public class CacheRule implements TriggerRule {

    private final EmailSentCache emailSentCache;

    @Inject
    public CacheRule(EmailSentCache emailSentCache) {
        this.emailSentCache = emailSentCache;
    }

    @Override
    public void validate(TriggerInfo triggerInfo) {
        if (emailSentCache.isInCache(triggerInfo.getBookingDetail().getBookingBasicInfo().getId(), triggerInfo.getEmailType())) {
            throw new TriggerRuleException(Validation.BOOKING_IN_CACHE);
        }
    }

}

