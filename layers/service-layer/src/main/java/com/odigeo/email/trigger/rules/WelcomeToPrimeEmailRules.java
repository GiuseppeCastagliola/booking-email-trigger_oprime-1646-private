package com.odigeo.email.trigger.rules;

import com.edreams.configuration.ConfigurationEngine;
import lombok.EqualsAndHashCode;

import java.util.Arrays;
import java.util.List;

@EqualsAndHashCode
public class WelcomeToPrimeEmailRules implements RulesInterface {

    @Override
    public List<TriggerRule> get() {
        return Arrays.asList(
                new EmailTypeRule(),
                ConfigurationEngine.getInstance(CacheRule.class),
                ConfigurationEngine.getInstance(WelcomeToPrimeNeverTriggeredRule.class));
    }
}
