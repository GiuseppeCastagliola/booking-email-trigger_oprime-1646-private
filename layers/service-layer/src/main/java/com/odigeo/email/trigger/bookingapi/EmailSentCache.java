package com.odigeo.email.trigger.bookingapi;

import com.edreams.persistance.cache.Cache;
import com.google.inject.Inject;
import com.google.inject.Singleton;
import com.odigeo.email.render.v1.contract.EmailType;
import com.odigeo.email.trigger.trigger.TriggerInfo;

import java.util.Optional;

@Singleton
public class EmailSentCache {

    private final Cache bookingEmailSentCache;

    @Inject
    public EmailSentCache(Cache simpleMemoryOnlyCache) {
        bookingEmailSentCache = simpleMemoryOnlyCache;
    }

    public boolean isInCache(Long bookingId, EmailType emailType) {
        Optional cacheValue = Optional.ofNullable(bookingEmailSentCache.fetchValue(bookingId));
        return cacheValue.isPresent() && cacheValue.get().equals(emailType);
    }

    public void addInCache(TriggerInfo bookingTriggerInfo) {
        bookingEmailSentCache.addEntry(bookingTriggerInfo.getBookingDetail().getBookingBasicInfo().getId(), bookingTriggerInfo.getEmailType());
    }

}
