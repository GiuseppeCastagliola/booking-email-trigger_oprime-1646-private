package com.odigeo.email.trigger.cancellation;

import com.google.common.collect.ImmutableMap;
import com.google.inject.Inject;
import com.google.inject.Singleton;
import com.odigeo.bookingchange.message.UserBookingChangeRequestStatus;
import com.odigeo.email.render.v1.contract.EmailType;
import com.odigeo.email.trigger.bookingapi.BookingServiceClient;
import com.odigeo.email.trigger.events.EventController;
import com.odigeo.email.trigger.factories.TriggerInfoFactory;
import com.odigeo.email.trigger.trigger.TriggerExecutor;
import com.odigeo.email.trigger.trigger.TriggerServiceRulesValidator;

import java.util.Map;
import java.util.Optional;

@Singleton
public class VoluntaryCancellationRequestedController extends EventController {


    private final Map<UserBookingChangeRequestStatus, EmailType> emailTypeMapper = new ImmutableMap.Builder<UserBookingChangeRequestStatus, EmailType>()
            .put(UserBookingChangeRequestStatus.REQUESTED, EmailType.CANCELLATION_REQUESTED)
            .put(UserBookingChangeRequestStatus.PROCESSING, EmailType.CANCELLATION_PROCESSING)
            .build();

    @Inject
    VoluntaryCancellationRequestedController(BookingServiceClient bookingServiceClient, TriggerExecutor triggerExecutor, TriggerInfoFactory triggerInfoFactory, TriggerServiceRulesValidator triggerServiceRulesValidator) {
        super(bookingServiceClient, triggerExecutor, triggerInfoFactory, triggerServiceRulesValidator);
    }

    @Override
    protected EmailType getEmailTypeFromUserBookingChangeRequestStatus(final UserBookingChangeRequestStatus userBookingChangeRequestStatus) {
        return Optional.ofNullable(emailTypeMapper.get(userBookingChangeRequestStatus))
                .orElseThrow(() -> new IllegalArgumentException("There is no EmailTypeMapping for the following UserBookingChangeRequestStatus" + userBookingChangeRequestStatus)
                );
    }
}



