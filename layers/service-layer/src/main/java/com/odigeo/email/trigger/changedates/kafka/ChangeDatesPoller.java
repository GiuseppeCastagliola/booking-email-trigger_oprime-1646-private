package com.odigeo.email.trigger.changedates.kafka;

import com.google.inject.Inject;
import com.odigeo.bookingchange.message.ChangeDatesStatusUpdatedMessage;
import com.odigeo.email.trigger.changedates.ChangeDatesMessageProcessor;
import com.odigeo.messaging.utils.Consumer;
import com.odigeo.messaging.utils.kafka.KafkaJsonConsumer;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class ChangeDatesPoller {

    private final ExecutorService executorService;
    private final Consumer<ChangeDatesStatusUpdatedMessage> consumer;
    private final KafkaChangeDatesConfiguration configuration;

    @Inject
    public ChangeDatesPoller(final KafkaChangeDatesConfiguration kafkaChangeDatesConfiguration) {
        this.configuration = kafkaChangeDatesConfiguration;
        this.executorService = Executors.newFixedThreadPool(configuration.getNumThreads());
        this.consumer = new KafkaJsonConsumer<>(ChangeDatesStatusUpdatedMessage.class, configuration.getZookeeperUrl(), configuration.getGroupId(), configuration.getTopic());
    }

    public void init() {
        consumer.connectAndIterate(configuration.getNumThreads()).forEach(consumerIterator -> {
            executorService.submit(new ChangeDatesMessageProcessor(consumerIterator));
        });
    }

    public void destroy() {
        if (consumer != null) {
            consumer.close();
        }
    }
}

