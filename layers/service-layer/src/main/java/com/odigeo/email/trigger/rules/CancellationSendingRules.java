package com.odigeo.email.trigger.rules;

import lombok.EqualsAndHashCode;

import java.util.Collections;
import java.util.List;

@EqualsAndHashCode
public class CancellationSendingRules implements RulesInterface {

    private static final List<TriggerRule> RULES = Collections.singletonList(new IgnoredEmailTypesRule());

    @Override
    public List<TriggerRule> get() {
        return RULES;
    }
}
