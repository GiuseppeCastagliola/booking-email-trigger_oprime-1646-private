package com.odigeo.email.trigger.rules;

import com.odigeo.email.trigger.trigger.TriggerInfo;
import com.odigeo.email.trigger.trigger.Validation;
import lombok.EqualsAndHashCode;

@EqualsAndHashCode
public class WhiteLabelRule implements TriggerRule {

    @Override
    public void validate(TriggerInfo triggerInfo) {
        if (triggerInfo.getBookingDetail().isIsWhiteLabel()) {
            throw new TriggerRuleException(Validation.WHITE_LABEL);
        }
    }
}

