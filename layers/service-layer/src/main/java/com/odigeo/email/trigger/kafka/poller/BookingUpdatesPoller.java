package com.odigeo.email.trigger.kafka.poller;

import com.google.inject.Inject;
import com.google.inject.Singleton;
import com.odigeo.email.trigger.configuration.Configuration;
import com.odigeo.email.trigger.kafka.processor.BookingUpdatesProcessor;

@Singleton
public final class BookingUpdatesPoller extends KafkaPoller<BookingUpdatesProcessor> {

    @Inject
    public BookingUpdatesPoller(Configuration configuration) throws NoSuchMethodException {
        super(configuration.getKafkaConfiguration().getNumThreadsUpdates(),
                configuration.getKafkaConfiguration().getGroupId(),
                configuration.getKafkaConfiguration().getZookeeperUrl(),
                configuration.getKafkaConfiguration().getBookingUpdatesTopic(),
                BookingUpdatesProcessor.class);
    }
}
