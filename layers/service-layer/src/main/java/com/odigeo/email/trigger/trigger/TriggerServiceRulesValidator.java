package com.odigeo.email.trigger.trigger;

import com.odigeo.email.trigger.rules.RulesInterface;
import com.odigeo.email.trigger.rules.TriggerRule;
import com.odigeo.email.trigger.rules.TriggerRuleException;
import com.odigeo.email.trigger.util.Constants;
import org.apache.log4j.Logger;

public class TriggerServiceRulesValidator {

    private static final Logger LOGGER = Logger.getLogger(TriggerServiceRulesValidator.class);

    public ExecutionResult validate(TriggerInfo triggerInfo, RulesInterface rules) {
        ExecutionResult executionResult = ExecutionResult.OK;
        for (TriggerRule triggerRule : rules.get()) {
            try {
                triggerRule.validate(triggerInfo);
            } catch (TriggerRuleException e) {
                LOGGER.warn("Validation result " + Constants.LOG_SEPARATOR
                        + e.getValidation() + Constants.LOG_SEPARATOR
                        + triggerInfo.getBookingDetail().getBookingBasicInfo().getId() + Constants.LOG_SEPARATOR
                        + triggerInfo.getEmailType());
                executionResult = e.getValidation().getExecutionResult();
                break;
            }
        }
        return executionResult;
    }


}
