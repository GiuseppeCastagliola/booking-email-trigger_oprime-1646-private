package com.odigeo.email.trigger.rules;

import java.util.List;

public interface RulesInterface {

    List<TriggerRule> get();
}
