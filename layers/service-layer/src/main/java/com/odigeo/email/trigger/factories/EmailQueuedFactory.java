package com.odigeo.email.trigger.factories;

import com.odigeo.email.render.v1.contract.EmailType;
import com.odigeo.retail.bookingemailtrigger.email.EmailQueued;

public class EmailQueuedFactory {

    private static final String TRANSACTION_TYPE_BOOKING = "BOOKING";

    public EmailQueued createEmailQueued(final Long bookingId, final EmailType emailType, final Long mailerId) {

        return new EmailQueued(TRANSACTION_TYPE_BOOKING, bookingId, emailType.name(), mailerId);
    }
}
