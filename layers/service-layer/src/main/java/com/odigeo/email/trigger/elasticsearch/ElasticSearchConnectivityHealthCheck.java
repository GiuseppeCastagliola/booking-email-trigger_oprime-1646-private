package com.odigeo.email.trigger.elasticsearch;

import com.codahale.metrics.health.HealthCheck;
import com.odigeo.email.trigger.configuration.ElasticsearchConfiguration;

import javax.net.SocketFactory;
import java.io.IOException;
import java.net.Socket;

public class ElasticSearchConnectivityHealthCheck extends HealthCheck {
    private final ElasticsearchConfiguration elasticsearchConfiguration;

    public ElasticSearchConnectivityHealthCheck(ElasticsearchConfiguration elasticsearchConfiguration) {
        this.elasticsearchConfiguration = elasticsearchConfiguration;
    }

    @Override
    protected Result check() {
        if (isHealthy(elasticsearchConfiguration.getHostName(), elasticsearchConfiguration.getPort())) {
            return Result.healthy("Elasticsearch is up on  " + elasticsearchConfiguration.getHostName() + ":" + elasticsearchConfiguration.getPort());
        } else {
            return Result.unhealthy("Elasticsearch is down on  " + elasticsearchConfiguration.getHostName() + ":" + elasticsearchConfiguration.getPort());
        }
    }

    private boolean isHealthy(String hostName, int port) {
        try {
            Socket pingSocket = SocketFactory.getDefault().createSocket(hostName, port);
            pingSocket.close();
        } catch (IOException e) {
            return false;
        }
        return true;
    }
}
