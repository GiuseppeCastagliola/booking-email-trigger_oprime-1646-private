package com.odigeo.email.trigger.trigger;

public enum Validation {

    OK(ExecutionResult.OK),
    OK_PRODUCT_CHANGES(ExecutionResult.OK_PRODUCT_CHANGES),
    BOOKING_IN_CACHE(ExecutionResult.BOOKING_IN_CACHE),
    DEPARTURE_PAST(ExecutionResult.DEPARTURE_PAST),
    INVALID_STATUS(ExecutionResult.INVALID_STATUS),
    MISSING_BUYER(ExecutionResult.MISSING_BUYER),
    ALREADY_TRIGGERED(ExecutionResult.ALREADY_TRIGGERED),
    MEMBERSHIP_RENEWAL(ExecutionResult.MEMBERSHIP_RENEWAL),
    WHITE_LABEL(ExecutionResult.WHITE_LABEL),
    MISSING_EMAIL_TYPE(ExecutionResult.MISSING_EMAIL_TYPE),
    MUST_BE_IGNORED(ExecutionResult.MUST_BE_IGNORED),
    MISSING_ADDITIONAL_PARAMETER(ExecutionResult.MISSING_ADDITIONAL_PARAMETER);

    private ExecutionResult executionResult;

    Validation(ExecutionResult executionResult) {
        this.executionResult = executionResult;
    }

    public ExecutionResult getExecutionResult() {
        return executionResult;
    }

    public static boolean isOkayValidation(Validation validation) {
        return validation.equals(OK) || validation.equals(OK_PRODUCT_CHANGES);
    }
}

