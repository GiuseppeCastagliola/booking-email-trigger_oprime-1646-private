package com.odigeo.email.trigger.rules;

import com.odigeo.bookingapi.mock.v12.response.BookingBasicInfoBuilder;
import com.odigeo.bookingapi.mock.v12.response.BookingDetailBuilder;
import com.odigeo.bookingapi.mock.v12.response.BuilderException;
import com.odigeo.email.trigger.tracking.CustomerEmailManager;
import com.odigeo.email.trigger.trigger.TriggerInfo;
import com.odigeo.email.trigger.trigger.testbuilder.TriggerInfoTestDataBuilder;
import org.elasticsearch.action.search.SearchResponse;
import org.elasticsearch.search.SearchHit;
import org.elasticsearch.search.SearchHits;
import org.mockito.Mock;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import java.util.Optional;
import java.util.Random;

import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static org.mockito.MockitoAnnotations.initMocks;

public class WelcomeToPrimeNeverTriggeredRuleTest {
    private static final long BOOKING_ID = 9876543L;
    private static final SearchHits ZERO_HITS_SEARCH = new SearchHits(new SearchHit[0], 0, 0);
    private static final SearchHits ONE_HIT_SEARCH = new SearchHits(new SearchHit[1], 1, 0);
    @Mock
    private CustomerEmailManager customerEmailManager;
    @Mock
    private SearchResponse searchResponse;
    private WelcomeToPrimeNeverTriggeredRule welcomeToPrimeNeverTriggeredRule;
    private TriggerInfo triggerInfo;

    @BeforeMethod
    public void setUp() throws BuilderException {
        initMocks(this);
        triggerInfo = TriggerInfoTestDataBuilder
                .aTriggerInfo()
                .withBookingDetail(new BookingDetailBuilder()
                        .bookingBasicInfoBuilder(new BookingBasicInfoBuilder().id(BOOKING_ID))
                        .build(new Random()))
                .build();
        welcomeToPrimeNeverTriggeredRule = new WelcomeToPrimeNeverTriggeredRule(customerEmailManager);
    }

    @Test
    public void testIsWelcomeToPrimeNeverTriggered() {
        when(searchResponse.getHits()).thenReturn(ZERO_HITS_SEARCH);
        when(customerEmailManager.isWelcomeToPrimeNeverTriggered(eq(BOOKING_ID))).thenReturn(Optional.of(searchResponse));
        welcomeToPrimeNeverTriggeredRule.validate(triggerInfo);
        verify(customerEmailManager).isWelcomeToPrimeNeverTriggered(eq(BOOKING_ID));
    }

    @Test
    public void testIsWelcomeToPrimeNeverTriggeredElasticFails() {
        when(customerEmailManager.isWelcomeToPrimeNeverTriggered(eq(BOOKING_ID))).thenReturn(Optional.empty());
        welcomeToPrimeNeverTriggeredRule.validate(triggerInfo);
        verify(customerEmailManager).isWelcomeToPrimeNeverTriggered(eq(BOOKING_ID));
    }

    @Test(expectedExceptions = TriggerRuleException.class)
    public void testIsWelcomeToPrimeAlreadyTriggered() {
        when(searchResponse.getHits()).thenReturn(ONE_HIT_SEARCH);
        when(customerEmailManager.isWelcomeToPrimeNeverTriggered(eq(BOOKING_ID))).thenReturn(Optional.of(searchResponse));
        welcomeToPrimeNeverTriggeredRule.validate(triggerInfo);
    }
}
