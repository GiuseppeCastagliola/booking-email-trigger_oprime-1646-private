package com.odigeo.email.trigger.rules;

import com.odigeo.email.render.v1.contract.EmailType;
import com.odigeo.email.trigger.trigger.TriggerInfo;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

public class EmailTypeRuleTest {

    private EmailTypeRule emailTypeRule;

    @BeforeMethod
    public void init() {
        emailTypeRule = new EmailTypeRule();
    }

    @Test(expectedExceptions = TriggerRuleException.class)
    public void hasNotEmailTypeShouldReturnException() {
        TriggerInfo triggerInfoWithEmailTypeNull = new TriggerInfo.Builder().build();
        emailTypeRule.validate(triggerInfoWithEmailTypeNull);
    }

    @Test
    public void hasEmailTypeShouldNotReturnException() {
        EmailType anyEmailType = EmailType.CONFIRMATION;
        TriggerInfo triggerInfo = new TriggerInfo.Builder().withEmailType(anyEmailType).build();
        emailTypeRule.validate(triggerInfo);
    }
}
