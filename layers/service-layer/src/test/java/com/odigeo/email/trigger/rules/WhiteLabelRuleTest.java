package com.odigeo.email.trigger.rules;

import com.edreams.configuration.ConfigurationEngine;
import com.odigeo.bookingapi.v12.responses.BookingDetail;
import com.odigeo.email.render.v1.contract.EmailType;
import com.odigeo.email.trigger.trigger.EmailRequester;
import com.odigeo.email.trigger.trigger.TriggerInfo;
import org.mockito.MockitoAnnotations;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import java.util.ArrayList;
import java.util.Collections;

public class WhiteLabelRuleTest {

    private WhiteLabelRule whiteLabelRule;
    private BookingDetail bookingDetail;
    private TriggerInfo triggerInfo;

    @BeforeMethod
    public void init() {
        MockitoAnnotations.initMocks(this);
        ConfigurationEngine.init();
        whiteLabelRule = new WhiteLabelRule();
        bookingDetail = new BookingDetail();
        bookingDetail.setBookingProducts(new ArrayList<>());
    }

    @Test(expectedExceptions = TriggerRuleException.class)
    public void shouldThrowAnException() {
        bookingDetail.setIsWhiteLabel(true);
        triggerInfo = new TriggerInfo(bookingDetail, EmailType.CONFIRMATION, null, EmailRequester.UPDATE, Collections.emptyMap());
        whiteLabelRule.validate(triggerInfo);
    }

    @Test
    public void shouldNotThrowAnyException() {
        bookingDetail.setIsWhiteLabel(false);
        triggerInfo = new TriggerInfo(bookingDetail, EmailType.CONFIRMATION, null, EmailRequester.UPDATE, Collections.emptyMap());
        whiteLabelRule.validate(triggerInfo);
    }
}

