package com.odigeo.email.trigger.factories;

import com.odigeo.email.render.v1.contract.EmailType;
import com.odigeo.retail.bookingemailtrigger.email.EmailQueued;
import org.testng.annotations.Test;

import static org.testng.Assert.assertEquals;

public class EmailQueuedFactoryTest {

    private EmailQueuedFactory emailQueuedFactory = new EmailQueuedFactory();
    private static final long A_TRANSACTION_ID = 1L;
    private static final String TRANSACTION_TYPE_BOOKING = "BOOKING";
    private static final long AN_EMAIL_ID = 2L;
    private static final EmailType AN_EMAIL_TYPE = EmailType.CANCELLATION_PENDING_REFUND;

    @Test
    public void shouldCreateEmailQueued() {
        final EmailQueued emailQueued = emailQueuedFactory.createEmailQueued(A_TRANSACTION_ID, AN_EMAIL_TYPE, AN_EMAIL_ID);
        assertEquals(A_TRANSACTION_ID, emailQueued.getTransactionId());
        assertEquals(TRANSACTION_TYPE_BOOKING, emailQueued.getTransactionType());
        assertEquals(AN_EMAIL_ID, emailQueued.getEmailId());
        assertEquals(AN_EMAIL_TYPE, EmailType.valueOf(emailQueued.getEmailType()));
    }
}
