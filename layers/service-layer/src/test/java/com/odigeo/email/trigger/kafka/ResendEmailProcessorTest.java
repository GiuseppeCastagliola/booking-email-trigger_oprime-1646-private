package com.odigeo.email.trigger.kafka;

import com.edreams.configuration.ConfigurationEngine;
import com.google.inject.AbstractModule;
import com.odigeo.email.trigger.factories.RulesFactory;
import com.odigeo.email.trigger.kafka.processor.ResendEmailProcessor;
import com.odigeo.email.trigger.kafka.pusher.RecoveryPusher;
import com.odigeo.email.trigger.rules.ResendEmailRules;
import com.odigeo.email.trigger.rules.RulesInterface;
import com.odigeo.email.trigger.trigger.EmailRequester;
import com.odigeo.email.trigger.trigger.ExecutionResult;
import com.odigeo.email.trigger.trigger.TriggerService;
import com.odigeo.messaging.utils.BasicMessage;
import com.odigeo.messaging.utils.ConsumerIterator;
import com.odigeo.messaging.utils.MessageDataAccessException;
import com.odigeo.messaging.utils.MessageParserException;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import java.io.IOException;

import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

public class ResendEmailProcessorTest {

    private static final RulesInterface SOME_RULES = new ResendEmailRules();
    private static final EmailRequester REQUESTER_RESEND = EmailRequester.RESEND;
    private static final String A_KEY = "111";
    private static final int ATTEMPTS = 1;

    @Mock
    private ConsumerIterator<BasicMessage> consumerIterator;
    @Mock
    private TriggerService triggerService;
    @Mock
    private RecoveryPusher recoveryPusher;
    @Mock
    private RulesFactory rulesFactory;

    private final BasicMessage basicMessage = new BasicMessage();
    private ResendEmailProcessor resendEmailProcessor;

    @BeforeMethod
    public void setUp() throws MessageDataAccessException, MessageParserException {
        MockitoAnnotations.initMocks(this);
        ConfigurationEngine.init(new AbstractModule() {
            @Override
            protected void configure() {
                bind(TriggerService.class).toInstance(triggerService);
                bind(RecoveryPusher.class).toInstance(recoveryPusher);
                bind(RulesFactory.class).toInstance(rulesFactory);
            }
        });
        resendEmailProcessor = new ResendEmailProcessor(consumerIterator);
        when(consumerIterator.hasNext()).thenReturn(true).thenReturn(false);
        basicMessage.setKey(A_KEY);
        when(consumerIterator.next()).thenReturn(basicMessage);
        when(rulesFactory.getResendEmailRules()).thenReturn(SOME_RULES);
    }

    @Test
    public void shouldCallExecuteWhenOK() throws IOException {
        when(triggerService.execute(A_KEY, SOME_RULES, REQUESTER_RESEND)).thenReturn(ExecutionResult.OK);
        resendEmailProcessor.run();
        verifyTriggerService();
    }

    @Test
    public void shouldCallExecuteWhenKoUnrecoverable() throws IOException {
        when(triggerService.execute(A_KEY, SOME_RULES, REQUESTER_RESEND)).thenReturn(ExecutionResult.KO_UNRECOVERABLE);
        resendEmailProcessor.run();
        verifyTriggerService();
    }

    @Test
    public void shouldCallExecuteAndPublishMessageWhenKoRecoverable() throws IOException {
        when(triggerService.execute(A_KEY, SOME_RULES, REQUESTER_RESEND)).thenReturn(ExecutionResult.KO_RECOVERABLE);
        resendEmailProcessor.run();
        verifyTriggerService();
        verify(recoveryPusher).publishMessage(basicMessage, ATTEMPTS);
    }

    @Test
    public void shouldCallExecuteWhenAlreadyTriggered() throws IOException {
        when(triggerService.execute(A_KEY, SOME_RULES, REQUESTER_RESEND)).thenReturn(ExecutionResult.ALREADY_TRIGGERED);
        resendEmailProcessor.run();
        verifyTriggerService();
    }

    @Test
    public void shouldCallExecuteWhenBookingInCache() throws IOException {
        when(triggerService.execute(A_KEY, SOME_RULES, REQUESTER_RESEND)).thenReturn(ExecutionResult.BOOKING_IN_CACHE);
        resendEmailProcessor.run();
        verifyTriggerService();
    }

    @Test
    public void shouldCallExecuteWhenDeparturePast() throws IOException {
        when(triggerService.execute(A_KEY, SOME_RULES, REQUESTER_RESEND)).thenReturn(ExecutionResult.DEPARTURE_PAST);
        resendEmailProcessor.run();
        verifyTriggerService();
    }

    @Test
    public void shouldCallExecuteWhenInvalidStatus() throws IOException {
        when(triggerService.execute(A_KEY, SOME_RULES, REQUESTER_RESEND)).thenReturn(ExecutionResult.INVALID_STATUS);
        resendEmailProcessor.run();
        verifyTriggerService();
    }

    @Test
    public void shouldCallExecuteWhenMissingBuyer() throws IOException {
        when(triggerService.execute(A_KEY, SOME_RULES, REQUESTER_RESEND)).thenReturn(ExecutionResult.MISSING_BUYER);
        resendEmailProcessor.run();
        verifyTriggerService();
    }

    private void verifyTriggerService() throws IOException {
        verify(triggerService).execute(A_KEY, SOME_RULES, REQUESTER_RESEND);
    }
}

