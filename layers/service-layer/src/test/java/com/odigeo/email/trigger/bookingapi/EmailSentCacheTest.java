package com.odigeo.email.trigger.bookingapi;

import com.edreams.configuration.ConfigurationEngine;
import com.edreams.persistance.cache.impl.SimpleMemoryOnlyCache;
import com.odigeo.bookingapi.mock.v12.response.BookingDetailBuilder;
import com.odigeo.bookingapi.mock.v12.response.BuilderException;
import com.odigeo.bookingapi.v12.responses.BookingDetail;
import com.odigeo.email.render.v1.contract.EmailType;
import com.odigeo.email.trigger.trigger.EmailRequester;
import com.odigeo.email.trigger.trigger.TriggerInfo;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import java.util.Collections;
import java.util.Random;

import static junit.framework.Assert.assertFalse;
import static junit.framework.Assert.assertTrue;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

public class EmailSentCacheTest {

    @Mock
    private SimpleMemoryOnlyCache simpleMemoryOnlyCache;
    private static final EmailType AN_EMAIL_TYPE = EmailType.CONFIRMATION;
    private EmailSentCache emailSentCache;
    private BookingDetail bookingDetailWithBuyer;

    @BeforeMethod
    public void init() throws BuilderException {
        ConfigurationEngine.init();
        MockitoAnnotations.initMocks(this);
        emailSentCache = new EmailSentCache(simpleMemoryOnlyCache);
        bookingDetailWithBuyer = new BookingDetailBuilder().build(new Random());
    }

    @Test
    public void shouldReturnTrueBecauseIsInCache() {
        when(simpleMemoryOnlyCache.fetchValue(bookingDetailWithBuyer.getBookingBasicInfo().getId())).thenReturn(AN_EMAIL_TYPE);
        assertTrue(emailSentCache.isInCache(bookingDetailWithBuyer.getBookingBasicInfo().getId(), AN_EMAIL_TYPE));
    }

    @Test
    public void shouldReturnFalseBecauseIsNotInCache() {
        when(simpleMemoryOnlyCache.fetchValue(bookingDetailWithBuyer.getBookingBasicInfo().getId())).thenReturn(null);
        assertFalse(emailSentCache.isInCache(bookingDetailWithBuyer.getBookingBasicInfo().getId(), AN_EMAIL_TYPE));
    }

    @Test
    public void shouldAddInCache() {
        emailSentCache.addInCache(new TriggerInfo(bookingDetailWithBuyer, AN_EMAIL_TYPE, null, EmailRequester.UPDATE, Collections.emptyMap()));
        verify(simpleMemoryOnlyCache).addEntry(bookingDetailWithBuyer.getBookingBasicInfo().getId(), AN_EMAIL_TYPE);
    }

}

