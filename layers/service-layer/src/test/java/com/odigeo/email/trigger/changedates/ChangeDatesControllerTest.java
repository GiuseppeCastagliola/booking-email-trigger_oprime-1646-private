package com.odigeo.email.trigger.changedates;

import com.edreams.configuration.ConfigurationEngine;
import com.odigeo.bookingapi.mock.v12.response.BookingDetailBuilder;
import com.odigeo.bookingapi.mock.v12.response.BuilderException;
import com.odigeo.bookingapi.v12.responses.BookingDetail;
import com.odigeo.bookingchange.message.ChangeDatesStatusUpdatedMessage;
import com.odigeo.bookingchange.message.UserBookingChangeRequestStatus;
import com.odigeo.email.render.v1.contract.EmailType;
import com.odigeo.email.trigger.booking.product.BookingProducts;
import com.odigeo.email.trigger.bookingapi.BookingServiceClient;
import com.odigeo.email.trigger.factories.TriggerInfoFactory;
import com.odigeo.email.trigger.rules.CancellationSendingRules;
import com.odigeo.email.trigger.rules.RulesInterface;
import com.odigeo.email.trigger.trigger.EmailRequester;
import com.odigeo.email.trigger.trigger.ExecutionResult;
import com.odigeo.email.trigger.trigger.TriggerExecutor;
import com.odigeo.email.trigger.trigger.TriggerInfo;
import com.odigeo.email.trigger.trigger.TriggerServiceRulesValidator;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import java.io.IOException;
import java.util.Locale;
import java.util.Random;

import static org.mockito.Matchers.any;
import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

public class ChangeDatesControllerTest {

    private static final long A_BOOKING_ID = 1;
    private static final TriggerInfo A_TRIGGER_INFO = new TriggerInfo.Builder().build();
    private static final UserBookingChangeRequestStatus A_NON_PROCESSABLE_STATUS = UserBookingChangeRequestStatus.PRIORITISING;
    private static final RulesInterface SOME_RULES = new CancellationSendingRules();
    private static final ExecutionResult A_FAILURE_STATUS = ExecutionResult.MUST_BE_IGNORED;

    @Mock
    private BookingServiceClient bookingServiceClient;
    @Mock
    private TriggerExecutor triggerExecutor;
    @Mock
    private TriggerInfoFactory triggerInfoFactory;
    @Mock
    private TriggerServiceRulesValidator triggerServiceRulesValidator;

    private ChangeDatesController changeDatesController;
    private BookingDetail bookingDetail;
    private ChangeDatesStatusUpdatedMessage changeDatesStatusUpdatedMessage = new ChangeDatesStatusUpdatedMessage();

    @BeforeMethod
    public void setUp() throws BuilderException, IOException {
        MockitoAnnotations.initMocks(this);
        ConfigurationEngine.init();
        changeDatesController = new ChangeDatesController(bookingServiceClient, triggerExecutor, triggerInfoFactory, triggerServiceRulesValidator);
        bookingDetail = new BookingDetailBuilder().build(new Random());
        changeDatesStatusUpdatedMessage.setBookingId(A_BOOKING_ID);
        when(bookingServiceClient.getBookingDetail(Locale.getDefault(), A_BOOKING_ID)).thenReturn(bookingDetail);
        when(triggerServiceRulesValidator.validate(A_TRIGGER_INFO, SOME_RULES)).thenReturn(ExecutionResult.OK);
    }

    @Test
    public void shouldCallExecuteOnRequestedStatus() throws IOException {
        changeDatesStatusUpdatedMessage.setRequestStatus(UserBookingChangeRequestStatus.REQUESTED);
        when(triggerInfoFactory.constructTriggerInfo(eq(bookingDetail), eq(EmailType.CHANGE_REQUESTED), any(BookingProducts.class), eq(EmailRequester.UPDATE))).thenReturn(A_TRIGGER_INFO);

        changeDatesController.onMessage(changeDatesStatusUpdatedMessage, SOME_RULES);

        verify(triggerExecutor).execute(A_TRIGGER_INFO);
    }

    @Test
    public void shouldCallExecuteOnProcessingStatus() throws IOException {
        changeDatesStatusUpdatedMessage.setRequestStatus(UserBookingChangeRequestStatus.PROCESSING);
        when(triggerInfoFactory.constructTriggerInfo(eq(bookingDetail), eq(EmailType.CHANGE_PROCESSING), any(BookingProducts.class), eq(EmailRequester.UPDATE))).thenReturn(A_TRIGGER_INFO);

        changeDatesController.onMessage(changeDatesStatusUpdatedMessage, SOME_RULES);

        verify(triggerExecutor).execute(A_TRIGGER_INFO);
    }

    @Test
    public void shouldCallExecuteOnReviewStatus() throws IOException {
        changeDatesStatusUpdatedMessage.setRequestStatus(UserBookingChangeRequestStatus.AGENT_REVIEW);
        when(triggerInfoFactory.constructTriggerInfo(eq(bookingDetail), eq(EmailType.CHANGE_UNDER_REVIEW), any(BookingProducts.class), eq(EmailRequester.UPDATE))).thenReturn(A_TRIGGER_INFO);

        changeDatesController.onMessage(changeDatesStatusUpdatedMessage, SOME_RULES);

        verify(triggerExecutor).execute(A_TRIGGER_INFO);
    }

    @Test(expectedExceptions = IllegalArgumentException.class)
    public void shouldNotCallExecuteOnWrongStatus() throws IOException {
        changeDatesStatusUpdatedMessage.setRequestStatus(A_NON_PROCESSABLE_STATUS);

        when(bookingServiceClient.getBookingDetail(Locale.getDefault(), changeDatesStatusUpdatedMessage.getBookingId())).thenReturn(bookingDetail);

        changeDatesController.onMessage(changeDatesStatusUpdatedMessage, SOME_RULES);
        verify(triggerExecutor, never()).execute(A_TRIGGER_INFO);
    }

    @Test(expectedExceptions = IOException.class)
    public void shouldNotCallExecuteOnError() throws IOException {
        changeDatesStatusUpdatedMessage.setRequestStatus(UserBookingChangeRequestStatus.AGENT_REVIEW);

        when(bookingServiceClient.getBookingDetail(Locale.getDefault(), changeDatesStatusUpdatedMessage.getBookingId())).thenThrow(new IOException());

        changeDatesController.onMessage(changeDatesStatusUpdatedMessage, SOME_RULES);
        verify(triggerExecutor, never()).execute(A_TRIGGER_INFO);
    }

    @Test
    public void shouldNotCallExecuteWhenValidationKO() throws IOException {
        changeDatesStatusUpdatedMessage.setRequestStatus(UserBookingChangeRequestStatus.REQUESTED);
        when(triggerInfoFactory.constructTriggerInfo(eq(bookingDetail), eq(EmailType.CHANGE_REQUESTED), any(BookingProducts.class), eq(EmailRequester.UPDATE))).thenReturn(A_TRIGGER_INFO);
        when(triggerServiceRulesValidator.validate(A_TRIGGER_INFO, SOME_RULES)).thenReturn(A_FAILURE_STATUS);

        changeDatesController.onMessage(changeDatesStatusUpdatedMessage, SOME_RULES);

        verify(triggerExecutor, never()).execute(A_TRIGGER_INFO);
    }
}
