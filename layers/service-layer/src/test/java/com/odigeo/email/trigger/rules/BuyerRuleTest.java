package com.odigeo.email.trigger.rules;

import com.edreams.configuration.ConfigurationEngine;

import com.odigeo.bookingapi.mock.v12.response.BookingDetailBuilder;
import com.odigeo.bookingapi.mock.v12.response.BuilderException;
import com.odigeo.bookingapi.v12.responses.BookingDetail;
import com.odigeo.email.render.v1.contract.EmailType;
import com.odigeo.email.trigger.trigger.TriggerInfo;
import org.mockito.MockitoAnnotations;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import java.util.Random;

public class BuyerRuleTest {

    private BuyerRule buyerRule;
    private BookingDetail bookingDetail;
    private TriggerInfo triggerInfo;

    @BeforeMethod
    public void init() throws BuilderException {
        MockitoAnnotations.initMocks(this);
        ConfigurationEngine.init();
        buyerRule = new BuyerRule();
        bookingDetail = new BookingDetailBuilder().build(new Random());
        triggerInfo = new TriggerInfo.Builder().withBookingDetail(bookingDetail)
                .withEmailType(EmailType.CONFIRMATION).build();
    }

    @Test(expectedExceptions = TriggerRuleException.class)
    public void hasNotBuyerShouldReturnException() {
        bookingDetail.setBuyer(null);
        buyerRule.validate(triggerInfo);
    }

    @Test
    public void hasBuyerShouldNotReturnException() {
        buyerRule.validate(triggerInfo);
    }
}
