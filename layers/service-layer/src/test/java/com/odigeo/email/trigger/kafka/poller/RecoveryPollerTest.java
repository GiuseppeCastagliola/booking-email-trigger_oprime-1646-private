package com.odigeo.email.trigger.kafka.poller;

import com.edreams.configuration.ConfigurationEngine;
import org.testng.Assert;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

public class RecoveryPollerTest {
    private RecoveryPoller recoveryPoller;

    @BeforeMethod
    public void setUp() {
        ConfigurationEngine.init();
    }

    @Test
    public void test() {
        recoveryPoller = ConfigurationEngine.getInstance(RecoveryPoller.class);
        Assert.assertNotNull(recoveryPoller);
    }

}
