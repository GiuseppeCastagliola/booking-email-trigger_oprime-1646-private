package com.odigeo.email.trigger.kafka;

import com.edreams.configuration.ConfigurationEngine;
import com.google.inject.AbstractModule;
import com.odigeo.email.trigger.kafka.processor.BookingUpdatesProcessor;
import com.odigeo.email.trigger.kafka.pusher.RecoveryPusher;
import com.odigeo.email.trigger.trigger.EmailRequester;
import com.odigeo.email.trigger.trigger.ExecutionResult;
import com.odigeo.email.trigger.trigger.TriggerService;
import com.odigeo.messaging.utils.BasicMessage;
import com.odigeo.messaging.utils.ConsumerIterator;
import com.odigeo.messaging.utils.MessageDataAccessException;
import com.odigeo.messaging.utils.MessageParserException;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.BeforeSuite;
import org.testng.annotations.Test;

import java.io.IOException;

import static org.mockito.Matchers.any;
import static org.mockito.Matchers.anyInt;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

public class BookingUpdatesProcessorTest {

    @Mock
    private ConsumerIterator<BasicMessage> consumerIterator;

    @Mock
    private TriggerService triggerService;
    @Mock
    private RecoveryPusher recoveryPusher;

    private final BasicMessage basicMessage = new BasicMessage();
    private BookingUpdatesProcessor bookingUpdatesProcessor;
    private int callsRecoveryPusher = 0, callsTriggerService = 0;


    @BeforeSuite
    public void init() {
        MockitoAnnotations.initMocks(this);
        ConfigurationEngine.init(new AbstractModule() {
            @Override
            protected void configure() {
                bind(TriggerService.class).toInstance(triggerService);
                bind(RecoveryPusher.class).toInstance(recoveryPusher);
            }
        });
        ExecutionResult.values();
    }

    @BeforeMethod
    public void setUp() throws MessageDataAccessException, MessageParserException {
        ConfigurationEngine.init(new AbstractModule() {
            @Override
            protected void configure() {
                bind(TriggerService.class).toInstance(triggerService);
                bind(RecoveryPusher.class).toInstance(recoveryPusher);
            }
        });
        bookingUpdatesProcessor = new BookingUpdatesProcessor(consumerIterator);
        when(consumerIterator.hasNext()).thenReturn(true).thenReturn(false);
        basicMessage.setKey("111");
        when(consumerIterator.next()).thenReturn(basicMessage);
    }

    @Test
    public void testRun() throws IOException {
        when(triggerService.execute(basicMessage.getKey(), BookingUpdatesProcessor.getBookingUpdatesRules(), EmailRequester.UPDATE)).thenReturn(ExecutionResult.OK);
        bookingUpdatesProcessor.run();
        verifyMethodCalls();
    }

    @Test
    public void testKoUnrecoverable() throws IOException {
        when(triggerService.execute(basicMessage.getKey(), BookingUpdatesProcessor.getBookingUpdatesRules(), EmailRequester.UPDATE)).thenReturn(ExecutionResult.KO_UNRECOVERABLE);

        bookingUpdatesProcessor.run();
        verifyMethodCalls();
    }

    @Test
    public void testKoRecoverable() throws IOException {
        when(triggerService.execute(basicMessage.getKey(), BookingUpdatesProcessor.getBookingUpdatesRules(), EmailRequester.UPDATE)).thenReturn(ExecutionResult.KO_RECOVERABLE);
        bookingUpdatesProcessor.run();
        ++callsRecoveryPusher;
        verifyMethodCalls();
    }

    @Test
    public void testAlreadyTriggered() throws IOException {
        when(triggerService.execute(basicMessage.getKey(), BookingUpdatesProcessor.getBookingUpdatesRules(), EmailRequester.UPDATE)).thenReturn(ExecutionResult.ALREADY_TRIGGERED);
        bookingUpdatesProcessor.run();
        verifyMethodCalls();
    }

    @Test
    public void testBookingInCache() throws IOException {
        when(triggerService.execute(basicMessage.getKey(), BookingUpdatesProcessor.getBookingUpdatesRules(), EmailRequester.UPDATE)).thenReturn(ExecutionResult.BOOKING_IN_CACHE);
        bookingUpdatesProcessor.run();
        verifyMethodCalls();
    }

    @Test
    public void testDeparturePast() throws IOException {
        when(triggerService.execute(basicMessage.getKey(), BookingUpdatesProcessor.getBookingUpdatesRules(), EmailRequester.UPDATE)).thenReturn(ExecutionResult.DEPARTURE_PAST);
        bookingUpdatesProcessor.run();
        verifyMethodCalls();
    }

    @Test
    public void testInvalidStatus() throws IOException {
        when(triggerService.execute(basicMessage.getKey(), BookingUpdatesProcessor.getBookingUpdatesRules(), EmailRequester.UPDATE)).thenReturn(ExecutionResult.INVALID_STATUS);
        bookingUpdatesProcessor.run();
        verifyMethodCalls();
    }

    @Test
    public void testMissingBuyer() throws IOException {
        when(triggerService.execute(basicMessage.getKey(), BookingUpdatesProcessor.getBookingUpdatesRules(), EmailRequester.UPDATE)).thenReturn(ExecutionResult.MISSING_BUYER);
        bookingUpdatesProcessor.run();
        verifyMethodCalls();
    }

    public void verifyMethodCalls() throws IOException {
        ++callsTriggerService;
        verify(triggerService, times(callsTriggerService)).execute(basicMessage.getKey(), BookingUpdatesProcessor.getBookingUpdatesRules(), EmailRequester.UPDATE);
        verify(recoveryPusher, times(callsRecoveryPusher)).publishMessage(any(BasicMessage.class), anyInt());
    }
}
