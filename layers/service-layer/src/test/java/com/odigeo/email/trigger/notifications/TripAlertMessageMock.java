package com.odigeo.email.trigger.notifications;

import com.odigeo.itinerary.notifications.v1.AlertDetailsMessage;
import com.odigeo.itinerary.notifications.v1.FlightStatusMessage;
import com.odigeo.itinerary.notifications.v1.FlightWithStatusMessage;
import com.odigeo.itinerary.notifications.v1.TripAlertMessage;
import com.odigeo.itinerary.notifications.v1.TripMessage;

import java.util.Collections;
import java.util.List;

class TripAlertMessageMock {

    private static final String AN_ID = "ID";
    private static final String FLIGHT_CANCELLED_TYPE = "Flight Cancellation";
    private static final String LEG_DEPARTURE_INFO_TYPE = "Leg Departure Info";
    private static final String A_PNR = "XSDDSD";
    private static final String A_FLIGHT_NUMBER = "AA212";
    private static final String AIRLINE_CODE = "AA";
    private static final String STATUS_CANCELLED = "Cancelled";
    private static final String STATUS_LANDED = "Landed";


    static TripAlertMessage createTripAlertMessageCancelled() {
        return TripAlertMessageMock.createTripAlertMessage(createTripMessage(TripAlertMessageMock.createFlightStatusMessageCancelled()), FLIGHT_CANCELLED_TYPE);
    }

    static TripAlertMessage createTripAlertMessageLanded() {
        return TripAlertMessageMock.createTripAlertMessage(createTripMessage(TripAlertMessageMock.createFlightStatusMessageLanded()), LEG_DEPARTURE_INFO_TYPE);
    }

    static TripAlertMessage createFlightCancelledWithoutFlightMessageCancelled() {
        return TripAlertMessageMock.createTripAlertMessage(createTripMessage(TripAlertMessageMock.createFlightStatusMessageLanded()), FLIGHT_CANCELLED_TYPE);
    }

    static TripAlertMessage createFlightCancelledWithoutTripMessage() {
        return TripAlertMessageMock.createTripAlertMessage(null, FLIGHT_CANCELLED_TYPE);
    }

    static TripAlertMessage createFlightCancelledWithTripMessageWithoutFlights() {
        return TripAlertMessageMock.createTripAlertMessage(createTripMessageWithFlights(null), FLIGHT_CANCELLED_TYPE);
    }

    private static TripAlertMessage createTripAlertMessage(TripMessage tripMessage, String type) {
        TripAlertMessage tripAlertMessage = new TripAlertMessage();
        tripAlertMessage.setId(AN_ID);
        AlertDetailsMessage alertDetailsMessage = new AlertDetailsMessage();
        alertDetailsMessage.setType(type);
        tripAlertMessage.setAlertDetails(alertDetailsMessage);
        tripAlertMessage.setTrip(tripMessage);
        return tripAlertMessage;
    }

    private static TripMessage createTripMessageWithFlights(List<FlightWithStatusMessage> flightWithStatusMessageList) {
        TripMessage tripMessage = new TripMessage();
        tripMessage.setReferenceNumber(AN_ID);
        tripMessage.setFlights(flightWithStatusMessageList);
        return tripMessage;
    }

    private static TripMessage createTripMessage(FlightStatusMessage flightStatusMessage) {
        TripMessage tripMessage = new TripMessage();
        tripMessage.setReferenceNumber(AN_ID);
        tripMessage.setFlights(createFlightsWithStatusMessage(flightStatusMessage));
        return tripMessage;
    }

    private static List<FlightWithStatusMessage> createFlightsWithStatusMessage(FlightStatusMessage flightStatusMessage) {
        FlightWithStatusMessage flightWithStatusMessage = new FlightWithStatusMessage();
        flightWithStatusMessage.setPnr(A_PNR);
        flightWithStatusMessage.setFlightNumber(A_FLIGHT_NUMBER);
        flightWithStatusMessage.setBookedAirlineCode(AIRLINE_CODE);
        flightWithStatusMessage.setFlightStatuses(Collections.singletonList(flightStatusMessage));
        return Collections.singletonList(flightWithStatusMessage);
    }

    private static FlightStatusMessage createFlightStatusMessageCancelled() {
        FlightStatusMessage flightStatusMessage = new FlightStatusMessage();
        flightStatusMessage.setStatus(STATUS_CANCELLED);
        return flightStatusMessage;
    }

    private static FlightStatusMessage createFlightStatusMessageLanded() {
        FlightStatusMessage flightStatusMessage = new FlightStatusMessage();
        flightStatusMessage.setStatus(STATUS_LANDED);
        return flightStatusMessage;
    }
}
