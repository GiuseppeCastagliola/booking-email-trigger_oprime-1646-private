package com.odigeo.email.trigger.rules;

import com.edreams.configuration.ConfigurationEngine;
import com.google.inject.AbstractModule;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import java.util.Arrays;

import static org.testng.Assert.assertEquals;

public class ResendEmailRulesTest {

    private ResendEmailRules resendEmailRules = new ResendEmailRules();

    @Mock
    private CacheRule cacheRule;

    @BeforeMethod
    private void setUp() {
        MockitoAnnotations.initMocks(this);
        ConfigurationEngine.init(new AbstractModule() {
            @Override
            protected void configure() {
                bind(CacheRule.class).toInstance(cacheRule);
            }
        });
    }

    @Test
    public void shouldReturnListWithCorrectRules() {
        assertEquals(resendEmailRules.get(), Arrays.asList(
                cacheRule,
                new WhiteLabelRule(),
                new BookingStatusRule(),
                new BuyerRule(),
                new RenewalValidator()
        ));
    }
}
