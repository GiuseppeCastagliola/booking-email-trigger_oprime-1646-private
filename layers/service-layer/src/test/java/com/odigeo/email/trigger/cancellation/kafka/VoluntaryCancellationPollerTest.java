package com.odigeo.email.trigger.cancellation.kafka;

import com.edreams.configuration.ConfigurationEngine;
import com.google.inject.AbstractModule;
import com.odigeo.bookingchange.message.CancellationStatusUpdatedMessage;
import com.odigeo.messaging.utils.Consumer;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.testng.Assert;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import java.util.concurrent.ExecutorService;

import static org.mockito.Mockito.when;

public class VoluntaryCancellationPollerTest {

    private static final Integer A_THREAD_NUMBER = 3;
    private static final String A_ZOOKEEPER_URL = "http://thisismykafka.server";
    private static final String A_GROUP_ID = "group";
    private static final String A_TOPIC = "topic";
    private VoluntaryCancellationPoller voluntaryCancellationPoller;

    @Mock
    private ExecutorService executorService;

    @Mock
    private Consumer<CancellationStatusUpdatedMessage> consumer;

    @Mock
    private KafkaVoluntaryCancellationConfiguration configuration;


    @BeforeMethod
    public void setUp() {
        MockitoAnnotations.initMocks(this);
        ConfigurationEngine.init(new AbstractModule() {
            @Override
            protected void configure() {
                bind(ExecutorService.class).toInstance(executorService);
                bind(Consumer.class).toInstance(consumer);
            }
        });
        when(configuration.getNumThreads()).thenReturn(A_THREAD_NUMBER);
        when(configuration.getZookeeperUrl()).thenReturn(A_ZOOKEEPER_URL);
        when(configuration.getGroupId()).thenReturn(A_GROUP_ID);
        when(configuration.getTopic()).thenReturn(A_TOPIC);

        voluntaryCancellationPoller = new VoluntaryCancellationPoller(configuration);
    }

    @Test
    public void instanceShouldNotBeNull() {
        Assert.assertNotNull(voluntaryCancellationPoller);
    }
}
