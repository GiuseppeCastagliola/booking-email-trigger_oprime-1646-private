package com.odigeo.email.trigger.membership.kafka;

import com.odigeo.membership.message.MembershipMailerMessage;
import org.mockito.Mock;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import java.io.IOException;

import static com.odigeo.membership.message.enums.MessageType.PRIME_RENEWAL;
import static com.odigeo.membership.message.enums.MessageType.WELCOME_TO_PRIME;
import static org.mockito.Mockito.doThrow;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.verify;
import static org.mockito.MockitoAnnotations.initMocks;

public class MembershipMailerMessageProcessorTest {

    @Mock
    MembershipMailerMessageController controller;

    @BeforeMethod
    public void setup() {
        initMocks(this);
    }

    @Test
    public void onMessageShouldCallController() throws IOException {
        MembershipMailerMessageProcessor processor = new MembershipMailerMessageProcessor(controller);
        MembershipMailerMessage message = new MembershipMailerMessage.Builder()
                .withMessageType(WELCOME_TO_PRIME)
                .build();
        processor.onMessage(message);
        verify(controller).onMessage(message);
    }

    @Test
    public void shouldNotCallControllerWhenNotWelcomeToPrime() throws IOException {
        MembershipMailerMessageProcessor processor = new MembershipMailerMessageProcessor(controller);
        MembershipMailerMessage message = new MembershipMailerMessage.Builder()
                .withMessageType(PRIME_RENEWAL)
                .build();
        processor.onMessage(message);
        verify(controller, never()).onMessage(message);
    }

    @Test
    public void shouldCatchExceptionWhenIOExceptionIsThrown() throws IOException {
        MembershipMailerMessageProcessor processor = new MembershipMailerMessageProcessor(controller);
        MembershipMailerMessage message = new MembershipMailerMessage.Builder()
                .withMessageType(WELCOME_TO_PRIME)
                .build();
        doThrow(new IOException()).when(controller).onMessage(message);
        processor.onMessage(message);
    }
}
