package com.odigeo.email.trigger.changedates.kafka;

import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.testng.Assert;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import static org.mockito.Mockito.when;

public class ChangeDatesPollerTest {

    private static final Integer A_THREAD_NUMBER = 3;
    private static final String A_ZOOKEEPER_URL = "http://thisismykafka.server";
    private static final String A_GROUP_ID = "group";
    private static final String A_TOPIC = "topic";
    private ChangeDatesPoller changeDatesPoller;

    @Mock
    private KafkaChangeDatesConfiguration configuration;

    @BeforeMethod
    public void setUp() {
        MockitoAnnotations.initMocks(this);
        when(configuration.getNumThreads()).thenReturn(A_THREAD_NUMBER);
        when(configuration.getZookeeperUrl()).thenReturn(A_ZOOKEEPER_URL);
        when(configuration.getGroupId()).thenReturn(A_GROUP_ID);
        when(configuration.getTopic()).thenReturn(A_TOPIC);

        changeDatesPoller = new ChangeDatesPoller(configuration);
    }

    @Test
    public void instanceShouldNotBeNull() {
        Assert.assertNotNull(changeDatesPoller);
    }
}
