package com.odigeo.email.trigger.rules;

import com.edreams.configuration.ConfigurationEngine;

import com.odigeo.bookingapi.mock.v12.response.BookingBasicInfoBuilder;
import com.odigeo.bookingapi.mock.v12.response.BookingDetailBuilder;
import com.odigeo.bookingapi.mock.v12.response.BuilderException;
import com.odigeo.bookingapi.v12.requests.ProductCategoryBookingItemType;
import com.odigeo.bookingapi.v12.responses.BookingDetail;
import com.odigeo.email.render.v1.contract.EmailType;
import com.odigeo.email.trigger.booking.product.BookingProducts;
import com.odigeo.email.trigger.trigger.EmailRequester;
import com.odigeo.email.trigger.trigger.TriggerInfo;
import com.odigeo.email.trigger.util.Constants;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import java.util.Collections;
import java.util.Random;

public class RenewalValidatorTest {

    private RenewalValidator renewalValidator;

    private BookingDetail bookingDetail;

    private ProductCategoryBookingFixtures productCategoryBookingFixtures;

    @BeforeMethod
    public void init() throws BuilderException {
        ConfigurationEngine.init();
        renewalValidator = new RenewalValidator();
        productCategoryBookingFixtures = new ProductCategoryBookingFixtures();
        bookingDetail = new BookingDetailBuilder().bookingBasicInfoBuilder(new BookingBasicInfoBuilder()).build(new Random());
    }

    @Test(expectedExceptions = TriggerRuleException.class)
    public void shouldThrowExceptionMembershipRenewal() {
        bookingDetail.setBookingProducts(productCategoryBookingFixtures.createProductCategoryBooking(Constants.MEMBERSHIP_RENEWAL));
        renewalValidator.validate(createTriggerInfo());
    }

    @Test
    public void shouldNotThrowException() {
        bookingDetail.setBookingProducts(productCategoryBookingFixtures.createProductCategoryBooking(ProductCategoryBookingItemType.ITINERARY.name()));
        renewalValidator.validate(createTriggerInfo());
    }

    private TriggerInfo createTriggerInfo() {
        return new TriggerInfo(bookingDetail, EmailType.CONFIRMATION, new BookingProducts(), EmailRequester.RESEND, Collections.emptyMap());
    }
}

