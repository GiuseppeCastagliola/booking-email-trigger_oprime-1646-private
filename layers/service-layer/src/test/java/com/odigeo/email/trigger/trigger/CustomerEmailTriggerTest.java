package com.odigeo.email.trigger.trigger;

import com.edreams.configuration.ConfigurationEngine;
import com.odigeo.bookingapi.mock.v12.response.BookingBasicInfoBuilder;
import com.odigeo.bookingapi.mock.v12.response.BookingDetailBuilder;
import com.odigeo.bookingapi.mock.v12.response.BuilderException;
import com.odigeo.bookingapi.mock.v12.response.WebsiteBuilder;
import com.odigeo.bookingapi.v12.responses.BookingDetail;
import com.odigeo.email.render.v1.contract.EmailType;
import com.odigeo.email.trigger.factories.EmailQueuedFactory;
import com.odigeo.domainevents.EmailQueuedServicePublisher;
import com.odigeo.email.trigger.bookingapi.EmailSentCache;
import com.odigeo.email.trigger.configuration.Configuration;
import com.odigeo.email.trigger.email.CustomerEmailTagManager;
import com.odigeo.email.trigger.email.FromEmailSenderNameHelper;
import com.odigeo.email.trigger.mailer.MailerService;
import com.odigeo.email.trigger.render.RenderInfo;
import com.odigeo.email.trigger.render.testbuilder.RenderInfoTestDataBuilder;
import com.odigeo.email.trigger.tracking.CustomerEmailManager;
import com.odigeo.email.trigger.trigger.testbuilder.TriggerInfoTestDataBuilder;
import com.odigeo.email.trigger.util.Constants;
import com.odigeo.mailer.v1.EmailSendResponse;
import com.odigeo.mailer.v1.InvalidParametersException;
import com.odigeo.mailer.v1.RemoteException;
import com.odigeo.mailer.v1.Status;
import com.odigeo.retail.bookingemailtrigger.email.EmailQueued;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.testng.Assert;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import java.util.Collections;
import java.util.Random;

import static org.mockito.Mockito.doThrow;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyZeroInteractions;
import static org.mockito.Mockito.when;

public class CustomerEmailTriggerTest {


    @Mock
    private MailerService mailerService;
    @Mock
    private CustomerEmailManager customerEmailManager;
    @Mock
    private Configuration configuration;
    @Mock
    private EmailSentCache emailSentCache;
    @Mock
    private EmailQueuedServicePublisher emailSentServicePublisher;
    @Mock
    private EmailQueuedFactory emailSentFactory;

    private CustomerEmailTagManager customerEmailTagManager;

    private BookingDetail bookingDetail;
    private TriggerInfo triggerInfo;
    private RenderInfo renderInfo;
    private CustomerEmailTrigger customerEmailTrigger;
    private FromEmailSenderNameHelper fromEmailSenderNameHelper;
    private static final long A_BOOKING_ID = 1L;
    private static final EmailType CONFIRMATION_EMAIL_TYPE = EmailType.CONFIRMATION;
    private static final long A_MAILER_ID = 1L;
    private static final EmailQueued AN_EMAIL_QUEUED = new EmailQueuedFactory().createEmailQueued(A_BOOKING_ID, CONFIRMATION_EMAIL_TYPE, A_MAILER_ID);
    private static final EmailSendResponse AN_EMAIL_QUEUED_RESPONSE = new EmailSendResponse();

    @BeforeMethod
    private void setUp() throws BuilderException, InvalidParametersException, RemoteException {
        ConfigurationEngine.init();
        MockitoAnnotations.initMocks(this);
        bookingDetail = new BookingDetailBuilder().bookingBasicInfoBuilder(new BookingBasicInfoBuilder().website(new WebsiteBuilder().brand("ED"))).build(new Random());
        triggerInfo = TriggerInfoTestDataBuilder.aTriggerInfo().withBookingDetail(bookingDetail).build();
        renderInfo = RenderInfoTestDataBuilder.aRenderInfo().build();
        customerEmailTagManager = ConfigurationEngine.getInstance(CustomerEmailTagManager.class);
        fromEmailSenderNameHelper = ConfigurationEngine.getInstance(FromEmailSenderNameHelper.class);
        customerEmailTrigger = new CustomerEmailTrigger(mailerService, customerEmailManager, emailSentCache, customerEmailTagManager, fromEmailSenderNameHelper, emailSentServicePublisher, emailSentFactory);
        AN_EMAIL_QUEUED_RESPONSE.setDeliveryId(A_MAILER_ID);

        when(emailSentFactory.createEmailQueued(A_BOOKING_ID, CONFIRMATION_EMAIL_TYPE, A_MAILER_ID)).thenReturn(AN_EMAIL_QUEUED);
        when(mailerService.sendEmail(
                fromEmailSenderNameHelper.determineFromAddressAndName(bookingDetail),
                Collections.singletonList(bookingDetail.getBuyer().getMail()),
                bookingDetail.getUserSessionLocale(),
                bookingDetail.getBookingBasicInfo().getWebsite().getCode(),
                bookingDetail.getBookingBasicInfo().getWebsite().getBrand(),
                Constants.TEMPLATE_ID,
                customerEmailTagManager.getEmailTagFromTriggerInfo(triggerInfo),
                renderInfo.getParameters(),
                renderInfo.getAttachments()
        )).thenReturn(AN_EMAIL_QUEUED_RESPONSE);

    }

    @Test
    public void shouldSendRequestToMailerAndSendEvent() throws InvalidParametersException, RemoteException {
        AN_EMAIL_QUEUED_RESPONSE.setStatus(Status.QUEUED);

        ExecutionResult actual = customerEmailTrigger.execute(triggerInfo, renderInfo);
        Assert.assertEquals(actual, ExecutionResult.OK);

        verify(customerEmailManager).index(triggerInfo, renderInfo);
        verify(emailSentServicePublisher).publishMessage(AN_EMAIL_QUEUED);
    }

    @Test
    public void shouldSendRequestToMailerButNotSendEvent() throws InvalidParametersException, RemoteException {
        AN_EMAIL_QUEUED_RESPONSE.setStatus(Status.SENT);

        ExecutionResult actual = customerEmailTrigger.execute(triggerInfo, renderInfo);
        Assert.assertEquals(actual, ExecutionResult.OK);

        verify(customerEmailManager).index(triggerInfo, renderInfo);
        verifyZeroInteractions(emailSentServicePublisher);
    }


    @Test
    public void testExecuteRemoteException() throws InvalidParametersException, RemoteException {
        doThrow(new RemoteException()).when(mailerService).sendEmail(
                fromEmailSenderNameHelper.determineFromAddressAndName(bookingDetail),
                Collections.singletonList(bookingDetail.getBuyer().getMail()),
                bookingDetail.getUserSessionLocale(),
                bookingDetail.getBookingBasicInfo().getWebsite().getCode(),
                bookingDetail.getBookingBasicInfo().getWebsite().getBrand(),
                Constants.TEMPLATE_ID,
                customerEmailTagManager.getEmailTagFromTriggerInfo(triggerInfo),
                renderInfo.getParameters(),
                renderInfo.getAttachments()
        );

        ExecutionResult actual = customerEmailTrigger.execute(triggerInfo, renderInfo);
        verifyKORecoverableResult(actual);
    }

    @Test
    public void testExecuteInvalidParametersException() throws InvalidParametersException, RemoteException {
        doThrow(new InvalidParametersException()).when(mailerService).sendEmail(
                fromEmailSenderNameHelper.determineFromAddressAndName(bookingDetail),
                Collections.singletonList(bookingDetail.getBuyer().getMail()),
                bookingDetail.getUserSessionLocale(),
                bookingDetail.getBookingBasicInfo().getWebsite().getCode(),
                bookingDetail.getBookingBasicInfo().getWebsite().getBrand(),
                Constants.TEMPLATE_ID,
                customerEmailTagManager.getEmailTagFromTriggerInfo(triggerInfo),
                renderInfo.getParameters(),
                renderInfo.getAttachments()
        );

        ExecutionResult actual = customerEmailTrigger.execute(triggerInfo, renderInfo);
        verifyKORecoverableResult(actual);
    }

    @Test
    public void testExecuteRuntimeException() throws InvalidParametersException, RemoteException {
        doThrow(new RuntimeException()).when(mailerService).sendEmail(
                fromEmailSenderNameHelper.determineFromAddressAndName(bookingDetail),
                Collections.singletonList(bookingDetail.getBuyer().getMail()),
                bookingDetail.getUserSessionLocale(),
                bookingDetail.getBookingBasicInfo().getWebsite().getCode(),
                bookingDetail.getBookingBasicInfo().getWebsite().getBrand(),
                Constants.TEMPLATE_ID,
                customerEmailTagManager.getEmailTagFromTriggerInfo(triggerInfo),
                renderInfo.getParameters(),
                renderInfo.getAttachments()
        );

        ExecutionResult actual = customerEmailTrigger.execute(triggerInfo, renderInfo);
        verifyKORecoverableResult(actual);
    }

    private void verifyKORecoverableResult(ExecutionResult actual) {

        verify(customerEmailManager, never()).index(triggerInfo, renderInfo);
        verify(emailSentServicePublisher, never()).publishMessage(Mockito.any(EmailQueued.class));
        Assert.assertEquals(actual, ExecutionResult.KO_RECOVERABLE);
    }

    @Test
    public void testWithNullEmailTypeExecute() throws InvalidParametersException, RemoteException, BuilderException {
        triggerInfo = TriggerInfoTestDataBuilder.aTriggerInfo().withBookingDetail(bookingDetail).withEmailType(null).build();
        ExecutionResult actual = customerEmailTrigger.execute(triggerInfo, renderInfo);
        Assert.assertEquals(actual, ExecutionResult.OK);
        verify(customerEmailManager).index(triggerInfo, renderInfo);
        verify(mailerService).sendEmail(
                fromEmailSenderNameHelper.determineFromAddressAndName(bookingDetail),
                Collections.singletonList(bookingDetail.getBuyer().getMail()),
                bookingDetail.getUserSessionLocale(),
                bookingDetail.getBookingBasicInfo().getWebsite().getCode(),
                bookingDetail.getBookingBasicInfo().getWebsite().getBrand(),
                Constants.TEMPLATE_ID,
                customerEmailTagManager.getEmailTagFromTriggerInfo(triggerInfo),
                renderInfo.getParameters(),
                renderInfo.getAttachments()
        );
    }

}
