package com.odigeo.email.trigger.kafka.consumer;

import com.odigeo.commons.messaging.KafkaConsumer;
import com.odigeo.membership.message.MembershipMailerMessage;
import org.testng.annotations.Test;

import static org.testng.Assert.assertNotNull;

public class ConsumerFactoryTest {

    private static final String KAFKA_TOPIC = "TOPIC_1";
    private static final String MESSAGE_GROUP = "group-1";

    @Test
    public void shouldCreateConsumer() {
        ConsumerFactory consumerFactory = new ConsumerFactory();
        KafkaConsumer<MembershipMailerMessage> consumer = consumerFactory.newConsumer(KAFKA_TOPIC, MESSAGE_GROUP, MembershipMailerMessage.class);
        assertNotNull(consumer);
    }
}

