package com.odigeo.email.trigger.membership.kafka;

import com.odigeo.email.render.v1.contract.EmailType;
import com.odigeo.email.trigger.rules.RulesInterface;
import com.odigeo.email.trigger.trigger.EmailRequester;
import com.odigeo.email.trigger.trigger.TriggerService;
import com.odigeo.membership.message.MembershipMailerMessage;
import org.mockito.Mock;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import java.io.IOException;

import static com.odigeo.membership.message.enums.MessageType.PRIME_RENEWAL;
import static com.odigeo.membership.message.enums.MessageType.WELCOME_TO_PRIME;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.anyMap;
import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.verify;
import static org.mockito.MockitoAnnotations.initMocks;

public class MembershipMailerMessageControllerTest {

    private static final Long BOOKING_ID = 1084L;
    @Mock
    private TriggerService triggerService;

    private MembershipMailerMessageController controller;


    @BeforeMethod
    public void setup() {
        initMocks(this);
        controller = new MembershipMailerMessageController(triggerService);
    }

    @Test
    public void shouldCallTriggerService() throws IOException {
        MembershipMailerMessage message = new MembershipMailerMessage.Builder()
                .withBookingId(BOOKING_ID)
                .withMessageType(WELCOME_TO_PRIME).build();
        controller.onMessage(message);
        verify(triggerService).execute(eq(String.valueOf(BOOKING_ID)), any(RulesInterface.class), eq(EmailRequester.UPDATE), eq(EmailType.WELCOME_TO_PRIME), anyMap());
    }

    @Test(expectedExceptions = {UnsupportedOperationException.class})
    public void shouldThrowExceptionForUnsupportedType() throws IOException {
        MembershipMailerMessage message = new MembershipMailerMessage.Builder()
                .withBookingId(BOOKING_ID)
                .withMessageType(PRIME_RENEWAL)
                .build();
        controller.onMessage(message);
    }
}
