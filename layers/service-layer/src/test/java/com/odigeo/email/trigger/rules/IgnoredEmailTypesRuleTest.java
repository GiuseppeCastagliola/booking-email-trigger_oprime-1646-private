package com.odigeo.email.trigger.rules;

import com.odigeo.email.render.v1.contract.EmailType;
import com.odigeo.email.trigger.trigger.TriggerInfo;
import org.testng.annotations.Test;

public class IgnoredEmailTypesRuleTest {

    private IgnoredEmailTypesRule ignoredEmailTypesRule = new IgnoredEmailTypesRule();
    private TriggerInfo triggerInfo;

    @Test(expectedExceptions = TriggerRuleException.class)
    public void shouldThrowExceptionWhenCancellationRequested() {
        triggerInfo = constructTriggerInfoWithEmailType(EmailType.CANCELLATION_REQUESTED);
        ignoredEmailTypesRule.validate(triggerInfo);
    }

    @Test(expectedExceptions = TriggerRuleException.class)
    public void shouldThrowExceptionWhenChangeRequested() {
        triggerInfo = constructTriggerInfoWithEmailType(EmailType.CHANGE_REQUESTED);
        ignoredEmailTypesRule.validate(triggerInfo);
    }

    @Test
    public void shouldDoNothingWhenValidEmailType() {
        triggerInfo = constructTriggerInfoWithEmailType(EmailType.CONFIRMATION);
        ignoredEmailTypesRule.validate(triggerInfo);
    }


    private TriggerInfo constructTriggerInfoWithEmailType(EmailType emailType) {
        return new TriggerInfo.Builder()
                .withEmailType(emailType)
                .build();
    }
}
