package com.odigeo.email.trigger.factories;

import com.odigeo.bookingapi.v12.responses.BookingDetail;
import com.odigeo.email.render.v1.contract.EmailType;
import com.odigeo.email.trigger.booking.product.BookingProducts;
import com.odigeo.email.trigger.trigger.EmailRequester;
import com.odigeo.email.trigger.trigger.TriggerInfo;
import org.testng.Assert;
import org.testng.annotations.Test;

import java.util.Collections;

public class TriggerInfoFactoryTest {

    private TriggerInfoFactory triggerInfoFactory = new TriggerInfoFactory();

    private static final BookingDetail BOOKING_DETAIL = new BookingDetail();
    private static final EmailType EMAIL_TYPE = EmailType.CONFIRMATION;
    private static final BookingProducts BOOKING_PRODUCTS = new BookingProducts();
    private static final EmailRequester EMAIL_REQUESTER = EmailRequester.UPDATE;

    @Test
    public void testConstructTriggerInfo() {
        TriggerInfo result = triggerInfoFactory.
                constructTriggerInfo(BOOKING_DETAIL, EMAIL_TYPE, BOOKING_PRODUCTS, EMAIL_REQUESTER);

        Assert.assertEquals(result.getBookingDetail(), BOOKING_DETAIL);
        Assert.assertEquals(result.getEmailType(), EMAIL_TYPE);
        Assert.assertEquals(result.getBookingProducts(), BOOKING_PRODUCTS);
        Assert.assertEquals(result.getEmailRequester(), EMAIL_REQUESTER);
        Assert.assertEquals(result.getAdditionalParameters(), Collections.emptyMap());

    }
}
