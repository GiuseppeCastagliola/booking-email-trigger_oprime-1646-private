package com.odigeo.email.trigger.rules;

import com.odigeo.bookingapi.v12.responses.MembershipSubscriptionBooking;
import com.odigeo.bookingapi.v12.responses.ProductCategoryBooking;

import java.util.ArrayList;
import java.util.List;

class ProductCategoryBookingFixtures {

    List<ProductCategoryBooking> createProductCategoryBooking(String productType) {
        List<ProductCategoryBooking> productCategoryBookingList = new ArrayList<>();
        MembershipSubscriptionBooking membershipSubscriptionBooking = new MembershipSubscriptionBooking();
        membershipSubscriptionBooking.setProductType(productType);
        productCategoryBookingList.add(membershipSubscriptionBooking);
        return productCategoryBookingList;
    }
}
