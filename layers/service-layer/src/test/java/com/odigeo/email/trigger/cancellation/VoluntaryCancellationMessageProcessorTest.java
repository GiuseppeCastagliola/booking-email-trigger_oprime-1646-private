package com.odigeo.email.trigger.cancellation;

import com.edreams.configuration.ConfigurationEngine;
import com.google.inject.AbstractModule;
import com.odigeo.bookingchange.message.CancellationStatusUpdatedMessage;
import com.odigeo.bookingchange.message.UserBookingChangeRequestStatus;
import com.odigeo.email.trigger.rules.CancellationSendingRules;
import com.odigeo.email.trigger.rules.RulesInterface;
import com.odigeo.messaging.utils.ConsumerIterator;
import com.odigeo.messaging.utils.MessageDataAccessException;
import com.odigeo.messaging.utils.MessageParserException;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import java.io.IOException;
import java.util.EnumSet;

import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

public class VoluntaryCancellationMessageProcessorTest {

    private static final EnumSet<UserBookingChangeRequestStatus> EMAILABLE_STATUSES = EnumSet.of(
            UserBookingChangeRequestStatus.REQUESTED,
            UserBookingChangeRequestStatus.PROCESSING
    );
    private static final RulesInterface SOME_RULES = new CancellationSendingRules();

    private VoluntaryCancellationMessageProcessor voluntaryCancellationMessageProcessor;

    @Mock
    private ConsumerIterator<CancellationStatusUpdatedMessage> consumerIterator;

    @Mock
    private VoluntaryCancellationRequestedController voluntaryCancellationRequestedController;

    private CancellationStatusUpdatedMessage message;

    @BeforeMethod
    public void init() throws MessageDataAccessException, MessageParserException {
        MockitoAnnotations.initMocks(this);
        ConfigurationEngine.init(new AbstractModule() {
            @Override
            protected void configure() {
                bind(VoluntaryCancellationRequestedController.class).toInstance(voluntaryCancellationRequestedController);
            }
        });
        voluntaryCancellationMessageProcessor = new VoluntaryCancellationMessageProcessor(consumerIterator);
        message = new CancellationStatusUpdatedMessage();

        when(consumerIterator.next()).thenReturn(message);
    }

    @Test
    public void shouldCallOnMessageMethodWhenStatusIsEmailable() throws IOException, MessageDataAccessException {
        for (int i = 0; i < UserBookingChangeRequestStatus.values().length; i++) {
            when(consumerIterator.hasNext()).thenReturn(true).thenReturn(false);
            message.setRequestStatus(UserBookingChangeRequestStatus.values()[i]);
            voluntaryCancellationMessageProcessor.run();
        }

        verify(voluntaryCancellationRequestedController, times(EMAILABLE_STATUSES.size())).onMessage(message, SOME_RULES);

    }
}
