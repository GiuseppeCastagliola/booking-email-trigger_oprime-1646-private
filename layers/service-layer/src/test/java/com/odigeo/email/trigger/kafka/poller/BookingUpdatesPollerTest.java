package com.odigeo.email.trigger.kafka.poller;

import com.edreams.configuration.ConfigurationEngine;
import org.testng.Assert;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

public class BookingUpdatesPollerTest {

    private BookingUpdatesPoller bookingUpdatesPoller;

    @BeforeMethod
    public void setUp() {
        ConfigurationEngine.init();
    }

    @Test
    public void test() {
        bookingUpdatesPoller = ConfigurationEngine.getInstance(BookingUpdatesPoller.class);
        Assert.assertNotNull(bookingUpdatesPoller);
    }
}
