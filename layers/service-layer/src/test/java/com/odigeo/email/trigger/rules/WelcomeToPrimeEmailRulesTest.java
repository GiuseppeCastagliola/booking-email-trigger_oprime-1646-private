package com.odigeo.email.trigger.rules;


import com.edreams.configuration.ConfigurationEngine;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import java.util.List;

import static org.testng.Assert.assertEquals;

public class WelcomeToPrimeEmailRulesTest {
    @Mock
    private CacheRule cacheRule;
    @Mock
    private WelcomeToPrimeNeverTriggeredRule welcomeNeverTriggeredRule;
    private WelcomeToPrimeEmailRules welcomeToPrimeEmailRules;

    @BeforeMethod
    public void setUp() {
        MockitoAnnotations.initMocks(this);
        ConfigurationEngine.init(binder -> {
            binder.bind(CacheRule.class).toInstance(cacheRule);
            binder.bind(WelcomeToPrimeNeverTriggeredRule.class).toInstance(welcomeNeverTriggeredRule);
        });
        welcomeToPrimeEmailRules = new WelcomeToPrimeEmailRules();
    }

    @Test
    public void testRulesGet() {
        List<TriggerRule> triggerRules = welcomeToPrimeEmailRules.get();
        assertEquals(triggerRules.get(0).getClass(), EmailTypeRule.class);
        assertEquals(triggerRules.get(1), cacheRule);
        assertEquals(triggerRules.get(2), welcomeNeverTriggeredRule);
    }
}
