package com.odigeo.email.trigger.cancellation;

import com.edreams.configuration.ConfigurationEngine;
import com.odigeo.bookingapi.mock.v12.response.BookingBasicInfoBuilder;
import com.odigeo.bookingapi.mock.v12.response.BookingDetailBuilder;
import com.odigeo.bookingapi.mock.v12.response.BuilderException;
import com.odigeo.bookingapi.v12.responses.BookingDetail;
import com.odigeo.bookingchange.message.CancellationStatusUpdatedMessage;
import com.odigeo.bookingchange.message.UserBookingChangeRequestStatus;
import com.odigeo.email.render.v1.contract.EmailType;
import com.odigeo.email.trigger.booking.product.BookingProducts;
import com.odigeo.email.trigger.bookingapi.BookingServiceClient;
import com.odigeo.email.trigger.factories.TriggerInfoFactory;
import com.odigeo.email.trigger.rules.CancellationSendingRules;
import com.odigeo.email.trigger.rules.RulesInterface;
import com.odigeo.email.trigger.trigger.EmailRequester;
import com.odigeo.email.trigger.trigger.ExecutionResult;
import com.odigeo.email.trigger.trigger.TriggerExecutor;
import com.odigeo.email.trigger.trigger.TriggerInfo;
import com.odigeo.email.trigger.trigger.TriggerServiceRulesValidator;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import java.io.IOException;
import java.util.Locale;
import java.util.Random;

import static org.mockito.Matchers.any;
import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

public class VoluntaryCancellationRequestedControllerTest {

    private static final ExecutionResult A_FAILURE_STATUS = ExecutionResult.MUST_BE_IGNORED;
    private static final RulesInterface SOME_RULES = new CancellationSendingRules();
    private static final Long A_BOOKING_ID = 1L;
    private static final TriggerInfo A_TRIGGER_INFO = new TriggerInfo.Builder().build();

    @Mock
    private BookingServiceClient bookingServiceClient;
    @Mock
    private TriggerExecutor triggerExecutor;
    @Mock
    private TriggerInfoFactory triggerInfoFactory;
    @Mock
    private TriggerServiceRulesValidator triggerServiceRulesValidator;

    private VoluntaryCancellationRequestedController voluntaryCancellationRequestedController;
    private BookingDetail bookingDetail;


    @BeforeMethod
    public void init() throws BuilderException {
        MockitoAnnotations.initMocks(this);
        ConfigurationEngine.init();
        voluntaryCancellationRequestedController = new VoluntaryCancellationRequestedController(bookingServiceClient, triggerExecutor, triggerInfoFactory, triggerServiceRulesValidator);
        bookingDetail = new BookingDetailBuilder().bookingBasicInfoBuilder(new BookingBasicInfoBuilder().id(A_BOOKING_ID)).build(new Random());
        when(triggerInfoFactory.constructTriggerInfo(eq(bookingDetail), any(EmailType.class), any(BookingProducts.class), eq(EmailRequester.UPDATE))).thenReturn(A_TRIGGER_INFO);
        when(triggerServiceRulesValidator.validate(A_TRIGGER_INFO, SOME_RULES)).thenReturn(ExecutionResult.OK);
    }

    @Test
    public void shouldTriggerEmailSend() throws IOException {
        final CancellationStatusUpdatedMessage message = new CancellationStatusUpdatedMessage();
        message.setBookingId(A_BOOKING_ID);
        message.setRequestStatus(UserBookingChangeRequestStatus.REQUESTED);


        when(bookingServiceClient.getBookingDetail(Locale.getDefault(), message.getBookingId())).thenReturn(bookingDetail);

        voluntaryCancellationRequestedController.onMessage(message, SOME_RULES);
        verify(triggerExecutor).execute(A_TRIGGER_INFO);
    }

    @Test(expectedExceptions = IllegalArgumentException.class)
    public void shouldNotTriggerEmailSend() throws IOException {
        final CancellationStatusUpdatedMessage message = new CancellationStatusUpdatedMessage();
        message.setBookingId(A_BOOKING_ID);
        message.setRequestStatus(UserBookingChangeRequestStatus.AGENT_REVIEW);

        when(bookingServiceClient.getBookingDetail(Locale.getDefault(), message.getBookingId())).thenReturn(bookingDetail);

        voluntaryCancellationRequestedController.onMessage(message, SOME_RULES);
        verify(triggerExecutor, never()).execute(A_TRIGGER_INFO);
    }

    @Test(expectedExceptions = IOException.class)
    public void shouldNotTriggerEmailSendBookingNotFound() throws IOException {
        final CancellationStatusUpdatedMessage message = new CancellationStatusUpdatedMessage();
        message.setBookingId(A_BOOKING_ID);

        when(bookingServiceClient.getBookingDetail(Locale.getDefault(), message.getBookingId())).thenThrow(new IOException());

        voluntaryCancellationRequestedController.onMessage(message, SOME_RULES);
        verify(triggerExecutor, never()).execute(A_TRIGGER_INFO);
    }

    @Test
    public void shouldNotCallExecuteWhenValidationKO() throws IOException {
        final CancellationStatusUpdatedMessage message = new CancellationStatusUpdatedMessage();
        message.setBookingId(A_BOOKING_ID);
        message.setRequestStatus(UserBookingChangeRequestStatus.REQUESTED);

        when(bookingServiceClient.getBookingDetail(Locale.getDefault(), message.getBookingId())).thenReturn(bookingDetail);
        when(triggerServiceRulesValidator.validate(A_TRIGGER_INFO, SOME_RULES)).thenReturn(A_FAILURE_STATUS);

        voluntaryCancellationRequestedController.onMessage(message, SOME_RULES);
        verify(triggerExecutor, never()).execute(A_TRIGGER_INFO);

    }
}
