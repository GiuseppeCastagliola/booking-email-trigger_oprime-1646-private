package com.odigeo.email.trigger.factories;

import com.odigeo.email.trigger.rules.OnDemandSendingRules;
import com.odigeo.email.trigger.rules.ResendEmailRules;
import org.testng.annotations.Test;

import static org.testng.Assert.assertEquals;

public class RulesFactoryTest {

    private RulesFactory rulesFactory = new RulesFactory();

    @Test
    public void testGetResendEmailRules() {
        assertEquals(rulesFactory.getResendEmailRules(), new ResendEmailRules());
    }

    @Test
    public void testGetOnDemandEmailRules() {
        assertEquals(rulesFactory.getOnDemandEmailRules(), new OnDemandSendingRules());
    }
}
