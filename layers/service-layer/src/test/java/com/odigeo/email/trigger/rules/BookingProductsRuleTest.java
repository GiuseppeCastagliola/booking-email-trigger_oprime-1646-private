package com.odigeo.email.trigger.rules;


import com.edreams.configuration.ConfigurationEngine;
import com.odigeo.bookingapi.mock.v12.response.BookingDetailBuilder;
import com.odigeo.bookingapi.mock.v12.response.BuilderException;
import com.odigeo.bookingapi.v12.responses.BookingDetail;
import com.odigeo.email.render.v1.contract.EmailType;
import com.odigeo.email.trigger.booking.product.BookingProducts;
import com.odigeo.email.trigger.booking.product.Seats;
import com.odigeo.email.trigger.tracking.CustomerEmailManager;
import com.odigeo.email.trigger.trigger.EmailRequester;
import com.odigeo.email.trigger.trigger.TriggerInfo;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import java.util.Collections;
import java.util.Optional;
import java.util.Random;

import static org.mockito.Mockito.when;

public class BookingProductsRuleTest {

    @Mock
    private CustomerEmailManager customerEmailManager;
    private BookingDetail bookingDetail;
    private BookingProductsRule bookingProductsRule;
    private BookingProducts bookingProducts;

    @BeforeMethod
    public void init() throws BuilderException {
        MockitoAnnotations.initMocks(this);
        ConfigurationEngine.init();
        bookingProductsRule = new BookingProductsRule(customerEmailManager);
        bookingDetail = new BookingDetailBuilder().build(new Random());
        bookingProducts = new BookingProducts();
    }

    @Test(expectedExceptions = TriggerRuleException.class)
    public void shouldReturnExceptionWhenEqualBookingProducts() {
        TriggerInfo triggerInfo = new TriggerInfo(bookingDetail, EmailType.CONFIRMATION, bookingProducts, EmailRequester.UPDATE, Collections.emptyMap());
        when(customerEmailManager.searchBookingProducts(bookingDetail.getBookingBasicInfo().getId(), EmailType.CONFIRMATION)).thenReturn(Optional.of(bookingProducts));
        bookingProductsRule.validate(triggerInfo);
    }

    @Test
    public void shouldNotReturnExceptionWhenDifferentBookingProducts() {
        bookingProducts.setSeats(new Seats());
        when(customerEmailManager.searchBookingProducts(bookingDetail.getBookingBasicInfo().getId(), EmailType.CONFIRMATION)).thenReturn(Optional.of(new BookingProducts()));
        bookingProductsRule.validate(new TriggerInfo(bookingDetail, EmailType.CONFIRMATION, bookingProducts, EmailRequester.UPDATE, Collections.emptyMap()));
    }

    @Test(expectedExceptions = TriggerRuleException.class)
    public void shouldReturnExceptionWhenEmailTypeInProgress() {
        bookingProducts.setSeats(new Seats());
        when(customerEmailManager.searchBookingProducts(bookingDetail.getBookingBasicInfo().getId(), EmailType.IN_PROGRESS)).thenReturn(Optional.of(new BookingProducts()));
        bookingProductsRule.validate(new TriggerInfo(bookingDetail, EmailType.IN_PROGRESS, bookingProducts, EmailRequester.UPDATE, Collections.emptyMap()));
    }
}


