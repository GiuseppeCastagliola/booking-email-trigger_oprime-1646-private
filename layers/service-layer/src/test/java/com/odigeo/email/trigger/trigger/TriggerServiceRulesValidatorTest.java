package com.odigeo.email.trigger.trigger;

import com.edreams.configuration.ConfigurationEngine;
import com.google.inject.AbstractModule;
import com.odigeo.bookingapi.mock.v12.response.BookingBasicInfoBuilder;
import com.odigeo.bookingapi.mock.v12.response.BookingDetailBuilder;
import com.odigeo.bookingapi.mock.v12.response.BuilderException;
import com.odigeo.bookingapi.mock.v12.response.BuyerBuilder;
import com.odigeo.bookingapi.v12.requests.Status;
import com.odigeo.bookingapi.v12.responses.BookingDetail;
import com.odigeo.email.render.v1.contract.EmailType;
import com.odigeo.email.trigger.booking.product.BookingProducts;
import com.odigeo.email.trigger.email.TriggerHelper;
import com.odigeo.email.trigger.rules.BookingProductsRule;
import com.odigeo.email.trigger.rules.CacheRule;
import com.odigeo.email.trigger.rules.Rules;
import com.odigeo.email.trigger.tracking.CustomerEmailManager;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import java.util.Optional;
import java.util.Random;

import static org.mockito.Mockito.never;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static org.testng.Assert.assertEquals;

public class TriggerServiceRulesValidatorTest {

    @Mock
    private CacheRule cacheRule;

    @Mock
    private CustomerEmailManager customerEmailManager;

    private TriggerServiceRulesValidator triggerServiceRulesValidator;
    private TriggerInfo triggerInfo;
    private BookingDetail bookingDetail;
    private TriggerHelper triggerHelper = new TriggerHelper();
    private BookingProductsRule bookingProductsRule;
    private final long anyID = 1L;

    @BeforeMethod
    private void setUp() throws BuilderException {
        MockitoAnnotations.initMocks(this);
        bookingProductsRule = new BookingProductsRule(customerEmailManager);

        ConfigurationEngine.init(new AbstractModule() {
            @Override
            protected void configure() {
                bind(CacheRule.class).toInstance(cacheRule);
                bind(TriggerHelper.class).toInstance(triggerHelper);
                bind(BookingProductsRule.class).toInstance(bookingProductsRule);
            }
        });

        triggerServiceRulesValidator = new TriggerServiceRulesValidator();
        bookingDetail = createBaseBookingDetail(anyID);
        triggerInfo = createBaseTriggerInfo(bookingDetail);
    }

    @Test
    public void shouldReturnWHITELABELWhenWhiteLabelIsTrue() {

        bookingDetail.setIsWhiteLabel(true);

        ExecutionResult executionResult = triggerServiceRulesValidator.validate(triggerInfo, new Rules());
        assertEquals(executionResult, ExecutionResult.WHITE_LABEL);
    }

    @Test
    public void shouldReturnINVALIDSTATUSINITWhenBookingStatusDiffFromRequestAndContractAndDidNotBy() {

        bookingDetail.setBookingStatus(Status.INIT.name());

        ExecutionResult executionResult = triggerServiceRulesValidator.validate(triggerInfo, new Rules());
        assertEquals(executionResult, ExecutionResult.INVALID_STATUS);
    }

    @Test
    public void shouldReturnINVALIDSTATUSHOLDWhenBookingStatusDiffFromRequestAndContractAndDidNotBy() {

        bookingDetail.setBookingStatus(Status.HOLD.name());

        ExecutionResult executionResult = triggerServiceRulesValidator.validate(triggerInfo, new Rules());
        assertEquals(executionResult, ExecutionResult.INVALID_STATUS);
    }

    @Test
    public void shouldReturnINVALIDSTATUSPENDINGWhenBookingStatusDiffFromRequestAndContractAndDidNotBy() {

        bookingDetail.setBookingStatus(Status.PENDING.name());

        ExecutionResult executionResult = triggerServiceRulesValidator.validate(triggerInfo, new Rules());
        assertEquals(executionResult, ExecutionResult.INVALID_STATUS);
    }

    @Test
    public void shouldReturnINVALIDSTATUSRETAINEDWhenBookingStatusDiffFromRequestAndContractAndDidNotBy() {

        bookingDetail.setBookingStatus(Status.RETAINED.name());

        ExecutionResult executionResult = triggerServiceRulesValidator.validate(triggerInfo, new Rules());
        assertEquals(executionResult, ExecutionResult.INVALID_STATUS);
    }

    @Test
    public void shouldReturnINVALIDSTATUSUNKNOWNWhenBookingStatusDiffFromRequestAndContractAndDidNotBy() {

        bookingDetail.setBookingStatus(Status.UNKNOWN.name());

        ExecutionResult executionResult = triggerServiceRulesValidator.validate(triggerInfo, new Rules());
        assertEquals(executionResult, ExecutionResult.INVALID_STATUS);
    }

    @Test
    public void shouldReturnINVALIDSTATUSFINALRETWhenBookingStatusDiffFromRequestAndContractAndDidNotBy() {

        bookingDetail.setBookingStatus(Status.UNKNOWN.name());

        ExecutionResult executionResult = triggerServiceRulesValidator.validate(triggerInfo, new Rules());
        assertEquals(executionResult, ExecutionResult.INVALID_STATUS);
    }

    @Test
    public void shouldReturnINVALIDSTATUSWhenHasValidBookingStatusButEmailTypeIsOK() {

        TriggerInfo triggerInfoKO = new TriggerInfo.Builder().withEmailType(EmailType.KO).withBookingDetail(bookingDetail).build();

        ExecutionResult executionResult = triggerServiceRulesValidator.validate(triggerInfoKO, new Rules());
        assertEquals(executionResult, ExecutionResult.INVALID_STATUS);
    }

    @Test
    public void shouldReturnMISSINGEMAILTYPEWhenEmailTypeEmpty() {

        TriggerInfo triggerInfoWithoutEmailType = new TriggerInfo.Builder().withBookingDetail(bookingDetail).build();

        ExecutionResult executionResult = triggerServiceRulesValidator.validate(triggerInfoWithoutEmailType, new Rules());
        assertEquals(executionResult, ExecutionResult.MISSING_EMAIL_TYPE);

        verify(customerEmailManager, never()).searchBookingProducts(Mockito.eq(anyID), Mockito.eq(triggerInfoWithoutEmailType.getEmailType()));
    }

    @Test
    public void shouldReturnOKWhenAllDataIsValid() {

        TriggerInfo triggerInfoConfirmation = new TriggerInfo.Builder().withBookingDetail(bookingDetail).withEmailType(EmailType.CONFIRMATION).build();
        when(customerEmailManager.searchBookingProducts(Mockito.eq(anyID), Mockito.eq(triggerInfoConfirmation.getEmailType()))).thenReturn(Optional.empty());

        ExecutionResult executionResult = triggerServiceRulesValidator.validate(triggerInfoConfirmation, new Rules());
        assertEquals(executionResult, ExecutionResult.OK);
        verify(customerEmailManager).searchBookingProducts(Mockito.eq(anyID), Mockito.eq(triggerInfoConfirmation.getEmailType()));
    }

    @Test
    public void shouldReturnALREADYTRIGGEREDWhenBookingAlreadyExistWithEmailTypeDiffFromInProgress() {

        triggerInfo = new TriggerInfo.Builder().withBookingDetail(bookingDetail).withEmailType(EmailType.IN_PROGRESS).build();
        BookingProducts anyBookingProducts = new BookingProducts();
        when(customerEmailManager.searchBookingProducts(Mockito.eq(anyID), Mockito.eq(triggerInfo.getEmailType()))).thenReturn(Optional.of(anyBookingProducts));

        ExecutionResult executionResult = triggerServiceRulesValidator.validate(triggerInfo, new Rules());
        assertEquals(executionResult, ExecutionResult.ALREADY_TRIGGERED);
        verify(customerEmailManager).searchBookingProducts(Mockito.eq(anyID), Mockito.eq(triggerInfo.getEmailType()));
    }

    private BookingDetail createBaseBookingDetail(Long id) throws BuilderException {

        return new BookingDetailBuilder()
                .bookingBasicInfoBuilder(new BookingBasicInfoBuilder())
                .isWhiteLabel(false)
                .bookingStatus(Status.REQUEST.name())
                .bookingBasicInfoBuilder(new BookingBasicInfoBuilder().id(id))
                .buyer(new BuyerBuilder())
                .build(new Random());
    }

    private TriggerInfo createBaseTriggerInfo(BookingDetail bookingDetail) {
        return new TriggerInfo.Builder().withBookingDetail(bookingDetail).build();
    }
}
