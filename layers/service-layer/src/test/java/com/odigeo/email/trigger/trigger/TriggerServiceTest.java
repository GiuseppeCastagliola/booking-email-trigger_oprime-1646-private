package com.odigeo.email.trigger.trigger;

import com.edreams.configuration.ConfigurationEngine;
import com.odigeo.bookingapi.mock.v12.response.BookingDetailBuilder;
import com.odigeo.bookingapi.mock.v12.response.BuilderException;
import com.odigeo.bookingapi.v12.responses.BookingDetail;
import com.odigeo.email.render.v1.contract.EmailType;
import com.odigeo.email.trigger.booking.product.BookingProducts;
import com.odigeo.email.trigger.bookingapi.BookingServiceClient;
import com.odigeo.email.trigger.email.EmailTypeCalculator;
import com.odigeo.email.trigger.factories.TriggerInfoFactory;
import com.odigeo.email.trigger.rules.Rules;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import java.io.IOException;
import java.io.UncheckedIOException;
import java.util.Collections;
import java.util.Locale;
import java.util.Map;
import java.util.Random;

import static org.mockito.Matchers.any;
import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static org.testng.Assert.assertEquals;

public class TriggerServiceTest {

    private static final Long A_BOOKING_ID = 1234567890L;
    private static final String A_TEMPLATE_NAME = EmailType.CANCELLATION_PENDINGAIRLINE.getTemplateName();
    private static final Long AN_INVALID_BOOKING_ID = -987654321L;
    private static final String AN_INVALID_TEMPLATE_NAME = "_-_invalidTemplateName_-_";
    @Mock
    private TriggerExecutor triggerExecutor;
    @Mock
    private BookingServiceClient bookingServiceClient;
    @Mock
    private EmailTypeCalculator emailTypeCalculator;
    @Mock
    private TriggerServiceRulesValidator triggerServiceRulesValidator;
    @Mock
    private TriggerInfoFactory triggerInfoFactory;

    private static final String KEY = "1234567890";
    private TriggerService triggerService;
    private Rules rules;
    private TriggerInfo triggerInfo;

    @BeforeMethod
    private void setUp() throws IOException, BuilderException {
        MockitoAnnotations.initMocks(this);
        ConfigurationEngine.init();
        BookingDetail bookingDetail = new BookingDetailBuilder().build(new Random());
        when(bookingServiceClient.getBookingDetail(Locale.getDefault(), Long.parseLong(KEY))).thenReturn(bookingDetail);
        when(bookingServiceClient.getBookingDetail(Locale.getDefault(), A_BOOKING_ID)).thenReturn(bookingDetail);
        when(bookingServiceClient.getBookingDetail(Locale.getDefault(), AN_INVALID_BOOKING_ID)).thenThrow(new IOException());
        when(emailTypeCalculator.calculate(bookingDetail)).thenReturn(EmailType.CONFIRMATION);
        when(emailTypeCalculator.getEmailTypeFromTemplateName(A_TEMPLATE_NAME)).thenReturn(EmailType.CONFIRMATION);
        when(emailTypeCalculator.getEmailTypeFromTemplateName(AN_INVALID_TEMPLATE_NAME)).thenThrow(new IllegalArgumentException());
        when(triggerInfoFactory.constructTriggerInfo(eq(bookingDetail), eq(EmailType.CONFIRMATION), any(BookingProducts.class), any(EmailRequester.class))).thenReturn(triggerInfo);
        when(triggerInfoFactory.constructTriggerInfo(eq(bookingDetail), eq(EmailType.CANCELLATION_PENDINGAIRLINE), any(BookingProducts.class), eq(EmailRequester.UPDATE))).thenReturn(triggerInfo);
        when(triggerInfoFactory.constructTriggerInfo(eq(bookingDetail), eq(EmailType.WELCOME_TO_PRIME), any(BookingProducts.class), eq(EmailRequester.UPDATE))).thenReturn(triggerInfo);
        when(triggerServiceRulesValidator.validate(triggerInfo, rules)).thenReturn(ExecutionResult.OK);
        triggerService = new TriggerService(triggerExecutor, bookingServiceClient, emailTypeCalculator, triggerInfoFactory, triggerServiceRulesValidator);
    }

    @Test
    public void testExecuteValidationOK() throws IOException {
        when(triggerExecutor.execute(triggerInfo)).thenReturn(ExecutionResult.OK);
        ExecutionResult result = triggerService.execute(KEY, rules, EmailRequester.UPDATE);
        assertEquals(result, ExecutionResult.OK);
    }

    @Test
    public void testExecuteValidationNotOKAlreadyTriggered() throws IOException {
        when(triggerServiceRulesValidator.validate(triggerInfo, rules)).thenReturn(Validation.ALREADY_TRIGGERED.getExecutionResult());
        triggerService.execute(KEY, rules, EmailRequester.UPDATE);
        verify(triggerExecutor, never()).execute(triggerInfo);
    }

    @Test
    public void testMissingItineraryAndAccommodation() throws IOException {
        when(triggerExecutor.execute(triggerInfo)).thenReturn(ExecutionResult.MUST_BE_IGNORED);
        when(triggerServiceRulesValidator.validate(triggerInfo, rules)).thenReturn(ExecutionResult.MUST_BE_IGNORED);

        ExecutionResult result = triggerService.execute(KEY, rules, EmailRequester.UPDATE);
        verify(triggerExecutor, never()).execute(triggerInfo);
        assertEquals(result, ExecutionResult.MUST_BE_IGNORED);
    }

    @Test
    public void shouldTriggerEmailSend() throws IOException {
        triggerService.triggerEmailSend(A_BOOKING_ID, A_TEMPLATE_NAME, Collections.emptyMap(), rules);
        verify(triggerExecutor).execute(triggerInfo);
    }

    @Test(expectedExceptions = UncheckedIOException.class)
    public void shouldNotTriggerEmailSendBecauseBookingIsInvalid() throws IOException {
        triggerService.triggerEmailSend(AN_INVALID_BOOKING_ID, A_TEMPLATE_NAME, Collections.emptyMap(), rules);
        verify(triggerExecutor, never()).execute(triggerInfo);
    }

    @Test(expectedExceptions = IllegalArgumentException.class)
    public void shouldNotTriggerEmailSendBecauseEmailTemplateIsInvalid() throws IOException {
        triggerService.triggerEmailSend(A_BOOKING_ID, AN_INVALID_TEMPLATE_NAME, Collections.emptyMap(), rules);
        verify(triggerExecutor, never()).execute(triggerInfo);
    }

    @Test
    public void shouldExcludeIgnoredEmailTypes() throws IOException {
        when(emailTypeCalculator.getEmailTypeFromTemplateName(EmailType.CHANGE_REQUESTED.getTemplateName())).thenReturn(EmailType.CHANGE_REQUESTED);
        when(triggerServiceRulesValidator.validate(triggerInfo, rules)).thenReturn(ExecutionResult.MUST_BE_IGNORED);
        triggerService.triggerEmailSend(A_BOOKING_ID, EmailType.CHANGE_REQUESTED.getTemplateName(), Collections.emptyMap(), rules);
        verify(triggerExecutor, never()).execute(triggerInfo);
    }

    @Test
    public void shouldIncludeAdditionalParameters() {
        Map<String, Object> additionalParameters = Collections.singletonMap("param1", "value1");
        triggerService.triggerEmailSend(A_BOOKING_ID, A_TEMPLATE_NAME, additionalParameters, rules);
        verify(triggerInfoFactory).constructTriggerInfo(any(BookingDetail.class), any(EmailType.class), any(BookingProducts.class), any(EmailRequester.class), eq(additionalParameters));
    }

    @Test
    public void testExecutorWithEmailProvided() throws IOException {
        when(triggerExecutor.execute(triggerInfo)).thenReturn(ExecutionResult.OK);
        ExecutionResult executionResult = triggerService.execute(KEY, rules, EmailRequester.UPDATE, EmailType.WELCOME_TO_PRIME, Collections.emptyMap());
        assertEquals(executionResult, ExecutionResult.OK);
    }
}
