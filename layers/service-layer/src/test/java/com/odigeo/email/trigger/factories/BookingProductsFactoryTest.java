package com.odigeo.email.trigger.factories;

import com.edreams.configuration.ConfigurationEngine;
import com.odigeo.bookingapi.mock.v12.response.BookingDetailBuilder;
import com.odigeo.bookingapi.mock.v12.response.BuilderException;
import com.odigeo.bookingapi.v12.responses.BookingDetail;
import com.odigeo.email.trigger.booking.builder.BookingProductsBuilder;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import java.util.Random;

import static org.testng.Assert.assertEquals;

public class BookingProductsFactoryTest {

    private BookingProductsFactory bookingProductsFactory = new BookingProductsFactory();

    @BeforeMethod
    public void setup() {
        ConfigurationEngine.init(binder -> binder.bind(BookingProductsFactory.class).toInstance(bookingProductsFactory));
    }

    @Test
    public void shouldCreateBookingProducts() throws BuilderException {
        BookingDetail bookingDetail = new BookingDetailBuilder().build(new Random());
        assertEquals(bookingProductsFactory.create(bookingDetail), new BookingProductsBuilder(bookingDetail).build());
    }
}
