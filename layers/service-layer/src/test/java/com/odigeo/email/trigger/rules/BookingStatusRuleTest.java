package com.odigeo.email.trigger.rules;

import com.edreams.configuration.ConfigurationEngine;
import com.odigeo.bookingapi.mock.v12.response.BookingDetailBuilder;
import com.odigeo.bookingapi.mock.v12.response.BuilderException;
import com.odigeo.bookingapi.v12.requests.Status;
import com.odigeo.bookingapi.v12.responses.BookingDetail;
import com.odigeo.email.render.v1.contract.EmailType;
import com.odigeo.email.trigger.trigger.EmailRequester;
import com.odigeo.email.trigger.trigger.TriggerInfo;
import org.mockito.MockitoAnnotations;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import java.util.Collections;
import java.util.Random;

public class BookingStatusRuleTest {

    private BookingStatusRule bookingStatusRule;
    private BookingDetail bookingDetail;
    private TriggerInfo triggerInfo;

    @BeforeMethod
    public void init() throws BuilderException {
        MockitoAnnotations.initMocks(this);
        ConfigurationEngine.init();
        bookingStatusRule = new BookingStatusRule();
        bookingDetail = new BookingDetailBuilder().build(new Random());
    }

    @Test(expectedExceptions = TriggerRuleException.class)
    public void invalidStatusShouldReturnException() {
        bookingDetail.setBookingStatus(Status.FINAL_RET.name());
        triggerInfo = new TriggerInfo(bookingDetail, EmailType.CONFIRMATION, null, EmailRequester.UPDATE, Collections.emptyMap());
        bookingStatusRule.validate(triggerInfo);
    }

    @Test(expectedExceptions = TriggerRuleException.class)
    public void koAndNotDidNotBuyShouldReturnException() {
        bookingDetail.setBookingStatus(Status.REQUEST.name());
        triggerInfo = new TriggerInfo(bookingDetail, EmailType.KO, null, EmailRequester.UPDATE, Collections.emptyMap());
        bookingStatusRule.validate(triggerInfo);
    }

    @Test
    public void bookingStatusValidShouldNotReturnException() {
        bookingDetail.setBookingStatus(Status.DIDNOTBUY.name());
        triggerInfo = new TriggerInfo(bookingDetail, EmailType.KO, null, EmailRequester.UPDATE, Collections.emptyMap());
        bookingStatusRule.validate(triggerInfo);
    }
}

