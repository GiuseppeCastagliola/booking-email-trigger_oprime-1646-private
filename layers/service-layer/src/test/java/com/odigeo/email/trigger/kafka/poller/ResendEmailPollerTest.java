package com.odigeo.email.trigger.kafka.poller;

import com.edreams.configuration.ConfigurationEngine;
import org.testng.Assert;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

public class ResendEmailPollerTest {
    private ResendEmailPoller resendEmailPoller;

    @BeforeMethod
    public void setUp() {
        ConfigurationEngine.init();
    }

    @Test
    public void test() {
        resendEmailPoller = ConfigurationEngine.getInstance(ResendEmailPoller.class);
        Assert.assertNotNull(resendEmailPoller);
    }

}
