package com.odigeo.email.trigger.rules;

import org.testng.annotations.Test;

import java.util.Collections;

import static org.testng.Assert.assertEquals;

public class CancellationSendingRulesTest {

    private CancellationSendingRules cancellationSendingRules = new CancellationSendingRules();

    @Test
    public void shouldReturnOnlyIgnoredEmailTypesRule() {
        assertEquals(cancellationSendingRules.get(), Collections.singletonList(new IgnoredEmailTypesRule()));
    }

}
