package com.odigeo.email.trigger.rules;

import org.testng.annotations.Test;

import java.util.List;

import static org.testng.Assert.assertEquals;

public class OnDemandSendingRulesTest {

    private OnDemandSendingRules onDemandSendingRules = new OnDemandSendingRules();

    @Test
    public void shouldReturnOnlyIgnoredEmailTypesRule() {
        List<TriggerRule> triggerRules = onDemandSendingRules.get();
        assertEquals(triggerRules.size(), 1);
        assertEquals(triggerRules.get(0).getClass(), TopUpVoucherRule.class);
    }
}
