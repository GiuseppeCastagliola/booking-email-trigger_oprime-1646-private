package com.odigeo.email.trigger.rules;

import com.odigeo.bookingapi.mock.v12.response.BookingBasicInfoBuilder;
import com.odigeo.bookingapi.mock.v12.response.BookingDetailBuilder;
import com.odigeo.bookingapi.mock.v12.response.BuilderException;
import com.odigeo.bookingapi.v12.responses.AccommodationBooking;
import com.odigeo.bookingapi.v12.responses.BookingDetail;
import com.odigeo.email.trigger.trigger.TriggerInfo;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import java.util.Random;

public class ItineraryOrAccommodationRuleTest {

    private ItineraryOrAccommodationRule itineraryOrAccommodationRule;

    @BeforeMethod
    public void init() {
        itineraryOrAccommodationRule = new ItineraryOrAccommodationRule();
    }

    @Test
    public void hasItineraryBookingShouldPass() throws BuilderException {
        BookingDetail bookingDetail = new BookingDetailBuilder().bookingBasicInfoBuilder(new BookingBasicInfoBuilder()).build(new Random());
        TriggerInfo triggerInfo = new TriggerInfo.Builder().withBookingDetail(bookingDetail).build();
        itineraryOrAccommodationRule.validate(triggerInfo);
    }

    @Test
    public void hasAccommodationBookingShouldPass() throws BuilderException {
        BookingDetail bookingDetail = new BookingDetailBuilder().bookingBasicInfoBuilder(new BookingBasicInfoBuilder()).build(new Random());
        TriggerInfo triggerInfo = new TriggerInfo.Builder().withBookingDetail(bookingDetail).build();
        triggerInfo.getBookingDetail().getBookingProducts().clear();
        triggerInfo.getBookingDetail().getBookingProducts().add(new AccommodationBooking());
        itineraryOrAccommodationRule.validate(triggerInfo);
    }

    @Test(expectedExceptions = TriggerRuleException.class)
    public void hasNoItineraryBookingOrAccommodationShouldReturnException() throws BuilderException {
        BookingDetail bookingDetail = new BookingDetailBuilder().bookingBasicInfoBuilder(new BookingBasicInfoBuilder()).build(new Random());
        TriggerInfo triggerInfo = new TriggerInfo.Builder().withBookingDetail(bookingDetail).build();
        triggerInfo.getBookingDetail().getBookingProducts().clear();
        itineraryOrAccommodationRule.validate(triggerInfo);
    }
}
