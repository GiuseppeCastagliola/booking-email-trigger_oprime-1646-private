package com.odigeo.email.trigger.rules;

import com.edreams.configuration.ConfigurationEngine;
import com.google.inject.AbstractModule;
import com.odigeo.bookingapi.mock.v12.response.BookingDetailBuilder;
import com.odigeo.bookingapi.mock.v12.response.BuilderException;
import com.odigeo.bookingapi.v12.responses.BookingDetail;
import com.odigeo.email.render.v1.contract.EmailType;
import com.odigeo.email.trigger.email.TriggerHelper;
import com.odigeo.email.trigger.trigger.EmailRequester;
import com.odigeo.email.trigger.trigger.TriggerInfo;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import java.util.Collections;
import java.util.Random;

import static org.mockito.Mockito.when;

public class DepartureDateRuleTest {

    private DepartureDateRule departureDateRule;

    private TriggerInfo triggerInfo;

    private BookingDetail bookingDetail;

    @Mock
    private TriggerHelper triggerHelper;


    @BeforeMethod
    public void init() throws BuilderException {
        MockitoAnnotations.initMocks(this);
        ConfigurationEngine.init(new AbstractModule() {
            @Override
            protected void configure() {
                bind(TriggerHelper.class).toInstance(triggerHelper);
            }
        });
        departureDateRule = new DepartureDateRule(triggerHelper);
        bookingDetail = new BookingDetailBuilder().build(new Random());
    }

    @Test
    public void validateShouldNotReturnException() {
        when(triggerHelper.isDepartureDatePast(bookingDetail)).thenReturn(false);
        triggerInfo = new TriggerInfo(bookingDetail, EmailType.CONFIRMATION, null, EmailRequester.UPDATE, Collections.emptyMap());
        departureDateRule.validate(triggerInfo);
    }

    @Test(expectedExceptions = TriggerRuleException.class)
    public void validateShouldReturnException() {
        when(triggerHelper.isDepartureDatePast(bookingDetail)).thenReturn(true);
        triggerInfo = new TriggerInfo(bookingDetail, EmailType.CONFIRMATION, null, EmailRequester.UPDATE, Collections.emptyMap());
        departureDateRule.validate(triggerInfo);
    }
}

