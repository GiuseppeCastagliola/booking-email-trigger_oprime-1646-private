package com.odigeo.email.trigger.membership.kafka.consumer;

import com.edreams.configuration.ConfigurationEngine;
import com.odigeo.commons.messaging.KafkaConsumer;
import com.odigeo.email.trigger.kafka.consumer.ConsumerFactory;
import com.odigeo.email.trigger.membership.kafka.MembershipMailerMessageProcessor;
import com.odigeo.membership.message.MembershipMailerMessage;
import org.mockito.Mock;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import static com.odigeo.email.trigger.membership.kafka.consumer.MembershipMailerMessageConsumer.MEMBERSHIP_ACTIVATIONS_KAFKA_TOPIC;
import static com.odigeo.email.trigger.membership.kafka.consumer.MembershipMailerMessageConsumer.MESSAGE_GROUP;
import static com.odigeo.email.trigger.membership.kafka.consumer.MembershipMailerMessageConsumer.THREADS_COUNT;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static org.mockito.MockitoAnnotations.initMocks;

public class MembershipMailerMessageConsumerTest {

    @Mock
    ConsumerFactory consumerFactory;

    @Mock
    KafkaConsumer<MembershipMailerMessage> consumer;

    @Mock
    MembershipMailerMessageProcessor messageProcessor;

    private MembershipMailerMessageConsumer membershipMailerMessageConsumer;

    @BeforeMethod
    public void setup() {
        initMocks(this);
        when(consumerFactory.newConsumer(MEMBERSHIP_ACTIVATIONS_KAFKA_TOPIC, MESSAGE_GROUP, MembershipMailerMessage.class)).thenReturn(consumer);
        ConfigurationEngine.init(binder -> binder.bind(ConsumerFactory.class).toInstance(consumerFactory));
        membershipMailerMessageConsumer = new MembershipMailerMessageConsumer(messageProcessor, consumerFactory);
    }

    @Test
    public void launchShouldStartConsumer() {
        membershipMailerMessageConsumer.launch();
        verify(consumerFactory).newConsumer(MEMBERSHIP_ACTIVATIONS_KAFKA_TOPIC, MESSAGE_GROUP, MembershipMailerMessage.class);
        verify(consumer).start(messageProcessor, THREADS_COUNT);
    }
}
