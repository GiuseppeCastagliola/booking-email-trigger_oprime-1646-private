package com.odigeo.email.trigger.trigger;

import com.edreams.configuration.ConfigurationEngine;
import com.odigeo.bookingapi.mock.v12.response.BuilderException;
import com.odigeo.email.trigger.render.RenderInfo;
import com.odigeo.email.trigger.render.RenderServiceClient;
import com.odigeo.email.trigger.render.testbuilder.RenderInfoTestDataBuilder;
import com.odigeo.email.trigger.trigger.testbuilder.TriggerInfoTestDataBuilder;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import java.io.IOException;

import static org.mockito.Mockito.never;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static org.testng.Assert.assertEquals;

public class TriggerExecutorTestBookings {

    @Mock
    private RenderServiceClient renderServiceClient;
    @Mock
    private CustomerEmailTrigger customerEmailTrigger;

    private TriggerInfo triggerInfo;
    private TriggerExecutor triggerExecutor;

    @BeforeMethod
    public void init() throws BuilderException {
        ConfigurationEngine.init();
        MockitoAnnotations.initMocks(this);
        triggerInfo = TriggerInfoTestDataBuilder.aTriggerInfo().build();
        triggerExecutor = new TriggerExecutor(renderServiceClient, customerEmailTrigger);
    }

    @Test
    public void testExecuteOK() throws IOException {
        ExecutionResult executionResult = ExecutionResult.OK;
        RenderInfo renderInfo = RenderInfoTestDataBuilder.aRenderInfo().build();
        when(renderServiceClient.render(triggerInfo)).thenReturn(renderInfo);
        when(customerEmailTrigger.execute(triggerInfo, renderInfo)).thenReturn(executionResult);
        ExecutionResult actual = triggerExecutor.execute(triggerInfo);
        verify(renderServiceClient).render(triggerInfo);
        verify(customerEmailTrigger).execute(triggerInfo, renderInfo);
        assertEquals(actual, executionResult);
    }

    @Test(expectedExceptions = IOException.class)
    public void testExecuteRenderException() throws IOException {
        ExecutionResult executionResult = ExecutionResult.KO_RECOVERABLE;
        RenderInfo renderInfo = RenderInfoTestDataBuilder.aRenderInfo().build();
        when(renderServiceClient.render(triggerInfo)).thenThrow(new IOException());
        triggerExecutor.execute(triggerInfo);
        verify(renderServiceClient).render(triggerInfo);
        verify(customerEmailTrigger, never()).execute(triggerInfo, renderInfo);
    }

    @Test
    public void testExecuteCustomerTriggerNotOK() throws IOException {
        ExecutionResult executionResult = ExecutionResult.KO_RECOVERABLE;
        RenderInfo renderInfo = RenderInfoTestDataBuilder.aRenderInfo().build();
        when(renderServiceClient.render(triggerInfo)).thenReturn(renderInfo);
        when(customerEmailTrigger.execute(triggerInfo, renderInfo)).thenReturn(executionResult);
        ExecutionResult actual = triggerExecutor.execute(triggerInfo);
        verify(renderServiceClient).render(triggerInfo);
        verify(customerEmailTrigger).execute(triggerInfo, renderInfo);
        assertEquals(actual, executionResult);
    }

    @Test
    public void testExecuteCopyTriggerNotOK() throws IOException {
        RenderInfo renderInfo = RenderInfoTestDataBuilder.aRenderInfo().build();
        when(renderServiceClient.render(triggerInfo)).thenReturn(renderInfo);
        when(customerEmailTrigger.execute(triggerInfo, renderInfo)).thenReturn(ExecutionResult.OK);
        ExecutionResult actual = triggerExecutor.execute(triggerInfo);
        verify(renderServiceClient).render(triggerInfo);
        verify(customerEmailTrigger).execute(triggerInfo, renderInfo);
    }

}
