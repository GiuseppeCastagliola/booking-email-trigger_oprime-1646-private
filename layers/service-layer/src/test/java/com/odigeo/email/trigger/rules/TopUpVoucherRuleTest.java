package com.odigeo.email.trigger.rules;

import com.odigeo.email.render.v1.contract.EmailType;
import com.odigeo.email.trigger.trigger.TriggerInfo;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import java.util.Collections;

import static org.mockito.Mockito.when;

public class TopUpVoucherRuleTest {

    private TopUpVoucherRule topUpVoucherRule = new TopUpVoucherRule();

    @Mock
    private TriggerInfo triggerInfo;

    @BeforeMethod
    public void init() {
        MockitoAnnotations.initMocks(this);
        when(triggerInfo.getEmailType()).thenReturn(EmailType.TOP_UP_VOUCHER_CREATED);
    }

    @Test
    public void shouldNotThrowException() {
        when(triggerInfo.getAdditionalParameters()).thenReturn(Collections.singletonMap("voucherId", 2345));
        topUpVoucherRule.validate(triggerInfo);
    }

    @Test(expectedExceptions = TriggerRuleException.class)
    public void shouldThrowException() {
        when(triggerInfo.getAdditionalParameters()).thenReturn(Collections.emptyMap());
        topUpVoucherRule.validate(triggerInfo);
    }
}
