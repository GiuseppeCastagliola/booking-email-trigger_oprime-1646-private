package com.odigeo.email.trigger.kafka;

import com.edreams.configuration.ConfigurationEngine;
import com.edreams.util.date.LocalizedDate;
import com.google.inject.AbstractModule;
import com.odigeo.email.trigger.kafka.processor.RecoveryProcessor;
import com.odigeo.email.trigger.kafka.pusher.RecoveryPusher;
import com.odigeo.email.trigger.trigger.EmailRequester;
import com.odigeo.email.trigger.trigger.ExecutionResult;
import com.odigeo.email.trigger.trigger.TriggerService;
import com.odigeo.email.trigger.util.Constants;
import com.odigeo.messaging.utils.BasicMessage;
import com.odigeo.messaging.utils.ConsumerIterator;
import com.odigeo.messaging.utils.MessageDataAccessException;
import com.odigeo.messaging.utils.MessageParserException;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import java.io.IOException;

import static org.mockito.Matchers.any;
import static org.mockito.Mockito.doThrow;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

public class RecoveryProcessorTest {

    @Mock
    private TriggerService triggerService;
    @Mock
    private ConsumerIterator<BasicMessage> consumerIterator;
    @Mock
    private RecoveryPusher recoveryPusher;

    private final BasicMessage basicMessage = new BasicMessage();
    private RecoveryProcessor recoveryProcessor;

    @BeforeMethod
    public void setUp() throws MessageDataAccessException, MessageParserException {
        MockitoAnnotations.initMocks(this);
        ConfigurationEngine.init(new AbstractModule() {
            @Override
            protected void configure() {
                bind(TriggerService.class).toInstance(triggerService);
                bind(RecoveryPusher.class).toInstance(recoveryPusher);
            }
        });
        basicMessage.setKey("key");
        basicMessage.addExtraParameter(Constants.ATTEMPT, "1");
        basicMessage.addExtraParameter(Constants.TIMESTAMP, LocalizedDate.getInstance().addTimeMinutes(-9).addTimeSeconds(-59).addTimeMilliseconds(-100).convertToString(Constants.DATE_FORMAT));
        recoveryProcessor = new RecoveryProcessor(consumerIterator);
        when(consumerIterator.hasNext()).thenReturn(true).thenReturn(false);
        when(consumerIterator.next()).thenReturn(basicMessage);
    }

    @Test
    public void testRun() throws IOException {
        basicMessage.addExtraParameter(Constants.ATTEMPT, "18");
        when(triggerService.execute(basicMessage.getKey(), RecoveryProcessor.getRecoveryRules(), EmailRequester.RECOVERY)).thenReturn(ExecutionResult.OK);
        recoveryProcessor.run();
        verify(triggerService, times(1)).execute(basicMessage.getKey(), RecoveryProcessor.getRecoveryRules(), EmailRequester.RECOVERY);
        verify(recoveryPusher, times(0)).publishMessage(any(), any());

    }

    @Test
    public void testRunTriggerException() throws IOException {
        when(triggerService.execute(basicMessage.getKey(), RecoveryProcessor.getRecoveryRules(), EmailRequester.RECOVERY)).thenThrow(new RuntimeException());
        recoveryProcessor.run();
        verify(triggerService, times(1)).execute(basicMessage.getKey(), RecoveryProcessor.getRecoveryRules(), EmailRequester.RECOVERY);
        verify(recoveryPusher, times(0)).publishMessage(any(), any());

    }

    @Test
    public void testMaxAttempts() throws IOException {
        basicMessage.addExtraParameter(Constants.ATTEMPT, "19");
        recoveryProcessor.run();
        verify(triggerService, times(0)).execute(basicMessage.getKey(), RecoveryProcessor.getRecoveryRules(), EmailRequester.RECOVERY);
        verify(recoveryPusher, times(0)).publishMessage(any(), any());
    }

    @Test
    public void testWaitTime() throws IOException {
        basicMessage.addExtraParameter(Constants.TIMESTAMP, LocalizedDate.getInstance().addTimeMinutes(-10).addTimeSeconds(-59).addTimeMilliseconds(-100).convertToString(Constants.DATE_FORMAT));
        when(triggerService.execute(basicMessage.getKey(), RecoveryProcessor.getRecoveryRules(), EmailRequester.RECOVERY)).thenReturn(ExecutionResult.OK);
        recoveryProcessor.run();
        verify(triggerService, times(1)).execute(basicMessage.getKey(), RecoveryProcessor.getRecoveryRules(), EmailRequester.RECOVERY);
        verify(recoveryPusher, times(0)).publishMessage(any(), any());
    }

    @Test
    public void testMessageDataAccessException() throws MessageDataAccessException, IOException {
        doThrow(new MessageDataAccessException("error", new Exception())).when(consumerIterator).hasNext();
        recoveryProcessor.run();
        verify(triggerService, times(0)).execute(basicMessage.getKey(), RecoveryProcessor.getRecoveryRules(), EmailRequester.RECOVERY);
        verify(recoveryPusher, times(0)).publishMessage(any(), any());
    }

    @Test
    public void testMessageParserException() throws MessageDataAccessException, MessageParserException, IOException {
        doThrow(new MessageParserException("error", new Exception(), "error")).when(consumerIterator).next();
        recoveryProcessor.run();
        verify(triggerService, times(0)).execute(basicMessage.getKey(), RecoveryProcessor.getRecoveryRules(), EmailRequester.RECOVERY);
        verify(recoveryPusher, times(0)).publishMessage(any(), any());
    }
}
