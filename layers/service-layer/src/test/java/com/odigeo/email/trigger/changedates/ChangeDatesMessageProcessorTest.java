package com.odigeo.email.trigger.changedates;

import com.edreams.configuration.ConfigurationEngine;
import com.google.inject.AbstractModule;
import com.odigeo.bookingchange.message.ChangeDatesStatusUpdatedMessage;
import com.odigeo.bookingchange.message.UserBookingChangeRequestStatus;
import com.odigeo.email.trigger.factories.RulesFactory;
import com.odigeo.email.trigger.rules.CancellationSendingRules;
import com.odigeo.email.trigger.rules.RulesInterface;
import com.odigeo.messaging.utils.ConsumerIterator;
import com.odigeo.messaging.utils.MessageDataAccessException;
import com.odigeo.messaging.utils.MessageParserException;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import java.io.IOException;

import static org.mockito.Mockito.never;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

public class ChangeDatesMessageProcessorTest {


    private static final RulesInterface SOME_RULES = new CancellationSendingRules();
    private ChangeDatesMessageProcessor changeDatesMessageProcessor;

    @Mock
    private ConsumerIterator<ChangeDatesStatusUpdatedMessage> consumerIterator;
    @Mock
    private ChangeDatesController changeDatesController;
    @Mock
    private RulesFactory rulesFactory;

    private ChangeDatesStatusUpdatedMessage changeDatesStatusUpdatedMessage;

    @BeforeMethod
    public void init() throws MessageDataAccessException, MessageParserException {
        MockitoAnnotations.initMocks(this);
        ConfigurationEngine.init(new AbstractModule() {
            @Override
            protected void configure() {
                bind(ChangeDatesController.class).toInstance(changeDatesController);
                bind(RulesFactory.class).toInstance(rulesFactory);
            }
        });
        changeDatesMessageProcessor = new ChangeDatesMessageProcessor(consumerIterator);
        changeDatesStatusUpdatedMessage = new ChangeDatesStatusUpdatedMessage();

        when(consumerIterator.next()).thenReturn(changeDatesStatusUpdatedMessage);
        when(consumerIterator.hasNext()).thenReturn(true).thenReturn(false);
        when(rulesFactory.getCancellationEmailRules()).thenReturn(SOME_RULES);
    }

    @Test
    public void shouldCallOnMessageWhenStatusRequested() throws IOException {
        changeDatesStatusUpdatedMessage.setRequestStatus(UserBookingChangeRequestStatus.REQUESTED);
        changeDatesMessageProcessor.run();

        verify(changeDatesController).onMessage(changeDatesStatusUpdatedMessage, SOME_RULES);
    }

    @Test
    public void shouldCallOnMessageWhenStatusProcessing() throws IOException {
        changeDatesStatusUpdatedMessage.setRequestStatus(UserBookingChangeRequestStatus.PROCESSING);
        changeDatesMessageProcessor.run();

        verify(changeDatesController).onMessage(changeDatesStatusUpdatedMessage, SOME_RULES);
    }

    @Test
    public void shouldNotCallOnMessageWhenStatusAgentReview() throws IOException {
        changeDatesStatusUpdatedMessage.setRequestStatus(UserBookingChangeRequestStatus.AGENT_REVIEW);
        changeDatesMessageProcessor.run();

        verify(changeDatesController, never()).onMessage(changeDatesStatusUpdatedMessage, SOME_RULES);
    }

    @Test
    public void shouldNotCallOnMessageWhenStatusPrioritizing() throws IOException {
        changeDatesStatusUpdatedMessage.setRequestStatus(UserBookingChangeRequestStatus.PRIORITISING);
        changeDatesMessageProcessor.run();

        verify(changeDatesController, never()).onMessage(changeDatesStatusUpdatedMessage, SOME_RULES);
    }

    @Test
    public void shouldNotCallOnMessageWhenStatusAirlineRequested() throws IOException {
        changeDatesStatusUpdatedMessage.setRequestStatus(UserBookingChangeRequestStatus.AIRLINE_REQUESTED);
        changeDatesMessageProcessor.run();

        verify(changeDatesController, never()).onMessage(changeDatesStatusUpdatedMessage, SOME_RULES);
    }

    @Test
    public void shouldNotCallOnMessageWhenStatusAirlinePending() throws IOException {
        changeDatesStatusUpdatedMessage.setRequestStatus(UserBookingChangeRequestStatus.AIRLINE_PENDING);
        changeDatesMessageProcessor.run();

        verify(changeDatesController, never()).onMessage(changeDatesStatusUpdatedMessage, SOME_RULES);
    }
}
