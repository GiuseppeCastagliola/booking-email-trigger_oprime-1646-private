package com.odigeo.commons.monitoring.metrics;

import com.odigeo.email.trigger.trigger.ExecutionResult;
import org.testng.Assert;
import org.testng.annotations.Test;

public class MetricsBuilderTest {

    @Test
    public void testBuildMetric() {
        Assert.assertNotNull(MetricsBuilder.buildTriggerMetric(MetricsConstants.BOOKING_UPDATES, ExecutionResult.OK));
    }
}
