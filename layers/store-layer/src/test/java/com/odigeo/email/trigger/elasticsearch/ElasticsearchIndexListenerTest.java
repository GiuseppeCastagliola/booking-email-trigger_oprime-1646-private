package com.odigeo.email.trigger.elasticsearch;

import com.odigeo.email.render.v1.contract.EmailType;
import org.elasticsearch.action.index.IndexResponse;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import static org.mockito.Mockito.verify;

public class ElasticsearchIndexListenerTest {

    @Mock
    private IndexResponse indexResponse;
    @Mock
    private Exception exception;

    private ElasticsearchIndexListener elasticsearchIndexListener;

    @BeforeClass
    public void init() {
        MockitoAnnotations.initMocks(this);
        elasticsearchIndexListener = new ElasticsearchIndexListener("index", 12131231L, EmailType.CONFIRMATION);
    }

    @Test
    public void testOnResponse() {
        elasticsearchIndexListener.onResponse(indexResponse);
        verify(indexResponse).status();
    }

    @Test
    public void testOnFailure() {
        elasticsearchIndexListener.onFailure(exception);
        verify(exception).getMessage();
    }

}
