package com.odigeo.email.trigger.tracking;

import com.edreams.configuration.ConfigurationEngine;
import com.odigeo.bookingapi.mock.v12.response.BuilderException;
import com.odigeo.email.render.v1.contract.EmailType;
import com.odigeo.email.trigger.elasticsearch.ElasticsearchClient;
import com.odigeo.email.trigger.elasticsearch.ElasticsearchClientFactory;
import com.odigeo.email.trigger.elasticsearch.ElasticsearchIndexListener;
import com.odigeo.email.trigger.email.CustomerEmailTagManager;
import com.odigeo.email.trigger.render.RenderInfo;
import com.odigeo.email.trigger.render.testbuilder.RenderInfoTestDataBuilder;
import com.odigeo.email.trigger.trigger.TriggerInfo;
import com.odigeo.email.trigger.trigger.testbuilder.TriggerInfoTestDataBuilder;
import com.odigeo.email.trigger.util.Constants;
import org.elasticsearch.action.index.IndexRequest;
import org.elasticsearch.action.search.SearchRequest;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import java.io.IOException;

import static org.mockito.Matchers.any;
import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

public class CustomerEmailStoreTest {

    @Mock
    private ElasticsearchClientFactory elasticsearchClientFactory;
    @Mock
    private ElasticsearchClient elasticsearchClient;

    private TriggerInfo triggerInfo;
    private RenderInfo renderInfo;
    private CustomerEmailStore customerEmailStore;
    private CustomerEmailTagManager customerEmailTagManager;

    @BeforeMethod
    public void setUp() throws BuilderException {
        MockitoAnnotations.initMocks(this);
        customerEmailTagManager = ConfigurationEngine.getInstance(CustomerEmailTagManager.class);
        when(elasticsearchClientFactory.createClient()).thenReturn(elasticsearchClient);
        customerEmailStore = new CustomerEmailStore(elasticsearchClientFactory, customerEmailTagManager);
        triggerInfo = TriggerInfoTestDataBuilder.aTriggerInfo().build();
        renderInfo = RenderInfoTestDataBuilder.aRenderInfo().build();
    }

    @Test
    public void testIndexAsync() throws IOException {
        ElasticsearchIndexListener elasticsearchIndexListener = new ElasticsearchIndexListener(Constants.TRIGGER_INDEX, 1234567890L, EmailType.CONFIRMATION);
        customerEmailStore.indexAsync(triggerInfo, renderInfo, elasticsearchIndexListener);
        verify(elasticsearchClient).indexAsync(any(IndexRequest.class), eq(elasticsearchIndexListener));
    }

    @Test
    public void testIndexSync() throws IOException {
        customerEmailStore.indexSync(triggerInfo, renderInfo);
        verify(elasticsearchClient).index(any(IndexRequest.class));
    }

    @Test
    public void testSearchPreviouslyTriggered() throws IOException {
        customerEmailStore.searchPreviouslyTriggered(1234567890L, EmailType.CONFIRMATION);
        verify(elasticsearchClient).search(any(SearchRequest.class));
    }


}
