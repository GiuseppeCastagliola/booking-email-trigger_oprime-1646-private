package com.odigeo.email.trigger.tracking;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.odigeo.bookingapi.mock.v12.response.BuilderException;
import com.odigeo.email.render.v1.contract.EmailType;
import com.odigeo.email.trigger.booking.product.BookingProducts;
import com.odigeo.email.trigger.booking.testbuilder.BookingProductsTestDataBuilder;
import com.odigeo.email.trigger.render.RenderInfo;
import com.odigeo.email.trigger.render.testbuilder.RenderInfoTestDataBuilder;
import com.odigeo.email.trigger.trigger.TriggerInfo;
import com.odigeo.email.trigger.trigger.testbuilder.TriggerInfoTestDataBuilder;
import com.odigeo.email.trigger.util.Constants;
import org.elasticsearch.action.search.SearchResponse;
import org.elasticsearch.common.bytes.BytesReference;
import org.elasticsearch.common.xcontent.XContentBuilder;
import org.elasticsearch.common.xcontent.json.JsonXContent;
import org.elasticsearch.search.SearchHit;
import org.elasticsearch.search.SearchHits;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import java.io.IOException;
import java.util.Collections;
import java.util.Optional;

import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.doThrow;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static org.testng.Assert.assertNotNull;
import static org.testng.AssertJUnit.assertEquals;
import static org.testng.AssertJUnit.assertFalse;
import static org.testng.AssertJUnit.assertTrue;

public class CustomerEmailManagerTest {

    @Mock
    private CustomerEmailStore customerEmailStore;
    @Mock
    private SearchResponse searchResponse;

    private static final long BOOKING_ID = 1234567890L;
    private static final EmailType EMAIL_TYPE = EmailType.CONFIRMATION;
    private static final EmailType WELCOME_TO_PRIME_EMAIL_TYPE = EmailType.WELCOME_TO_PRIME;
    private SearchHits searchHits;
    private TriggerInfo triggerInfo;
    private RenderInfo renderInfo;
    private CustomerEmailManager customerEmailManager;

    @BeforeMethod
    public void init() throws BuilderException {
        MockitoAnnotations.initMocks(this);
        triggerInfo = TriggerInfoTestDataBuilder.aTriggerInfo().build();
        renderInfo = RenderInfoTestDataBuilder.aRenderInfo().build();
        customerEmailManager = new CustomerEmailManager(customerEmailStore);
    }

    @Test
    public void testIndex() throws IOException {
        customerEmailManager.index(triggerInfo, renderInfo);
        verify(customerEmailStore).indexSync(eq(triggerInfo), eq(renderInfo));
    }

    @Test
    public void testIndexIOException() throws IOException {
        doThrow(new IOException()).when(customerEmailStore).indexSync(eq(triggerInfo), eq(renderInfo));
        customerEmailManager.index(triggerInfo, renderInfo);
        verify(customerEmailStore).indexSync(eq(triggerInfo), eq(renderInfo));
    }

    @Test
    public void testSearchBookingProducts() throws IOException {
        BookingProducts bookingProducts = BookingProductsTestDataBuilder.abookingProducts().build();
        XContentBuilder xContentBuilder = JsonXContent.contentBuilder().startObject().field(Constants.BOOKING_PRODUCTS, new ObjectMapper().writeValueAsString(bookingProducts)).endObject();
        SearchHit searchHit = SearchHit.createFromMap(Collections.singletonMap("_source", BytesReference.bytes(xContentBuilder)));
        SearchHit[] hits = {searchHit};
        searchHits = new SearchHits(hits, 1, 1);
        when(searchResponse.getHits()).thenReturn(searchHits);
        when(customerEmailStore.searchPreviouslyTriggered(BOOKING_ID, EMAIL_TYPE)).thenReturn(searchResponse);
        Optional<BookingProducts> searchResponse = customerEmailManager.searchBookingProducts(BOOKING_ID, EMAIL_TYPE);
        verify(customerEmailStore).searchPreviouslyTriggered(BOOKING_ID, EMAIL_TYPE);
        assertTrue(searchResponse.isPresent());
        assertEquals(searchResponse.get(), bookingProducts);
    }

    @Test
    public void testSearchBookingProductsZeroHits() throws IOException {
        zeroHitsResponse();
        when(customerEmailStore.searchPreviouslyTriggered(BOOKING_ID, EMAIL_TYPE)).thenReturn(searchResponse);
        Optional<BookingProducts> searchResponse = customerEmailManager.searchBookingProducts(BOOKING_ID, EMAIL_TYPE);
        verify(customerEmailStore).searchPreviouslyTriggered(BOOKING_ID, EMAIL_TYPE);
        assertFalse(searchResponse.isPresent());
    }

    @Test
    public void testSearchBookingProductsIOException() throws IOException {
        doThrow(new IOException()).when(customerEmailStore).searchPreviouslyTriggered(BOOKING_ID, EMAIL_TYPE);
        Optional<BookingProducts> searchResponse = customerEmailManager.searchBookingProducts(BOOKING_ID, EMAIL_TYPE);
        verify(customerEmailStore).searchPreviouslyTriggered(BOOKING_ID, EMAIL_TYPE);
        assertFalse(searchResponse.isPresent());
    }

    @Test
    public void shouldHaveZeroHitsResponseWhenWasNeverTriggered() throws IOException {
        zeroHitsResponse();
        when(customerEmailStore.searchPreviouslyTriggered(BOOKING_ID, WELCOME_TO_PRIME_EMAIL_TYPE)).thenReturn(searchResponse);
        Optional<SearchResponse> searchResponse = customerEmailManager.isWelcomeToPrimeNeverTriggered(BOOKING_ID);
        assertTrue(searchResponse.isPresent());
        assertEquals(0, searchResponse.get().getHits().totalHits);
        verify(customerEmailStore).searchPreviouslyTriggered(BOOKING_ID, WELCOME_TO_PRIME_EMAIL_TYPE);
    }

    @Test
    public void shouldReturnAnEmptyOptionalWhenResponseWasNotRetrieved() throws IOException {
        doThrow(new IOException()).when(customerEmailStore).searchPreviouslyTriggered(BOOKING_ID, WELCOME_TO_PRIME_EMAIL_TYPE);
        Optional<SearchResponse> searchResponse = customerEmailManager.isWelcomeToPrimeNeverTriggered(BOOKING_ID);
        assertNotNull(searchResponse);
        assertFalse(searchResponse.isPresent());
        verify(customerEmailStore).searchPreviouslyTriggered(BOOKING_ID, WELCOME_TO_PRIME_EMAIL_TYPE);
    }

    private void zeroHitsResponse() {
        searchHits = new SearchHits(new SearchHit[0], 0, 0);
        when(searchResponse.getHits()).thenReturn(searchHits);
    }
}
