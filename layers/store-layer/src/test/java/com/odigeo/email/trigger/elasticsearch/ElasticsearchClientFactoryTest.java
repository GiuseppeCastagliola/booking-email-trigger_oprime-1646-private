package com.odigeo.email.trigger.elasticsearch;

import com.edreams.configuration.ConfigurationEngine;
import com.odigeo.email.trigger.configuration.Configuration;
import org.testng.Assert;
import org.testng.annotations.Test;

public class ElasticsearchClientFactoryTest {

    @Test
    public void testCreateClient() {
        ConfigurationEngine.init();
        Configuration configuration = ConfigurationEngine.getInstance(Configuration.class);
        ElasticsearchClient elasticsearchClient = new ElasticsearchClientFactory(configuration).createClient();
        Assert.assertNotNull(elasticsearchClient);
    }
}
