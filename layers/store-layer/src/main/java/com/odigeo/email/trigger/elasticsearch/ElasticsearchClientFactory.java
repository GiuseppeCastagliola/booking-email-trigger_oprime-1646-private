package com.odigeo.email.trigger.elasticsearch;

import com.google.inject.Inject;
import com.google.inject.Singleton;
import com.odigeo.email.trigger.configuration.Configuration;
import com.odigeo.email.trigger.configuration.ElasticsearchConfiguration;
import org.apache.http.HttpHost;
import org.elasticsearch.client.RestClient;
import org.elasticsearch.client.RestHighLevelClient;

@Singleton
public class ElasticsearchClientFactory {

    private final ElasticsearchConfiguration configuration;

    @Inject
    public ElasticsearchClientFactory(Configuration configuration) {
        this.configuration = configuration.getElasticsearchConfiguration();
    }

    public ElasticsearchClient createClient() {
        RestHighLevelClient client = new RestHighLevelClient(
                RestClient.builder(
                        new HttpHost(
                                configuration.getHostName(),
                                configuration.getPort(),
                                configuration.getProtocol()))
        );
        return new ElasticsearchClient(client);
    }

}
