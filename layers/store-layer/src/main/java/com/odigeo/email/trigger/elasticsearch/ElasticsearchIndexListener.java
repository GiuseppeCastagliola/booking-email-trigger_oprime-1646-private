package com.odigeo.email.trigger.elasticsearch;

import com.odigeo.email.render.v1.contract.EmailType;
import com.odigeo.email.trigger.tracking.CustomerEmailManager;
import com.odigeo.email.trigger.util.Constants;
import org.apache.log4j.Logger;
import org.elasticsearch.action.ActionListener;
import org.elasticsearch.action.index.IndexResponse;

public class ElasticsearchIndexListener implements ActionListener<IndexResponse> {
    private static final Logger LOGGER = Logger.getLogger(CustomerEmailManager.class);

    private final String index;
    private final long bookingId;
    private final EmailType emailType;

    public ElasticsearchIndexListener(String index, long bookingId, EmailType emailType) {
        this.index = index;
        this.bookingId = bookingId;
        this.emailType = emailType;
    }

    @Override
    public void onResponse(IndexResponse indexResponse) {
        LOGGER.info("Indexed " + Constants.LOG_SEPARATOR + index + Constants.LOG_SEPARATOR + bookingId + Constants.LOG_SEPARATOR + emailType + Constants.LOG_SEPARATOR + indexResponse.status());
    }
    @Override
    public void onFailure(Exception e) {
        LOGGER.error("Exception indexing " + Constants.LOG_SEPARATOR  + index + Constants.LOG_SEPARATOR + bookingId + Constants.LOG_SEPARATOR + emailType + Constants.LOG_SEPARATOR + e.getMessage(), e);
    }
}
