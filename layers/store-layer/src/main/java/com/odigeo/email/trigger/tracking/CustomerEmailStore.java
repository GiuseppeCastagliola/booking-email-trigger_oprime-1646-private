package com.odigeo.email.trigger.tracking;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.inject.Inject;
import com.google.inject.Singleton;
import com.odigeo.email.render.v1.contract.EmailType;
import com.odigeo.email.trigger.elasticsearch.ElasticsearchClient;
import com.odigeo.email.trigger.elasticsearch.ElasticsearchClientFactory;
import com.odigeo.email.trigger.email.CustomerEmailTagManager;
import com.odigeo.email.trigger.render.RenderInfo;
import com.odigeo.email.trigger.trigger.TriggerInfo;
import com.odigeo.email.trigger.util.Constants;
import com.odigeo.mailer.v1.Attachment;
import org.elasticsearch.action.ActionListener;
import org.elasticsearch.action.index.IndexRequest;
import org.elasticsearch.action.index.IndexResponse;
import org.elasticsearch.action.search.SearchRequest;
import org.elasticsearch.action.search.SearchResponse;
import org.elasticsearch.common.xcontent.XContentBuilder;
import org.elasticsearch.common.xcontent.XContentFactory;
import org.elasticsearch.index.query.BoolQueryBuilder;
import org.elasticsearch.index.query.QueryBuilders;
import org.elasticsearch.search.builder.SearchSourceBuilder;

import java.io.IOException;
import java.time.Instant;
import java.time.ZoneOffset;
import java.util.List;

import static com.odigeo.email.trigger.util.Constants.HYPHEN;

@Singleton
public class CustomerEmailStore {

    private final ElasticsearchClient client;
    private final CustomerEmailTagManager customerEmailTagManager;

    @Inject
    public CustomerEmailStore(ElasticsearchClientFactory clientFactory, CustomerEmailTagManager customerEmailTagManager) {
        this.client = clientFactory.createClient();
        this.customerEmailTagManager = customerEmailTagManager;
    }

    public void indexAsync(TriggerInfo triggerInfo, RenderInfo renderInfo, ActionListener<IndexResponse> actionListener) throws IOException {
        client.indexAsync(createIndexRequest(triggerInfo, renderInfo), actionListener);
    }

    public IndexResponse indexSync(TriggerInfo triggerInfo, RenderInfo renderInfo) throws IOException {
        return client.index(createIndexRequest(triggerInfo, renderInfo));
    }

    public SearchResponse searchPreviouslyTriggered(long bookingId, EmailType emailType) throws IOException {
        BoolQueryBuilder queryBuilder = QueryBuilders.boolQuery();
        queryBuilder.must(QueryBuilders.matchQuery(Constants.BOOKING_ID, bookingId)).must(QueryBuilders.matchQuery(Constants.EMAIL_TYPE, emailType));
        SearchRequest searchRequest = new SearchRequest(Constants.TRIGGER_INDEX_FOR_SEARCH).source(new SearchSourceBuilder().query(queryBuilder));
        return client.search(searchRequest);
    }

    private IndexRequest createIndexRequest(TriggerInfo triggerInfo, RenderInfo renderInfo) throws IOException {
        final Instant instant = Instant.now();
        XContentBuilder builder = XContentFactory.jsonBuilder();
        builder.startObject();
        builder.field(Constants.BOOKING_ID, triggerInfo.getBookingDetail().getBookingBasicInfo().getId());
        builder.field(Constants.CUSTOMER_ADDRESS, triggerInfo.getBookingDetail().getBuyer().getMail());
        builder.field(Constants.EMAIL_TYPE, triggerInfo.getEmailType());
        builder.field(Constants.EMAIL_REQUESTER, triggerInfo.getEmailRequester());
        builder.field(Constants.TIMESTAMP, instant.toEpochMilli());
        builder.field(Constants.BOOKING_PRODUCTS, new ObjectMapper().writeValueAsString(triggerInfo.getBookingProducts()));
        builder.field(Constants.SUBJECT, renderInfo.getParameters().get(Constants.SUBJECT));
        builder.field(Constants.BODY, renderInfo.getParameters().get(Constants.BODY));
        builder.field(Constants.TAG, customerEmailTagManager.getEmailTagFromTriggerInfo(triggerInfo));
        builder.startArray(Constants.ATTACHMENTS);
        createAttachmentsObjects(builder, renderInfo.getAttachments());
        builder.endArray();
        builder.endObject();

        return new IndexRequest(getIndexName(instant), Constants.TRIGGER_TYPE).source(builder);
    }

    private void createAttachmentsObjects(XContentBuilder builder, List<Attachment> attachments) throws IOException {
        for (Attachment attachment: attachments) {
            builder.startObject().field(Constants.FILE_NAME, attachment.getFileName()).field(Constants.FILE_CONTENT, attachment.getFileContent()).endObject();
        }
    }

    private String getIndexName(Instant instant) {
        return Constants.TRIGGER_INDEX + HYPHEN + instant.atZone(ZoneOffset.UTC).getYear() + HYPHEN + instant.atZone(ZoneOffset.UTC).getMonth().getValue();
    }

}
