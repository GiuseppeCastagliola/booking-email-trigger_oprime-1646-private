package com.odigeo.email.trigger.tracking;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.inject.Inject;
import com.google.inject.Singleton;
import com.odigeo.email.render.v1.contract.EmailType;
import com.odigeo.email.trigger.booking.product.BookingProducts;
import com.odigeo.email.trigger.render.RenderInfo;
import com.odigeo.email.trigger.trigger.TriggerInfo;
import com.odigeo.email.trigger.util.Constants;
import org.apache.log4j.Logger;
import org.elasticsearch.action.index.IndexResponse;
import org.elasticsearch.action.search.SearchResponse;

import java.io.IOException;
import java.util.Map;
import java.util.Optional;

@Singleton
public class CustomerEmailManager {
    private static final Logger LOGGER = Logger.getLogger(CustomerEmailManager.class);

    private final CustomerEmailStore customerEmailStore;

    @Inject
    public CustomerEmailManager(CustomerEmailStore customerEmailStore) {
        this.customerEmailStore = customerEmailStore;
    }

    @SuppressWarnings("PMD.AvoidCatchingGenericException")
    public void index(TriggerInfo triggerInfo, RenderInfo renderInfo) {
        long bookingId = triggerInfo.getBookingDetail().getBookingBasicInfo().getId();
        try {
            IndexResponse response = customerEmailStore.indexSync(triggerInfo, renderInfo);
            LOGGER.info("Index result " + Constants.LOG_SEPARATOR + bookingId + Constants.LOG_SEPARATOR + response.status());
        } catch (Exception e) {
            LOGGER.error("Exception indexing " + Constants.LOG_SEPARATOR + bookingId + Constants.LOG_SEPARATOR + triggerInfo.getEmailType(), e);
        }
    }

    public Optional<BookingProducts> searchBookingProducts(long bookingId, EmailType emailType) {
        Optional<BookingProducts> searchResult = Optional.empty();
        try {
            SearchResponse response = customerEmailStore.searchPreviouslyTriggered(bookingId, emailType);
            if (response.getHits().getTotalHits() > 0) {
                Map<String, Object> mapSource = response.getHits().getAt(0).getSourceAsMap();
                searchResult = Optional.of(new ObjectMapper().readValue(String.valueOf(mapSource.get(Constants.BOOKING_PRODUCTS)), BookingProducts.class));
            }
        } catch (IOException e) {
            LOGGER.error("IOException searching " + Constants.LOG_SEPARATOR + bookingId + Constants.LOG_SEPARATOR + emailType, e);
        }
        return searchResult;
    }

    public Optional<SearchResponse> isWelcomeToPrimeNeverTriggered(long bookingId) {
        Optional<SearchResponse> response = Optional.empty();
        try {
            response = Optional.ofNullable(customerEmailStore.searchPreviouslyTriggered(bookingId, EmailType.WELCOME_TO_PRIME));
        } catch (IOException e) {
            LOGGER.error("IOException searching " + Constants.LOG_SEPARATOR + bookingId + Constants.LOG_SEPARATOR + EmailType.WELCOME_TO_PRIME, e);
        }
        return response;
    }

}
