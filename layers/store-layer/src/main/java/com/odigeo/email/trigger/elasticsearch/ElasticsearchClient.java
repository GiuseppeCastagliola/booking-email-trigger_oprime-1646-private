package com.odigeo.email.trigger.elasticsearch;

import org.elasticsearch.action.ActionListener;
import org.elasticsearch.action.index.IndexRequest;
import org.elasticsearch.action.index.IndexResponse;
import org.elasticsearch.action.search.SearchRequest;
import org.elasticsearch.action.search.SearchResponse;
import org.elasticsearch.client.RestHighLevelClient;

import java.io.IOException;

public class ElasticsearchClient {

    private final RestHighLevelClient client;

    public ElasticsearchClient(RestHighLevelClient client) {
        this.client = client;
    }

    public IndexResponse index(IndexRequest indexRequest) throws IOException {
        return client.index(indexRequest);
    }

    public void indexAsync(IndexRequest indexRequest, ActionListener<IndexResponse> actionListener) {
        client.indexAsync(indexRequest, actionListener);
    }

    public SearchResponse search(SearchRequest searchRequest) throws IOException {
        return client.search(searchRequest);
    }

}
