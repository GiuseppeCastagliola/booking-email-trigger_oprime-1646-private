package com.odigeo.email.rest.v3.exceptions;

import com.odigeo.commons.rest.error.DefaultExceptionMapper;

import javax.ws.rs.core.Response;
import javax.ws.rs.ext.ExceptionMapper;
import java.util.NoSuchElementException;

public class NoSuchElementExceptionMapper extends DefaultExceptionMapper<NoSuchElementException> implements ExceptionMapper<NoSuchElementException> {
    @Override
    protected Response.Status statusToSend() {
        return Response.Status.NOT_FOUND;
    }
}
