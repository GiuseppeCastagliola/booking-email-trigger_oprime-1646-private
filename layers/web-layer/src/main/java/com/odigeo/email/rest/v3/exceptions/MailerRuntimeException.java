package com.odigeo.email.rest.v3.exceptions;

public class MailerRuntimeException extends RuntimeException {

    public MailerRuntimeException(String message, Throwable previousException) {
        super(message, previousException);
    }
}
