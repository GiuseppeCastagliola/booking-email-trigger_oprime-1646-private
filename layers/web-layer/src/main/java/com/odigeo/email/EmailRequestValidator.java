package com.odigeo.email;

public class EmailRequestValidator {
    public void validate(Long bookingId, String subject, String body) {
        if (bookingId == null) {
            throw new IllegalArgumentException("\"bookingId\" must not be empty");
        }

        if (subject == null || subject.isEmpty()) {
            throw new IllegalArgumentException("\"subject\" must not be empty");
        }

        if (body == null || body.isEmpty()) {
            throw new IllegalArgumentException("\"body\" must not be empty");
        }
    }
}
