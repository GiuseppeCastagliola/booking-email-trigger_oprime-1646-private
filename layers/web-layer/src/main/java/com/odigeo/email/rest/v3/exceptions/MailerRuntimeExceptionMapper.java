package com.odigeo.email.rest.v3.exceptions;

import com.odigeo.commons.rest.error.DefaultExceptionMapper;

import javax.ws.rs.core.Response;
import javax.ws.rs.ext.ExceptionMapper;

public class MailerRuntimeExceptionMapper extends DefaultExceptionMapper<MailerRuntimeException> implements ExceptionMapper<MailerRuntimeException> {
    @Override
    protected Response.Status statusToSend() {
        return Response.Status.INTERNAL_SERVER_ERROR;
    }
}
