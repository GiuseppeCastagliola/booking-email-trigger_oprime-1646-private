package com.odigeo.email.rest.v3.exceptions;

import com.odigeo.commons.rest.error.DefaultExceptionMapper;

import javax.ws.rs.ext.ExceptionMapper;
import java.io.UncheckedIOException;

public class UncheckedIOExceptionMapper extends DefaultExceptionMapper<UncheckedIOException> implements ExceptionMapper<UncheckedIOException> {
}

