package com.odigeo.email.rest.v3.exceptions;

import com.odigeo.commons.rest.error.DefaultExceptionMapper;

import javax.ws.rs.ext.ExceptionMapper;

public class IllegalArgumentExceptionMapper extends DefaultExceptionMapper<IllegalArgumentException> implements ExceptionMapper<IllegalArgumentException> {
}

