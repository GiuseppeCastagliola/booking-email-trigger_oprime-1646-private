package com.odigeo.email.rest.v3.exceptions;

import com.odigeo.commons.rest.error.DefaultExceptionMapper;

import javax.ws.rs.ext.ExceptionMapper;
import java.io.IOException;

public class IOExceptionMapper extends DefaultExceptionMapper<IOException> implements ExceptionMapper<IOException> {
}

