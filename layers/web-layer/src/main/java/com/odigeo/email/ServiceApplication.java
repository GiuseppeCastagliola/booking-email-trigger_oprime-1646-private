package com.odigeo.email;

import com.edreams.configuration.ConfigurationEngine;
import com.odigeo.email.rest.v3.exceptions.IOExceptionMapper;
import com.odigeo.email.rest.v3.exceptions.IllegalArgumentExceptionMapper;
import com.odigeo.email.rest.v3.exceptions.UncheckedIOExceptionMapper;

import javax.ws.rs.core.Application;
import java.util.HashSet;
import java.util.Set;

public class ServiceApplication extends Application {

    private final Set<Object> singletons = new HashSet<>();

    public ServiceApplication() {
        singletons.add(ConfigurationEngine.getInstance(EmailController.class));
    }

    @Override
    public Set<Object> getSingletons() {
        return singletons;
    }

    @Override
    public Set<Class<?>> getClasses() {
        Set<Class<?>> resources = new HashSet<>();
        resources.add(IllegalArgumentExceptionMapper.class);
        resources.add(UncheckedIOExceptionMapper.class);
        resources.add(IOExceptionMapper.class);
        return resources;
    }
}
