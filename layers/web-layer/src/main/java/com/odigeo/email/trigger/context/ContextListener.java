package com.odigeo.email.trigger.context;

import com.codahale.metrics.health.HealthCheckRegistry;
import com.edreams.configuration.ConfigurationEngine;
import com.edreams.configuration.ConfigurationFilesManager;
import com.odigeo.commons.messaging.KafkaTopicHealthCheck;
import com.odigeo.commons.messaging.MessagePublisherModule;
import com.odigeo.commons.messaging.domain.events.DomainEventPublisherModule;
import com.odigeo.commons.monitoring.healthcheck.HealthCheckServlet;
import com.odigeo.commons.monitoring.healthcheck.HttpPingHealthCheck;
import com.odigeo.commons.monitoring.metrics.MetricsConstants;
import com.odigeo.commons.monitoring.metrics.MetricsManager;
import com.odigeo.commons.monitoring.metrics.reporter.ReporterStatus;
import com.odigeo.commons.rest.guice.DefaultRestUtilsModule;
import com.odigeo.commons.rest.monitoring.healthcheck.AutoHealthCheckRegister;
import com.odigeo.commons.rest.monitoring.healthcheck.ServiceHealthCheck;
import com.odigeo.domainevents.EmailQueuedServicePublisher;
import com.odigeo.email.render.v1.client.RenderServiceModuleBuilder;
import com.odigeo.email.render.v1.contract.RenderService;
import com.odigeo.email.trigger.cancellation.kafka.VoluntaryCancellationPoller;
import com.odigeo.email.trigger.changedates.kafka.ChangeDatesPoller;
import com.odigeo.email.trigger.configuration.Configuration;
import com.odigeo.email.trigger.elasticsearch.ElasticSearchConnectivityHealthCheck;
import com.odigeo.email.trigger.kafka.KafkaConnectivityHealthCheck;
import com.odigeo.email.trigger.kafka.poller.BookingUpdatesPoller;
import com.odigeo.email.trigger.kafka.poller.RecoveryPoller;
import com.odigeo.email.trigger.kafka.poller.ResendEmailPoller;
import com.odigeo.email.trigger.membership.kafka.consumer.MembershipMailerMessageConsumer;
import com.odigeo.visitengineapi.v1.client.configuration.multitest.TestAssignmentServiceModule;
import com.odigeo.visitengineapi.v1.multitest.TestAssignmentService;
import com.odigeo.visitengineapi.v1.multitest.TestDimensionService;
import org.apache.log4j.Logger;
import org.apache.log4j.PropertyConfigurator;
import org.jboss.vfs.VFS;
import org.jboss.vfs.VFSUtils;
import org.jboss.vfs.VirtualFile;

import javax.servlet.ServletContext;
import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;
import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.net.URL;

public class ContextListener implements ServletContextListener {
    private static final Logger LOGGER = Logger.getLogger(ContextListener.class);

    private static final String LOG4J_FILENAME = "/log4j.properties";
    private static final long LOG4J_WATCH_DELAY_MS = 1800000L;

    @Override
    public void contextInitialized(ServletContextEvent servletContextEvent) {
        LOGGER.info("Context initializing...");

        initPropertiesFileBasedConfigurator();
        initLog4J(servletContextEvent);
        initConfigurationEngine(servletContextEvent.getServletContext());
        initMetrics();
        initHealthChecks(servletContextEvent);
        initKafkaPollers();

        LOGGER.info("Context initialized!");
    }

    @Override
    public void contextDestroyed(ServletContextEvent servletContextEvent) {
        destroyKafkaPollers();
        LOGGER.info("Context destroyed");
    }

    private void initKafkaPollers() {
        initBookingUpdatesPoller();
        initRecoveryPoller();
        initResendEmailPoller();
        initCancellationRequestedPoller();
        initChangeDatesPoller();
        initMembershipMailerMessageConsumer();
    }

    private void destroyKafkaPollers() {
        destroyBookingUpdatesPoller();
        destroyResendEmailPoller();
        destroyRecoveryPoller();
        destroyCancellationRequestedPoller();
        destroyChangeDatesPoller();
        destroyMembershipMailerMessageConsumer();
    }

    void initPropertiesFileBasedConfigurator() {
        ConfigurationFilesManager.setAppName("booking-email-trigger");
    }

    void initLog4J(ServletContextEvent servletContextEvent) {
        VirtualFile virtualFile = VFS.getChild(ContextListener.class.getResource(LOG4J_FILENAME).getFile());
        try {
            URL fileRealURL = VFSUtils.getPhysicalURL(virtualFile);
            PropertyConfigurator.configureAndWatch(fileRealURL.getFile(), LOG4J_WATCH_DELAY_MS);
        } catch (IOException e) {
            servletContextEvent.getServletContext().log("Log4j failed to be initialized with config file " + LOG4J_FILENAME);
        }
    }

    private void initConfigurationEngine(ServletContext servletContext) {
        HealthCheckRegistry registry = new HealthCheckRegistry();
        ConfigurationEngine.init(new TriggerCacheModule(),
                new DefaultRestUtilsModule<>(TestDimensionService.class, new AutoHealthCheckRegister(registry)),
                new TestAssignmentServiceModule(TestAssignmentService.class, new AutoHealthCheckRegister(registry)),
                new RenderServiceModuleBuilder().build(),
                new MessagePublisherModule(), new DomainEventPublisherModule()
        );
        servletContext.setAttribute(HealthCheckServlet.REGISTRY_KEY, registry);
    }

    void initMetrics() {
        MetricsManager.getInstance().addMetricsReporter(MetricsConstants.REGISTRY_BOOKING_EMAIL_TRIGGER);
        MetricsManager.getInstance().changeReporterStatus(ReporterStatus.STARTED);
    }

    private void initHealthChecks(ServletContextEvent servletContextEvent) {
        try {
            final ServletContext servletContext = servletContextEvent.getServletContext();
            HealthCheckRegistry registry = new HealthCheckRegistry();
            Configuration configuration = ConfigurationEngine.getInstance(Configuration.class);

            registry.register("Booking API connection", new HttpPingHealthCheck(configuration.getBookingServiceConfiguration().getUrl()));

            registry.register("Mailer Service connection", new HttpPingHealthCheck(configuration.getMailerConfiguration().getUrl()));

            registry.register("Kafka Zookeepers connection", new KafkaConnectivityHealthCheck("Kafka Zookeepers", configuration.getKafkaConfiguration().getZookeeperUrl()));

            registry.register("Kafka bootstrap servers connection", new KafkaConnectivityHealthCheck("Kafka bootstrap servers", configuration.getKafkaConfiguration().getBootstrapServerUrl()));

            registry.register("Elasticsearch connection", new ElasticSearchConnectivityHealthCheck(configuration.getElasticsearchConfiguration()));

            registry.register("email-render connection", new ServiceHealthCheck(RenderService.class));

            registry.register(String.format("Kafka %s topic", EmailQueuedServicePublisher.EMAILQUEUED_TOPIC), new KafkaTopicHealthCheck(EmailQueuedServicePublisher.EMAILQUEUED_TOPIC));

            registry.register("Membership mailer messages consumer", new KafkaTopicHealthCheck(MembershipMailerMessageConsumer.MEMBERSHIP_ACTIVATIONS_KAFKA_TOPIC));

            servletContext.setAttribute(HealthCheckServlet.REGISTRY_KEY, registry);
        } catch (IllegalAccessException | IOException | InvocationTargetException e) {
            throw new IllegalStateException(e);
        }
    }

    private void initBookingUpdatesPoller() {
        BookingUpdatesPoller bookingUpdatesPoller = ConfigurationEngine.getInstance(BookingUpdatesPoller.class);
        bookingUpdatesPoller.init();
    }

    private void destroyBookingUpdatesPoller() {
        BookingUpdatesPoller bookingUpdatesPoller = ConfigurationEngine.getInstance(BookingUpdatesPoller.class);
        bookingUpdatesPoller.destroy();
    }

    private void initResendEmailPoller() {
        ResendEmailPoller resendEmailPoller = ConfigurationEngine.getInstance(ResendEmailPoller.class);
        resendEmailPoller.init();
    }

    private void destroyResendEmailPoller() {
        ResendEmailPoller resendEmailPoller = ConfigurationEngine.getInstance(ResendEmailPoller.class);
        resendEmailPoller.destroy();
    }

    private void initRecoveryPoller() {
        RecoveryPoller recoveryPoller = ConfigurationEngine.getInstance(RecoveryPoller.class);
        recoveryPoller.init();
    }

    private void destroyRecoveryPoller() {
        RecoveryPoller recoveryPoller = ConfigurationEngine.getInstance(RecoveryPoller.class);
        recoveryPoller.destroy();
    }

    private void initCancellationRequestedPoller() {
        final VoluntaryCancellationPoller voluntaryCancellationPoller = ConfigurationEngine.getInstance(VoluntaryCancellationPoller.class);
        voluntaryCancellationPoller.init();
    }

    private void initMembershipMailerMessageConsumer() {
        MembershipMailerMessageConsumer membershipMailerMessageConsumer = ConfigurationEngine.getInstance(MembershipMailerMessageConsumer.class);
        membershipMailerMessageConsumer.launch();
    }

    private void destroyMembershipMailerMessageConsumer() {
        MembershipMailerMessageConsumer membershipMailerMessageConsumer = ConfigurationEngine.getInstance(MembershipMailerMessageConsumer.class);
        membershipMailerMessageConsumer.shutdown();
    }

    private void destroyCancellationRequestedPoller() {
        final VoluntaryCancellationPoller voluntaryCancellationPoller = ConfigurationEngine.getInstance(VoluntaryCancellationPoller.class);
        voluntaryCancellationPoller.destroy();
    }

    private void initChangeDatesPoller() {
        final ChangeDatesPoller changeDatesPoller = ConfigurationEngine.getInstance(ChangeDatesPoller.class);
        changeDatesPoller.init();
    }

    private void destroyChangeDatesPoller() {
        final ChangeDatesPoller changeDatesPoller = ConfigurationEngine.getInstance(ChangeDatesPoller.class);
        changeDatesPoller.destroy();
    }
}

