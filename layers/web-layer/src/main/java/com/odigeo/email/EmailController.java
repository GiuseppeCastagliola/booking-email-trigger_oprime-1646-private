package com.odigeo.email;

import com.google.inject.Inject;
import com.google.inject.Singleton;
import com.odigeo.bookingapi.v12.responses.BookingDetail;
import com.odigeo.crm.mailer.EmailApi;
import com.odigeo.crm.mailer.ForwardEmailRequest;
import com.odigeo.crm.mailer.SendEmailRequest;
import com.odigeo.email.render.v1.contract.EmailType;
import com.odigeo.email.rest.v3.exceptions.MailerRuntimeException;
import com.odigeo.email.trigger.bookingapi.BookingServiceClient;
import com.odigeo.email.trigger.factories.RulesFactory;
import com.odigeo.email.trigger.render.RenderInfo;
import com.odigeo.email.trigger.trigger.CustomerEmailTrigger;
import com.odigeo.email.trigger.trigger.EmailRequester;
import com.odigeo.email.trigger.trigger.TriggerInfo;
import com.odigeo.email.trigger.trigger.TriggerService;
import com.odigeo.email.trigger.util.Constants;
import com.odigeo.mailer.v1.InvalidParametersException;
import com.odigeo.mailer.v1.RemoteException;

import java.io.IOException;
import java.util.Collections;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;
import java.util.Optional;

@Singleton
public class EmailController implements EmailApi {

    private static final String ODIGEO_GLOBAL_WEBSITE_CODE = "ODGB";
    private static final String ODIGEO_WEBSITE_BRAND = "OD";
    private static final String ODIGEO_EMPTY_TEMPLATE_ID = "emptyInternalEmail";
    private final BookingServiceClient bookingServiceClient;
    private final CustomerEmailTrigger customerEmailTrigger;
    private final TriggerService triggerService;
    private final EmailRequestValidator emailRequestValidator;
    private final RulesFactory rulesFactory;


    @Inject
    public EmailController(TriggerService triggerService, BookingServiceClient bookingServiceClient, CustomerEmailTrigger customerEmailTrigger, EmailRequestValidator emailRequestValidator, RulesFactory rulesFactory) {
        this.triggerService = triggerService;
        this.bookingServiceClient = bookingServiceClient;
        this.customerEmailTrigger = customerEmailTrigger;
        this.emailRequestValidator = emailRequestValidator;
        this.rulesFactory = rulesFactory;
    }

    @Override
    public void sendEmail(SendEmailRequest sendEmailRequest) throws IllegalArgumentException {
        triggerService.triggerEmailSend(sendEmailRequest.getBookingId(), sendEmailRequest.getTemplateName(), Optional.ofNullable(sendEmailRequest.getAdditionalParameters()).orElse(Collections.emptyMap()), rulesFactory.getOnDemandEmailRules());
    }

    @Override
    public void resendEmail(Long bookingId) throws IllegalArgumentException {
        try {
            triggerService.execute(String.valueOf(bookingId), rulesFactory.getResendEmailRules(), EmailRequester.RESEND);
        } catch (IOException e) {
            throw new IllegalArgumentException(String.format("Don't find booking: %s", bookingId), e);
        }
    }

    @Override
    public void forwardEmail(ForwardEmailRequest forwardEmailRequest) throws IllegalArgumentException {
        emailRequestValidator.validate(
                forwardEmailRequest.getBookingId(),
                forwardEmailRequest.getSubject(),
                forwardEmailRequest.getBody()
        );
        BookingDetail bookingDetail = getBookingDetail(forwardEmailRequest.getBookingId());
        TriggerInfo triggerInfo = buildTriggerInfo(bookingDetail);
        RenderInfo renderInfo = buildRenderInfo(
                forwardEmailRequest.getBookingId().toString(),
                forwardEmailRequest.getSubject(),
                forwardEmailRequest.getBody()
        );
        sendEmail(triggerInfo, renderInfo);
    }


    private RenderInfo buildRenderInfo(String bookingId, String subject, String body) {
        Map<String, String> parameters = new HashMap<>();
        parameters.put(Constants.BOOKING_ID, bookingId);
        parameters.put(Constants.SUBJECT, subject);
        parameters.put(Constants.BODY, body);
        return new RenderInfo(parameters, Collections.emptyList());
    }

    private TriggerInfo buildTriggerInfo(BookingDetail bookingDetail) {
        return new TriggerInfo(bookingDetail, EmailType.AIRLINE_VOUCHER, null, EmailRequester.AIRLINE_VOUCHER, null);
    }

    private BookingDetail getBookingDetail(Long bookingId) throws IllegalArgumentException {
        BookingDetail bookingDetail;
        try {
            bookingDetail = bookingServiceClient.getBookingDetail(Locale.getDefault(), bookingId);
        } catch (IOException e) {
            throw new IllegalArgumentException("Booking with id " + bookingId + " not found", e);
        }
        return bookingDetail;
    }

    private void sendEmail(TriggerInfo triggerInfo, RenderInfo renderInfo) throws IllegalArgumentException, MailerRuntimeException {
        try {
            customerEmailTrigger.sendEmail(triggerInfo, renderInfo, ODIGEO_EMPTY_TEMPLATE_ID, ODIGEO_GLOBAL_WEBSITE_CODE, ODIGEO_WEBSITE_BRAND);
        } catch (InvalidParametersException e) {
            throw new IllegalArgumentException("Invalid argument exception ", e);
        } catch (RemoteException e) {
            throw new MailerRuntimeException("Exception when calling mailer service", e);
        }
    }
}
