package com.odigeo.email;

import com.edreams.configuration.ConfigurationEngine;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

public class EmailRequestValidatorTest {
    private static final Long A_VALID_BOOKING_ID = 123L;
    private static final String A_VALID_SUBJECT = "subject";
    private static final String A_VALID_BODY = "body";

    private EmailRequestValidator emailRequestValidator;

    @BeforeMethod
    public void setUp() {
        ConfigurationEngine.init();

        this.emailRequestValidator = new EmailRequestValidator();
    }

    @Test
    public void shouldThrowNoExceptionsWhenGivenValidParameters() {
        emailRequestValidator.validate(
                A_VALID_BOOKING_ID,
                A_VALID_SUBJECT,
                A_VALID_BODY
        );
    }

    @Test(expectedExceptions = IllegalArgumentException.class, expectedExceptionsMessageRegExp = "^\"bookingId\" must not be empty$")
    public void shouldThrowIllegalArgumentExceptionWhenBookingIdIsNull() {
        emailRequestValidator.validate(
                null,
                A_VALID_SUBJECT,
                A_VALID_BODY
        );
    }

    @Test(expectedExceptions = IllegalArgumentException.class, expectedExceptionsMessageRegExp = "^\"subject\" must not be empty$")
    public void shouldThrowIllegalArgumentExceptionWhenSubjectIsNull() {
        emailRequestValidator.validate(
                A_VALID_BOOKING_ID,
                null,
                A_VALID_BODY
        );
    }

    @Test(expectedExceptions = IllegalArgumentException.class, expectedExceptionsMessageRegExp = "^\"subject\" must not be empty$")
    public void shouldThrowIllegalArgumentExceptionWhenSubjectIsEmpty() {
        emailRequestValidator.validate(
                A_VALID_BOOKING_ID,
                "",
                A_VALID_BODY
        );
    }

    @Test(expectedExceptions = IllegalArgumentException.class, expectedExceptionsMessageRegExp = "^\"body\" must not be empty$")
    public void shouldThrowIllegalArgumentExceptionWhenBodyIsNull() {
        emailRequestValidator.validate(
                A_VALID_BOOKING_ID,
                A_VALID_SUBJECT,
                null
        );
    }

    @Test(expectedExceptions = IllegalArgumentException.class, expectedExceptionsMessageRegExp = "^\"body\" must not be empty$")
    public void shouldThrowIllegalArgumentExceptionWhenBodyIsEmpty() {
        emailRequestValidator.validate(
                A_VALID_BOOKING_ID,
                A_VALID_SUBJECT,
                ""
        );
    }
}
