package com.odigeo.email.trigger.context;

import com.edreams.configuration.ConfigurationEngine;
import com.google.inject.AbstractModule;
import com.odigeo.email.trigger.trigger.ExecutionResult;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.testng.Assert;
import org.testng.annotations.BeforeSuite;
import org.testng.annotations.Test;

import javax.servlet.ServletContext;
import javax.servlet.ServletContextEvent;

import static org.mockito.Mockito.when;

public class ContextListenerTest {


    @Mock
    ServletContextEvent servletContextEvent;
    @Mock
    ServletContext servletContext;

    private static final String BOOKING_ID_PARAM = "bookingId";

    @BeforeSuite
    public void init() {
        MockitoAnnotations.initMocks(this);
        ConfigurationEngine.init(new AbstractModule() {
            @Override
            protected void configure() {
            }
        });
        ExecutionResult.values();
    }

    @Test
    public void startContextTestTest() {
        when(servletContextEvent.getServletContext()).thenReturn(servletContext);
        ContextListener contextListener = new ContextListener();
        contextListener.initPropertiesFileBasedConfigurator();
        contextListener.initLog4J(servletContextEvent);
        contextListener.initMetrics();
        Assert.assertNotNull(contextListener);
    }
}
