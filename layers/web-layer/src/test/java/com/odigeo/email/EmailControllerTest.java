package com.odigeo.email;

import com.edreams.configuration.ConfigurationEngine;
import com.odigeo.bookingapi.mock.v12.response.BookingDetailBuilder;
import com.odigeo.bookingapi.mock.v12.response.BuilderException;
import com.odigeo.bookingapi.v12.responses.BookingDetail;
import com.odigeo.crm.mailer.ForwardEmailRequest;
import com.odigeo.email.rest.v3.exceptions.MailerRuntimeException;
import com.odigeo.email.trigger.bookingapi.BookingServiceClient;
import com.odigeo.email.trigger.factories.RulesFactory;
import com.odigeo.email.trigger.rules.ResendEmailRules;
import com.odigeo.email.trigger.rules.RulesInterface;
import com.odigeo.email.trigger.trigger.CustomerEmailTrigger;
import com.odigeo.email.trigger.trigger.EmailRequester;
import com.odigeo.email.trigger.trigger.TriggerService;
import com.odigeo.mailer.v1.InvalidParametersException;
import com.odigeo.mailer.v1.RemoteException;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import java.io.IOException;
import java.util.Locale;
import java.util.Random;

import static org.mockito.Matchers.any;
import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.doThrow;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;


public class EmailControllerTest {

    private static final long EXAMPLE_BOOKING_ID = 123L;
    private static final String EXAMPLE_SUBJECT = "subject";
    private static final String EXAMPLE_BODY = "body";
    private static final RulesInterface SOME_RULES = new ResendEmailRules();

    private ForwardEmailRequest validForwardEmailRequest;

    @Mock
    private TriggerService triggerService;
    @Mock
    private BookingServiceClient bookingServiceClient;
    @Mock
    private CustomerEmailTrigger customerEmailTrigger;
    @Mock
    private EmailRequestValidator emailRequestValidator;
    @Mock
    private RulesFactory rulesFactory;

    private EmailController controller;

    @BeforeMethod
    public void setUp() throws BuilderException, IOException {
        ConfigurationEngine.init();
        MockitoAnnotations.initMocks(this);
        BookingDetail bookingDetail = new BookingDetailBuilder().build(new Random());
        doReturn(bookingDetail).when(bookingServiceClient).getBookingDetail(Locale.getDefault(), EXAMPLE_BOOKING_ID);
        when(rulesFactory.getOnDemandEmailRules()).thenReturn(SOME_RULES);
        when(rulesFactory.getResendEmailRules()).thenReturn(SOME_RULES);
        this.controller = new EmailController(
                this.triggerService,
                this.bookingServiceClient,
                this.customerEmailTrigger,
                this.emailRequestValidator,
                this.rulesFactory);
        validForwardEmailRequest = buildValidForwardEmailRequest();
    }

    @Test
    public void shouldForwardEmailWhenValidParameters() {
        ForwardEmailRequest request = buildValidForwardEmailRequest();

        this.controller.forwardEmail(request);
    }

    @Test(expectedExceptions = IllegalArgumentException.class)
    public void forwardEmailShouldThrowExceptionWhenEmailRequestValidatorThrowsIllegalArgumentException() {
        doThrow(new IllegalArgumentException()).when(emailRequestValidator).validate(
                EXAMPLE_BOOKING_ID,
                EXAMPLE_SUBJECT,
                EXAMPLE_BODY
        );

        this.controller.forwardEmail(validForwardEmailRequest);
    }

    @Test(expectedExceptions = IllegalArgumentException.class, expectedExceptionsMessageRegExp = "^Booking with id 123 not found$")
    public void shouldThrowExceptionWhenBookingNotFoundOnForwardRequest() throws IOException {
        doThrow(new IOException()).when(bookingServiceClient).getBookingDetail(Locale.getDefault(), EXAMPLE_BOOKING_ID);
        this.controller.forwardEmail(validForwardEmailRequest);
    }

    @Test(expectedExceptions = MailerRuntimeException.class)
    public void shouldThrowExceptionWhenCustomerEmailTriggerThrowsRemoteException() throws InvalidParametersException, RemoteException {

        doThrow(new RemoteException()).when(customerEmailTrigger).sendEmail(any(), any(), any(), any(), any());
        this.controller.forwardEmail(validForwardEmailRequest);
    }

    @Test(expectedExceptions = IllegalArgumentException.class)
    public void shouldThrowIllegalArgumentExceptionWhenCustomerEmailTriggerThrowsInvalidParametersException() throws InvalidParametersException, RemoteException {
        doThrow(new InvalidParametersException()).when(customerEmailTrigger).sendEmail(any(), any(), any(), any(), any());
        this.controller.forwardEmail(validForwardEmailRequest);
    }

    @Test(expectedExceptions = IllegalArgumentException.class)
    public void shouldThrowAnErrorWhenTryResendEmailWithAnInexistentBookingId() throws IOException {
        doThrow(new IOException()).when(triggerService).execute(String.valueOf(EXAMPLE_BOOKING_ID), SOME_RULES, EmailRequester.RESEND);
        this.controller.resendEmail(EXAMPLE_BOOKING_ID);
    }

    @Test
    public void shouldResendEmailWhenBookingIdIExist() throws IOException {
        this.controller.resendEmail(EXAMPLE_BOOKING_ID);
        verify(triggerService).execute(String.valueOf(EXAMPLE_BOOKING_ID), SOME_RULES, EmailRequester.RESEND);
    }

    private ForwardEmailRequest buildValidForwardEmailRequest() {
        return ForwardEmailRequest.builder()
                .bookingId(EXAMPLE_BOOKING_ID)
                .subject(EXAMPLE_SUBJECT)
                .body(EXAMPLE_BODY)
                .build();
    }
}
