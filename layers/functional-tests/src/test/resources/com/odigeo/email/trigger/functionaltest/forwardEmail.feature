Feature: Forward email

  Background:
    Given a forward email request
    * with valid subject and body

  Scenario: Forward an email
    Given a default booking
    * the booking is complete
    * the forward request has the same id as the booking
    * the forward request is complete
    When the forward request is sent
    Then no exception was thrown
    And a AIRLINE_VOUCHER email is stored AIRLINE_VOUCHER with tag AIRLINE_VOUCHER

  Scenario: Forward an email with non-existent booking id
    Given there exists no booking
    And the forward request has a random booking Id
    * the forward request is complete
    When the forward request is sent
    Then an exception was thrown of type java.lang.IllegalArgumentException with message Booking with id %id% not found

  Scenario: Forward an email with null booking id
    Given the forward request has no Id
    * the forward request is complete
    When the forward request is sent
    Then an exception was thrown of type java.lang.IllegalArgumentException with message "bookingId" must not be empty
