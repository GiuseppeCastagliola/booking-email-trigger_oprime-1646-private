Feature: Transactional email render

  Background:
    Given a default booking
    * with status CONTRACT

  Scenario: Trigger a confirmation email
    Given the booking is complete
    When the update is submitted
    Then a CONFIRMATION email is stored UPDATE with tag CONFIRMATION

  Scenario: Trigger a confirmation email without itinerary information
    Given without itinerary
    * the booking is complete
    When the update is submitted
    Then a CONFIRMATION email is stored UPDATE with tag CONFIRMATION

  Scenario: Check that a confirmation email is not triggered when we receive a whitelabel booking
    Given is whitelabel
    * the booking is complete
    When the update is submitted
    Then there is no CONFIRMATION email stored

  Scenario: Check that a confirmation email is not triggered when we receive a booking without buyer
    Given without buyer
    * the booking is complete
    When the update is submitted
    Then there is no CONFIRMATION email stored

  Scenario: Check that a confirmation email is not triggered when we receive a booking with departure date in the past
    Given itinerary has departure date in the past
    * the booking is complete
    When the update is submitted
    Then there is no CONFIRMATION email stored

  Scenario: Check that a confirmation email is not triggered when we receive a booking with membership renewal
    Given itinerary has membership renewal
    * the booking is complete
    When the update is submitted
    Then there is no CONFIRMATION email stored
