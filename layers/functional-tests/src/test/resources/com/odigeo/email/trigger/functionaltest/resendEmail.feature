Feature: Resend email

  Scenario: Should resend email when booking id exist
    Given a default booking
    * with status CONTRACT
    * the booking is complete
    * the resend request has the same id as the booking
    When the resend email call is sent
    Then no exception was thrown
    And a CONFIRMATION email is stored RESEND with tag CONFIRMATION_RESEND

  Scenario: Should throw an error when try resend email with an inexistent booking id
    Given there exists no booking
    * the resend request has a random booking Id
    When the resend email call is sent
    Then an exception was thrown of type java.lang.IllegalArgumentException with message Don't find booking: %id%
    And there is no CONFIRMATION email stored
