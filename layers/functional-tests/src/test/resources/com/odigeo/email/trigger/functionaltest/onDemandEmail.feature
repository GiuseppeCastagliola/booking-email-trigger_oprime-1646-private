Feature: On demand emails

  Background:
    Given a default booking
    And the booking is complete

  Scenario Outline: Send an email that has to be stored

    Given a send email request
    * with template <TemplateName>
    * the send email request has the same id as the booking
    * the send email request is complete
    When the send email request is sent
    Then a <EmailType> email is stored UPDATE with tag <EmailType>
    Examples:
      | TemplateName                    | EmailType                   |
      | emailConfirmation               | CONFIRMATION                |
      | emailInProgress                 | IN_PROGRESS                 |
      | emailPending                    | PENDING                     |
      | emailKo                         | KO                          |
      | emailRefundCashout              | REFUND_CASHOUT              |
      | emailCancellation               | CANCELLATION                |
      | emailCancellationRequested      | CANCELLATION_REQUESTED      |
      | emailCancellationProcessing     | CANCELLATION_PROCESSING     |
      | emailCancellationPendingAirline | CANCELLATION_PENDINGAIRLINE |
      | emailChangeRequested            | CHANGE_REQUESTED            |
      | emailChangeProcessing           | CHANGE_PROCESSING           |
      | emailChangeUnderReview          | CHANGE_UNDER_REVIEW         |
      | emailApprovedByAirline          | APPROVED_BY_AIRLINE         |
      | welcomeToPrime                  | WELCOME_TO_PRIME            |
      | emailPendingRefund              | CANCELLATION_PENDING_REFUND |
      | emailCallBack                   | CALLBACK                    |
      | emailCashRefund                 | CASH_REFUND                 |
      | emailOTConfirmation             | OT_CONFIRMATION             |

  Scenario: Send topUpVoucher email that has to be stored

    Given a send email request
    * with template emailVoucherCreated
    * the send email request has the same id as the booking
    * the send email request has as additional parameters:
      | key       | value |
      | voucherId | 1234  |
    * the send email request is complete
    When the send email request is sent
    Then a TOP_UP_VOUCHER_CREATED email is stored UPDATE with tag TOP_UP_VOUCHER_CREATED
