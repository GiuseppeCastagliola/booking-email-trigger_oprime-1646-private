Feature: Email Resend

  Background:
    Given a default booking
    * with status CONTRACT

  Scenario: Resend a confirmation email
    Given the booking is complete
    When the request resend is submitted
    Then a CONFIRMATION email is stored RESEND with tag CONFIRMATION_RESEND

  Scenario: Check that a resend email is not triggered when we receive a whitelabel booking
    Given is whitelabel
    * the booking is complete
    When the request resend is submitted
    Then there is no CONFIRMATION email stored

  Scenario: Check that a resend email email is not triggered when we receive a booking without buyer
    Given without buyer
    * the booking is complete
    When the request resend is submitted
    Then there is no CONFIRMATION email stored

  Scenario: Check that a resend email is not triggered when we receive a booking with membership renewal
    Given itinerary has membership renewal
    * the booking is complete
    When the request resend is submitted
    Then there is no CONFIRMATION email stored

  Scenario: Should resend email when departure date is in the past
    Given itinerary has departure date in the past
    * the booking is complete
    When the request resend is submitted
    Then a CONFIRMATION email is stored RESEND with tag CONFIRMATION_RESEND
