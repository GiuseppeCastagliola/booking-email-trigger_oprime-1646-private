package com.odigeo.email.trigger.functionaltest.elasticsearch;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.common.base.Stopwatch;
import com.google.inject.Inject;
import com.odigeo.email.render.v1.contract.EmailType;
import com.odigeo.email.trigger.util.Constants;
import org.apache.log4j.Logger;
import org.elasticsearch.action.search.SearchResponse;

import java.io.IOException;
import java.util.concurrent.TimeUnit;

public class ElasticsearchManager {
    private static final Logger LOGGER = Logger.getLogger(ElasticsearchManager.class);
    private final ElasticsearchClient elasticsearchClient;

    @Inject
    public ElasticsearchManager(ElasticsearchClient elasticsearchClient) {
        this.elasticsearchClient = elasticsearchClient;
    }

    public SearchResult searchEmailTypeForBookingId(Long bookingId, EmailType emailType) throws IOException, InterruptedException {
        SearchResponse searchResponse = elasticsearchClient.searchEmail(bookingId, emailType);

        Stopwatch stopwatch = Stopwatch.createStarted();

        while (searchResponse.getHits().getHits().length < 1 && stopwatch.elapsed(TimeUnit.SECONDS) < 2) {
            Thread.sleep(250);
            searchResponse = elasticsearchClient.searchEmail(bookingId, emailType);
        }

        LOGGER.info("Waited " + stopwatch.elapsed(TimeUnit.MILLISECONDS) + " milliseconds.");
        stopwatch.stop();
        return constructSearchResult(searchResponse);

    }

    private SearchResult constructSearchResult(SearchResponse searchResponse) throws IOException {
        SearchResult searchResult = null;
        if (searchResponse.getHits().getHits().length > 0) {
            JsonNode node = new ObjectMapper().readTree(searchResponse.getHits().getHits()[0].getSourceAsString());
            searchResult = SearchResult.builder()
                    .bookingId(node.get(Constants.BOOKING_ID).asLong())
                    .emailType(node.get(Constants.EMAIL_TYPE).asText())
                    .emailRequester(node.get(Constants.EMAIL_REQUESTER).asText())
                    .tag(node.get(Constants.TAG).asText())
                    .build();
        }
        return searchResult;
    }

}
