package com.odigeo.email.trigger.functionaltest.kafka;

import com.google.inject.Inject;
import com.google.inject.Singleton;
import com.odigeo.messaging.utils.MessageDataAccessException;

@Singleton
public class KafkaInstaller {

    private final KafkaClient kafkaClient;

    @Inject
    public KafkaInstaller(KafkaClient kafkaClient) {
        this.kafkaClient = kafkaClient;
    }

    public void install() throws MessageDataAccessException {
        kafkaClient.publish("BOOKING_UPDATES_v1", "0");
        kafkaClient.publish("BOOKING_RESEND_EMAIL_v1", "0");
    }
}
