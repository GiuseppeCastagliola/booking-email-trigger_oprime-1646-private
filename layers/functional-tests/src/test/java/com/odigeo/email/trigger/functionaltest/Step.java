package com.odigeo.email.trigger.functionaltest;

import com.edreams.configuration.ConfigurationEngine;
import com.google.inject.Inject;
import com.odigeo.crm.mailer.EmailApi;
import com.odigeo.email.render.v1.contract.EmailType;
import com.odigeo.email.trigger.functionaltest.elasticsearch.ElasticsearchManager;
import com.odigeo.email.trigger.functionaltest.elasticsearch.SearchResult;
import com.odigeo.email.trigger.functionaltest.kafka.KafkaClient;
import com.odigeo.email.trigger.functionaltest.world.BookingApiWorld;
import com.odigeo.email.trigger.trigger.EmailRequester;
import com.odigeo.messaging.utils.MessageDataAccessException;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import cucumber.runtime.java.guice.ScenarioScoped;

import java.io.IOException;

import static junit.framework.Assert.assertEquals;
import static junit.framework.Assert.assertNull;

@ScenarioScoped
public class Step {

    private static final String BOOKING_UPDATES_TOPIC = "BOOKING_UPDATES_v1";
    private static final String RESEND_EMAIL_TOPIC = "BOOKING_RESEND_EMAIL_v1";
    private static final String PLACEHOLDER = "%id%";
    private ElasticsearchManager elasticsearchManager;
    private final KafkaClient kafkaClient;
    private final BookingApiWorld bookingApiWorld;
    private final EmailApi emailApi = ConfigurationEngine.getInstance(EmailApi.class);
    private Throwable lastEmailApiException;

    @Inject
    public Step(
            ElasticsearchManager elasticsearchManager, KafkaClient kafkaClient,
            BookingApiWorld bookingApiWorld
    ) {
        this.elasticsearchManager = elasticsearchManager;
        this.kafkaClient = kafkaClient;
        this.bookingApiWorld = bookingApiWorld;
    }

    @When("^the update is submitted$")
    public void submitUpdate() throws MessageDataAccessException {
        sendBookingIdToTopic(BOOKING_UPDATES_TOPIC);
    }

    @When("^the request resend is submitted$")
    public void submitResend() throws MessageDataAccessException {
        sendBookingIdToTopic(RESEND_EMAIL_TOPIC);
    }

    @When("^the send email request is sent$")
    public void sendSendEmailRequest() {
        emailApi.sendEmail(bookingApiWorld.getSendEmailRequest());
    }

    @When("^the forward request is sent$")
    public void sendForwardEmailRequest() {
        try {
            emailApi.forwardEmail(bookingApiWorld.getForwardEmailRequest());
        } catch (Throwable e) {
            this.lastEmailApiException = e;
        }
    }

    @When("^the resend email call is sent$")
    public void sendResendEmail() {
        try {
            emailApi.resendEmail(bookingApiWorld.getBookingId());
        } catch (Throwable e) {
            this.lastEmailApiException = e;
        }
    }

    @Then("^a (.*) email is stored (.*) with tag (.*)")
    public void submitUpdate(EmailType emailType, EmailRequester emailRequester, String emailTag) throws IOException, InterruptedException {
        SearchResult actual = elasticsearchManager.searchEmailTypeForBookingId(bookingApiWorld.getBookingId(), emailType);
        SearchResult expected = SearchResult.builder()
                .bookingId(bookingApiWorld.getBookingId())
                .emailType(emailType.name())
                .emailRequester(emailRequester.name())
                .tag(emailTag)
                .build();
        assertEquals(expected, actual);

    }

    @Then("^there is no (.*) email stored$")
    public void checkEmailIsNotIndexed(EmailType emailType) throws IOException, InterruptedException {
        SearchResult searchResult = elasticsearchManager.searchEmailTypeForBookingId(bookingApiWorld.getBookingId(), emailType);
        assertNull(searchResult);
    }

    @Then("^no exception was thrown$")
    public void checkNoExceptionWasThrown() {
        assertNull(this.lastEmailApiException);
    }

    @Then("^an exception was thrown of type (.*) with message (.*)$")
    public void checkJsonResponseBody(String className, String message) {
        assertEquals(className, this.lastEmailApiException.getClass().getName());
        assertEquals(replacePlaceholderWithId(message), this.lastEmailApiException.getMessage());
    }

    private String replacePlaceholderWithId(String message) {
        return message.replace(PLACEHOLDER, String.valueOf(bookingApiWorld.getBookingId()));
    }

    private void sendBookingIdToTopic(String topic) throws MessageDataAccessException {
        kafkaClient.publish(topic, String.valueOf(bookingApiWorld.getBookingId()));
    }
}
