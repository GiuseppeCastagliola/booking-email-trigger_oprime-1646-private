package com.odigeo.email.trigger.functionaltest.holder;

import com.google.inject.Inject;
import com.google.inject.Provider;
import com.odigeo.bookingapi.v12.BookingApiService;
import com.odigeo.email.trigger.functionaltest.world.BookingApiWorld;
import org.mockito.Mockito;

import java.util.Locale;

import static org.mockito.Matchers.any;
import static org.mockito.Matchers.anyLong;
import static org.mockito.Matchers.anyString;
import static org.mockito.Mockito.doAnswer;

public class BookingApiServiceHolder {

    private final BookingApiService bookingApiService;
    private final Provider<BookingApiWorld> bookingApiWorldProvider;

    @Inject
    public BookingApiServiceHolder(Provider<BookingApiWorld> bookingApiWorldProvider) {
        this.bookingApiWorldProvider = bookingApiWorldProvider;
        bookingApiService = Mockito.mock(BookingApiService.class);
        init();
    }

    private void init() {
        doAnswer(invocationOnMock -> bookingApiWorldProvider.get().getBookingDetail())
                .when(bookingApiService).getBooking(anyString(), anyString(), any(Locale.class), anyLong());
    }

    public BookingApiService getBookingApiService() {
        return bookingApiService;
    }

}
