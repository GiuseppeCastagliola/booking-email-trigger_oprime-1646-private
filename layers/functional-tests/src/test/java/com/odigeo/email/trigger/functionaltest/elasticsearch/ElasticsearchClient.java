package com.odigeo.email.trigger.functionaltest.elasticsearch;

import com.odigeo.email.render.v1.contract.EmailType;
import com.odigeo.email.trigger.util.Constants;
import org.apache.http.HttpHost;
import org.elasticsearch.action.admin.indices.create.CreateIndexRequest;
import org.elasticsearch.action.admin.indices.create.CreateIndexResponse;
import org.elasticsearch.action.search.SearchRequest;
import org.elasticsearch.action.search.SearchResponse;
import org.elasticsearch.client.RestClient;
import org.elasticsearch.client.RestHighLevelClient;
import org.elasticsearch.index.query.BoolQueryBuilder;
import org.elasticsearch.index.query.QueryBuilders;
import org.elasticsearch.search.builder.SearchSourceBuilder;

import java.io.IOException;

public class ElasticsearchClient {

    public static final int PORT = 9200;
    public static final String HOST = "localhost";
    public static final String SCHEME = "http";
    private final RestHighLevelClient restHighLevelClient;

    public ElasticsearchClient() {
        this.restHighLevelClient = new RestHighLevelClient(
                RestClient.builder(
                        new HttpHost(
                                HOST,
                                PORT,
                                SCHEME))
        );
    }


    public CreateIndexResponse createIndex(CreateIndexRequest createIndexRequest) throws IOException {
        return restHighLevelClient.indices().create(createIndexRequest);
    }

    public SearchResponse searchEmail(Long bookingId, EmailType emailType) throws IOException {
        BoolQueryBuilder boolQueryBuilder = QueryBuilders.boolQuery().must(QueryBuilders.matchQuery(Constants.BOOKING_ID, bookingId)).must(QueryBuilders.matchQuery(Constants.EMAIL_TYPE, emailType));
        SearchRequest searchRequest = new SearchRequest(Constants.TRIGGER_INDEX_FOR_SEARCH).source(new SearchSourceBuilder().query(boolQueryBuilder));
        return restHighLevelClient.search(searchRequest);
    }

}
