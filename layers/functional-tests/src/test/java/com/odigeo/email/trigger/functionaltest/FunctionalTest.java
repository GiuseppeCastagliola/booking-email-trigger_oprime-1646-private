package com.odigeo.email.trigger.functionaltest;

import ch.qos.logback.classic.Level;
import ch.qos.logback.classic.Logger;
import com.edreams.configuration.ConfigurationEngine;
import com.google.inject.Inject;
import com.odigeo.commons.test.docker.Artifact;
import com.odigeo.commons.test.docker.ContainerInfoBuilderFactory;
import com.odigeo.crm.mailer.EmailApi;
import com.odigeo.crm.mailer.EmailApiModule;
import com.odigeo.email.trigger.context.TriggerCacheModule;
import com.odigeo.email.trigger.functionaltest.elasticsearch.ElasticsearchClient;
import com.odigeo.email.trigger.functionaltest.elasticsearch.ElasticsearchInstaller;
import com.odigeo.email.trigger.functionaltest.kafka.KafkaClient;
import com.odigeo.email.trigger.functionaltest.kafka.KafkaInstaller;
import com.odigeo.technology.docker.ContainerComposer;
import com.odigeo.technology.docker.ContainerInfo;
import com.odigeo.technology.docker.ContainerInfoBuilder;
import com.odigeo.technology.docker.DockerModule;
import com.odigeo.technology.docker.ImageController;
import com.odigeo.technology.docker.SocketPingChecker;
import com.odigeo.technology.docker.UrlPingChecker;
import cucumber.api.CucumberOptions;
import cucumber.api.testng.AbstractTestNGCucumberTests;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.maven.model.Model;
import org.apache.maven.model.io.xpp3.MavenXpp3Reader;
import org.codehaus.plexus.util.xml.pull.XmlPullParserException;
import org.eclipse.jetty.util.log.Log;
import org.slf4j.LoggerFactory;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Guice;

import java.io.FileReader;
import java.io.IOException;
import java.util.Arrays;

@CucumberOptions(format = {"pretty", "json:target/cucumber.json", "html:target/cucumber-pretty"}, strict = true,
        tags = {"~@ignore"},
        glue = {"classpath:com/odigeo/email/trigger/functionaltest"},
        features = {"classpath:com/odigeo/email/trigger/functionaltest"}
    )
@Guice(modules = DockerModule.class)
public class FunctionalTest extends AbstractTestNGCucumberTests {

    @Inject
    private ElasticsearchInstaller elasticsearchInstaller;
    @Inject
    private KafkaInstaller kafkaInstaller;
    @Inject
    private ImageController imageController;
    @Inject
    private ContainerInfoBuilderFactory containerInfoBuilderFactory;
    @Inject
    private ContainerComposer containerComposer;

    private Artifact artifact = getArtifact();

    static {
        ConfigurationEngine.init(new TriggerCacheModule(), new EmailApiModule(EmailApi.class));
    }

    @BeforeClass
    public void setUp() throws Exception {

        ContainerInfo kafkaContainer = new ContainerInfoBuilder("spotify/kafka")
                .setName("kafka-functional-test")
                .addPortMapping(KafkaClient.ZOOKEEPER_PORT, KafkaClient.ZOOKEEPER_PORT)
                .addPortMapping(KafkaClient.HOST_PORT, KafkaClient.HOST_PORT)
                .addEvironmentVariable("ADVERTISED_HOST", "192.168.0.1")
                .addEvironmentVariable("ADVERTISED_PORT", Integer.toString(KafkaClient.HOST_PORT))
                .setPingChecker(new SocketPingChecker(KafkaClient.HOST, KafkaClient.HOST_PORT))
                .build();

        ContainerInfo elasticsearchContainer = new ContainerInfoBuilder("docker.elastic.co/elasticsearch/elasticsearch:6.3.2")
                .setName("elasticsearch-functional-test")
                .addPortMapping(ElasticsearchClient.PORT, ElasticsearchClient.PORT)
                .addEvironmentVariable("discovery.type", "single-node")
                .setPingChecker(new UrlPingChecker(HttpClientBuilder.create().build(), ElasticsearchClient.HOST, ElasticsearchClient.PORT, ""))
                .build();

        ContainerInfo triggerContainer = containerInfoBuilderFactory
                .newModule(artifact)
                .addDependency(kafkaContainer)
                .addDependency(elasticsearchContainer)
                .build();

        containerComposer.addServiceAndDependencies(triggerContainer, 10, 5000).composeUp(true);

        elasticsearchInstaller.install();
        kafkaInstaller.install();
        disableFunctionalTestsLogging();

    }

    private void disableFunctionalTestsLogging() {
        Log.setLog(null);
        Arrays.asList("org.apache.http", "org.apache.kafka", "org.elasticsearch.client").forEach(loggerName -> {
            Logger logger = (Logger) LoggerFactory.getLogger(loggerName);
            logger.setLevel(Level.INFO);
            logger.setAdditive(false);
        });
    }

    @AfterClass
    public void tearDown() throws Exception {
        containerComposer.composeDown();
        imageController.removeFuncionalTestsImage(artifact.getArtifactId());
    }

    private static Artifact getArtifact() {
        final MavenXpp3Reader reader = new MavenXpp3Reader();
        try {
            Model model = reader.read(new FileReader("pom.xml"));
            return new Artifact(model.getParent().getGroupId(), model.getParent().getArtifactId(), model.getParent().getVersion());
        } catch (IOException | XmlPullParserException e) {
            return new Artifact("", "", "");
        }
    }


}
