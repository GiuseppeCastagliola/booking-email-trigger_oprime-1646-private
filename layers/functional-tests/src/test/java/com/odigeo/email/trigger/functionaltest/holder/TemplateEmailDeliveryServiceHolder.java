package com.odigeo.email.trigger.functionaltest.holder;

import com.google.inject.Inject;
import com.google.inject.Provider;
import com.odigeo.email.trigger.functionaltest.world.MailerWorld;
import com.odigeo.mailer.v1.EmailSendRequest;
import com.odigeo.mailer.v1.InvalidParametersException;
import com.odigeo.mailer.v1.RemoteException;
import com.odigeo.mailer.v1.TemplateEmailDeliveryService;
import org.mockito.Mockito;

import static org.mockito.Matchers.any;
import static org.mockito.Mockito.doAnswer;

public class TemplateEmailDeliveryServiceHolder {

    private final TemplateEmailDeliveryService templateEmailDeliveryService;
    private final Provider<MailerWorld> mailerWorldProvider;

    @Inject
    public TemplateEmailDeliveryServiceHolder(Provider<MailerWorld> mailerWorldProvider) throws InvalidParametersException, RemoteException {
        this.mailerWorldProvider = mailerWorldProvider;
        templateEmailDeliveryService = Mockito.mock(TemplateEmailDeliveryService.class);
        init();
    }

    private void init() throws InvalidParametersException, RemoteException {
        doAnswer(invocationOnMock -> mailerWorldProvider.get().getEmailSendResponse())
                .when(templateEmailDeliveryService).sendEmail(any(EmailSendRequest.class));
    }

    public TemplateEmailDeliveryService getTemplateEmailDeliveryService() {
        return templateEmailDeliveryService;
    }
}
