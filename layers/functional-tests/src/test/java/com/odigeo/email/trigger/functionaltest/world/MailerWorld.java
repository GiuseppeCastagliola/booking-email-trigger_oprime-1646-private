package com.odigeo.email.trigger.functionaltest.world;

import com.odigeo.mailer.v1.EmailSendResponse;
import cucumber.runtime.java.guice.ScenarioScoped;

@ScenarioScoped
public class MailerWorld {

    private EmailSendResponse emailSendResponse;

    public EmailSendResponse getEmailSendResponse() {
        return emailSendResponse;
    }

    public void setEmailSendResponse(EmailSendResponse emailSendResponse) {
        this.emailSendResponse = emailSendResponse;
    }
}
