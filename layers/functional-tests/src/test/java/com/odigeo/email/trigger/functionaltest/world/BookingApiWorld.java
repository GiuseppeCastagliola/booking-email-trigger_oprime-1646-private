package com.odigeo.email.trigger.functionaltest.world;

import com.odigeo.bookingapi.v12.responses.BookingDetail;
import com.odigeo.crm.mailer.ForwardEmailRequest;
import com.odigeo.crm.mailer.SendEmailRequest;
import cucumber.runtime.java.guice.ScenarioScoped;
import lombok.Getter;
import lombok.Setter;

@ScenarioScoped
@Getter
@Setter
public class BookingApiWorld {

    private Long bookingId;
    private BookingDetail bookingDetail;
    private SendEmailRequest sendEmailRequest;
    private ForwardEmailRequest forwardEmailRequest;
    private Long resendRequestId;
}
