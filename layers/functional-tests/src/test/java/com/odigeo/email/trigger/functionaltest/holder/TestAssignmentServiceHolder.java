package com.odigeo.email.trigger.functionaltest.holder;

import com.google.inject.Inject;
import com.google.inject.Provider;
import com.odigeo.email.trigger.functionaltest.world.VisitEngineWorld;
import com.odigeo.visitengineapi.v1.VisitEngineException;
import com.odigeo.visitengineapi.v1.multitest.TestAssignmentNotFoundException;
import com.odigeo.visitengineapi.v1.multitest.TestAssignmentService;
import org.mockito.Mockito;

import static org.mockito.Matchers.anyString;
import static org.mockito.Mockito.doAnswer;

public class TestAssignmentServiceHolder {

    private final TestAssignmentService testAssignmentService;
    private final Provider<VisitEngineWorld> visitEngineWorldProvider;

    @Inject
    public TestAssignmentServiceHolder(Provider<VisitEngineWorld> visitEngineWorldProvider) throws VisitEngineException, TestAssignmentNotFoundException {
        this.visitEngineWorldProvider = visitEngineWorldProvider;
        testAssignmentService = Mockito.mock(TestAssignmentService.class);
        init();
    }

    private void init() throws VisitEngineException, TestAssignmentNotFoundException {
        doAnswer(invocationOnMock -> visitEngineWorldProvider.get().getTestAssignment())
                .when(testAssignmentService).getTestAssignment(anyString());
    }

    public TestAssignmentService getTestAssignmentService() {
        return testAssignmentService;
    }

}
