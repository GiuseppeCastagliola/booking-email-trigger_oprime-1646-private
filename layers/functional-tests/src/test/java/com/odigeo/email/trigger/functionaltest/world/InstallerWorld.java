package com.odigeo.email.trigger.functionaltest.world;

import com.google.inject.Inject;
import com.odigeo.email.trigger.functionaltest.holder.BookingApiServiceHolder;
import com.odigeo.email.trigger.functionaltest.holder.RenderServiceHolder;
import com.odigeo.email.trigger.functionaltest.holder.TemplateEmailDeliveryServiceHolder;
import com.odigeo.email.trigger.functionaltest.holder.TestAssignmentServiceHolder;
import com.odigeo.email.trigger.functionaltest.holder.TestDimensionServiceHolder;
import com.odigeo.email.trigger.functionaltest.mock.Installer;
import cucumber.api.java.Before;
import cucumber.runtime.java.guice.ScenarioScoped;

@ScenarioScoped
public class InstallerWorld {

    private static Installer installer;
    private BookingApiServiceHolder bookingApiServiceHolder;
    private TemplateEmailDeliveryServiceHolder templateEmailDeliveryServiceHolder;
    private RenderServiceHolder renderServiceHolder;
    private TestAssignmentServiceHolder testAssignmentServiceHolder;
    private TestDimensionServiceHolder testDimensionServiceHolder;

    @Inject
    public InstallerWorld(BookingApiServiceHolder bookingApiServiceHolder, TemplateEmailDeliveryServiceHolder templateEmailDeliveryServiceHolder, RenderServiceHolder renderServiceHolder, TestAssignmentServiceHolder testAssignmentServiceHolder, TestDimensionServiceHolder testDimensionServiceHolder) {
        this.bookingApiServiceHolder = bookingApiServiceHolder;
        this.templateEmailDeliveryServiceHolder = templateEmailDeliveryServiceHolder;
        this.renderServiceHolder = renderServiceHolder;
        this.testAssignmentServiceHolder = testAssignmentServiceHolder;
        this.testDimensionServiceHolder = testDimensionServiceHolder;
    }

    @Before
    public void install() {
        if (installer == null) {
            installer = new Installer();
            installer.install(bookingApiServiceHolder.getBookingApiService(),
                    templateEmailDeliveryServiceHolder.getTemplateEmailDeliveryService(),
                    renderServiceHolder.getRenderService(),
                    testAssignmentServiceHolder.getTestAssignmentService(),
                    testDimensionServiceHolder.getTestDimensionService()
            );
            Runtime.getRuntime().addShutdownHook(new Thread(() -> installer.uninstall()));
        }
    }

}
