package com.odigeo.email.trigger.functionaltest.world;

import cucumber.runtime.java.guice.ScenarioScoped;

@ScenarioScoped
public class EmailApiWorld {

    private Long bookingId;

    public Long getBookingId() {
        return bookingId;
    }

    public void setBookingId(Long bookingId) {
        this.bookingId = bookingId;
    }
}
