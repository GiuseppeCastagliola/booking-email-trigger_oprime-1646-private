package com.odigeo.email.trigger.functionaltest.holder;

import com.google.inject.Inject;
import com.google.inject.Provider;
import com.odigeo.email.render.v1.contract.InvalidParametersException;
import com.odigeo.email.render.v1.contract.RemoteException;
import com.odigeo.email.render.v1.contract.RenderRequest;
import com.odigeo.email.render.v1.contract.RenderService;
import com.odigeo.email.trigger.functionaltest.world.RenderWorld;
import org.mockito.Mockito;

import static org.mockito.Matchers.any;
import static org.mockito.Mockito.doAnswer;

public class RenderServiceHolder {

    private final RenderService renderService;
    private final Provider<RenderWorld> renderWorldProvider;

    @Inject
    public RenderServiceHolder(Provider<RenderWorld> renderWorldProvider) throws InvalidParametersException, RemoteException {
        this.renderWorldProvider = renderWorldProvider;
        renderService = Mockito.mock(RenderService.class);
        init();
    }

    private void init() throws InvalidParametersException, RemoteException {
        doAnswer(invocationOnMock -> renderWorldProvider.get().getRenderResponse())
                .when(renderService).renderTemplate(any(RenderRequest.class));
    }

    public RenderService getRenderService() {
        return renderService;
    }
}
