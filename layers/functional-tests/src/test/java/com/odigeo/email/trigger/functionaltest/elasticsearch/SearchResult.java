package com.odigeo.email.trigger.functionaltest.elasticsearch;

import lombok.Builder;
import lombok.EqualsAndHashCode;

@Builder
@EqualsAndHashCode
public class SearchResult {
    Long bookingId;
    String emailType;
    String emailRequester;
    String tag;
}
