package com.odigeo.email.trigger.functionaltest.kafka;

import com.odigeo.messaging.utils.BasicMessage;
import com.odigeo.messaging.utils.MessageDataAccessException;
import com.odigeo.messaging.utils.Publisher;
import com.odigeo.messaging.utils.kafka.KafkaJsonPublisher;

import java.util.Collections;

public class KafkaClient {

    public static final int ZOOKEEPER_PORT = 2181;
    public static final int HOST_PORT = 9092;
    public static final String HOST = "localhost";
    private final Publisher<BasicMessage> publisher;
    public static final String LOCAL_IP = System.getProperty("local.ip") != null ? System.getProperty("local.ip") : "192.168.0.1";
    public static final String HOST_URL = LOCAL_IP + ":" + HOST_PORT;

    public KafkaClient() {
        publisher = new KafkaJsonPublisher(new KafkaJsonPublisher.Builder(HOST_URL));
        //KafkaJsonConsumer kafkaJsonConsumer = new KafkaJsonConsumer(BasicMessage.class, HOST_URL, );
    }
    public KafkaClient(String host) {
        publisher = new KafkaJsonPublisher(new KafkaJsonPublisher.Builder(host + ":" + HOST_PORT));

        //KafkaJsonConsumer kafkaJsonConsumer = new KafkaJsonConsumer(BasicMessage.class, HOST_URL, );
    }

    public void publish(String topicName, String key) throws MessageDataAccessException {
        BasicMessage basicMessage = new BasicMessage();
        basicMessage.setTopicName(topicName);
        basicMessage.setKey(key);
        publisher.publishMessages(Collections.singletonList(basicMessage));
    }

}
