package com.odigeo.email.trigger.functionaltest;

import com.google.inject.Singleton;
import org.apache.commons.lang.StringUtils;

@Singleton
public class ServerConfiguration {

    public static final String SERVICE_CONTEXT = "/booking-email-trigger/service/v1/";

    private final String serviceServer;

    public ServerConfiguration() {
        serviceServer = StringUtils.defaultIfEmpty(MavenProperties.APPLICATION_HOST, "localhost:8080");
    }

    public String getServer() {
        return serviceServer;
    }
}
