package com.odigeo.email.trigger.functionaltest;

import com.google.inject.Inject;
import com.odigeo.bookingapi.mock.v12.response.AccommodationBookingBuilder;
import com.odigeo.bookingapi.mock.v12.response.BookingBasicInfoBuilder;
import com.odigeo.bookingapi.mock.v12.response.BookingDetailBuilder;
import com.odigeo.bookingapi.mock.v12.response.BookingItineraryBuilder;
import com.odigeo.bookingapi.mock.v12.response.BuilderException;
import com.odigeo.bookingapi.mock.v12.response.ItineraryBookingBuilder;
import com.odigeo.bookingapi.v12.requests.Status;
import com.odigeo.bookingapi.v12.responses.BookingDetail;
import com.odigeo.crm.mailer.ForwardEmailRequest;
import com.odigeo.crm.mailer.SendEmailRequest;
import com.odigeo.email.trigger.util.Constants;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.List;
import java.util.Random;
import java.util.stream.Collectors;

public class BookingDetailFixtures {

    private final Random random;
    private BookingDetailBuilder bookingDetailBuilder;
    private SendEmailRequest.SendEmailRequestBuilder sendEmailRequestBuilder;
    private ForwardEmailRequest.ForwardEmailRequestBuilder forwardEmailRequestBuilder;
    private boolean hasBuyer;
    private static final String A_VALID_SUBJECT = "test subject";
    private static final String A_VALID_BODY = "test body";

    @Inject
    public BookingDetailFixtures(Random random) {
        this.random = random;
    }

    void createBooking(Long bookingId) {
        bookingDetailBuilder = new BookingDetailBuilder()
                .bookingBasicInfoBuilder(new BookingBasicInfoBuilder().id(bookingId))
                .isWhiteLabel(false);
        hasBuyer = true;
    }

    void initializeSendEmailRequest() {
        sendEmailRequestBuilder = SendEmailRequest.builder();
    }

    void initializeForwardRequest() {
        forwardEmailRequestBuilder = ForwardEmailRequest.builder();
    }

    void setForwardRequestId(Long bookingId) {
        forwardEmailRequestBuilder.bookingId(bookingId);
    }

    void setSendEmailRequestId(Long bookingId) {
        sendEmailRequestBuilder.bookingId(bookingId);
    }


    void setSendEmailRequestAdditionalParameters(List<AdditionalParameter> additionalParameters) {
        sendEmailRequestBuilder.additionalParameters(additionalParameters.stream()
                .collect(Collectors.toMap(AdditionalParameter::getKey, AdditionalParameter::getValue)));
    }

    void withoutBuyer() {
        hasBuyer = false;
    }

    void addStatus(Status status) {
        bookingDetailBuilder.bookingStatus(status.name());
    }

    void addAccomodationWithoutItinerary() {
        bookingDetailBuilder.bookingProducts(Collections.singletonList(new AccommodationBookingBuilder()));
    }

    void addIsWhitelabel() {
        bookingDetailBuilder.isWhiteLabel(true);
    }

    void addItineraryWithPastDepartureDate() {
        if (bookingDetailBuilder.getBookingProducts() == null) {
            bookingDetailBuilder.bookingProducts(new ArrayList<>());
        }

        ItineraryBookingBuilder itineraryBookingBuilder = new ItineraryBookingBuilder().bookingItinerary(
                new BookingItineraryBuilder().departureDate(Calendar.getInstance()));

        bookingDetailBuilder.getBookingProducts().add(itineraryBookingBuilder);
    }

    void addItineraryWithMembershipRenewal() {
        if (bookingDetailBuilder.getBookingProducts() == null) {
            bookingDetailBuilder.bookingProducts(new ArrayList<>());
        }

        ItineraryBookingBuilder itineraryBookingBuilder = new ItineraryBookingBuilder()
                .bookingItinerary(new BookingItineraryBuilder())
                .productType(Constants.MEMBERSHIP_RENEWAL);

        bookingDetailBuilder.getBookingProducts().add(itineraryBookingBuilder);
    }

    void addTemplate(String templateName) {
        sendEmailRequestBuilder.templateName(templateName);

    }

    void addValidSubjectAndBody() {
        forwardEmailRequestBuilder.subject(A_VALID_SUBJECT).body(A_VALID_BODY);
    }

    BookingDetail buildBooking() throws BuilderException {
        BookingDetail bookingDetail = bookingDetailBuilder.build(random);
        if (!hasBuyer) {
            bookingDetail.setBuyer(null);
        }
        return bookingDetail;
    }

    SendEmailRequest buildSendEmailRequest(Long bookingId) {
        sendEmailRequestBuilder.bookingId(bookingId);
        return sendEmailRequestBuilder.build();
    }

    ForwardEmailRequest buildForwardEmailRequest() {
        return forwardEmailRequestBuilder.build();
    }

    BookingDetail createNotFoundBookingDetail() {
        BookingDetail bookingDetail = new BookingDetail();
        bookingDetail.setErrorMessage("Not found");

        return bookingDetail;
    }
}

