package com.odigeo.email.trigger.functionaltest;

import com.google.inject.Inject;
import com.odigeo.bookingapi.mock.v12.response.BuilderException;
import com.odigeo.bookingapi.v12.requests.Status;
import com.odigeo.email.render.v1.mock.RenderResponseBuider;
import com.odigeo.email.trigger.functionaltest.world.BookingApiWorld;
import com.odigeo.email.trigger.functionaltest.world.MailerWorld;
import com.odigeo.email.trigger.functionaltest.world.RenderWorld;
import com.odigeo.mailer.v1.EmailSendResponse;
import cucumber.api.java.Before;
import cucumber.api.java.en.And;
import cucumber.api.java.en.Given;
import cucumber.runtime.java.guice.ScenarioScoped;

import java.util.List;
import java.util.Random;

@ScenarioScoped
public class PreSteps {

    private final BookingApiWorld bookingApiWorld;
    private final RenderWorld renderWorld;
    private final MailerWorld mailerWorld;
    private final BookingDetailFixtures bookingDetailFixtures;
    private final Random random;

    @Inject
    public PreSteps(BookingApiWorld bookingApiWorld, RenderWorld renderWorld, MailerWorld mailerWorld, BookingDetailFixtures bookingDetailFixtures, Random random) {
        this.bookingApiWorld = bookingApiWorld;
        this.renderWorld = renderWorld;
        this.mailerWorld = mailerWorld;
        this.bookingDetailFixtures = bookingDetailFixtures;
        this.random = random;
    }

    @Before
    public void initializeEnvironment() {
        bookingApiWorld.setBookingId(random.nextLong());
        initializeRenderAndMailer();
    }

    @Given("^a default booking$")
    public void initializeBooking() {
        bookingDetailFixtures.createBooking(bookingApiWorld.getBookingId());
    }

    @Given("^a forward email request$")
    public void initializeForwardRequest() {
        bookingDetailFixtures.initializeForwardRequest();
    }

    @Given("^a send email request$")
    public void initializeSendEmailRequest() {
        bookingDetailFixtures.initializeSendEmailRequest();
    }

    @Given("^there exists no booking$")
    public void deleteBooking() {
        bookingApiWorld.setBookingDetail(
                bookingDetailFixtures.createNotFoundBookingDetail()
        );
    }

    @And("^the send email request has the same id as the booking$")
    public void addSharedIdToSendEmailRequest() {
        bookingDetailFixtures.setSendEmailRequestId(bookingApiWorld.getBookingId());
    }

    @And("^the send email request has as additional parameters:$")
    public void addSharedIdToSendEmailRequest(List<AdditionalParameter> additionalParameters) {
        bookingDetailFixtures.setSendEmailRequestAdditionalParameters(additionalParameters);
    }

    @And("^the forward request has the same id as the booking$")
    public void addSharedIdToForwardRequest() {
        bookingDetailFixtures.setForwardRequestId(bookingApiWorld.getBookingId());
    }

    @And("^the forward request has a random booking Id$")
    public void addRandomIdToForwardRequest() {
        final long anotherId = random.nextLong();
        bookingDetailFixtures.setForwardRequestId(anotherId);
        bookingApiWorld.setBookingId(anotherId);
    }

    @And("^the forward request has no Id$")
    public void withoutId() {
        bookingDetailFixtures.setForwardRequestId(null);
    }

    @And("^the resend request has the same id as the booking$")
    public void addSharedIdToResendRequest() {
        bookingApiWorld.setResendRequestId(bookingApiWorld.getBookingId());
    }

    @And("^the resend request has a random booking Id$")
    public void addRandomIdToResendRequestr() {
        final long anotherId = random.nextLong();
        bookingApiWorld.setResendRequestId(anotherId);
        bookingApiWorld.setBookingId(anotherId);
    }


    @And("^with valid subject and body$")
    public void addSubjectAndBody() {
        bookingDetailFixtures.addValidSubjectAndBody();
    }

    @And("^without buyer$")
    public void withoutBuyer() {
        bookingDetailFixtures.withoutBuyer();
    }

    @And("^with status (.*)$")
    public void addStatus(Status bookingStatus) {
        bookingDetailFixtures.addStatus(bookingStatus);
    }

    @And("^without itinerary")
    public void addAccomodationWithoutItinerary() {
        bookingDetailFixtures.addAccomodationWithoutItinerary();
    }

    @And("^is whitelabel")
    public void addIsWhitelabel() {
        bookingDetailFixtures.addIsWhitelabel();
    }

    @And("^itinerary has departure date in the past")
    public void addPastDepartureDate() {
        bookingDetailFixtures.addItineraryWithPastDepartureDate();
    }

    @And("^itinerary has membership renewal")
    public void addMembershipRenewal() {
        bookingDetailFixtures.addItineraryWithMembershipRenewal();
    }

    @And("^with template (.*)$")
    public void addTemplate(String templateName) {
        bookingDetailFixtures.addTemplate(templateName);
    }

    @And("^the booking is complete")
    public void buildBooking() throws BuilderException {
        bookingApiWorld.setBookingDetail(bookingDetailFixtures.buildBooking());
    }

    @And("^the send email request is complete")
    public void buildSendEmailRequest() {
        bookingApiWorld.setSendEmailRequest(bookingDetailFixtures.buildSendEmailRequest(bookingApiWorld.getBookingId()));
    }

    @And("^the forward request is complete")
    public void buildForwardEmailRequest() {
        bookingApiWorld.setForwardEmailRequest(bookingDetailFixtures.buildForwardEmailRequest());
    }


    private void initializeRenderAndMailer() {
        renderWorld.setRenderResponse(new RenderResponseBuider()
                .subject("")
                .body("")
                .build(random));
        mailerWorld.setEmailSendResponse(new EmailSendResponse());
    }

}
