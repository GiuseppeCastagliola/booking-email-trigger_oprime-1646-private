package com.odigeo.email.trigger.functionaltest.elasticsearch;

import com.google.inject.Inject;
import com.google.inject.Singleton;
import com.odigeo.email.trigger.util.Constants;
import org.elasticsearch.action.admin.indices.create.CreateIndexRequest;

import java.io.IOException;
import java.time.Instant;
import java.time.ZoneId;

import static com.odigeo.email.trigger.util.Constants.HYPHEN;

@Singleton
public class ElasticsearchInstaller {

    private final ElasticsearchClient client;

    @Inject
    public ElasticsearchInstaller(ElasticsearchClient client) {
        this.client = client;
    }

    public void install() throws IOException {
        createTrackingIndex();
    }

    private void createTrackingIndex() throws IOException {
        final Instant instant = Instant.now();
        CreateIndexRequest createIndexRequest = new CreateIndexRequest(Constants.TRIGGER_INDEX + HYPHEN + instant.atZone(ZoneId.systemDefault()).getMonth().getValue() + HYPHEN + instant.atZone(ZoneId.systemDefault()).getYear());
        client.createIndex(createIndexRequest);
    }

}
