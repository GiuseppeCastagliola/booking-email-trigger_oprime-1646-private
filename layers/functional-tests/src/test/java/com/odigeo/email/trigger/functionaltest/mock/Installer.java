package com.odigeo.email.trigger.functionaltest.mock;

import com.odigeo.bookingapi.v12.BookingApiService;
import com.odigeo.commons.test.bootstrap.Server;
import com.odigeo.commons.test.bootstrap.ServerBuilder;
import com.odigeo.commons.test.bootstrap.rest.ApplicationContext;
import com.odigeo.email.render.v1.contract.RenderService;
import com.odigeo.mailer.v1.TemplateEmailDeliveryService;
import com.odigeo.visitengineapi.v1.multitest.TestAssignmentService;
import com.odigeo.visitengineapi.v1.multitest.TestDimensionService;

public class Installer {

    private Server server;

    public void install(BookingApiService bookingApiService, TemplateEmailDeliveryService templateEmailDeliveryService, RenderService renderService, TestAssignmentService testAssignmentService, TestDimensionService testDimensionService) {
        server = new ServerBuilder()
                .port(57878)
                .listenInAllInterfaces(true)
                .jaxRsApplication(new ApplicationContext().addSingleton(bookingApiService), "/engine")
                .jaxRsApplication(new ApplicationContext().addSingleton(templateEmailDeliveryService), "/mailer")
                .jaxRsApplication(new ApplicationContext().addSingleton(renderService), "/email-render")
                .jaxRsApplication(new ApplicationContext().addSingleton(testAssignmentService).addSingleton(testDimensionService), "/visit-engine")
                .build();
        server.start();
    }

    public void uninstall() {
        server.stop();
    }

}

