package com.odigeo.email.trigger.functionaltest.world;

import com.odigeo.email.render.v1.contract.RenderResponse;
import cucumber.runtime.java.guice.ScenarioScoped;

@ScenarioScoped
public class RenderWorld {

    private RenderResponse renderResponse;

    public RenderResponse getRenderResponse() {
        return renderResponse;
    }

    public void setRenderResponse(RenderResponse renderResponse) {
        this.renderResponse = renderResponse;
    }

}
