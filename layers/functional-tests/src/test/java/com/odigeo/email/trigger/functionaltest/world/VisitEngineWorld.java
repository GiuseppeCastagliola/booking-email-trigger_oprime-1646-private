package com.odigeo.email.trigger.functionaltest.world;

import com.odigeo.visitengineapi.v1.multitest.TestAssignment;
import cucumber.runtime.java.guice.ScenarioScoped;

import java.util.Map;

@ScenarioScoped
public class VisitEngineWorld {

    private Map<Long, String> testDimensionLabels;
    private TestAssignment testAssignment;

    public Map<Long, String> getTestDimensionLabels() {
        return testDimensionLabels;
    }

    public void setTestDimensionLabels(Map<Long, String> testDimensionLabels) {
        this.testDimensionLabels = testDimensionLabels;
    }

    public TestAssignment getTestAssignment() {
        return testAssignment;
    }

    public void setTestAssignment(TestAssignment testAssignment) {
        this.testAssignment = testAssignment;
    }

}
