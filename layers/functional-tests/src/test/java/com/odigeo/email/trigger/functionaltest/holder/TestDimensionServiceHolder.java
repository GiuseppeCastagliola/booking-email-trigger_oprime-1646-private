package com.odigeo.email.trigger.functionaltest.holder;

import com.google.inject.Inject;
import com.google.inject.Provider;
import com.odigeo.email.trigger.functionaltest.world.VisitEngineWorld;
import com.odigeo.visitengineapi.v1.multitest.MultitestException;
import com.odigeo.visitengineapi.v1.multitest.TestDimensionService;
import org.mockito.Mockito;

import static org.mockito.Mockito.doAnswer;

public class TestDimensionServiceHolder {

    private final TestDimensionService testDimensionService;
    private final Provider<VisitEngineWorld> visitEngineWorldProvider;

    @Inject
    public TestDimensionServiceHolder(Provider<VisitEngineWorld> visitEngineWorldProvider) throws MultitestException {
        this.visitEngineWorldProvider = visitEngineWorldProvider;
        testDimensionService = Mockito.mock(TestDimensionService.class);
        init();
    }

    private void init() throws MultitestException {
        doAnswer(invocationOnMock -> visitEngineWorldProvider.get().getTestDimensionLabels())
                .when(testDimensionService).findAllTestDimensionLabels();
    }

    public TestDimensionService getTestDimensionService() {
        return testDimensionService;
    }




}
