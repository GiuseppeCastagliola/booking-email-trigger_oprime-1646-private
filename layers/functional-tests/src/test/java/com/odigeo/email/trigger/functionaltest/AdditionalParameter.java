package com.odigeo.email.trigger.functionaltest;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
class AdditionalParameter {

    private String key;
    private String value;

}
