package com.odigeo.email.trigger.trigger;

public enum Status {

    COPY_RECOVERY,
    COPY_TRIGGERED,
    CUSTOMER_RECOVERY,
    CUSTOMER_TRIGGERED;

}

