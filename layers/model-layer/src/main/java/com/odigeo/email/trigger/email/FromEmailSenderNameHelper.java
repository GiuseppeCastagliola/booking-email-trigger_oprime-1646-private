package com.odigeo.email.trigger.email;

import com.google.inject.Inject;
import com.google.inject.Singleton;
import com.odigeo.bookingapi.v12.responses.BookingDetail;
import com.odigeo.email.trigger.configuration.BrandNames;
import com.odigeo.email.trigger.configuration.Configuration;
import com.odigeo.email.trigger.configuration.FromAddresses;
import com.odigeo.email.trigger.configuration.PrimeBrandNames;

@Singleton
public class FromEmailSenderNameHelper {

    static final String EMAIL_LEFT_BRACKET = " <";
    static final String EMAIL_RIGHT_BRACKET = ">";
    private final FromAddresses fromAddresses;
    private final BrandNames brandNames;
    private final PrimeBrandNames primeBrandNames;

    @Inject
    public FromEmailSenderNameHelper(Configuration configuration) {
        this.fromAddresses = configuration.getFromAddresses();
        this.brandNames = configuration.getBrandNames();
        this.primeBrandNames = configuration.getPrimeBrandNames();
    }

    public String determineFromAddressAndName(BookingDetail bookingDetail) {
        final String brand = bookingDetail.getBookingBasicInfo().getWebsite().getBrand();
        final boolean isPrime = (bookingDetail.getMembershipPerks() != null);
        final String fromEmail = fromAddresses.getFrom(brand);
        return ((isPrime) ? primeBrandNames.getBrandNameByCode(brand) : brandNames.getBrandNameByCode(brand)) + EMAIL_LEFT_BRACKET + fromEmail + EMAIL_RIGHT_BRACKET;
    }

}
