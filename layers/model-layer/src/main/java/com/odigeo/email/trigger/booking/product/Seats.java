package com.odigeo.email.trigger.booking.product;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Objects;

public class Seats {

    private List<SeatSelection> seatSelections = new ArrayList<>();

    public List<SeatSelection> getSeatSelections() {
        return Collections.unmodifiableList(seatSelections);
    }

    public void setSeatSelections(List<SeatSelection> seatSelections) {
        this.seatSelections = seatSelections;
    }

    public boolean equals(Object o) {
        if (o instanceof Seats) {
            Seats seats = (Seats) o;
            return seatSelections.stream().allMatch(s -> seats.getSeatSelections().stream().anyMatch(s2 -> s.equals(s2)));
        }

        return false;
    }

    public int hashCode() {
        return Objects.hash(seatSelections);
    }

}
