package com.odigeo.email.trigger.configuration;

import com.google.inject.Singleton;

import java.util.HashMap;
import java.util.Map;
import java.util.Properties;

@Singleton
public class BrandNames {

    private final Map<String, String> brandNamesByCode = new HashMap<>();

    public String getBrandNameByCode(String brandCode) {
        return brandNamesByCode.get(brandCode);
    }

    public void setBrandNamesByCode(Properties fromByBrand) {
        for (Map.Entry<Object, Object> entry : fromByBrand.entrySet()) {
            this.brandNamesByCode.put(String.valueOf(entry.getKey()), String.valueOf(entry.getValue()));
        }
    }

}
