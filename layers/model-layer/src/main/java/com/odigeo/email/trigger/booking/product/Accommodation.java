package com.odigeo.email.trigger.booking.product;

import java.util.Collections;
import java.util.HashSet;
import java.util.Objects;
import java.util.Set;

public class Accommodation {

    private Set<String> locators = new HashSet<>();

    public Set<String> getLocators() {
        return Collections.unmodifiableSet(locators);
    }

    public void setLocators(Set<String> locators) {
        this.locators = Collections.unmodifiableSet(locators);
    }

    @Override
    public boolean equals(Object o) {
        return o instanceof Accommodation && locators.equals(((Accommodation) o).getLocators());
    }

    @Override
    public int hashCode() {
        return Objects.hash(locators);
    }
}
