package com.odigeo.email.trigger.trigger;

public enum EmailRequester {
    RESEND("email_resend"),
    UPDATE("email_update"),
    RECOVERY("email_recovery"),
    SCHEDULE_CHANGE("email_schedule_change"),
    AIRLINE_VOUCHER("email_airline_voucher");

    private final String subjectKey;

    EmailRequester(String subjectKey) {
        this.subjectKey = subjectKey;
    }

    public String getSubjectKey() {
        return this.subjectKey;
    }
}



