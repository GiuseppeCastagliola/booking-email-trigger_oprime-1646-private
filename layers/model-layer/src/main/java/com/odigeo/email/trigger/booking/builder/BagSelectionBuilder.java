package com.odigeo.email.trigger.booking.builder;

import com.odigeo.bookingapi.v12.responses.BaggageSelection;
import com.odigeo.email.trigger.booking.product.BagSelection;

public class BagSelectionBuilder {

    private final BaggageSelection baggageSelection;

    public BagSelectionBuilder(BaggageSelection baggageSelection) {
        this.baggageSelection = baggageSelection;
    }

    public BagSelection build() {
        BagSelection bagSelection = new BagSelection();
        bagSelection.setSegmentIndex(baggageSelection.getSegmentIndex());
        bagSelection.setPieces(baggageSelection.getPieces());
        bagSelection.setKilos(baggageSelection.getKilos());
        return bagSelection;
    }

}
