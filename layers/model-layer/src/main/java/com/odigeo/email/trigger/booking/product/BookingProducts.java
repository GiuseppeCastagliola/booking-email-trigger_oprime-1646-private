package com.odigeo.email.trigger.booking.product;

import java.util.Objects;

public class BookingProducts {

    private Accommodation accommodation;
    private Bags bags;
    private Seats seats;

    public Accommodation getAccommodation() {
        return accommodation;
    }

    public void setAccommodation(Accommodation accommodation) {
        this.accommodation = accommodation;
    }

    public Bags getBags() {
        return bags;
    }

    public void setBags(Bags bags) {
        this.bags = bags;
    }

    public Seats getSeats() {
        return seats;
    }

    public void setSeats(Seats seats) {
        this.seats = seats;
    }

    @Override
    public boolean equals(Object o) {
        if (o instanceof BookingProducts) {
            BookingProducts bookingProducts = (BookingProducts) o;
            return Objects.equals(accommodation, bookingProducts.getAccommodation())
                    && Objects.equals(bags, bookingProducts.getBags())
                    && Objects.equals(seats, bookingProducts.getSeats());
        }
        return false;
    }

    @Override
    public int hashCode() {
        return Objects.hash(accommodation, seats, bags);
    }
}
