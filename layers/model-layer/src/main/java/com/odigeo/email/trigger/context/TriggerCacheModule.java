package com.odigeo.email.trigger.context;

import com.edreams.persistance.cache.Cache;
import com.edreams.persistance.cache.impl.SimpleMemoryOnlyCache;
import com.google.inject.AbstractModule;
import com.google.inject.Provides;
import com.google.inject.Singleton;

public class TriggerCacheModule extends AbstractModule {

    @Override
    protected void configure() {
        //nothing to do here
    }

    @Provides
    @Singleton
    public Cache getEmailSentCache() {
        return new SimpleMemoryOnlyCache("BookingEmailTrigger", getClass().getSimpleName(), Long.class);
    }
}
