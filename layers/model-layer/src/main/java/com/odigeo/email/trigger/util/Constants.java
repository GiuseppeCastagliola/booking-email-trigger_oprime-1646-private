package com.odigeo.email.trigger.util;

public class Constants {

    public static final String ATTEMPT = "attempt";
    public static final String TIMESTAMP = "timestamp";
    public static final String DATE_FORMAT = "dd/MM/yyyy HH:mm:ss";
    public static final String LOG_SEPARATOR = " | ";

    public static final String GDS_FLIGHT_TYPE = "REG";
    public static final String LC_FLIGHT_TYPE = "LC";

    public static final String TRIGGER_INDEX = "customer";
    public static final String TRIGGER_INDEX_FOR_SEARCH = "customer*";
    public static final String TRIGGER_TYPE = "email";
    public static final String BOOKING_ID = "bookingId";
    public static final String CUSTOMER_ADDRESS = "customerAddress";
    public static final String EMAIL_TYPE = "emailType";
    public static final String BOOKING_PRODUCTS = "bookingProductsInfo";
    public static final String SUBJECT = "subject";
    public static final String BODY = "body";
    public static final String TAG = "tag";
    public static final String ATTACHMENTS = "attachments";

    public static final String TEMPLATE_ID = "transactional";

    public static final String MEMBERSHIP_RENEWAL = "MEMBERSHIP_RENEWAL";

    public static final String EMAIL_REQUESTER = "emailRequester";

    public static final String HYPHEN = "-";
    public static final String FILE_NAME = "fileName";
    public static final String FILE_CONTENT = "fileContent";
}
