package com.odigeo.email.trigger.booking.product;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import java.util.Collections;
import java.util.List;
import java.util.Objects;

@JsonIgnoreProperties(ignoreUnknown = true)
public class SeatSelection {

    private Integer segmentIndex;
    private List<Integer> sectionIndexes;
    private int travellerNumber;
    private int floor;
    private String position;

    public Integer getSegmentIndex() {
        return segmentIndex;
    }

    public void setSegmentIndex(Integer segmentIndex) {
        this.segmentIndex = segmentIndex;
    }

    public List<Integer> getSectionIndexes() {
        return Collections.unmodifiableList(sectionIndexes);
    }

    public void setSectionIndexes(List<Integer> sectionIndexes) {
        this.sectionIndexes = Collections.unmodifiableList(sectionIndexes);
    }

    public int getTravellerNumber() {
        return travellerNumber;
    }

    public void setTravellerNumber(int travellerNumber) {
        this.travellerNumber = travellerNumber;
    }

    public int getFloor() {
        return floor;
    }

    public void setFloor(int floor) {
        this.floor = floor;
    }

    public String getPosition() {
        return position;
    }

    public void setPosition(String position) {
        this.position = position;
    }

    public boolean equals(Object o) {
        if (o instanceof SeatSelection) {
            SeatSelection seatSelection = (SeatSelection) o;
            return travellerNumber == seatSelection.getTravellerNumber()
                    && floor == seatSelection.getFloor()
                    && Objects.equals(segmentIndex, seatSelection.getSegmentIndex())
                    && Objects.equals(position, seatSelection.getPosition())
                    && Objects.equals(sectionIndexes, seatSelection.getSectionIndexes());
        }
        return false;
    }

    public int hashCode() {
        return Objects.hash(floor, sectionIndexes, position, sectionIndexes, travellerNumber);
    }

}
