package com.odigeo.email.trigger.booking.product;

import java.util.Objects;

public class BagSelection {

    private int segmentIndex;
    private Integer pieces;
    private Integer kilos;

    public int getSegmentIndex() {
        return segmentIndex;
    }

    public void setSegmentIndex(int segmentIndex) {
        this.segmentIndex = segmentIndex;
    }

    public Integer getPieces() {
        return pieces;
    }

    public void setPieces(Integer pieces) {
        this.pieces = pieces;
    }

    public Integer getKilos() {
        return kilos;
    }

    public void setKilos(Integer kilos) {
        this.kilos = kilos;
    }

    @Override
    public boolean equals(Object o) {
        if (o instanceof BagSelection) {
            BagSelection bagSelection = (BagSelection) o;
            return bagSelection.getSegmentIndex() == segmentIndex
                    && Objects.equals(bagSelection.getPieces(), pieces)
                    && Objects.equals(bagSelection.getKilos(), kilos);
        }
        return false;
    }

    @Override
    public int hashCode() {
        return Objects.hash(segmentIndex, pieces, kilos);
    }
}
