package com.odigeo.email.trigger.trigger;

import com.odigeo.bookingapi.v12.responses.BookingDetail;
import com.odigeo.email.render.v1.contract.EmailType;
import com.odigeo.email.trigger.booking.product.BookingProducts;

import java.util.Map;

public class TriggerInfo {

    private final BookingDetail bookingDetail;
    private final EmailType emailType;
    private final BookingProducts bookingProducts;
    private final EmailRequester emailRequester;
    private final Map<String, Object> additionalParameters;

    public TriggerInfo(BookingDetail bookingDetail, EmailType emailType, BookingProducts bookingProducts, EmailRequester emailRequester, Map<String, Object> additionalParameters) {
        this.bookingDetail = bookingDetail;
        this.emailType = emailType;
        this.bookingProducts = bookingProducts;
        this.emailRequester = emailRequester;
        this.additionalParameters = additionalParameters;
    }

    public TriggerInfo(Builder builder) {
        this.bookingDetail = builder.bookingDetail;
        this.emailType = builder.emailType;
        this.bookingProducts = builder.bookingProducts;
        this.emailRequester = builder.emailRequester;
        this.additionalParameters = builder.additionalParameters;
    }

    public static class Builder {
        private BookingDetail bookingDetail;
        private EmailType emailType;
        private BookingProducts bookingProducts;
        private EmailRequester emailRequester;
        private Map<String, Object> additionalParameters;

        public Builder withBookingDetail(BookingDetail bookingDetail) {
            this.bookingDetail = bookingDetail;
            return this;
        }

        public Builder withEmailType(EmailType emailType) {
            this.emailType = emailType;
            return this;
        }
        public Builder withEmailRequester(EmailRequester emailRequester) {
            this.emailRequester = emailRequester;
            return this;
        }

        public Builder withBookingProducts(BookingProducts bookingProducts) {
            this.bookingProducts = bookingProducts;
            return this;
        }

        public Builder withAdditionalParameters(Map<String, Object> additionalParameters) {
            this.additionalParameters = additionalParameters;
            return this;
        }

        public TriggerInfo build() {
            return new TriggerInfo(this);
        }

    }

    public BookingDetail getBookingDetail() {
        return bookingDetail;
    }

    public EmailType getEmailType() {
        return emailType;
    }

    public BookingProducts getBookingProducts() {
        return bookingProducts;
    }

    public EmailRequester getEmailRequester() {
        return emailRequester;
    }

    public Map<String, Object> getAdditionalParameters() {
        return additionalParameters;
    }
}

