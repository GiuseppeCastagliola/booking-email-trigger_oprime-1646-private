package com.odigeo.email.trigger.booking.builder;

import com.odigeo.bookingapi.v12.responses.SeatLocation;
import com.odigeo.email.trigger.booking.product.SeatSelection;

import java.util.Collections;
import java.util.List;

public class SeatSelectionBuilder {

    private final Integer segmentIndex;
    private final List<Integer> sectionIndexes;
    private final int travellerNumber;
    private final SeatLocation seatLocation;

    public SeatSelectionBuilder(Integer segmentIndex, List<Integer> sectionIndexes, int travellerNumber, SeatLocation seatLocation) {
        this.segmentIndex = segmentIndex;
        Collections.sort(sectionIndexes);
        this.sectionIndexes = Collections.unmodifiableList(sectionIndexes);
        this.travellerNumber = travellerNumber;
        this.seatLocation = seatLocation;
    }

    public SeatSelection build() {
        SeatSelection seatSelection = new SeatSelection();
        seatSelection.setSegmentIndex(segmentIndex);
        seatSelection.setSectionIndexes(sectionIndexes);
        seatSelection.setTravellerNumber(travellerNumber);
        seatSelection.setFloor(seatLocation.getFloor());
        seatSelection.setPosition(seatLocation.getPosition());
        return seatSelection;
    }

}
