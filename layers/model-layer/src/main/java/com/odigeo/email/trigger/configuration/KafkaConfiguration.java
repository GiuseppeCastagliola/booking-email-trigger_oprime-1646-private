package com.odigeo.email.trigger.configuration;

import com.google.inject.Singleton;

@Singleton
public class KafkaConfiguration {

    private String zookeeperUrl;
    private String bootstrapServerUrl;
    private String groupId;
    private int numThreadsUpdates;
    private int numThreadsRecovery;
    private String bookingUpdatesTopic;
    private String resendEmailsTopic;
    private String recoveryTopic;

    public String getZookeeperUrl() {
        return zookeeperUrl;
    }

    public void setZookeeperUrl(String zookeeperUrl) {
        this.zookeeperUrl = zookeeperUrl;
    }

    public String getBootstrapServerUrl() {
        return bootstrapServerUrl;
    }

    public void setBootstrapServerUrl(String bootstrapServerUrl) {
        this.bootstrapServerUrl = bootstrapServerUrl;
    }

    public String getGroupId() {
        return groupId;
    }

    public void setGroupId(String groupId) {
        this.groupId = groupId;
    }

    public int getNumThreadsUpdates() {
        return numThreadsUpdates;
    }

    public void setNumThreadsUpdates(int numThreadsUpdates) {
        this.numThreadsUpdates = numThreadsUpdates;
    }

    public int getNumThreadsRecovery() {
        return numThreadsRecovery;
    }

    public void setNumThreadsRecovery(int numThreadsRecovery) {
        this.numThreadsRecovery = numThreadsRecovery;
    }

    public String getBookingUpdatesTopic() {
        return bookingUpdatesTopic;
    }

    public String getResendEmailsTopic() {
        return resendEmailsTopic;
    }

    public void setResendEmailsTopic(String resendEmailsTopic) {
        this.resendEmailsTopic = resendEmailsTopic;
    }

    public void setBookingUpdatesTopic(String bookingUpdatesTopic) {
        this.bookingUpdatesTopic = bookingUpdatesTopic;
    }

    public String getRecoveryTopic() {
        return recoveryTopic;
    }

    public void setRecoveryTopic(String recoveryTopic) {
        this.recoveryTopic = recoveryTopic;
    }
}

