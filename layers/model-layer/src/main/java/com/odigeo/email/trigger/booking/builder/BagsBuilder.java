package com.odigeo.email.trigger.booking.builder;

import com.odigeo.bookingapi.v12.responses.Traveller;
import com.odigeo.email.trigger.booking.product.BagSelection;
import com.odigeo.email.trigger.booking.product.Bags;

import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

public class BagsBuilder {

    private final List<Traveller> travellers;

    public BagsBuilder(List<Traveller> travellers) {
        this.travellers = Collections.unmodifiableList(travellers);
    }

    public Bags build() {
        Bags bags = new Bags();
        bags.setBagsSelectionsByPassenger(extractBagSelectionsByPassenger());
        return bags;
    }

    private Map<Integer, Set<BagSelection>> extractBagSelectionsByPassenger() {
        return travellers.stream()
                .collect(Collectors.toMap(Traveller::getNumPassenger, t -> t.getBaggageSelections().stream()
                        .map(s -> new BagSelectionBuilder(s).build())
                        .collect(Collectors.toSet())));
    }

}
