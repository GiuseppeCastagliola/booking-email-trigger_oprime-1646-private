package com.odigeo.email.trigger.email;

import com.google.inject.Singleton;
import com.odigeo.bookingapi.v12.responses.AccommodationBooking;
import com.odigeo.bookingapi.v12.responses.BookingDetail;
import com.odigeo.bookingapi.v12.responses.ItineraryBooking;
import com.odigeo.bookingapi.v12.responses.ProductCategoryBooking;

import java.util.Calendar;
import java.util.List;
import java.util.stream.Collectors;

@Singleton
public class TriggerHelper {

    public boolean isDepartureDatePast(BookingDetail bookingDetail) {
        List<ItineraryBooking> itineraryBookings = retrieveProductByCategory(bookingDetail.getBookingProducts(), ItineraryBooking.class);
        if (itineraryBookings.isEmpty()) {
            return checkDepartureDateByAccommodation(bookingDetail);
        }
        return itineraryBookings.get(0).getBookingItinerary().getDepartureDate().before(Calendar.getInstance());
    }

    public <T extends ProductCategoryBooking> List<T> retrieveProductByCategory(List<ProductCategoryBooking> products, Class<T> tClass) {
        return products.stream()
                .filter(p -> tClass.isInstance(p))
                .map(tClass::cast)
                .collect(Collectors.toList());
    }

    private boolean checkDepartureDateByAccommodation(BookingDetail bookingDetail) {
        List<AccommodationBooking> accommodationBookings = retrieveProductByCategory(bookingDetail.getBookingProducts(), AccommodationBooking.class);
        return !accommodationBookings.isEmpty() && accommodationBookings.get(0).getProviderBooking().getCheckInDate().before(Calendar.getInstance());
    }


}
