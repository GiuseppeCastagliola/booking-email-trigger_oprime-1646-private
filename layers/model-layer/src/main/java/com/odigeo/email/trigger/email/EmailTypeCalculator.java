package com.odigeo.email.trigger.email;

import com.google.inject.Inject;
import com.google.inject.Singleton;
import com.odigeo.bookingapi.v12.requests.Status;
import com.odigeo.bookingapi.v12.responses.AccommodationBooking;
import com.odigeo.bookingapi.v12.responses.BookingDetail;
import com.odigeo.bookingapi.v12.responses.ItineraryBooking;
import com.odigeo.email.render.v1.contract.EmailType;

import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Singleton
public class EmailTypeCalculator {

    private final Map<String, EmailType> emailTypeByStatus = new HashMap<>();
    private final EmailTypeHelper emailTypeHelper;
    private final TriggerHelper triggerHelper;

    @Inject
    public EmailTypeCalculator(EmailTypeHelper emailTypeHelper, TriggerHelper triggerHelper) {
        this.emailTypeHelper = emailTypeHelper;
        this.triggerHelper = triggerHelper;
        emailTypeByStatus.put(Status.CONTRACT.name(), EmailType.CONFIRMATION);
        emailTypeByStatus.put(Status.DIDNOTBUY.name(), EmailType.KO);
    }

    public EmailType calculate(BookingDetail bookingDetail) {
        if (Status.REQUEST.name().equals(bookingDetail.getBookingStatus())) {
            return calculateRequestStatusEmail(bookingDetail);
        }
        return emailTypeByStatus.get(bookingDetail.getBookingStatus());
    }

    private EmailType calculateRequestStatusEmail(BookingDetail bookingDetail) {
        List<ItineraryBooking> itineraryBookings = triggerHelper.retrieveProductByCategory(bookingDetail.getBookingProducts(), ItineraryBooking.class);
        List<AccommodationBooking> accommodationBookings = triggerHelper.retrieveProductByCategory(bookingDetail.getBookingProducts(), AccommodationBooking.class);
        if (emailTypeHelper.hasLcNotConfirmed(itineraryBookings) || emailTypeHelper.hasGdsPending(itineraryBookings) || emailTypeHelper.hasAccommodationPending(accommodationBookings)) {
            return EmailType.PENDING;
        }
        return EmailType.IN_PROGRESS;
    }

    public EmailType getEmailTypeFromTemplateName(String templateName) {
        return Arrays.stream(EmailType.values())
                .filter(emailType -> emailType.getTemplateName().equals(templateName))
                .findFirst()
                .orElseThrow(() -> new IllegalArgumentException("No EmailType found for template name " + templateName));
    }
}
