package com.odigeo.email.trigger.booking.builder;

import com.odigeo.bookingapi.v12.responses.ItineraryBooking;
import com.odigeo.bookingapi.v12.responses.SeatMapPreferencesSelection;
import com.odigeo.bookingapi.v12.responses.SeatMapPreferencesSelectionItem;
import com.odigeo.bookingapi.v12.responses.Section;
import com.odigeo.email.trigger.booking.product.SeatSelection;
import com.odigeo.email.trigger.booking.product.Seats;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

public class SeatsBuilder {

    private final ItineraryBooking itineraryBooking;

    public SeatsBuilder(List<ItineraryBooking> itineraryBookings) {
        this.itineraryBooking = itineraryBookings.stream().findFirst().orElse(null);
    }

    public Seats build() {
        Seats seatsInfo = new Seats();
        seatsInfo.setSeatSelections(extractSeatSelections());
        return seatsInfo;
    }

    private List<SeatSelection> extractSeatSelections() {
        List<SeatSelection> seatSelections = new ArrayList<>();
        if (itineraryBooking != null && itineraryBooking.getSeatPreferencesSelectionSet() != null) {
            itineraryBooking.getSeatPreferencesSelectionSet().getSeatMapPreferencesSelections()
                    .forEach(s -> s.getSeatMapPreferencesSelectionItems()
                            .forEach(i -> createSeatSelection(s, i).ifPresent(seatSelection -> seatSelections.add(seatSelection))));
        }
        return seatSelections;
    }

    private Optional<SeatSelection> createSeatSelection(SeatMapPreferencesSelection seatMapPreferencesSelection, SeatMapPreferencesSelectionItem seatMapPreferencesSelectionItem) {
        return  seatMapPreferencesSelectionItem.getSeatSelected() != null ? Optional.of(new SeatSelectionBuilder(
                seatMapPreferencesSelection.getSegment().getSegmentIndex(),
                seatMapPreferencesSelectionItem.getSections().stream()
                        .map(Section::getSectionIndex)
                        .collect(Collectors.toList()),
                seatMapPreferencesSelectionItem.getTravellerNumber(),
                seatMapPreferencesSelectionItem.getSeatSelected()).build()) : Optional.empty();
    }

}
