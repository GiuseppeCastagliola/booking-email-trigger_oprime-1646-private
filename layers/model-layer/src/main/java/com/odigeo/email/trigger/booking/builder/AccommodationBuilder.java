package com.odigeo.email.trigger.booking.builder;

import com.odigeo.bookingapi.v12.responses.AccommodationBooking;
import com.odigeo.email.trigger.booking.product.Accommodation;

import java.util.Collections;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

public class AccommodationBuilder {

    private final List<AccommodationBooking> accommodations;

    public AccommodationBuilder(List<AccommodationBooking> accommodations) {
        this.accommodations = Collections.unmodifiableList(accommodations);
    }

    public Accommodation build() {
        Accommodation accommodation = new Accommodation();
        accommodation.setLocators(extractLocators());
        return accommodation;
    }

    private Set<String> extractLocators() {
        return accommodations.stream()
                .map(a -> a.getProviderBooking().getLocator())
                .collect(Collectors.toSet());
    }

}
