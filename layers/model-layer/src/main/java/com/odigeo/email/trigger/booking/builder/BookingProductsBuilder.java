package com.odigeo.email.trigger.booking.builder;

import com.edreams.configuration.ConfigurationEngine;
import com.odigeo.bookingapi.v12.responses.AccommodationBooking;
import com.odigeo.bookingapi.v12.responses.BookingDetail;
import com.odigeo.bookingapi.v12.responses.ItineraryBooking;
import com.odigeo.bookingapi.v12.responses.Traveller;
import com.odigeo.email.trigger.booking.product.Accommodation;
import com.odigeo.email.trigger.booking.product.Bags;
import com.odigeo.email.trigger.booking.product.BookingProducts;
import com.odigeo.email.trigger.booking.product.Seats;
import com.odigeo.email.trigger.email.TriggerHelper;

import java.util.List;

public class BookingProductsBuilder {
    private final List<ItineraryBooking> itineraries;
    private final List<AccommodationBooking> accommodations;
    private final List<Traveller> travellers;

    public BookingProductsBuilder(BookingDetail bookingDetail) {
        TriggerHelper triggerHelper = ConfigurationEngine.getInstance(TriggerHelper.class);
        itineraries = triggerHelper.retrieveProductByCategory(bookingDetail.getBookingProducts(), ItineraryBooking.class);
        accommodations = triggerHelper.retrieveProductByCategory(bookingDetail.getBookingProducts(), AccommodationBooking.class);
        travellers = bookingDetail.getTravellers();
    }

    public BookingProducts build() {
        BookingProducts bookingProducts = new BookingProducts();
        bookingProducts.setAccommodation(createAccommodation());
        bookingProducts.setBags(createBags());
        bookingProducts.setSeats(createSeats());
        return bookingProducts;
    }

    private Accommodation createAccommodation() {
        return new AccommodationBuilder(accommodations).build();
    }

    private Bags createBags() {
        return new BagsBuilder(travellers).build();
    }

    private Seats createSeats() {
        return new SeatsBuilder(itineraries).build();
    }

}
