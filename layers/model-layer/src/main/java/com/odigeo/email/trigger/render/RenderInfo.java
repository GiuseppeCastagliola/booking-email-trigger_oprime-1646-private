package com.odigeo.email.trigger.render;

import com.odigeo.mailer.v1.Attachment;

import java.util.Collections;
import java.util.List;
import java.util.Map;

public class RenderInfo {

    private final Map<String, String> parameters;
    private final List<Attachment> attachments;

    public RenderInfo(Map<String, String> parameters, List<Attachment> attachments) {
        this.parameters = Collections.unmodifiableMap(parameters);
        this.attachments = Collections.unmodifiableList(attachments);
    }

    public Map<String, String> getParameters() {
        return parameters;
    }

    public List<Attachment> getAttachments() {
        return attachments;
    }
}
