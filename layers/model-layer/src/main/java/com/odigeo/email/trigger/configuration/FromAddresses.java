package com.odigeo.email.trigger.configuration;

import com.google.inject.Singleton;

import java.util.HashMap;
import java.util.Map;
import java.util.Properties;

@Singleton
public class FromAddresses {

    private final Map<String, String> fromByBrand = new HashMap<>();

    public String getFrom(String brandCode) {
        return fromByBrand.get(brandCode);
    }

    public void setFromByBrand(Properties fromByBrand) {
        for (Map.Entry<Object, Object> entry : fromByBrand.entrySet()) {
            this.fromByBrand.put(String.valueOf(entry.getKey()), String.valueOf(entry.getValue()));
        }
    }

}
