package com.odigeo.email.trigger.email;

import com.google.inject.Singleton;
import com.odigeo.email.trigger.trigger.EmailRequester;
import com.odigeo.email.trigger.trigger.TriggerInfo;

import java.util.ArrayList;
import java.util.List;

@Singleton
public class CustomerEmailTagManager {

    private final List<EmailRequester> customerEmailRequesterList = new ArrayList<>();
    private static final String SEPARATOR = "_";

    CustomerEmailTagManager() {
        customerEmailRequesterList.add(EmailRequester.RESEND);
    }

    public String getEmailTagFromTriggerInfo(final TriggerInfo triggerInfo) {
        if (triggerInfo.getEmailType() != null) {
            if (customerEmailRequesterList.contains(triggerInfo.getEmailRequester())) {
                return triggerInfo.getEmailType().name() + SEPARATOR + triggerInfo.getEmailRequester().name();
            }
            return triggerInfo.getEmailType().name();
        }
        return null;
    }
}
