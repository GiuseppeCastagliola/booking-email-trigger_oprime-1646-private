package com.odigeo.email.trigger.configuration;

import com.edreams.configuration.ConfiguredInPropertiesFile;
import com.google.inject.Singleton;
import lombok.Getter;

@Singleton
@ConfiguredInPropertiesFile
@Getter
@SuppressWarnings({"PMD.UnusedPrivateField", "PMD.SingularField"})
public class Configuration {

    private final BookingServiceConfiguration bookingServiceConfiguration;
    private final KafkaConfiguration kafkaConfiguration;
    private final MailerConfiguration mailerConfiguration;
    private final ElasticsearchConfiguration elasticsearchConfiguration;
    private final FromAddresses fromAddresses;
    private final ToAddresses toAddresses;
    private final BrandNames brandNames;
    private final PrimeBrandNames primeBrandNames;

    public Configuration() {
        bookingServiceConfiguration = new BookingServiceConfiguration();
        kafkaConfiguration = new KafkaConfiguration();
        mailerConfiguration = new MailerConfiguration();
        elasticsearchConfiguration = new ElasticsearchConfiguration();
        fromAddresses = new FromAddresses();
        toAddresses = new ToAddresses();
        brandNames = new BrandNames();
        primeBrandNames = new PrimeBrandNames();
    }

}
