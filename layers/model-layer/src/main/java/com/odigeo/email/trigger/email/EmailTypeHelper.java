package com.odigeo.email.trigger.email;

import com.google.inject.Singleton;
import com.odigeo.bookingapi.v12.requests.Status;
import com.odigeo.bookingapi.v12.responses.AccommodationBooking;
import com.odigeo.bookingapi.v12.responses.ItineraryBooking;
import com.odigeo.email.trigger.util.Constants;

import java.util.List;

@Singleton
class EmailTypeHelper {

    boolean hasLcNotConfirmed(List<ItineraryBooking> itineraryBookings) {
        return itineraryBookings.stream().anyMatch(
            i -> i.getProviderBookings().stream().anyMatch(
                iP -> !Status.CONTRACT.name().equals(iP.getBookingStatus())
                    && Constants.LC_FLIGHT_TYPE.equals(iP.getItineraryProviders().get(0).getFlightType())));
    }

    boolean hasGdsPending(List<ItineraryBooking> itineraryBookings) {
        return itineraryBookings.stream().anyMatch(
            i -> i.getProviderBookings().stream().anyMatch(
                iP -> Status.PENDING.name().equals(iP.getBookingStatus())
                    && Constants.GDS_FLIGHT_TYPE.equals(iP.getItineraryProviders().get(0).getFlightType())));
    }

    boolean hasAccommodationPending(List<AccommodationBooking> accommodationBookings) {
        return accommodationBookings.stream().anyMatch(
            a -> Status.PENDING.name().equals(a.getProviderBooking().getBookingStatus()));
    }

}




