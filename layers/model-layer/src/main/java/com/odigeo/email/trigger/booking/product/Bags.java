package com.odigeo.email.trigger.booking.product;

import java.util.HashMap;
import java.util.Map;
import java.util.Objects;
import java.util.Set;

public class Bags {

    private Map<Integer, Set<BagSelection>> bagsSelectionsByPassenger = new HashMap<>();

    public Map<Integer, Set<BagSelection>> getBagsSelectionsByPassenger() {
        return bagsSelectionsByPassenger;
    }

    public void setBagsSelectionsByPassenger(Map<Integer, Set<BagSelection>> bagsSelectionsByPassenger) {
        this.bagsSelectionsByPassenger = bagsSelectionsByPassenger;
    }

    @Override
    public boolean equals(Object o) {
        return o instanceof Bags && bagsSelectionsByPassenger.equals(((Bags) o).getBagsSelectionsByPassenger());
    }

    @Override
    public int hashCode() {
        return Objects.hash(bagsSelectionsByPassenger);
    }
}
