package com.odigeo.email.trigger.configuration;

import com.google.inject.Singleton;

@Singleton
public class ElasticsearchConfiguration {

    private String protocol;
    private String hostName;
    private int port;

    public String getProtocol() {
        return protocol;
    }

    public void setProtocol(String protocol) {
        this.protocol = protocol;
    }

    public String getHostName() {
        return hostName;
    }

    public void setHostName(String hostName) {
        this.hostName = hostName;
    }

    public int getPort() {
        return port;
    }

    public void setPort(int port) {
        this.port = port;
    }

}
