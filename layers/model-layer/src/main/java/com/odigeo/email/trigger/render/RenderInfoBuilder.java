package com.odigeo.email.trigger.render;

import com.odigeo.email.render.v1.contract.EmailType;
import com.odigeo.email.render.v1.contract.RenderResponse;
import com.odigeo.email.trigger.util.Constants;
import com.odigeo.mailer.v1.Attachment;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class RenderInfoBuilder {

    private final RenderResponse renderResponse;
    private final Long bookingId;
    private final EmailType emailType;

    public RenderInfoBuilder(RenderResponse renderResponse, Long bookingId, EmailType emailType) {
        this.renderResponse = renderResponse;
        this.bookingId = bookingId;
        this.emailType = emailType;
    }

    public RenderInfo build() {
        return new RenderInfo(
                createParameters(renderResponse.getSubject(), renderResponse.getBody(), bookingId, emailType),
                createAttachments(renderResponse.getAttachments())
        );
    }

    private Map<String, String> createParameters(String subject, String body, Long bookingId, EmailType emailType) {
        Map<String, String> parameters = new HashMap<>();
        parameters.put(Constants.SUBJECT, subject);
        parameters.put(Constants.BODY, body);
        parameters.put(Constants.BOOKING_ID, String.valueOf(bookingId));
        parameters.put(Constants.EMAIL_TYPE, emailType.name());
        return parameters;
    }

    private List<Attachment> createAttachments(List<com.odigeo.email.render.v1.contract.Attachment> renderAttachments) {
        List<Attachment> attachments = new ArrayList<>();
        for (com.odigeo.email.render.v1.contract.Attachment renderAttachment : renderAttachments) {
            Attachment attachment = new Attachment();
            attachment.setFileName(renderAttachment.getFilename());
            attachment.setFileContent(renderAttachment.getFileContent());
            attachments.add(attachment);
        }
        return attachments;
    }

}
