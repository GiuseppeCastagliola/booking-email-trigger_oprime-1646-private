package com.odigeo.email.trigger.configuration;

import com.google.inject.Singleton;

import java.util.HashMap;
import java.util.Map;
import java.util.Properties;

@Singleton
public class ToAddresses {

    private final Map<String, String> toByBrand = new HashMap<>();

    public String getTo(String brandCode) {
        return toByBrand.get(brandCode);
    }

    public void setToByBrand(Properties properties) {
        for (Map.Entry toByBrand : properties.entrySet()) {
            this.toByBrand.put(String.valueOf(toByBrand.getKey()), String.valueOf(toByBrand.getValue()));
        }
    }

}
