package com.odigeo.email.trigger.trigger.testbuilder;

import com.odigeo.bookingapi.mock.v12.response.BookingDetailBuilder;
import com.odigeo.bookingapi.mock.v12.response.BuilderException;
import com.odigeo.bookingapi.v12.responses.BookingDetail;
import com.odigeo.email.render.v1.contract.EmailType;
import com.odigeo.email.trigger.booking.product.BookingProducts;
import com.odigeo.email.trigger.booking.testbuilder.BookingProductsTestDataBuilder;
import com.odigeo.email.trigger.trigger.EmailRequester;
import com.odigeo.email.trigger.trigger.TriggerInfo;

import java.util.Collections;
import java.util.Map;
import java.util.Random;

public class TriggerInfoTestDataBuilder {

    private BookingDetail bookingDetail;
    private EmailType emailType;
    private BookingProducts bookingProducts;
    private EmailRequester emailRequester;
    private Map<String, Object> additionalParameters;

    private TriggerInfoTestDataBuilder() throws BuilderException {
        bookingDetail = new BookingDetailBuilder().build(new Random());
        emailType = EmailType.CONFIRMATION;
        bookingProducts = BookingProductsTestDataBuilder.abookingProducts().build();
        emailRequester = EmailRequester.UPDATE;
        additionalParameters = Collections.emptyMap();
    }

    public static TriggerInfoTestDataBuilder aTriggerInfo() throws BuilderException {
        return new TriggerInfoTestDataBuilder();
    }

    public TriggerInfoTestDataBuilder withBookingDetail(BookingDetail bookingDetail) {
        this.bookingDetail = bookingDetail;
        return this;
    }

    public TriggerInfoTestDataBuilder withEmailType(EmailType emailType) {
        this.emailType = emailType;
        return this;
    }

    public TriggerInfoTestDataBuilder withBookingProducts(BookingProducts bookingProducts) {
        this.bookingProducts = bookingProducts;
        return this;
    }

    public TriggerInfoTestDataBuilder withEmailRequester(EmailRequester emailRequester) {
        this.emailRequester = emailRequester;
        return this;
    }

    public TriggerInfo build() {
        return new TriggerInfo(bookingDetail, emailType, bookingProducts, emailRequester, additionalParameters);
    }

}


