package com.odigeo.email.trigger.configuration;

import com.edreams.configuration.ConfigurationEngine;
import org.testng.Assert;
import org.testng.annotations.Test;

public class ConfigurationTest {

    private Configuration configuration;

    @Test
    public void test() {
        ConfigurationEngine.init();
        configuration = ConfigurationEngine.getInstance(Configuration.class);
        Assert.assertNotNull(configuration);
        Assert.assertNotNull(configuration.getBookingServiceConfiguration());
        Assert.assertNotNull(configuration.getElasticsearchConfiguration());
        Assert.assertNotNull(configuration.getFromAddresses());
        Assert.assertNotNull(configuration.getKafkaConfiguration());
        Assert.assertNotNull(configuration.getMailerConfiguration());
        Assert.assertNotNull(configuration.getToAddresses());
    }

}
