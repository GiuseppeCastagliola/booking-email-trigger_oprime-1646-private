package com.odigeo.email.trigger.email;

import com.edreams.configuration.ConfigurationEngine;
import com.google.inject.AbstractModule;
import com.odigeo.bookingapi.mock.v12.response.BookingBasicInfoBuilder;
import com.odigeo.bookingapi.mock.v12.response.BookingDetailBuilder;
import com.odigeo.bookingapi.mock.v12.response.BuilderException;
import com.odigeo.bookingapi.mock.v12.response.WebsiteBuilder;
import com.odigeo.bookingapi.v12.responses.BookingDetail;
import com.odigeo.email.trigger.configuration.BrandNames;
import com.odigeo.email.trigger.configuration.Configuration;
import com.odigeo.email.trigger.configuration.FromAddresses;
import com.odigeo.email.trigger.configuration.PrimeBrandNames;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.testng.Assert;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import java.util.Random;

import static org.mockito.Mockito.when;

public class FromEmailSenderNameHelperTest {

    private FromEmailSenderNameHelper fromEmailSenderNameHelper;

    @Mock
    private Configuration configuration;

    @Mock
    private BrandNames brandNames;

    @Mock
    private PrimeBrandNames primeBrandNames;

    @Mock
    private FromAddresses fromAddresses;

    private static final String A_TEST_BRAND_CODE = "BRAND_TEST";
    private static final String A_SENDER = "Sender";
    private static final String A_PRIME_SENDER = "Prime Sender";
    private static final String A_TEST_EMAIL = "e@mail.com";

    private BookingDetail anEdreamsPrimeBookingDetail;
    private BookingDetail anEdreamsBookingDetail;
    private Random random;

    @BeforeClass
    public void setUp() throws BuilderException {
        MockitoAnnotations.initMocks(this);
        ConfigurationEngine.init(new AbstractModule() {
            @Override
            protected void configure() {
                bind(Configuration.class).toInstance(configuration);
            }
        });
        when(configuration.getBrandNames()).thenReturn(brandNames);
        when(configuration.getPrimeBrandNames()).thenReturn(primeBrandNames);
        when(configuration.getFromAddresses()).thenReturn(fromAddresses);
        when(fromAddresses.getFrom(A_TEST_BRAND_CODE)).thenReturn(A_TEST_EMAIL);

        fromEmailSenderNameHelper = new FromEmailSenderNameHelper(configuration);

        random = new Random();

        anEdreamsPrimeBookingDetail = new BookingDetailBuilder()
                .bookingBasicInfoBuilder(new BookingBasicInfoBuilder().website(new WebsiteBuilder().brand(A_TEST_BRAND_CODE)))
                .build(random);

        anEdreamsBookingDetail = new BookingDetailBuilder()
                .bookingBasicInfoBuilder(new BookingBasicInfoBuilder().website(new WebsiteBuilder().brand(A_TEST_BRAND_CODE)))
                .build(random);
        anEdreamsBookingDetail.setMembershipPerks(null);

    }

    @Test
    private void shouldReturnEdreamsPrimeSenderNameAndEmail() {
        when(primeBrandNames.getBrandNameByCode(A_TEST_BRAND_CODE)).thenReturn(A_PRIME_SENDER);

        final String result = fromEmailSenderNameHelper.determineFromAddressAndName(anEdreamsPrimeBookingDetail);
        Assert.assertEquals(
                buildSenderStringFromSenderNameAndEmail(A_PRIME_SENDER, A_TEST_EMAIL),
                result
        );
    }

    @Test
    private void shouldReturnEdreamsSenderNameAndEmail() {
        when(brandNames.getBrandNameByCode(A_TEST_BRAND_CODE)).thenReturn(A_SENDER);

        final String result = fromEmailSenderNameHelper.determineFromAddressAndName(anEdreamsBookingDetail);
        Assert.assertEquals(
                buildSenderStringFromSenderNameAndEmail(A_SENDER, A_TEST_EMAIL),
                result
        );
    }

    private String buildSenderStringFromSenderNameAndEmail(final String sender, final String email) {
        return sender + FromEmailSenderNameHelper.EMAIL_LEFT_BRACKET + email + FromEmailSenderNameHelper.EMAIL_RIGHT_BRACKET;
    }
}
