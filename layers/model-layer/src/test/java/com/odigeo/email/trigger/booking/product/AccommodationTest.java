package com.odigeo.email.trigger.booking.product;

import com.odigeo.email.trigger.booking.testbuilder.AccommodationTestDataBuilder;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import java.util.Collections;

import static org.testng.Assert.assertEquals;
import static org.testng.Assert.assertNotEquals;

public class AccommodationTest {

    private Accommodation accommodation;

    @BeforeMethod
    public void setUp() {
        accommodation = AccommodationTestDataBuilder.anAccommodation().build();
    }

    @Test
    public void testEqualsReflexive() {
        assertEquals(accommodation, accommodation);
    }

    @Test
    public void testEqualsSymmetric() {
        Accommodation accommodationCopy = AccommodationTestDataBuilder.anAccommodation().build();
        assertEquals(accommodation, accommodationCopy);
        assertEquals(accommodationCopy, accommodation);
    }

    @Test
    public void testEqualsTransitive() {
        Accommodation accommodationCopy = AccommodationTestDataBuilder.anAccommodation().build();
        Accommodation accommodationCopyBis = AccommodationTestDataBuilder.anAccommodation().build();
        assertEquals(accommodation, accommodationCopy);
        assertEquals(accommodationCopy, accommodationCopyBis);
        assertEquals(accommodation, accommodationCopyBis);
    }

    @Test
    public void testNotEqualsDiffObject() {
        assertNotEquals(accommodation, new Object());
    }

    @Test
    public void testNotEqualsDiffLocators() {
        Accommodation accommodationDiff = AccommodationTestDataBuilder.anAccommodation().withLocators(Collections.singleton("AT85GB")).build();
        assertNotEquals(accommodation, accommodationDiff);
    }

    @Test
    public void testHashCode() {
        int hashCode = accommodation.hashCode();
        int hasCodeCopy = AccommodationTestDataBuilder.anAccommodation().build().hashCode();
        assertEquals(hasCodeCopy, hashCode);
    }
}
