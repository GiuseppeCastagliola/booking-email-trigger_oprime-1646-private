package com.odigeo.email.trigger.booking.product;

import com.odigeo.email.trigger.booking.testbuilder.BagSelectionTestDataBuilder;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import static org.testng.Assert.assertEquals;
import static org.testng.Assert.assertNotEquals;

public class BagSelectionTest {

    private BagSelection bagSelection;

    @BeforeMethod
    public void setUp() {
        bagSelection = BagSelectionTestDataBuilder.aBagSelection().build();
    }

    @Test
    public void testEqualsReflexive() {
        assertEquals(bagSelection, bagSelection);
    }

    @Test
    public void testEqualsSymmetric() {
        BagSelection bagSelectionCopy = BagSelectionTestDataBuilder.aBagSelection().build();
        assertEquals(bagSelection, bagSelectionCopy);
        assertEquals(bagSelectionCopy, bagSelection);
    }

    @Test
    public void testEqualsTransitive() {
        BagSelection bagSelectionCopy = BagSelectionTestDataBuilder.aBagSelection().build();
        BagSelection bagSelectionCopyBis = BagSelectionTestDataBuilder.aBagSelection().build();
        assertEquals(bagSelection, bagSelectionCopy);
        assertEquals(bagSelectionCopy, bagSelectionCopyBis);
        assertEquals(bagSelection, bagSelectionCopyBis);
    }

    @Test
    public void testNotEqualsDiffObject() {
        assertNotEquals(bagSelection, new Object());
    }

    @Test
    public void testNotEqualsDiffSegmentIndex() {
        BagSelection bagSelectionDiff = BagSelectionTestDataBuilder.aBagSelection().withSegmentIndex(-9756214).build();
        assertNotEquals(bagSelection, bagSelectionDiff);
    }

    @Test
    public void testNotEqualsDiffPieces() {
        BagSelection bagSelectionDiff = BagSelectionTestDataBuilder.aBagSelection().withPieces(-9796214).build();
        assertNotEquals(bagSelection, bagSelectionDiff);
    }

    @Test
    public void testNotEqualsDiffKilos() {
        BagSelection bagSelectionDiff = BagSelectionTestDataBuilder.aBagSelection().withKilos(-9776214).build();
        assertNotEquals(bagSelection, bagSelectionDiff);
    }

    @Test
    public void testHashCode() {
        int hashCode = bagSelection.hashCode();
        int hashCodeCopy = BagSelectionTestDataBuilder.aBagSelection().build().hashCode();
        assertEquals(hashCodeCopy, hashCode);
    }

}
