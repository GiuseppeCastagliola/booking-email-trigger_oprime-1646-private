package com.odigeo.email.trigger.booking.builder;

import com.odigeo.bookingapi.mock.v12.response.AccommodationBookingBuilder;
import com.odigeo.bookingapi.mock.v12.response.AccommodationProviderBookingBuilder;
import com.odigeo.bookingapi.v12.responses.AccommodationBooking;
import com.odigeo.email.trigger.booking.product.Accommodation;
import org.testng.annotations.Test;

import java.util.Collections;
import java.util.Random;

import static org.testng.Assert.assertEquals;
import static org.testng.Assert.assertTrue;

public class AccommodationBuilderTest {

    @Test
    public void testBuild() throws Exception {
        String locator = "locator";
        AccommodationBooking accommodationBooking = new AccommodationBookingBuilder()
                .providerBooking(new AccommodationProviderBookingBuilder().locator(locator))
                .build(new Random());

        Accommodation actual = new AccommodationBuilder(Collections.singletonList(accommodationBooking)).build();
        assertEquals(actual.getLocators().size(), 1);
        assertTrue(actual.getLocators().contains(locator));
    }

}
