package com.odigeo.email.trigger.booking.testbuilder;

import com.odigeo.email.trigger.booking.product.BagSelection;
import com.odigeo.email.trigger.booking.product.Bags;

import java.util.Collections;
import java.util.Map;
import java.util.Set;

public class BagsTestDataBuilder {

    private Map<Integer, Set<BagSelection>> bagSelectionsByPassenger;

    private BagsTestDataBuilder() {
        bagSelectionsByPassenger =  Collections.singletonMap(1, Collections.singleton(BagSelectionTestDataBuilder.aBagSelection().build()));
    }

    public static BagsTestDataBuilder aBags() {
        return new BagsTestDataBuilder();
    }

    public BagsTestDataBuilder withBagSelectionsByPassenger(Map<Integer, Set<BagSelection>> bagSelectionsByPassenger) {
        this.bagSelectionsByPassenger = bagSelectionsByPassenger;
        return this;
    }

    public Bags build() {
        Bags bagSelection = new Bags();
        bagSelection.setBagsSelectionsByPassenger(bagSelectionsByPassenger);
        return bagSelection;
    }

}
