package com.odigeo.email.trigger.email;

import com.odigeo.bookingapi.v12.requests.Status;
import com.odigeo.bookingapi.mock.v12.response.AccommodationBookingBuilder;
import com.odigeo.bookingapi.mock.v12.response.BookingDetailBuilder;
import com.odigeo.bookingapi.mock.v12.response.BuilderException;
import com.odigeo.bookingapi.mock.v12.response.ItineraryBookingBuilder;
import com.odigeo.bookingapi.v12.responses.AccommodationBooking;
import com.odigeo.bookingapi.v12.responses.BookingDetail;
import com.odigeo.bookingapi.v12.responses.ItineraryBooking;
import com.odigeo.email.render.v1.contract.EmailType;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.testng.Assert;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import java.util.Collections;
import java.util.List;
import java.util.Random;

import static org.mockito.Mockito.when;

public class EmailTypeCalculatorTest {

    @Mock
    private EmailTypeHelper emailTypeHelper;
    @Mock
    private TriggerHelper triggerHelper;

    private EmailTypeCalculator emailTypeCalculator;
    private BookingDetail bookingDetail;
    private List<ItineraryBooking> itineraryBookings;
    private List<AccommodationBooking> accommodationBookings;

    @BeforeClass
    public void setUp() throws BuilderException {
        MockitoAnnotations.initMocks(this);
        bookingDetail = new BookingDetailBuilder().build(new Random());
        itineraryBookings = Collections.singletonList(new ItineraryBookingBuilder().build(new Random()));
        accommodationBookings = Collections.singletonList(new AccommodationBookingBuilder().build(new Random()));
        emailTypeCalculator = new EmailTypeCalculator(emailTypeHelper, triggerHelper);
    }

    @Test
    public void testCalculateEmailTypeConfirmation() {
        bookingDetail.setBookingStatus(Status.CONTRACT.name());
        EmailType result = emailTypeCalculator.calculate(bookingDetail);
        Assert.assertEquals(result, EmailType.CONFIRMATION);
    }

    @Test
    public void testCalculateEmailTypeUnsuccessful() {
        bookingDetail.setBookingStatus(Status.DIDNOTBUY.name());
        EmailType result = emailTypeCalculator.calculate(bookingDetail);
        Assert.assertEquals(result, EmailType.KO);
    }

    @Test
    public void testCalculateEmailTypeInProgress() {
        when(triggerHelper.retrieveProductByCategory(bookingDetail.getBookingProducts(), ItineraryBooking.class)).thenReturn(itineraryBookings);
        bookingDetail.setBookingStatus(Status.REQUEST.name());
        EmailType result = emailTypeCalculator.calculate(bookingDetail);
        Assert.assertEquals(result, EmailType.IN_PROGRESS);
    }

    @Test
    public void testCalculateEmailTypePendingLc() {
        when(triggerHelper.retrieveProductByCategory(bookingDetail.getBookingProducts(), ItineraryBooking.class)).thenReturn(itineraryBookings);
        when(emailTypeHelper.hasLcNotConfirmed(itineraryBookings)).thenReturn(true);
        bookingDetail.setBookingStatus(Status.REQUEST.name());
        EmailType result = emailTypeCalculator.calculate(bookingDetail);
        Assert.assertEquals(result, EmailType.PENDING);
    }

    @Test
    public void testCalculateEmailTypePendingGds() {
        when(triggerHelper.retrieveProductByCategory(bookingDetail.getBookingProducts(), ItineraryBooking.class)).thenReturn(itineraryBookings);
        when(emailTypeHelper.hasGdsPending(itineraryBookings)).thenReturn(true);
        bookingDetail.setBookingStatus(Status.REQUEST.name());
        EmailType result = emailTypeCalculator.calculate(bookingDetail);
        Assert.assertEquals(result, EmailType.PENDING);
    }

    @Test
    public void testCalculateEmailTypePendingAccommodation() {
        when(triggerHelper.retrieveProductByCategory(bookingDetail.getBookingProducts(), AccommodationBooking.class)).thenReturn(accommodationBookings);
        when(emailTypeHelper.hasAccommodationPending(accommodationBookings)).thenReturn(true);
        bookingDetail.setBookingStatus(Status.REQUEST.name());
        EmailType result = emailTypeCalculator.calculate(bookingDetail);
        Assert.assertEquals(result, EmailType.PENDING);
    }

}
