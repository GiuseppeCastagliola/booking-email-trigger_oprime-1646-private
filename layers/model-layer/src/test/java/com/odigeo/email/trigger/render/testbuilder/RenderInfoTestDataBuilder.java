package com.odigeo.email.trigger.render.testbuilder;

import com.odigeo.email.trigger.render.RenderInfo;
import com.odigeo.mailer.v1.Attachment;

import java.util.Collections;
import java.util.List;
import java.util.Map;

public class RenderInfoTestDataBuilder {

    private Map<String, String> parameters;
    private List<Attachment> attachments;

    private RenderInfoTestDataBuilder() {
        parameters = Collections.singletonMap("Id", "Parameter");
        attachments = Collections.singletonList(new Attachment());
    }
    
    public static RenderInfoTestDataBuilder aRenderInfo() {
        return new RenderInfoTestDataBuilder();
    }

    public RenderInfoTestDataBuilder withParameters(Map<String, String> parameters) {
        this.parameters = parameters;
        return this;
    }

    public RenderInfoTestDataBuilder withAttachments(List<Attachment> attachments) {
        this.attachments = attachments;
        return this;
    }
    
    public RenderInfo build() {
        return new RenderInfo(parameters, attachments);
    }
    
}
