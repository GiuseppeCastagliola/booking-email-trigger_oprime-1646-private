package com.odigeo.email.trigger.booking.testbuilder;

import com.odigeo.email.trigger.booking.product.Accommodation;
import com.odigeo.email.trigger.booking.product.Bags;
import com.odigeo.email.trigger.booking.product.BookingProducts;
import com.odigeo.email.trigger.booking.product.Seats;

public class BookingProductsTestDataBuilder {

    private Seats seats;
    private Bags bags;
    private Accommodation accommodation;
    
    private BookingProductsTestDataBuilder() {
        seats = SeatsTestDataBuilder.aSeats().build();
        bags = BagsTestDataBuilder.aBags().build();
        accommodation = AccommodationTestDataBuilder.anAccommodation().build();
    }

    public static BookingProductsTestDataBuilder abookingProducts() {
        return new BookingProductsTestDataBuilder();
    }

    public BookingProductsTestDataBuilder withSeats(Seats seats) {
        this.seats = seats;
        return this;
    }

    public BookingProductsTestDataBuilder withBags(Bags bags) {
        this.bags = bags;
        return this;
    }

    public BookingProductsTestDataBuilder withAccommodation(Accommodation accommodation) {
        this.accommodation = accommodation;
        return this;
    }

    public BookingProducts build() {
        BookingProducts bookingProducts = new BookingProducts();
        bookingProducts.setSeats(seats);
        bookingProducts.setBags(bags);
        bookingProducts.setAccommodation(accommodation);
        return bookingProducts;
    }

}
