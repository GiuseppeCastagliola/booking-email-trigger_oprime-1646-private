package com.odigeo.email.trigger.email;

import com.odigeo.email.render.v1.contract.EmailType;
import com.odigeo.email.trigger.trigger.EmailRequester;
import com.odigeo.email.trigger.trigger.TriggerInfo;
import org.apache.commons.lang.StringUtils;
import org.testng.Assert;
import org.testng.annotations.Test;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class CustomerEmailTagManagerTest {

    private CustomerEmailTagManager customerEmailTagManager = new CustomerEmailTagManager();

    private final List<EmailRequester> customerEmailRequesterList = new ArrayList<>();

    public CustomerEmailTagManagerTest() {
        customerEmailRequesterList.add(EmailRequester.RESEND);
    }

    @Test
    public void shouldAddPrefixToEmailTypeWhenApplicable() {
        Arrays.asList(EmailRequester.values()).forEach(emailRequester -> {
            TriggerInfo triggerInfo = new TriggerInfo.Builder().withEmailRequester(emailRequester).withEmailType(EmailType.CONFIRMATION).build();
            Assert.assertEquals(
                    generatesTagWithSuffix(triggerInfo),
                    shouldAddSuffix(emailRequester)
            );
        });
    }

    private boolean generatesTagWithSuffix(TriggerInfo triggerInfo) {
        final String emailTag = customerEmailTagManager.getEmailTagFromTriggerInfo(triggerInfo);
        return StringUtils.endsWith(emailTag, triggerInfo.getEmailRequester().name());
    }

    private boolean shouldAddSuffix(final EmailRequester emailRequester) {
        return customerEmailRequesterList.contains(emailRequester);
    }
}
