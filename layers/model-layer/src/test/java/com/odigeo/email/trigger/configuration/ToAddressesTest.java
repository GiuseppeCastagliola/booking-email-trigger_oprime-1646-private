package com.odigeo.email.trigger.configuration;

import org.testng.Assert;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import java.util.Properties;

public class ToAddressesTest {

    private ToAddresses toAddresses;
    private Properties properties;
    private String brandCode = "brandCode";
    private String address = "address";

    @BeforeClass
    public void init() {
        properties = new Properties();
        properties.put(brandCode, address);
        toAddresses = new ToAddresses();
    }

    @Test
    public void testTo() {
        toAddresses.setToByBrand(properties);
        Assert.assertEquals(toAddresses.getTo(brandCode), address);
    }
}
