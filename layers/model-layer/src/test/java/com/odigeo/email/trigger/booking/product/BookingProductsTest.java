package com.odigeo.email.trigger.booking.product;

import com.odigeo.email.trigger.booking.testbuilder.AccommodationTestDataBuilder;
import com.odigeo.email.trigger.booking.testbuilder.BagSelectionTestDataBuilder;
import com.odigeo.email.trigger.booking.testbuilder.BagsTestDataBuilder;
import com.odigeo.email.trigger.booking.testbuilder.BookingProductsTestDataBuilder;
import com.odigeo.email.trigger.booking.testbuilder.SeatSelectionTestDataBuilder;
import com.odigeo.email.trigger.booking.testbuilder.SeatsTestDataBuilder;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.Set;

import static org.testng.Assert.assertEquals;
import static org.testng.Assert.assertNotEquals;

public class BookingProductsTest {

    private BookingProducts bookingProducts;

    @BeforeMethod
    public void setUp() {
        bookingProducts = BookingProductsTestDataBuilder.abookingProducts().build();
    }

    @Test
    public void testEqualsReflexive() {
        assertEquals(bookingProducts, bookingProducts);
    }

    @Test
    public void testEqualsSymmetric() {
        BookingProducts bookingProductsCopy = BookingProductsTestDataBuilder.abookingProducts().build();
        assertEquals(bookingProducts, bookingProductsCopy);
        assertEquals(bookingProductsCopy, bookingProducts);
    }

    @Test
    public void testEqualsTransitive() {
        BookingProducts bookingProductsCopy = BookingProductsTestDataBuilder.abookingProducts().build();
        BookingProducts bookingProductsCopyBis = BookingProductsTestDataBuilder.abookingProducts().build();
        assertEquals(bookingProducts, bookingProductsCopy);
        assertEquals(bookingProductsCopy, bookingProductsCopyBis);
        assertEquals(bookingProducts, bookingProductsCopyBis);
    }

    @Test
    public void testNotEqualsDiffObject() {
        assertNotEquals(bookingProducts, new Object());
    }

    @Test
    public void testNotEqualsDiffAccommodation() {
        Accommodation accommodation = AccommodationTestDataBuilder.anAccommodation().withLocators(Collections.singleton("QV34")).build();
        BookingProducts bookingProductsDiff = BookingProductsTestDataBuilder.abookingProducts().withAccommodation(accommodation).build();
        assertNotEquals(bookingProductsDiff, bookingProducts);
    }

    @Test
    public void testNotEqualsDiffBags() {
        Map<Integer, Set<BagSelection>> bagSelectionsByPassenger = Collections.singletonMap(753, Collections.singleton(BagSelectionTestDataBuilder.aBagSelection().build()));
        BookingProducts bookingProductsDiff = BookingProductsTestDataBuilder.abookingProducts().withBags(BagsTestDataBuilder.aBags().withBagSelectionsByPassenger(bagSelectionsByPassenger).build()).build();
        assertNotEquals(bookingProductsDiff, bookingProducts);
    }

    @Test
    public void testNotEqualsDiffSeats() {
        List<SeatSelection> seatSelections = Collections.singletonList(SeatSelectionTestDataBuilder.aSeatSelection().withSegmentIndex(624).build());
        BookingProducts bookingProductsDiff = BookingProductsTestDataBuilder.abookingProducts().withSeats(SeatsTestDataBuilder.aSeats().withSeatSelections(seatSelections).build()).build();
        assertNotEquals(bookingProductsDiff, bookingProducts);
    }

    @Test
    public void testHashCode() {
        int hashCode = bookingProducts.hashCode();
        int hasCodeCopy = BookingProductsTestDataBuilder.abookingProducts().build().hashCode();
        assertEquals(hasCodeCopy, hashCode);
    }
}
