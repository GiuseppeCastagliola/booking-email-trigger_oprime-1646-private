package com.odigeo.email.trigger.booking.testbuilder;

import com.odigeo.email.trigger.booking.product.BagSelection;

public class BagSelectionTestDataBuilder {

    private int segmentIndex;
    private Integer pieces;
    private Integer kilos;

    private BagSelectionTestDataBuilder() {
        segmentIndex = 147;
        pieces = 258;
        kilos = 369;
    }

    public static BagSelectionTestDataBuilder aBagSelection() {
        return new BagSelectionTestDataBuilder();
    }

    public BagSelectionTestDataBuilder withSegmentIndex(Integer segmentIndex) {
        this.segmentIndex = segmentIndex;
        return this;
    }

    public BagSelectionTestDataBuilder withPieces(Integer pieces) {
        this.pieces = pieces;
        return this;
    }

    public BagSelectionTestDataBuilder withKilos(Integer kilos) {
        this.kilos = kilos;
        return this;
    }

    public BagSelection build() {
        BagSelection bagSelection = new BagSelection();
        bagSelection.setSegmentIndex(segmentIndex);
        bagSelection.setPieces(pieces);
        bagSelection.setKilos(kilos);
        return bagSelection;
    }

}
