package com.odigeo.email.trigger.booking.builder;

import com.odigeo.bookingapi.mock.v12.response.SeatLocationBuilder;
import com.odigeo.bookingapi.v12.responses.SeatLocation;
import com.odigeo.email.trigger.booking.product.SeatSelection;
import org.testng.annotations.Test;

import java.util.Collections;
import java.util.List;
import java.util.Random;

import static org.testng.Assert.assertEquals;

public class SeatSelectionBuilderTest {

    @Test
    public void testBuild() throws Exception {
        Integer segmentIndex = 745;
        List<Integer> sectionIndexes = Collections.singletonList(562);
        int travellerNumber = 632;
        int floor = 954;
        int rowNumber = 125;
        String position = "AGBN";

        SeatLocation seatLocation = new SeatLocationBuilder()
                .floor(floor)
                .rowNumber(rowNumber)
                .position(position)
                .build(new Random());

        SeatSelection seatSelection = new SeatSelectionBuilder(segmentIndex, sectionIndexes, travellerNumber, seatLocation).build();
        assertEquals(seatSelection.getSegmentIndex(), segmentIndex);
        assertEquals(seatSelection.getSectionIndexes(), sectionIndexes);
        assertEquals(seatSelection.getTravellerNumber(), travellerNumber);
        assertEquals(seatSelection.getFloor(), floor);
        assertEquals(seatSelection.getPosition(), position);

    }
}
