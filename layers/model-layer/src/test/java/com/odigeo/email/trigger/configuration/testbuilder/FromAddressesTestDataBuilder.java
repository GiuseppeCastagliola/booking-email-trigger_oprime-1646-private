package com.odigeo.email.trigger.configuration.testbuilder;

import com.odigeo.email.trigger.configuration.FromAddresses;

import java.util.Properties;

public class FromAddressesTestDataBuilder {

    private Properties fromByBrand;

    private FromAddressesTestDataBuilder() {
        Properties properties = new Properties();
        properties.setProperty("ED", "from@edreams.com");
        properties.setProperty("GV", "from@govoyages.com");
        properties.setProperty("OP", "from@opodo.com");
        properties.setProperty("TL", "from@travellink.com");
        fromByBrand = properties;
    }

    public static FromAddressesTestDataBuilder aFromAddresses() {
        return new FromAddressesTestDataBuilder();
    }

    public FromAddressesTestDataBuilder withFromByBrand(Properties fromByBrand) {
        this.fromByBrand = fromByBrand;
        return this;
    }

    public FromAddresses build() {
        FromAddresses fromAddresses = new FromAddresses();
        fromAddresses.setFromByBrand(fromByBrand);
        return fromAddresses;
    }

}
