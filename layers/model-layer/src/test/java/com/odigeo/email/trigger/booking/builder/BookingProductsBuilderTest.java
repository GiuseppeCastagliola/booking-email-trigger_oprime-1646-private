package com.odigeo.email.trigger.booking.builder;

import com.edreams.configuration.ConfigurationEngine;
import com.odigeo.bookingapi.mock.v12.response.AccommodationBookingBuilder;
import com.odigeo.bookingapi.mock.v12.response.BookingDetailBuilder;
import com.odigeo.bookingapi.mock.v12.response.ItineraryBookingBuilder;
import com.odigeo.bookingapi.mock.v12.response.ProductCategoryBookingBuilder;
import com.odigeo.bookingapi.mock.v12.response.TravellerBuilder;
import com.odigeo.bookingapi.v12.responses.BookingDetail;
import com.odigeo.email.trigger.booking.product.BookingProducts;
import org.testng.annotations.Test;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Random;

import static org.testng.Assert.assertNotNull;

public class BookingProductsBuilderTest {

    @Test
    public void testBuild() throws Exception {
        ConfigurationEngine.init();
        List<ProductCategoryBookingBuilder> productCategoryBookingBuilders = new ArrayList<>();
        productCategoryBookingBuilders.add(new ItineraryBookingBuilder());
        productCategoryBookingBuilders.add(new AccommodationBookingBuilder());

        BookingDetail bookingDetail = new BookingDetailBuilder()
                .bookingProducts(productCategoryBookingBuilders)
                .travellers(Collections.singletonList(new TravellerBuilder()))
                .build(new Random());

        BookingProducts bookingProducts = new BookingProductsBuilder(bookingDetail).build();
        assertNotNull(bookingProducts.getAccommodation());
        assertNotNull(bookingProducts.getBags());
        assertNotNull(bookingProducts.getSeats());
    }

}
