package com.odigeo.email.trigger.configuration;

import org.testng.Assert;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

public class MailerConfigurationTest {

    private MailerConfiguration mailerConfiguration;
    private String url = "url";
    private String service = "service";
    private String user = "user";
    private String pass = "pass";
    private int timeout = 1;

    @BeforeClass
    public void init() {
        mailerConfiguration = new MailerConfiguration();
    }

    @Test
    public void testUrl() {
        mailerConfiguration.setUrl(url);
        Assert.assertEquals(mailerConfiguration.getUrl(), url);
    }

    @Test
    public void testService() {
        mailerConfiguration.setService(service);
        Assert.assertEquals(mailerConfiguration.getService(), service);
    }

    @Test
    public void testTimeout() {
        mailerConfiguration.setTimeout(timeout);
        Assert.assertEquals(mailerConfiguration.getTimeout(), timeout);
    }

    @Test
    public void testUser() {
        mailerConfiguration.setUser(user);
        Assert.assertEquals(mailerConfiguration.getUser(), user);
    }

    @Test
    public void testPass() {
        mailerConfiguration.setPass(pass);
        Assert.assertEquals(mailerConfiguration.getPass(), pass);
    }
}
