package com.odigeo.email.trigger.configuration.testbuilder;

import com.odigeo.email.trigger.configuration.ElasticsearchConfiguration;

public class ElasticsearchConfigurationTestDataBuilder {

    private String protocol;
    private String hostName;
    private int port;

    private ElasticsearchConfigurationTestDataBuilder() {
        protocol = "protocol";
        hostName = "hostName";
        port = 0;
    }

    public static ElasticsearchConfigurationTestDataBuilder aElasticsearchConfiguration() {
        return new ElasticsearchConfigurationTestDataBuilder();
    }

    public ElasticsearchConfigurationTestDataBuilder withProtocol(String protocol) {
        this.protocol = protocol;
        return this;
    }

    public ElasticsearchConfigurationTestDataBuilder withHostName(String hostName) {
        this.hostName = hostName;
        return this;
    }

    public ElasticsearchConfigurationTestDataBuilder withPort(int port) {
        this.port = port;
        return this;
    }

    public ElasticsearchConfiguration build() {
        ElasticsearchConfiguration elasticsearchConfiguration = new ElasticsearchConfiguration();
        elasticsearchConfiguration.setProtocol(protocol);
        elasticsearchConfiguration.setHostName(hostName);
        elasticsearchConfiguration.setPort(port);
        return elasticsearchConfiguration;
    }

}
