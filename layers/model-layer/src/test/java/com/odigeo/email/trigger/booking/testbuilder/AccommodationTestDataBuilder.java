package com.odigeo.email.trigger.booking.testbuilder;

import com.odigeo.email.trigger.booking.product.Accommodation;

import java.util.Collections;
import java.util.Set;

public class AccommodationTestDataBuilder {

    private Set<String> locators;

    private AccommodationTestDataBuilder() {
        locators = Collections.singleton("defaultLocator");
    }

    public static AccommodationTestDataBuilder anAccommodation() {
        return new AccommodationTestDataBuilder();
    }

    public AccommodationTestDataBuilder withLocators(Set<String> locators) {
        this.locators = locators;
        return this;
    }

    public Accommodation build() {
        Accommodation accommodation = new Accommodation();
        accommodation.setLocators(locators);
        return accommodation;
    }

}
