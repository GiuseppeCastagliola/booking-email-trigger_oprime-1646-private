package com.odigeo.email.trigger.configuration.testbuilder;

import com.odigeo.email.trigger.configuration.BookingServiceConfiguration;

public class BookingServiceConfigurationTestDataBuilder {

    private String url;
    private String user;
    private String pass;

    private BookingServiceConfigurationTestDataBuilder() {
        url = "url";
        user = "user";
        pass = "pass";
    }

    public static BookingServiceConfigurationTestDataBuilder aBookingServiceConfiguration() {
        return new BookingServiceConfigurationTestDataBuilder();
    }

    public BookingServiceConfigurationTestDataBuilder withUrl(String url) {
        this.url = url;
        return this;
    }

    public BookingServiceConfigurationTestDataBuilder withUser(String user) {
        this.user = user;
        return this;
    }

    public BookingServiceConfigurationTestDataBuilder withPass(String pass) {
        this.pass = pass;
        return this;
    }

    public BookingServiceConfiguration build() {
        BookingServiceConfiguration bookingServiceConfiguration = new BookingServiceConfiguration();
        bookingServiceConfiguration.setUrl(url);
        bookingServiceConfiguration.setUser(user);
        bookingServiceConfiguration.setPass(pass);
        return bookingServiceConfiguration;
    }

}
