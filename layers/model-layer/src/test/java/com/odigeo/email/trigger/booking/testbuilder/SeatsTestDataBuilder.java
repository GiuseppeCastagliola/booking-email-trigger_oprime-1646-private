package com.odigeo.email.trigger.booking.testbuilder;

import com.odigeo.email.trigger.booking.product.SeatSelection;
import com.odigeo.email.trigger.booking.product.Seats;

import java.util.Collections;
import java.util.List;

public class SeatsTestDataBuilder {

    private List<SeatSelection> seatSelections;

    private SeatsTestDataBuilder() {
        seatSelections = Collections.singletonList(SeatSelectionTestDataBuilder.aSeatSelection().build());
    }

    public static SeatsTestDataBuilder aSeats() {
        return new SeatsTestDataBuilder();
    }

    public SeatsTestDataBuilder withSeatSelections(List<SeatSelection> seatSelections) {
        this.seatSelections = seatSelections;
        return this;
    }

    public Seats build() {
        Seats seats = new Seats();
        seats.setSeatSelections(seatSelections);
        return seats;
    }

}
