package com.odigeo.email.trigger.configuration;

import org.testng.Assert;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

public class ElasticsearchConfigurationTest {

    private ElasticsearchConfiguration elasticsearchConfiguration;
    private String protocol = "protocol";
    private String hostName = "host";
    private int port = 11;


    @BeforeClass
    public void init() {
        elasticsearchConfiguration = new ElasticsearchConfiguration();
    }

    @Test
    public void testProtocol() {
        elasticsearchConfiguration.setProtocol(protocol);
        Assert.assertEquals(elasticsearchConfiguration.getProtocol(), protocol);
    }

    @Test
    public void testHostName() {
        elasticsearchConfiguration.setHostName(hostName);
        Assert.assertEquals(elasticsearchConfiguration.getHostName(), hostName);
    }

    @Test
    public void testPort() {
        elasticsearchConfiguration.setPort(port);
        Assert.assertEquals(elasticsearchConfiguration.getPort(), port);
    }
}
