package com.odigeo.email.trigger.booking.builder;

import com.odigeo.bookingapi.mock.v12.response.ItineraryBookingBuilder;
import com.odigeo.bookingapi.mock.v12.response.SeatLocationBuilder;
import com.odigeo.bookingapi.mock.v12.response.SeatMapPreferencesSelectionBuilder;
import com.odigeo.bookingapi.mock.v12.response.SeatMapPreferencesSelectionItemBuilder;
import com.odigeo.bookingapi.mock.v12.response.SeatPreferencesSelectionSetBuilder;
import com.odigeo.bookingapi.mock.v12.response.SectionBuilder;
import com.odigeo.bookingapi.mock.v12.response.SegmentBuilder;
import com.odigeo.bookingapi.v12.responses.ItineraryBooking;
import com.odigeo.bookingapi.v12.responses.Section;
import com.odigeo.email.trigger.booking.product.SeatSelection;
import com.odigeo.email.trigger.booking.product.Seats;
import org.testng.annotations.Test;

import java.util.Collections;
import java.util.List;
import java.util.Random;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static org.testng.Assert.assertEquals;

public class SeatsBuilderTest {

    @Test
    public void testBuild() throws Exception {
        Integer segmentIndex = 632;
        Integer sectionIndex = 564;
        int travellerNumber = 852;
        int floor = 741;
        int rowNumber = 963;
        String position = "AB25";

        Random random = new Random();
        Section section = new SectionBuilder().build(random);
        section.setSectionIndex(sectionIndex);

        ItineraryBooking itineraryBooking =
                new ItineraryBookingBuilder()
                        .seatPreferencesSelectionSet(
                                new SeatPreferencesSelectionSetBuilder()
                                        .seatMapPreferencesSelections(Collections.singleton(
                                                new SeatMapPreferencesSelectionBuilder()
                                                        .segment(new SegmentBuilder().segmentIndex(segmentIndex).build(random))
                                                        .seatMapPreferencesSelectionItems(Collections.singleton(
                                                                new SeatMapPreferencesSelectionItemBuilder()
                                                                        .travellerNumber(travellerNumber)
                                                                        .sections(Collections.singletonList(section))
                                                                        .seatSelected(
                                                                                new SeatLocationBuilder()
                                                                                        .floor(floor)
                                                                                        .rowNumber(rowNumber)
                                                                                        .position(position)))))))
                        .build(random);

        List<ItineraryBooking> itineraryBookingList = Stream.of(itineraryBooking).collect(Collectors.toList());
        Seats seats = new SeatsBuilder(itineraryBookingList).build();
        assertEquals(seats.getSeatSelections().size(), 1);
        SeatSelection seatSelection = seats.getSeatSelections().get(0);
        assertEquals(seatSelection.getSegmentIndex(), segmentIndex);
        assertEquals(seatSelection.getSectionIndexes().size(), 1);
        assertEquals(seatSelection.getSectionIndexes().get(0), sectionIndex);
        assertEquals(seatSelection.getTravellerNumber(), travellerNumber);
        assertEquals(seatSelection.getFloor(), floor);
        assertEquals(seatSelection.getPosition(), position);
    }

}
