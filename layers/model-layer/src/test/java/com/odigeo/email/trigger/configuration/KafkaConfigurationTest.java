package com.odigeo.email.trigger.configuration;

import org.testng.Assert;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

public class KafkaConfigurationTest {

    private KafkaConfiguration kafkaConfiguration;
    private String zookeeperUrl = "zoourl";
    private String bootstrapServUrl = "boourl";
    private String groupId = "group";
    private String topic1 = "top1";
    private String topic2 = "top2";
    private String topic3 = "top3";
    private int nThreadsUpdates = 3;
    private int nThreadsRecovery = 1;

    @BeforeClass
    public void init() {
        kafkaConfiguration = new KafkaConfiguration();
    }

    @Test
    public void testZookeeperUrl() {
        kafkaConfiguration.setZookeeperUrl(zookeeperUrl);
        Assert.assertEquals(kafkaConfiguration.getZookeeperUrl(), zookeeperUrl);
    }

    @Test
    public void testBootstrapServerUrl() {
        kafkaConfiguration.setBootstrapServerUrl(bootstrapServUrl);
        Assert.assertEquals(kafkaConfiguration.getBootstrapServerUrl(), bootstrapServUrl);
    }

    @Test
    public void testGroupId() {
        kafkaConfiguration.setGroupId(groupId);
        Assert.assertEquals(kafkaConfiguration.getGroupId(), groupId);
    }

    @Test
    public void testNumThreadsUpdates() {
        kafkaConfiguration.setNumThreadsUpdates(nThreadsUpdates);
        Assert.assertEquals(kafkaConfiguration.getNumThreadsUpdates(), nThreadsUpdates);
    }

    @Test
    public void testNumThreadsRecovery() {
        kafkaConfiguration.setNumThreadsRecovery(nThreadsRecovery);
        Assert.assertEquals(kafkaConfiguration.getNumThreadsRecovery(), nThreadsRecovery);
    }

    @Test
    public void testBookingUpdatesTopic() {
        kafkaConfiguration.setBookingUpdatesTopic(topic1);
        Assert.assertEquals(kafkaConfiguration.getBookingUpdatesTopic(), topic1);
    }

    @Test
    public void testRecoveryTopic() {
        kafkaConfiguration.setRecoveryTopic(topic2);
        Assert.assertEquals(kafkaConfiguration.getRecoveryTopic(), topic2);
    }
}
