package com.odigeo.email.trigger.booking.testbuilder;

import com.odigeo.email.trigger.booking.product.SeatSelection;

import java.util.Collections;
import java.util.List;

public class SeatSelectionTestDataBuilder {

    private Integer segmentIndex;
    private List<Integer> sectionIndexes;
    private int travellerNumber;
    private int floor;
    private String position;

    private SeatSelectionTestDataBuilder() {
        this.segmentIndex = 1;
        this.sectionIndexes = Collections.singletonList(1);
        this.travellerNumber = 1;
        this.floor = 1;
        this.position = "A";
    }

    public static SeatSelectionTestDataBuilder aSeatSelection() {
        return new SeatSelectionTestDataBuilder();
    }

    public SeatSelectionTestDataBuilder withSegmentIndex(Integer segmentIndex) {
        this.segmentIndex = segmentIndex;
        return this;
    }

    public SeatSelectionTestDataBuilder withSectionIndexes(List<Integer> sectionIndexes) {
        this.sectionIndexes = sectionIndexes;
        return this;
    }

    public SeatSelectionTestDataBuilder withTravellerNumber(int travellerNumber) {
        this.travellerNumber = travellerNumber;
        return this;
    }

    public SeatSelectionTestDataBuilder withFloor(int floor) {
        this.floor = floor;
        return this;
    }

    public SeatSelectionTestDataBuilder withPosition(String position) {
        this.position = position;
        return this;
    }

    public SeatSelection build() {
        SeatSelection seatSelection = new SeatSelection();
        seatSelection.setSegmentIndex(segmentIndex);
        seatSelection.setSectionIndexes(sectionIndexes);
        seatSelection.setTravellerNumber(travellerNumber);
        seatSelection.setFloor(floor);
        seatSelection.setPosition(position);
        return seatSelection;
    }

}
