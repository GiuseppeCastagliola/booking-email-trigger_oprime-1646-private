package com.odigeo.email.trigger.configuration.testbuilder;

import com.odigeo.email.trigger.configuration.MailerConfiguration;

public class MailerConfigurationTestDataBuilder {

    private String url;
    private String service;
    private int timeout;
    private String user;
    private String pass;
    
    private MailerConfigurationTestDataBuilder() {
        url = "url";
        service = "service";
        timeout = 1;
        user = "user";
        pass = "pass";
    }

    public static MailerConfigurationTestDataBuilder aMailerConfiguration() {
        return new MailerConfigurationTestDataBuilder();
    }

    public MailerConfigurationTestDataBuilder withUrl(String url) {
        this.url = url;
        return this;
    }

    public MailerConfigurationTestDataBuilder withService(String service) {
        this.service = service;
        return this;
    }

    public MailerConfigurationTestDataBuilder withTimeout(int timeout) {
        this.timeout = timeout;
        return this;
    }

    public MailerConfigurationTestDataBuilder withUser(String user) {
        this.user = user;
        return this;
    }

    public MailerConfigurationTestDataBuilder withPass(String pass) {
        this.pass = pass;
        return this;
    }

    public MailerConfiguration build() {
        MailerConfiguration mailerConfiguration = new MailerConfiguration();
        mailerConfiguration.setUrl(url);
        mailerConfiguration.setService(service);
        mailerConfiguration.setTimeout(timeout);
        mailerConfiguration.setUser(user);
        mailerConfiguration.setPass(pass);
        return mailerConfiguration;
    }

}
