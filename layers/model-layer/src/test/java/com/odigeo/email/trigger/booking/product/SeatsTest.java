package com.odigeo.email.trigger.booking.product;

import com.odigeo.email.trigger.booking.testbuilder.SeatSelectionTestDataBuilder;
import com.odigeo.email.trigger.booking.testbuilder.SeatsTestDataBuilder;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import java.util.Collections;

import static org.testng.Assert.assertEquals;
import static org.testng.Assert.assertNotEquals;

public class SeatsTest {

    private Seats seats;

    @BeforeMethod
    public void setUp() {
        seats = SeatsTestDataBuilder.aSeats().build();
    }

    @Test
    public void testEqualsReflexive() {
        assertEquals(seats, seats);
    }

    @Test
    public void testEqualsSymmetric() {
        Seats seatsCopy = SeatsTestDataBuilder.aSeats().build();
        assertEquals(seats, seatsCopy);
        assertEquals(seatsCopy, seats);
    }

    @Test
    public void testEqualsTransitive() {
        Seats seatsCopy = SeatsTestDataBuilder.aSeats().build();
        Seats seatsCopyBis = SeatsTestDataBuilder.aSeats().build();
        assertEquals(seats, seatsCopy);
        assertEquals(seatsCopy, seatsCopyBis);
        assertEquals(seats, seatsCopyBis);
    }

    @Test
    public void testNotEqualsDiffObject() {
        assertNotEquals(seats, new Object());
    }

    @Test
    public void testNotEqualsDiffSeatSelections() {
        Seats seatsDiff = SeatsTestDataBuilder.aSeats().withSeatSelections(Collections.singletonList(SeatSelectionTestDataBuilder.aSeatSelection().withSegmentIndex(546).build())).build();
        assertNotEquals(seatsDiff, seats);
    }

    @Test
    public void testHashCode() {
        int hashCode = seats.hashCode();
        int hasCodeCopy = SeatsTestDataBuilder.aSeats().build().hashCode();
        assertEquals(hasCodeCopy, hashCode);
    }
}
