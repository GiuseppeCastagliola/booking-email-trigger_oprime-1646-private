package com.odigeo.email.trigger.email;

import com.odigeo.bookingapi.mock.v12.response.AccommodationBookingBuilder;
import com.odigeo.bookingapi.mock.v12.response.BuilderException;
import com.odigeo.bookingapi.mock.v12.response.ItineraryBookingBuilder;
import com.odigeo.bookingapi.v12.requests.Status;
import com.odigeo.bookingapi.v12.responses.AccommodationBooking;
import com.odigeo.bookingapi.v12.responses.ItineraryBooking;
import com.odigeo.email.trigger.util.Constants;
import org.testng.Assert;
import org.testng.annotations.Test;

import java.util.Collections;
import java.util.List;
import java.util.Random;

import static org.testng.Assert.assertTrue;

public class EmailTypeHelperTest {

    private EmailTypeHelper emailTypeHelper = new EmailTypeHelper();

    @Test
    public void testHasLcNotConfirmed() throws BuilderException {
        List<ItineraryBooking> itineraryBookings = Collections.singletonList(new ItineraryBookingBuilder().build(new Random()));
        fillWithSameStatus(itineraryBookings, Status.REQUEST.name(), Constants.LC_FLIGHT_TYPE);
        assertTrue(emailTypeHelper.hasLcNotConfirmed(itineraryBookings));
        Assert.assertFalse(emailTypeHelper.hasGdsPending(itineraryBookings));
    }

    @Test
    public void testLcNotConfirmed() throws BuilderException {
        List<ItineraryBooking> itineraryBookings = Collections.singletonList(new ItineraryBookingBuilder().build(new Random()));
        fillWithSameStatus(itineraryBookings, Status.CONTRACT.name(), Constants.LC_FLIGHT_TYPE);
        Assert.assertFalse(emailTypeHelper.hasLcNotConfirmed(itineraryBookings));
        Assert.assertFalse(emailTypeHelper.hasGdsPending(itineraryBookings));
    }

    @Test
    public void testHasGdsPending() throws BuilderException {
        List<ItineraryBooking> itineraryBookings = Collections.singletonList(new ItineraryBookingBuilder().build(new Random()));
        fillWithSameStatus(itineraryBookings, Status.PENDING.name(), Constants.GDS_FLIGHT_TYPE);
        assertTrue(emailTypeHelper.hasGdsPending(itineraryBookings));
        Assert.assertFalse(emailTypeHelper.hasLcNotConfirmed(itineraryBookings));
    }

    @Test
    public void testGdsPending() throws BuilderException {
        List<ItineraryBooking> itineraryBookings = Collections.singletonList(new ItineraryBookingBuilder().build(new Random()));
        fillWithSameStatus(itineraryBookings, Status.CONTRACT.name(), Constants.GDS_FLIGHT_TYPE);
        Assert.assertFalse(emailTypeHelper.hasGdsPending(itineraryBookings));
        Assert.assertFalse(emailTypeHelper.hasLcNotConfirmed(itineraryBookings));
    }

    @Test
    public void testAccommodationPending() throws BuilderException {
        List<AccommodationBooking> accommodationBookings = Collections.singletonList(new AccommodationBookingBuilder().build(new Random()));
        fillWithSameStatus(accommodationBookings, Status.CONTRACT.name());
        Assert.assertFalse(emailTypeHelper.hasAccommodationPending(accommodationBookings));
        fillWithSameStatus(accommodationBookings, Status.REQUEST.name());
        Assert.assertFalse(emailTypeHelper.hasAccommodationPending(accommodationBookings));
        fillWithSameStatus(accommodationBookings, Status.PENDING.name());
        assertTrue(emailTypeHelper.hasAccommodationPending(accommodationBookings));
    }

    @Test
    public void testEmptyLists() {
        List<AccommodationBooking> accommodationBookings = Collections.emptyList();
        List<ItineraryBooking> itineraryBookings = Collections.emptyList();
        Assert.assertFalse(emailTypeHelper.hasAccommodationPending(accommodationBookings));
        Assert.assertFalse(emailTypeHelper.hasGdsPending(itineraryBookings));
        Assert.assertFalse(emailTypeHelper.hasLcNotConfirmed(itineraryBookings));
    }

    private void fillWithSameStatus(List<ItineraryBooking> itineraryBookings, String bookingStatus, String flightType) {
        itineraryBookings.forEach(itineraryBooking ->
                itineraryBooking.getProviderBookings().forEach(itineraryProviderBooking -> {
                    itineraryProviderBooking.setBookingStatus(bookingStatus);
                    itineraryProviderBooking.getItineraryProviders().forEach(itineraryProvider -> itineraryProvider.setFlightType(flightType));
                }));
    }

    private void fillWithSameStatus(List<AccommodationBooking> accommodationBookings, String bookingStatus) {
        accommodationBookings.forEach(accommodationBooking -> accommodationBooking.getProviderBooking().setBookingStatus(bookingStatus));
    }

}


