package com.odigeo.email.trigger.configuration.testbuilder;

import com.odigeo.email.trigger.configuration.KafkaConfiguration;

public class KafkaConfigurationTestDataBuilder {

    private String zookeeperUrl;
    private String bootstrapServerUrl;
    private String groupId;
    private int numThreads;
    private String bookingUpdatesTopic;
    private String recoveryTopic;
    
    private KafkaConfigurationTestDataBuilder() {
        zookeeperUrl = "zookeeperUrl";
        bootstrapServerUrl = "bootstrapServerUrl";
        groupId = "groupId";
        numThreads = 1;
        bookingUpdatesTopic = "bookingUpdatesTopic";
        recoveryTopic = "recoveryTopic";
    }

    public static KafkaConfigurationTestDataBuilder aKafkaConfiguration() {
        return new KafkaConfigurationTestDataBuilder();
    }

    public KafkaConfigurationTestDataBuilder withZookeeperUrl(String zookeeperUrl) {
        this.zookeeperUrl = zookeeperUrl;
        return this;
    }

    public KafkaConfigurationTestDataBuilder withBootstrapServerUrl(String bootstrapServerUrl) {
        this.bootstrapServerUrl = bootstrapServerUrl;
        return this;
    }

    public KafkaConfigurationTestDataBuilder withGroupId(String groupId) {
        this.groupId = groupId;
        return this;
    }

    public KafkaConfigurationTestDataBuilder withNumThreads(int numThreads) {
        this.numThreads = numThreads;
        return this;
    }

    public KafkaConfigurationTestDataBuilder withBookingUpdatesTopic(String bookingUpdatesTopic) {
        this.bookingUpdatesTopic = bookingUpdatesTopic;
        return this;
    }

    public KafkaConfigurationTestDataBuilder withRecoveryTopic(String recoveryTopic) {
        this.recoveryTopic = recoveryTopic;
        return this;
    }


    public KafkaConfiguration build() {
        KafkaConfiguration kafkaConfiguration = new KafkaConfiguration();
        kafkaConfiguration.setZookeeperUrl(zookeeperUrl);
        kafkaConfiguration.setBootstrapServerUrl(bootstrapServerUrl);
        kafkaConfiguration.setGroupId(groupId);
        kafkaConfiguration.setNumThreadsUpdates(numThreads);
        kafkaConfiguration.setNumThreadsRecovery(numThreads);
        kafkaConfiguration.setBookingUpdatesTopic(bookingUpdatesTopic);
        kafkaConfiguration.setRecoveryTopic(recoveryTopic);
        return kafkaConfiguration;
    }

}
