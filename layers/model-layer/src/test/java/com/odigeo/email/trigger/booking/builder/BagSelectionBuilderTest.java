package com.odigeo.email.trigger.booking.builder;

import com.odigeo.bookingapi.mock.v12.response.BaggageSelectionBuilder;
import com.odigeo.bookingapi.v12.responses.BaggageSelection;
import com.odigeo.email.trigger.booking.product.BagSelection;
import org.testng.annotations.Test;

import java.util.Random;

import static org.testng.Assert.assertEquals;

public class BagSelectionBuilderTest {

    @Test
    public void testBuild() throws Exception {
        int segmentIndex = 14;
        Integer pieces = 25;
        Integer kilos = 36;
        BaggageSelection baggageSelection = new BaggageSelectionBuilder()
                .segmentIndex(segmentIndex)
                .pieces(pieces)
                .kilos(kilos)
                .build(new Random());

        BagSelection bagSelection = new BagSelectionBuilder(baggageSelection).build();
        assertEquals(bagSelection.getSegmentIndex(), segmentIndex);
        assertEquals(bagSelection.getPieces(), pieces);
        assertEquals(bagSelection.getKilos(), kilos);
    }

}
