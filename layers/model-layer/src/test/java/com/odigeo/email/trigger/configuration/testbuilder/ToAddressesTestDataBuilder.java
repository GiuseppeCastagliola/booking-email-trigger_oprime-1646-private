package com.odigeo.email.trigger.configuration.testbuilder;

import com.odigeo.email.trigger.configuration.ToAddresses;

import java.util.Properties;

public class ToAddressesTestDataBuilder {

    private Properties toByBrand;

    private ToAddressesTestDataBuilder() {
        Properties properties = new Properties();
        properties.setProperty("ED", "to@edreams.com");
        properties.setProperty("GV", "to@govoyages.com");
        properties.setProperty("OP", "to@opodo.com");
        properties.setProperty("TL", "to@travellink.com");
        toByBrand = properties;
    }

    public static ToAddressesTestDataBuilder aToAddresses() {
        return new ToAddressesTestDataBuilder();
    }

    public ToAddressesTestDataBuilder withToByBrand(Properties toByBrand) {
        this.toByBrand = toByBrand;
        return this;
    }

    public ToAddresses build() {
        ToAddresses toAddresses = new ToAddresses();
        toAddresses.setToByBrand(toByBrand);
        return toAddresses;
    }

}
