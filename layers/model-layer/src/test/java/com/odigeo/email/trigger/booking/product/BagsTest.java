package com.odigeo.email.trigger.booking.product;

import com.odigeo.email.trigger.booking.testbuilder.BagsTestDataBuilder;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import java.util.Collections;

import static org.testng.Assert.assertEquals;
import static org.testng.Assert.assertNotEquals;

public class BagsTest {

    private Bags baggage;

    @BeforeMethod
    public void setUp() {
        baggage = BagsTestDataBuilder.aBags().build();
    }

    @Test
    public void testEqualsReflexive() {
        assertEquals(baggage, baggage);
    }

    @Test
    public void testEqualsSymmetric() {
        Bags baggageCopy = BagsTestDataBuilder.aBags().build();
        assertEquals(baggage, baggageCopy);
        assertEquals(baggageCopy, baggage);
    }

    @Test
    public void testEqualsTransitive() {
        Bags baggageCopy = BagsTestDataBuilder.aBags().build();
        Bags baggageCopyBis = BagsTestDataBuilder.aBags().build();
        assertEquals(baggage, baggageCopy);
        assertEquals(baggageCopy, baggageCopyBis);
        assertEquals(baggage, baggageCopyBis);
    }

    @Test
    public void testNotEqualsDiffObject() {
        assertNotEquals(baggage, new Object());
    }

    @Test
    public void testNotEqualsDiffBagSelections() {
        Bags baggageDiff = BagsTestDataBuilder.aBags().withBagSelectionsByPassenger(Collections.singletonMap(652, Collections.emptySet())).build();
        assertNotEquals(baggage, baggageDiff);
    }

    @Test
    public void testHashCode() {
        int hashCode = baggage.hashCode();
        int hasCodeCopy = BagsTestDataBuilder.aBags().build().hashCode();
        assertEquals(hasCodeCopy, hashCode);
    }
}
