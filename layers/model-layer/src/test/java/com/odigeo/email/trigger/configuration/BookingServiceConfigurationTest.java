package com.odigeo.email.trigger.configuration;

import org.testng.Assert;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

public class BookingServiceConfigurationTest {

    private BookingServiceConfiguration bookingServiceConfiguration;
    private String url = "url";
    private String user = "user";
    private String pass = "pass";

    @BeforeClass
    public void init() {
        bookingServiceConfiguration = new BookingServiceConfiguration();
    }

    @Test
    public void testUrl() {
        bookingServiceConfiguration.setUrl(url);
        Assert.assertEquals(bookingServiceConfiguration.getUrl(), url);
    }

    @Test
    public void tesUser() {
        bookingServiceConfiguration.setUser(user);
        Assert.assertEquals(bookingServiceConfiguration.getUser(), user);
    }

    @Test
    public void testPass() {
        bookingServiceConfiguration.setPass(pass);
        Assert.assertEquals(bookingServiceConfiguration.getPass(), pass);
    }
}
