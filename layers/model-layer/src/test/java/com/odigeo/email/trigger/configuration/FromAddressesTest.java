package com.odigeo.email.trigger.configuration;

import org.testng.Assert;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import java.util.Properties;

public class FromAddressesTest {

    private FromAddresses fromAddresses;
    private Properties properties;
    private String brandCode = "brandCode";
    private String address = "address";

    @BeforeClass
    public void init() {
        properties = new Properties();
        properties.put(brandCode, address);
        fromAddresses = new FromAddresses();
    }

    @Test
    public void testTo() {
        fromAddresses.setFromByBrand(properties);
        Assert.assertEquals(fromAddresses.getFrom(brandCode), address);
    }
}
