package com.odigeo.email.trigger.booking.product;

import com.odigeo.email.trigger.booking.testbuilder.SeatSelectionTestDataBuilder;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import java.util.Collections;

import static org.testng.Assert.assertEquals;
import static org.testng.Assert.assertNotEquals;

public class SeatSelectionTest {

    private SeatSelection seatSelection;

    @BeforeMethod
    public void setUp() {
        seatSelection = SeatSelectionTestDataBuilder.aSeatSelection().build();
    }

    @Test
    public void testEqualsReflexive() {
        assertEquals(seatSelection, seatSelection);
    }

    @Test
    public void testEqualsSymmetric() {
        SeatSelection seatSelectionCopy = SeatSelectionTestDataBuilder.aSeatSelection().build();
        assertEquals(this.seatSelection, seatSelectionCopy);
        assertEquals(seatSelectionCopy, this.seatSelection);
    }

    @Test
    public void testEqualsTransitive() {
        SeatSelection seatSelectionCopy = SeatSelectionTestDataBuilder.aSeatSelection().build();
        SeatSelection seatSelectionCopyBis = SeatSelectionTestDataBuilder.aSeatSelection().build();
        assertEquals(seatSelection, seatSelectionCopy);
        assertEquals(seatSelectionCopy, seatSelectionCopyBis);
        assertEquals(seatSelection, seatSelectionCopyBis);
    }

    @Test
    public void testNotEqualsDiffObject() {
        assertNotEquals(seatSelection, new Object());
    }

    @Test
    public void testNotEqualsDiffTravellerNum() {
        SeatSelection seatSelectionDiff = SeatSelectionTestDataBuilder.aSeatSelection().withTravellerNumber(745).build();
        assertNotEquals(seatSelection, seatSelectionDiff);
    }

    @Test
    public void testNotEqualsDiffFloor() {
        SeatSelection seatSelectionDiff = SeatSelectionTestDataBuilder.aSeatSelection().withFloor(745).build();
        assertNotEquals(seatSelection, seatSelectionDiff);
    }

    @Test
    public void testNotEqualsDiffPosition() {
        SeatSelection seatSelectionDiff = SeatSelectionTestDataBuilder.aSeatSelection().withPosition("TR34").build();
        assertNotEquals(seatSelection, seatSelectionDiff);
    }

    @Test
    public void testNotEqualsDiffSegmentIndex() {
        SeatSelection seatSelectionDiff = SeatSelectionTestDataBuilder.aSeatSelection().withSegmentIndex(745).build();
        assertNotEquals(seatSelection, seatSelectionDiff);
    }

    @Test
    public void testNotEqualsDiffSectionIndexes() {
        SeatSelection seatSelectionDiff = SeatSelectionTestDataBuilder.aSeatSelection().withSectionIndexes(Collections.singletonList(745)).build();
        assertNotEquals(seatSelection, seatSelectionDiff);
    }

    @Test
    public void testHashCode() {
        int hashCode = seatSelection.hashCode();
        int hasCodeCopy = SeatSelectionTestDataBuilder.aSeatSelection().build().hashCode();
        assertEquals(hasCodeCopy, hashCode);
    }
}
