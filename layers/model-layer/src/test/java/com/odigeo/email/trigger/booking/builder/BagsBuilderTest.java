package com.odigeo.email.trigger.booking.builder;

import com.odigeo.bookingapi.mock.v12.response.BaggageSelectionBuilder;
import com.odigeo.bookingapi.mock.v12.response.TravellerBuilder;
import com.odigeo.bookingapi.v12.responses.Traveller;
import com.odigeo.email.trigger.booking.product.Bags;
import org.testng.annotations.Test;

import java.util.Collections;
import java.util.Random;
import java.util.Set;

import static org.testng.Assert.assertEquals;
import static org.testng.Assert.assertTrue;

public class BagsBuilderTest {

    @Test
    public void testBuild() throws Exception {
        Integer numPassenger = 789;
        Set<BaggageSelectionBuilder> baggageSelectionBuilders = Collections.singleton(new BaggageSelectionBuilder());
        Traveller traveller = new TravellerBuilder()
                .numPassenger(numPassenger)
                .baggageSelections(Collections.singleton(new BaggageSelectionBuilder()))
                .build(new Random());

        Bags baggage = new BagsBuilder(Collections.singletonList(traveller)).build();
        assertTrue(baggage.getBagsSelectionsByPassenger().containsKey(numPassenger));
        assertEquals(baggage.getBagsSelectionsByPassenger().get(numPassenger).size(), baggageSelectionBuilders.size());
    }

}
